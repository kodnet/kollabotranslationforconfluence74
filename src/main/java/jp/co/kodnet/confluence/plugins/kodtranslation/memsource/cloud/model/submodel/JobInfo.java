package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author lucvd
 */
public class JobInfo {

    private String uid;
    private String task;
    private String filename;
    private String targetLang;
    private String status;
    private WorkflowStep workflowStep;
    private String workflowLevel;

    /**
     * @return the id
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the id to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }

    /**
     * @return the task
     */
    public String getTask() {
        return task;
    }

    /**
     * @param task the task to set
     */
    public void setTask(String task) {
        this.task = task;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String targetLang) {
        this.targetLang = targetLang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WorkflowStep getWorkflowStep() {
        return workflowStep;
    }

    public void setWorkflowStep(WorkflowStep workflowStep) {
        this.workflowStep = workflowStep;
    }

    public String getWorkflowLevel() {
        return workflowLevel;
    }

    public void setWorkflowLevel(String workflowLevel) {
        this.workflowLevel = workflowLevel;
    }
}
