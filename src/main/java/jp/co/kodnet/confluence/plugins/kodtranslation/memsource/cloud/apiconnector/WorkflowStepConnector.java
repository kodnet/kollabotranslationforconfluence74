package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetWorkflowStepRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetWorkflowStepResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;

/**
 *
 * @author anhlq
 */
public class WorkflowStepConnector {

    private final static String GET_API_URL = "api2/v1/workflowSteps";
    private final String token;

    public WorkflowStepConnector(String token) {
        this.token = token;
    }

    public GetWorkflowStepResponse get() throws Exception {
        GetWorkflowStepRequest request = new GetWorkflowStepRequest();
        request.setToken(this.token);
        return (GetWorkflowStepResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + GET_API_URL, request, GetWorkflowStepResponse.class, Constants.GET_METHOD);
    }
}
