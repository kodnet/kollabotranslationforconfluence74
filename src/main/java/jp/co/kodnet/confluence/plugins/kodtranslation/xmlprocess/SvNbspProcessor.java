package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.XMLEvent;

public class SvNbspProcessor extends SpanProcessor {

    private final XMLEventFactory xmlEventFactory;

    public SvNbspProcessor(XMLEventFactory xmlEventFactory) {
        super("sv-nbsp");
        this.xmlEventFactory = xmlEventFactory;
    }

    @Override
    public void marshal(XMLEvent paramXMLEvent, XMLEventReader paramXMLEventReader, XMLEventWriter paramXMLEventWriter) throws Exception {
    }

    @Override
    public void unmarshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter) throws Exception {
        xmlEventWriter.add(this.xmlEventFactory.createCharacters("?"));
        reader.nextEvent();
    }
}
