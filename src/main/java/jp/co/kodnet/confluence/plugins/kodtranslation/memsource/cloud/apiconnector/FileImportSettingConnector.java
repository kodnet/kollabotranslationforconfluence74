package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.*;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;



/**
 *
 * @author lucvd
 */
public class FileImportSettingConnector {

    //api2/v1/importSettings
    private final static String CREATE_API_URL = "api2/v1/importSettings";

    private final String token;

    public FileImportSettingConnector(String token) {
        this.token = token;
    }

    public FileImportSettingResponse create(String inlineTagValue, String notInlineTag) throws Exception {
        FileImportSettingRequest request = new FileImportSettingRequest();
        request.setInlineTag(inlineTagValue);
        request.setNotInlineTag(notInlineTag);
        request.setToken(this.token);
        return (FileImportSettingResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + CREATE_API_URL, request, FileImportSettingResponse.class, Constants.POST_METHOD);
    }
}
