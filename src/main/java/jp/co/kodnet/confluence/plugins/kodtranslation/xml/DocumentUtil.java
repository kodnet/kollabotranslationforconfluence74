package jp.co.kodnet.confluence.plugins.kodtranslation.xml;

import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.links.OutgoingLink;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.beans.PageIncomingLinks;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.fugue.Maybe;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;
import jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess.MacroDrawIoProcessor;
import org.apache.commons.codec.binary.Base64;
import static org.apache.commons.lang.StringEscapeUtils.escapeJava;
import static org.apache.commons.lang.StringEscapeUtils.escapeXml;
import static org.apache.commons.lang.StringEscapeUtils.unescapeJava;
import static org.apache.commons.lang.StringEscapeUtils.unescapeXml;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

/**
 *
 * @author TamPT
 */
public class DocumentUtil {

    // <editor-fold defaultstate="collapsed" desc="インスタンス定数・変数">
    public final static int SKIP_TRANSLATION = 1;
    public final static int NEED_TRANSLATION = 2;
    public final static int REMOVE_NAME_SPACE_TRANSLATION = 3;
    public final static int REMOVE_TITLE_ATTRIBUTE_TRANSLATION = 4;
    public final static int CONVERT_CDATA_ELEMENT = 5;
    public final static int REMOVE_GENGO_MACRO_TRANSLATION = 6;
    public final static int REPLACE_LINK_ANCHOR = 7;
    public final static String REGEX_NBSP = "\"[^\"|^>]*%s[^\"|^<]*\"";
    public final static String SPACE_HTML_CODE = "&nbsp;";
    public final static String SPACE_HTML_ATTRIBUTE_CODE = "SPACE_HTML_ATTRIBUTE_CODE";
    public final static String SPACE_REPLACETION = "<spannbsp />";
    public final static String SPACE_REPLACETION_1 = "<spannbsp/>";
    public final static String SPACE_REPLACETION_2 = "<spannbsp></spannbsp>";
    private final static String AC_PREFIX = "ac";
    private final static String AC_CONTENT = "http://atlassian.com/content";
    private final static String DRAWIO_PARAMETER = "//structured-macro[@name='drawio']/parameter[@name='diagramName']";
    private final static String ANCHOR_PARAMETER = "//structured-macro[@name='anchor']/parameter[@name='']";
    private final static String ANCHOR_HEADING = "//*[self::h1 or self::h2 or self::h3 or self::h4 or self::h5 or self::h6]";
    private final static String INCLUDE_PAGE = "//structured-macro[@name='include']/parameter[@name='']/link/page/@content-title";
    private final static String ANCHOR_NODE = "//*[@anchor]";
    private final static String ANCHOR_ATTRIBUTE = "ac:anchor";
    private final static String TITLE_ATTRIBUTE = "ri:content-title";
    private final static String SPACE_ATTRIBUTE = "ri:space-key";
    private final static String ATTR_PARAMETER = "kod-inorge-body";
    private final static String ATTR_PARAMETER_XPATH = "//*[@kod-inorge-body]";
    private final static String SPAN_XPATH = "//span";
    private final static String ATTR_REF_NODE = "//*[@ref]";
    private final static String INLINE_COMMENT_NODE = "ac:inline-comment-marker";
    public static final String AC_MACRO_VALUE = "ac:macro";
    public static final String AC_STRUC_MACRO_VALUE = "ac:structured-macro";
    public static final String AC_NAME_VALUE = "ac:name";
    public static final String ATTR_INCLUDE_VALUE = "include";
    public static final String AC_LINK_VALUE = "ac:link";
    public static final String A_HREF_VALUE = "ri:href";
    public static final String AC_LINK_KODTR = "kodtr";
    public static final String AC_LINK_KODTR_LINK = "kodtr_ri-href";
    public static final String AC_LINK_KODTR_TITLE = "kodtr_ri-content-title";
    public static final String AC_LINK_KODTR_ANCHOR = "kodtr_ac-anchor";
    public static final String AC_LINK_KODTR_INCLUDE = "kodtr_include";
    public static final String ATTR_KODTR_INCLUDE_TITLE = "content-title";
    public static final String ATTR_KODTR_INCLUDE_SPACE_KEY = "space-key";
    public static final String RI_PAGE_VALUE = "ri:page";
    public static final String AC_PLAIN_TEXT_LINK = "ac:plain-text-link-body";
    public static final String XML_CONTENT = "content";
    private final static String MXCEll_XPATH = "//mxCell";
    private final static String MXCEll_XPATH_LOWER = "//mxcell";
    private final static String ARIAL_FONT = "Arial";
    private final static String SIMHEI_FONT = "Simhei";
    private final static String SIMIHEI_FONT = "Simihei";
    private final static String GLIM_FONT = "Glim";
    public static final String XML_TITLE = "title";
    public static final String CONVERT_CDATA_ELEMENT_START = "(&lt;)";
    public static final String CONVERT_CDATA_ELEMENT_END = "(&gt;)";
    public static final String XML_AC_NAME_JIRA = "jira";
    public static final String TEXT_ANCHOR_NOTRANS = "anchor-notrans";
    public static final String TEXT_HEADER_NOTRANS = "header-notrans";
    public static final String HEADERS_CONTENT[] = new String[]{"h1", "h2", "h3", "h4", "h5", "h6"};

    // </editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="Translation">
    /**
     *
     * @param page
     * @param args
     * @return
     */
    public static String xmlTranslationProccess(Page page, List<Integer> args) throws Exception {
        //LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
        //Language language = languageService.getLanguageBySpaceKey(page.getSpaceKey());
        MemoryLogUtil.outputLog("BEGIN TRANSLATION XML PROCESS : " + page.getIdAsString());
        String xmlPage = ImportExportUtil.exportXmlString(page);
        xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);

        //spanタグを削除します。
        xmlPage = removeSpan(xmlPage);
        //ac:inline-comment-markerタグを削除します
        xmlPage = removeInlineComment(xmlPage);

        if (args == null || args.isEmpty()) {
            args = new ArrayList<Integer>();
            args.add(DocumentUtil.NEED_TRANSLATION);
            args.add(DocumentUtil.SKIP_TRANSLATION);
        }

        for (Integer param : args) {
            switch (param) {
                case DocumentUtil.SKIP_TRANSLATION:
                    xmlPage = skipTranslation(xmlPage);
                    break;
                case DocumentUtil.NEED_TRANSLATION:
                    xmlPage = needTranslation(xmlPage);
                case DocumentUtil.REMOVE_NAME_SPACE_TRANSLATION:
                    xmlPage = removeNameSpaceTranslation(xmlPage);
                    break;
                case DocumentUtil.REMOVE_TITLE_ATTRIBUTE_TRANSLATION:
                    xmlPage = removeTitlePageTranslation(xmlPage);
                    break;
                case DocumentUtil.CONVERT_CDATA_ELEMENT:
                    xmlPage = convertCdataTranslation(xmlPage);
                case DocumentUtil.REMOVE_GENGO_MACRO_TRANSLATION:
                    //xmlPage = removeGengoMacro(xmlPage, language.getFromLanguage(), language.getLanguageKey());
                    break;
                default:
                //なし
            }
        }

        xmlPage = DocumentUtil.skipProcessChar(xmlPage, true);
        return xmlPage;
    }

    public static String skipProcessChar(String xmlData, boolean isRevert) throws Exception {
        String result = xmlData;
        if (CommonFunction.isNullOrEmpty(xmlData)) {
            return StringUtils.EMPTY;
        }

        ////////////////////////////////////////
        /// 01. Skip escapse &nbsp; MKN-353 (2018/07/04 - Lucvd)
        ////////////////////////////////////////
        if (!isRevert) {
            //Replace nbsp in attribute to Constant
            String regexNbsp = String.format(REGEX_NBSP, SPACE_HTML_CODE);
            Pattern pattern = Pattern.compile(regexNbsp);
            Matcher matcher = pattern.matcher(result);
            while (matcher.find() == true) {
                String text = matcher.group();
                text = text.replace(SPACE_HTML_CODE, SPACE_HTML_ATTRIBUTE_CODE);
                result = result.replace(matcher.group(), text);
            }

            result = result.replace(SPACE_HTML_CODE, SPACE_REPLACETION);
            //Replace Constant in attribute to nbsp
            regexNbsp = String.format(REGEX_NBSP, SPACE_HTML_ATTRIBUTE_CODE);
            pattern = Pattern.compile(regexNbsp);
            matcher = pattern.matcher(result);
            while (matcher.find() == true) {
                String text = matcher.group();
                text = text.replace(SPACE_HTML_ATTRIBUTE_CODE, SPACE_HTML_CODE);
                result = result.replace(matcher.group(), text);
            }
        } else {
            result = result.replace(SPACE_REPLACETION, SPACE_HTML_CODE);
            result = result.replace(SPACE_REPLACETION_1, SPACE_HTML_CODE);
            result = result.replace(SPACE_REPLACETION_2, SPACE_HTML_CODE);
        }

        return result;
    }

    public static String addAttribueForLinkAnchor(String xmlData) throws Exception {
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return xmlData;
            }

            Element root = document.getDocumentElement();
            List<Element> anchors = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "anchor");
            for (Element anchor : anchors) {
                Element e = XMLUtils.getFirstElementByTagName(anchor, "ac:parameter");
                if (e == null) {
                    continue;
                }
                String textAnchor = e.getTextContent();
                if (textAnchor == null) {
                    continue;
                }
                e.setAttribute(TEXT_ANCHOR_NOTRANS, textAnchor);
            }

            for (String item : HEADERS_CONTENT) {
                List<Element> headers = XMLUtils.getElementByTagName(root, item);
                for (Element header : headers) {
                    String textHeader = header.getTextContent();
                    if (textHeader == null) {
                        continue;
                    }
                    header.setAttribute(TEXT_HEADER_NOTRANS, textHeader);
                }
            }
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }

    public static String convertLinkTranslation(String xmlData) throws Exception {
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlData, false);
            if (document == null) {
                return xmlData;
            }

            Element root = document.getDocumentElement();
            List<Element> acLinks = XMLUtils.getElementByTagName(root, AC_LINK_VALUE);
            for (Element acLink : acLinks) {
                Element acImage = XMLUtils.getFirstElementByTagName(acLink, "ri:attachment");
                if (acImage != null) {
                    continue;
                }
                Element riPage = XMLUtils.getFirstElementByTagName(acLink, RI_PAGE_VALUE);
                if (riPage == null) {
                    if (acLink.hasAttribute(ANCHOR_ATTRIBUTE)) {
                        String anchor = acLink.getAttribute(ANCHOR_ATTRIBUTE);
                        Element kodtrAnchorTitle = document.createElement(AC_LINK_KODTR_ANCHOR);
                        kodtrAnchorTitle.setTextContent(anchor);
                        if (acLink.getFirstChild() != null) {
                            acLink.insertBefore(kodtrAnchorTitle, acLink.getFirstChild());
                        } else {
                            acLink.appendChild(kodtrAnchorTitle);
                        }
                    }
                    continue;
                }
                String title = riPage.getAttribute("ri:content-title");
                if (title == null || title.isEmpty()) {
                    continue;
                }
                acLink.setAttribute("ri:content-title", title);
                if (acLink.hasAttribute(ANCHOR_ATTRIBUTE)) {
                    String anchor = acLink.getAttribute(ANCHOR_ATTRIBUTE);
                    Element kodtrAnchorTitle = document.createElement(AC_LINK_KODTR_ANCHOR);
                    kodtrAnchorTitle.setTextContent(anchor);
                    acLink.insertBefore(kodtrAnchorTitle, riPage);
                }
                Element kodtrTitle = document.createElement(AC_LINK_KODTR_TITLE);
                kodtrTitle.setTextContent(title);
                acLink.insertBefore(kodtrTitle, riPage);
            }

            List<Element> aHrefs = XMLUtils.getElementByTagName(root, "a");
            for (int i = aHrefs.size() - 1; i >= 0; i--) {
                Element aHref = aHrefs.get(i);
                String href = "";
                if (aHref.hasAttribute("href") == true) {
                    href = aHref.getAttribute("href");
                }

                String textLink = aHref.getTextContent();
                Element acLinkHref = document.createElement(AC_LINK_VALUE);
                //Set child element kodtr
                Element kodtrLink = document.createElement(AC_LINK_KODTR_LINK);
                Text textNode = document.createTextNode(textLink);
                kodtrLink.setTextContent(href);
                acLinkHref.appendChild(kodtrLink);
                acLinkHref.appendChild(textNode);
                acLinkHref.setAttribute(A_HREF_VALUE, href);
                aHref.getParentNode().insertBefore(acLinkHref, aHref);
                aHref.getParentNode().removeChild(aHref);
            }
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            MemoryLogUtil.outputLog("END CONVERT LINK PAGE");
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }
  
    public static String convertIncludeMacro(String xmlData) throws Exception {
        MemoryLogUtil.outputLog("BEGIN CONVERT INCLUDE ADD TAG PAGE");
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlData, false);
            if (document == null) {
                return xmlData;
            }

            Element root = document.getDocumentElement();
            List<Element> macroIncludes = XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, ATTR_INCLUDE_VALUE);
            for (Element macroInclude : macroIncludes) {
                Element acLink = XMLUtils.getFirstElementByTagName(macroInclude, AC_LINK_VALUE);
                if (acLink == null) {
                    continue;
                }
                Element riPage = XMLUtils.getFirstElementByTagName(acLink, RI_PAGE_VALUE);
                if (riPage == null) {
                    continue;
                }
                String title = riPage.getAttribute(TITLE_ATTRIBUTE);
                if (title == null || title.isEmpty()) {
                    continue;
                }

                Element kodtrIncludeTitle = document.createElement(AC_LINK_KODTR_INCLUDE);
                kodtrIncludeTitle.setAttribute(ATTR_KODTR_INCLUDE_TITLE, "");
                kodtrIncludeTitle.setTextContent(title);
                acLink.insertBefore(kodtrIncludeTitle, riPage);
                if (riPage.hasAttribute(SPACE_ATTRIBUTE)) {
                    Element kodtrIncludeSpace = document.createElement(AC_LINK_KODTR_INCLUDE);
                    kodtrIncludeSpace.setAttribute(ATTR_KODTR_INCLUDE_SPACE_KEY, "");
                    kodtrIncludeSpace.setTextContent(riPage.getAttribute(SPACE_ATTRIBUTE));
                    acLink.insertBefore(kodtrIncludeSpace, riPage);
                }
                
                Element tagTitleContent = XMLUtils.getFirstElementByTagName(acLink, AC_LINK_KODTR_TITLE);
                if(tagTitleContent != null){
                    tagTitleContent.getParentNode().removeChild(tagTitleContent);
                }
            }
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            MemoryLogUtil.outputLog("END CONVERT INCLUDE ADD TAG PAGE");
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }

    public static Map<String, String> getMapLinkAnchor(String xmlData) throws Exception {
        Map<String, String> mapAnchor = new LinkedHashMap<>();
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return mapAnchor;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return mapAnchor;
            }

            Element root = document.getDocumentElement();
            List<Element> anchors = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "anchor");
            for (Element anchor : anchors) {
                Element e = XMLUtils.getFirstElementByTagName(anchor, "ac:parameter");
                if (e == null) {
                    continue;
                }
                if (!e.hasAttribute(TEXT_ANCHOR_NOTRANS)) {
                    continue;
                }
                String text = e.getTextContent();
                if (text == null) {
                    continue;
                }
                String textNoTrans = e.getAttribute(TEXT_ANCHOR_NOTRANS);
                mapAnchor.put(textNoTrans, text);
                e.removeAttribute(TEXT_ANCHOR_NOTRANS);
            }
            return mapAnchor;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return mapAnchor;
        }
    }

    public static Map<String, String> getMapLinkHeader(String xmlData) throws Exception {
        Map<String, String> mapHeader = new LinkedHashMap<>();
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return mapHeader;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return mapHeader;
            }
            Element root = document.getDocumentElement();
            for (String item : HEADERS_CONTENT) {
                List<Element> headers = XMLUtils.getElementByTagName(root, item);
                for (Element header : headers) {
                    String textHeader = header.getTextContent();
                    if (textHeader == null) {
                        continue;
                    }
                    if (!header.hasAttribute(TEXT_HEADER_NOTRANS)) {
                        continue;
                    }
                    String textHeaderNoTrans = header.getAttribute(TEXT_HEADER_NOTRANS);
                    mapHeader.put(textHeaderNoTrans, textHeader);
                }
            }
            return mapHeader;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return mapHeader;
        }
    }

    public static String setNumberIndexForTagText(String xmlData) throws Exception {
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return xmlData;
            }
            Element root = document.getDocumentElement();
            setNumberIndexForTagText(root, 0);
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }

    public static int setNumberIndexForTagText(Element root, int index) throws Exception {
        NodeList nodes = root.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            try {
                Node child = nodes.item(i);
                if (child.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                if (child.getNodeName().equals("#cdata-section") == false
                        && child.getNodeName().equals("content") == false
                        && child.getNodeName().equals("kod") == false
                        && child.getNodeName().equals("page") == false) {
                    Element element = (Element) child;
                    element.setAttribute("trans-index", String.valueOf(index));
                    index++;
                }

                if (child.hasChildNodes() == true) {
                    index = setNumberIndexForTagText((Element) child, index);
                }
            } catch (Exception e) {
                continue;
            }
        }
        return index;
    }

    public static String removeNumberIndexForTagText(String xmlData) throws Exception {
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return xmlData;
            }
            Element root = document.getDocumentElement();
            removeNumberIndexForTagText(root);
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }

    public static void removeNumberIndexForTagText(Element root) throws Exception {
        NodeList nodes = root.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            try {
                Node node = nodes.item(i);
                if (node.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element child = (Element) node;
                if (child.hasAttribute("trans-index")) {
                    child.removeAttribute("trans-index");
                }
                if (child.hasChildNodes() == true) {
                    removeNumberIndexForTagText((Element) child);
                }
            } catch (Exception e) {
                continue;
            }
        }
    }

    public static String convertMacroDeckTranslation(String xmlData, String fromLangKey, String toLangKey) throws Exception {
        try {
            TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return xmlData;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
            elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
            for (int i = elements.size() - 1; i >= 0; i--) {
                Element element = elements.get(i);
                Element deskIdElement = XMLUtils.getFirstElementByTagNameAndAttr(element, "ac:parameter", "ac:name", "id");
                if (deskIdElement == null) {
                    continue;
                }
                String deskId = deskIdElement.getTextContent();
                deskIdElement.setAttribute("data-desk-id", deskId);
                deskIdElement.setTextContent("");
                if (element.getParentNode().getParentNode() != null && element.getParentNode().getParentNode().getNodeType() == Node.ELEMENT_NODE) {
                    Element parent = (Element) element.getParentNode().getParentNode();
                    String attr = parent.getAttribute("ac:name");
                    if (attr != null && attr.equals("card")) {
                        continue;
                    }
                }
                boolean isExistsLangKeyFromInMacroDesk = isExistsLangKeyInMacroDesk(element, fromLangKey);

                if (isExistsLangKeyFromInMacroDesk == false) {
                    continue;
                }
                Element riBody = XMLUtils.getFirstElementByTagName(element, "ac:rich-text-body");
                if(riBody == null){
                    continue;
                }
                String langKey = fromLangKey.toLowerCase();
                List<Element> listCard = XMLUtils.getElementsByTagNameAndAttr(element, AC_MACRO_VALUE, AC_NAME_VALUE, "card");
                listCard.addAll(XMLUtils.getElementsByTagNameAndAttr(element, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "card"));
                for (Element card : listCard) {
                    Element parentNode = (Element)card.getParentNode();
                    if(parentNode != riBody){
                        continue;
                    }
                    Element nodeLabel = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "label");
                    if (nodeLabel == null) {
                        continue;
                    }

                    Element nodeid = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "id");
                    if (nodeid == null) {
                        card.getParentNode().removeChild(card);
                        continue;
                    }

                    String langKeyId = nodeid.getTextContent().toLowerCase();
                    if (langKeyId.equals(langKey) == false) {
                        card.getParentNode().removeChild(card);
                        continue;
                    }

                    LanguageList languageList = translationLanguageListService.getByLangKey(langKey);
                    if (languageList == null) {
                        card.getParentNode().removeChild(card);
                        continue;
                    }

                    languageList = translationLanguageListService.getByLangKey(toLangKey);
                    nodeLabel.setAttribute("data-lang-key-target", languageList.getLanguageDisplay());
                    nodeLabel.setTextContent("");
                    nodeid.setAttribute("data-lang-key-target", toLangKey);
                    nodeid.setTextContent("");
                }
            }
            String result = DocumentUtil.convertXmlToString(document, false, true);
            return result;
        } catch (Exception e) {
            return xmlData;
        }
    }

    public static String convertMacroDeckSync(String xmlData, Long pageId, String langKey) throws Exception {
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlData, true);
            if (document == null) {
                return xmlData;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
            elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
            JsonArray arr = new JsonArray();
            for (int i = elements.size() - 1; i >= 0; i--) {
                Element element = elements.get(i);
                Element deskIdElement = XMLUtils.getFirstElementByTagNameAndAttr(element, "ac:parameter", "data-desk-id");
                if (deskIdElement != null) {
                    String deskId = deskIdElement.getAttribute("data-desk-id");
                    deskIdElement.setTextContent(deskId);
                    deskIdElement.removeAttribute("data-desk-id");
                }
                if (element.getParentNode().getParentNode() != null && element.getParentNode().getParentNode().getNodeType() == Node.ELEMENT_NODE) {
                    Element parent = (Element) element.getParentNode().getParentNode();
                    String attr = parent.getAttribute("ac:name");
                    if (attr != null && attr.equals("card")) {
                        continue;
                    }
                }
                String macroIdDesk = element.getAttribute("ac:macro-id");
                Element richBody = XMLUtils.getFirstElementByTagName(element, "ac:rich-text-body");
                if (richBody == null) {
                    continue;
                }
                boolean hasReplaceCard = false;
                List<Element> listCard = XMLUtils.getElementsByTagNameAndAttr(element, AC_MACRO_VALUE, AC_NAME_VALUE, "card");
                listCard.addAll(XMLUtils.getElementsByTagNameAndAttr(element, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "card"));
                for (Element card : listCard) {
                     Element parentNode = (Element)card.getParentNode();
                    if(parentNode != richBody){
                        continue;
                    }
                    Element nodeLabel = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "label");
                    if (nodeLabel == null) {
                        continue;
                    }

                    Element nodeid = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "id");
                    if (nodeid == null) {
                        continue;
                    }

                    if (!nodeid.hasAttribute("data-lang-key-target")) {
                        continue;
                    }

                    if (!nodeLabel.hasAttribute("data-lang-key-target")) {
                        continue;
                    }
                    nodeLabel.setTextContent(nodeLabel.getAttribute("data-lang-key-target"));
                    nodeLabel.removeAttribute("data-lang-key-target");
                    if (nodeid != null && nodeid.hasAttribute("data-lang-key-target")) {
                        nodeid.removeAttribute("data-lang-key-target");
                        nodeid.setTextContent(langKey.toUpperCase());
                    }
                    String macroId = card.getAttribute("ac:macro-id");
                    String cloneCard = DocumentUtil.convertXmlToString(card, false, true);
                    JsonObject obj = new JsonObject();
                    obj.addProperty("cloneCard", cloneCard);
                    obj.addProperty("langKey", langKey);
                    obj.addProperty("macroIdDesk", macroIdDesk);
                    obj.addProperty("macroId", macroId);

                    arr.add(obj);
                    hasReplaceCard = true;
                }

                if (hasReplaceCard == true) {
                    if (element.getParentNode().getParentNode() != null) {
                        Element parent = (Element)element.getParentNode().getParentNode();
                        String attr = parent.getAttribute("ac:name");
                        if(attr != null & attr.equals("card")){
                            continue;
                        }
                    }
                    element.getParentNode().removeChild(element);
                }
            }
            String result = DocumentUtil.convertXmlToString(document, false, true);
            updateFromPageLanguage(pageId, arr);
            return result;
        } catch (Exception e) {
            return xmlData;
        }
    }

    public static String convertMacroDeckSyncSourceLang(Long pageIdSource, String xmlDataNew, String fromLangKey) throws Exception {
        try {
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            Page pageSource = pageManager.getPage(pageIdSource);
            if (pageSource == null) {
                return xmlDataNew;
            }
            String xmlDataOld = pageSource.getBodyAsString();
            Document document = DocumentUtil.convertStringToDocument(xmlDataNew, true);
            if (document == null) {
                return xmlDataNew;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
            elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
            Map<String, JsonObject> mapDeskOld = getArrayMacroDeck(xmlDataOld, fromLangKey);
            if (mapDeskOld.isEmpty()) {
                return xmlDataNew;
            }
            for (int i = elements.size() - 1; i >= 0; i--) {
                Element element = elements.get(i);
                if (element.hasAttribute("ac:macro-id") == false) {
                    continue;
                }
                String macroIdDesk = element.getAttribute("ac:macro-id");
                JsonObject obj = mapDeskOld.get(macroIdDesk);
                if (obj == null) {
                    continue;
                }
                Element cardNew = XMLUtils.getFirstElementByTagNameAndAttr(element, "ac:structured-macro", "ac:name", "card");
                if (cardNew == null) {
                    continue;
                }

                Element riBodyNew = XMLUtils.getFirstElementByTagName(cardNew, "ac:rich-text-body");
                if (riBodyNew == null) {
                    continue;
                }
                Boolean isExistsLangKeyFromInMacroDesk = obj.get("isSourceLang").getAsBoolean();
                String deskOld = obj.get("sourceDesk").getAsString();
                Document docDeskOld = DocumentUtil.convertStringToDocument(deskOld, true);
                Element elementDeskOld = docDeskOld.getDocumentElement();
                if (isExistsLangKeyFromInMacroDesk == true) {
                    List<Element> listCardOld = XMLUtils.getElementsByTagNameAndAttr(elementDeskOld, "ac:structured-macro", "ac:name", "card");
                    for (int j = listCardOld.size() - 1; j >= 0; j--) {
                        Element card = listCardOld.get(j);
                        Element nodeid = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "id");
                        if (nodeid == null) {
                            continue;
                        }
                        String langKey = nodeid.getTextContent();
                        if (langKey == null || langKey.equalsIgnoreCase(fromLangKey) == false) {
                            continue;
                        }
                        Element riBodyOld = XMLUtils.getFirstElementByTagName(card, "ac:rich-text-body");
                        if (riBodyOld == null) {
                            continue;
                        }
                        Node importNodeOld = docDeskOld.importNode(riBodyNew, true);
                        riBodyOld.getParentNode().insertBefore(importNodeOld, riBodyOld);
                        riBodyOld.getParentNode().removeChild(riBodyOld);
                    }
                    Node importedNode = document.importNode(elementDeskOld, true);
                    element.getParentNode().insertBefore(importedNode, element);
                    element.getParentNode().removeChild(element);
                }
            }
            String result = DocumentUtil.convertXmlToString(document, false, true);
            return result;
        } catch (Exception e) {
            MemoryLogUtil.outputLog("Exception Convert Macro Deck Sync Source Lang : " + e.getMessage());
            return xmlDataNew;
        }
    }

    public static boolean onlyMacroDeckExist(String xmlPage, String toLangKey) throws Exception {
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return false;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
            elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
            for (int i = elements.size() - 1; i >= 0; i--) {
                Element element = elements.get(i);
                Element card = XMLUtils.getFirstElementByTagNameAndAttr(element, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "card");
                if (card == null) {
                    continue;
                }
                Element nodeid = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "data-lang-key-target", toLangKey);
                if (nodeid != null) {
                    element.getParentNode().removeChild(element);
                }
            }

            Element content = XMLUtils.getFirstElementByTagName(root, "content");
            if (!content.getTextContent().isEmpty()) {
                return false;
            }

            List<Element> brs = XMLUtils.getElementByTagName(content, "br");
            for (int i = brs.size() - 1; i >= 0; i--) {
                Element br = brs.get(i);
                br.getParentNode().removeChild(br);
            }

            List<Element> ps = XMLUtils.getElementByTagName(content, "p");
            for (int i = ps.size() - 1; i >= 0; i--) {
                Element p = ps.get(i);
                if (p.hasChildNodes() == false) {
                    p.getParentNode().removeChild(p);
                }
            }
            
            List<Element> spans = XMLUtils.getElementByTagName(content, "span");
            for (int i = spans.size() - 1; i >= 0; i--) {
                Element span = spans.get(i);
                if (span.hasChildNodes() == false) {
                    span.getParentNode().removeChild(span);
                }
            }
            
            Element kod = XMLUtils.getFirstElementByTagName(content, "kod");
            if(kod != null && kod.hasChildNodes() == false){
                kod.getParentNode().removeChild(kod);
            }

            if(content.hasChildNodes()){
                return false;
            }
            
            return true;
        } catch (Exception e) {
            MemoryLogUtil.outputLog("Exception Convert Macro Deck Sync Source Lang : " + e.getMessage());
            return false;
        }
    }

    public static Map<String, JsonObject> getArrayMacroDeck(String xmlPage, String fromLangKey) throws Exception {
        Map<String, JsonObject> result = new HashMap<>();
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, false);
            if (document == null) {
                return result;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
            elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
            for (Element element : elements) {
                if (element.hasAttribute("ac:macro-id") == false) {
                    continue;
                }
                Element deskIdElement = XMLUtils.getFirstElementByTagNameAndAttr(element, "ac:parameter", "data-desk-id");
                if (deskIdElement != null) {
                    String deskId = deskIdElement.getAttribute("data-desk-id");
                    deskIdElement.setTextContent(deskId);
                    deskIdElement.removeAttribute("data-desk-id");
                }
                JsonObject obj = new JsonObject();
                String macroId = element.getAttribute("ac:macro-id");
                boolean isExistsLangKey = isExistsLangKeyInMacroDesk(element, fromLangKey);
                if (isExistsLangKey == false) {
                    continue;
                }
                String deskString = DocumentUtil.convertXmlToString(element, false, true);
                obj.addProperty("isSourceLang", isExistsLangKey);
                obj.addProperty("sourceDesk", deskString);
                result.put(macroId, obj);
            }
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static void updateFromPageLanguage(Long pageId, JsonArray array) throws Exception {
        try {
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            Page page = pageManager.getPage(pageId);
            if (page == null) {
                return;
            }
            String xmlData = page.getBodyAsString();
            if (xmlData.equals("")) {
                return;
            }
            MemoryLogUtil.outputLog(xmlData);
            Document document = DocumentUtil.convertStringToDocument(xmlData, false);

            if (document == null) {
                return;
            }
            Element root = document.getDocumentElement();
            for (int i = array.size() - 1; i >= 0; i--) {
                JsonObject obj = array.get(i).getAsJsonObject();
                String xmlCardTranslated = obj.get("cloneCard").getAsString();
                String toLangKey = obj.get("langKey").getAsString();
                String macroIdDesk = obj.get("macroIdDesk").getAsString();
                String macroIdCard = obj.get("macroId").getAsString();
                Document docCardTranslated = DocumentUtil.convertStringToDocument(xmlCardTranslated, true);
                Element cardTranslated = docCardTranslated.getDocumentElement();
                //Node importedNode = document.importNode(cardTranslated, true);
                List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "deck");
                elements.addAll(XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "deck"));
                for (Element element : elements) {
                    if (element.getParentNode().getParentNode() != null 
                            && element.getParentNode().getParentNode().getNodeType() == Node.ELEMENT_NODE) {
                        Element parent = (Element) element.getParentNode().getParentNode();
                        String attr = parent.getAttribute("ac:name");
                        if (attr != null && attr.equals("card")) {
                            continue;
                        }
                    }
                    if (!element.hasAttribute("ac:macro-id")) {
                        continue;
                    }
                    String idParent = element.getAttribute("ac:macro-id");
                    if (!idParent.equals(macroIdDesk)) {
                        continue;
                    }

                    Element richBody = XMLUtils.getFirstElementByTagName(element, "ac:rich-text-body");
                    if (richBody == null) {
                        continue;
                    }
                    boolean isExistsLangKeyInMacroDesk = isExistsLangKeyInMacroDesk(element, toLangKey);
                    if (isExistsLangKeyInMacroDesk == false) {
                        Node importedNode = document.importNode(cardTranslated, true);
                        richBody.appendChild(importedNode);
                    } else {
                        List<Element> listCard = XMLUtils.getElementsByTagNameAndAttr(element, AC_MACRO_VALUE, AC_NAME_VALUE, "card");
                        listCard.addAll(XMLUtils.getElementsByTagNameAndAttr(element, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "card"));
                        for (int j = listCard.size() - 1; j >= 0; j--) {
                            Element card = listCard.get(j);
                            Element parentNode = (Element) card.getParentNode();
                            if (parentNode != richBody) {
                                continue;
                            }
                            Element nodeId = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "id");
                            if (nodeId == null) {
                                continue;
                            }
                            String langKey = nodeId.getTextContent().toLowerCase();
                            if (langKey != null && !langKey.equals(toLangKey.toLowerCase())) {
                                continue;
                            }
                            Node importedNode = document.importNode(cardTranslated, true);
                            card.getParentNode().insertBefore(importedNode, card);
                            card.getParentNode().removeChild(card);
                        }
                    }
                }
            }
            List<Element> svTrans = XMLUtils.getElementsByTagNameAndAttr(root, "span", "class", "sv-cdata-translate");
            for (int i = svTrans.size() - 1; i >= 0; i--) {
                try {
                    Element svTran = svTrans.get(i);
                    String textPlain = svTran.getTextContent();
                    CDATASection cdata = document.createCDATASection(textPlain);
                    svTran.getParentNode().insertBefore(cdata, svTran);
                    svTran.getParentNode().removeChild(svTran);
                } catch (Exception ex) {
                    MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
                    continue;
                }
            }
            xmlData = DocumentUtil.convertXmlToString(document, true, true);
            page.setBodyAsString(xmlData);
            pageManager.saveContentEntity(page, null);
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
        }
    }

    public static boolean isExistsLangKeyInMacroDesk(Element element, String langKey) throws Exception {
        try {
            boolean result = false;
//            String xml = DocumentUtil.convertXmlToString(element, false, true);
//            String replaceRegex = "<ac:parameter[^>]*\\sac:name=['\"]id[\"']>(?i)" + langKey + "<\\/ac:parameter>";
//            Pattern p = Pattern.compile(replaceRegex);
//            Matcher m = p.matcher(xml);
//            result = m.find();
            Element richBody = XMLUtils.getFirstElementByTagName(element, "ac:rich-text-body");
            if (richBody == null) {
                return false;
            }
            List<Element> listCard = XMLUtils.getElementsByTagNameAndAttr(element, AC_MACRO_VALUE, AC_NAME_VALUE, "card");
            listCard.addAll(XMLUtils.getElementsByTagNameAndAttr(element, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, "card"));
            for (Element card : listCard) {
                Element parentNode = (Element) card.getParentNode();
                if (parentNode != richBody) {
                    continue;
                }
                Element nodeId = XMLUtils.getFirstElementByTagNameAndAttr(card, "ac:parameter", "ac:name", "id");
                if (nodeId == null) {
                    continue;
                }
                String keyParam = nodeId.getTextContent().toLowerCase();
                if(langKey.equalsIgnoreCase(keyParam)){
                    return true;
                }
            }
            return result;
        } catch (Exception e) {
            return false;
        }
    }

    public static String changeFont(String xmlPage, String toLanguageKey) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }
        String font = getFontByLang(toLanguageKey);
        if (font.equals("")) {
            return xmlPage;
        }
        String fontElement = "<font face=\"" + font + "\"";
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression_mxcell = MXCEll_XPATH;
            NodeList nodeListMxcell = (NodeList) xPath.compile(expression_mxcell).evaluate(document, XPathConstants.NODESET);
            if (nodeListMxcell.getLength() == 0) {
                expression_mxcell = MXCEll_XPATH_LOWER;
                nodeListMxcell = (NodeList) xPath.compile(expression_mxcell).evaluate(document, XPathConstants.NODESET);
            }
            if (nodeListMxcell.getLength() > 0) {
                boolean lowerCell = false;
                String replaceRegex = "<mxCell[^>]*>((?!<\\/mxCell>).)*<\\/mxCell>";
                Pattern p = Pattern.compile(replaceRegex);
                Matcher m = p.matcher(xmlPage);
                if (m.find() == false) {
                    lowerCell = true;
                    replaceRegex = "<mxcell[^>]*>((?!<\\/mxcell>).)*<\\/mxcell>";
                    p = Pattern.compile(replaceRegex);
                    m = p.matcher(xmlPage);
                }
                m.reset();
                while (m.find()) {
                    String mcellXml = m.group();
                    //Remove old font
                    String removeFont = "face[ ]*=[ ]*\"[^\"]*\"";
                    mcellXml = mcellXml.replaceAll(removeFont, "");
                    if (mcellXml.contains("<font")) {
                        mcellXml = mcellXml.replaceAll("<font", fontElement);
                    } else {
                        mcellXml = mcellXml.replaceFirst(">", ">" + fontElement + ">");
                        if (lowerCell) {
                            mcellXml = mcellXml.replaceAll("</mxcell>", "</font></mxcell>");
                        } else {
                            mcellXml = mcellXml.replaceAll("</mxCell>", "</font></mxCell>");
                        }
                    }
                    xmlPage = xmlPage.replace(m.group(), mcellXml);
                }
            }
        } catch (Exception ex) {
            return xmlPage;
        }

        return xmlPage;
    }

    public static String getFontByLang(String language) {
        String font = "";
        language = checkLang(language);
        switch (language) {
            case "en":
                font = ARIAL_FONT;
                break;
            case "ko":
                font = GLIM_FONT;
                break;
            case "zh_cn":
                font = SIMHEI_FONT;
                break;
            case "zh_tw":
                font = SIMIHEI_FONT;
                break;
        }
        return font;
    }

    public static String checkLang(String lang) {
        if (lang.equals("zh_cn") || lang.equals("zh_tw")) {
            return lang;
        }
        lang = lang.toLowerCase().replaceAll("_", "-");
        if (lang.length() < 2) {
            return lang;
        }

        //言語は2つ文字列がある場合
        String twoLang = lang.substring(0, 2);
        if (twoLang != null) {
            lang = twoLang;
        }
        //存在しない場合はjaとする
        return lang;
    }

    public static String removeSpan(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression = SPAN_XPATH;
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
            while (nodeList.getLength() > 0) {
                boolean isChange = false;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node nNode = nodeList.item(i);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Node nParrent = nNode.getParentNode();
                        if (nNode.hasAttributes() == false) {
                            String xmlStringParent = DocumentUtil.convertXmlToString(nParrent, true, true);
                            String xmlStringElement = DocumentUtil.convertXmlToString(nNode, true, true);
                            xmlStringElement = xmlStringElement.trim();
                            String newXmlStringElement = xmlStringElement;
                            if (newXmlStringElement.startsWith("<span>")) {
                                newXmlStringElement = newXmlStringElement.substring(6);
                            }

                            if (newXmlStringElement.endsWith("</span>")) {
                                newXmlStringElement = newXmlStringElement.substring(0, newXmlStringElement.length() - 7);
                            }

                            if (newXmlStringElement.equals("<span/>") || newXmlStringElement.equals("<span />")) {
                                newXmlStringElement = StringUtils.EMPTY;
                            }

                            xmlStringParent = xmlStringParent.replace(xmlStringElement, newXmlStringElement);
                            if (nParrent.isSameNode(document.getDocumentElement())) {
                                document = DocumentUtil.convertStringToDocument(xmlStringParent, true);
                            } else {
                                Document docParent = DocumentUtil.convertStringToDocument(xmlStringParent, true);
                                Node newParrent = document.importNode(docParent.getDocumentElement(), true);
                                nParrent.getParentNode().insertBefore(newParrent, nParrent);
                                nParrent.getParentNode().removeChild(nParrent);
                            }
                            isChange = true;
                            break;
                        }
                    }
                }

                if (isChange == false) {
                    break;
                }

                nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
            }
            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            return xmlPage;
        }

        return xmlPage;
    }

    public static String removeInlineComment(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression = ATTR_REF_NODE;
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
            while (nodeList.getLength() > 0) {
                boolean isChange = false;
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node nNode = nodeList.item(i);
                    if (INLINE_COMMENT_NODE.equals(nNode.getNodeName()) && (nNode.getNodeType() == Node.ELEMENT_NODE)) {
                        Node nParrent = nNode.getParentNode();
                        String xmlStringParent = DocumentUtil.convertXmlToString(nParrent, true, true);

                        //Get acRef value
                        String acRef = StringUtils.EMPTY;
                        acRef = nNode.getAttributes().getNamedItem("ac:ref").getNodeValue();

                        String xmlStringElement = DocumentUtil.convertXmlToString(nNode, true, true);
                        xmlStringElement = xmlStringElement.trim();
                        System.out.println(xmlStringParent);
                        String newXmlStringElement = xmlStringElement;
                        String inlCmtTag = "<ac:inline-comment-marker ac:ref=\"" + acRef + "\">";
                        if (newXmlStringElement.startsWith(inlCmtTag)) {
                            newXmlStringElement = newXmlStringElement.substring(inlCmtTag.length());
                        }

                        if (newXmlStringElement.endsWith("</ac:inline-comment-marker>")) {
                            newXmlStringElement = newXmlStringElement.substring(0, newXmlStringElement.length() - 27);
                        }

                        xmlStringParent = xmlStringParent.replace(xmlStringElement, newXmlStringElement);
                        Document docParent = DocumentUtil.convertStringToDocument(xmlStringParent, true);
                        Node newParrent = document.importNode(docParent.getDocumentElement(), true);

                        nParrent.getParentNode().insertBefore(newParrent, nParrent);
                        nParrent.getParentNode().removeChild(nParrent);
                        isChange = true;
                        break;
                    }
                }

                if (isChange == false) {
                    break;
                }

                nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
            }
            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }
        return xmlPage;
    }

    public static String removeNameSpaceTranslation(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            NodeList nodeList = document.getElementsByTagName(XML_CONTENT);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(0);
                if (node.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                Element element = (Element) node;
                NamedNodeMap attributes = element.getAttributes();
                for (int j = 0; j < attributes.getLength(); j++) {
                    Node attribute = attributes.item(j);
                    if (attribute.getNodeType() != Node.ATTRIBUTE_NODE) {
                        continue;
                    }

                    String value = attribute.getTextContent();
                    if (value.equalsIgnoreCase(XhtmlConstants.XHTML_NAMESPACE_URI)) {
                        element.removeAttributeNode((Attr) attribute);
                    }
                }
            }

            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }
        return xmlPage;
    }

    public static String removeTitlePageTranslation(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            NodeList nodes = document.getElementsByTagName(XML_TITLE);
            if (nodes.getLength() == 0) {
                return xmlPage;
            }

            Node node = nodes.item(0);
            if (node != null) {
                node.getParentNode().removeChild(node);
            }
            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }
        return xmlPage;
    }

    /**
     *
     * @param xmlPage
     * @return
     */
    private static String skipTranslation(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        //SkipTransPathService skipTranslationXPathService = TranslationMemoryToolFactory.getSkipTransPathService();
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

//            for (SkipTransPath skipTran : skipTranslationXPathService.getAll()) {
//                String expression = skipTran.getPath();
//                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
//                for (int i = 0; i < nodeList.getLength(); i++) {
//                    Node nNode = nodeList.item(i);
//                    if (nNode.getNodeType() == Node.ELEMENT_NODE && (nNode.getAttributes().getLength() == 0 || nNode.getAttributes().getNamedItem(ATTR_PARAMETER) == null)) {
//                        Element eElement = (Element) nNode;
//                        String eValue = DocumentUtil.convertXmlToString(eElement, false, true);
//                        eElement.setAttribute(DocumentUtil.ATTR_PARAMETER, Base64.encodeBase64String(eValue.getBytes()));
//                        eElement.setTextContent(null);
//                    }
//                }
//            }
            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }

        return xmlPage;
    }

    /**
     *
     * @param xmlPage
     * @return
     */
    private static String needTranslation(String xmlPage) throws Exception {
        //
        return xmlPage;
    }

    private static String convertCdataTranslation(String xmlPage) throws Exception {
        return convertCdataTranslation(xmlPage, CONVERT_CDATA_ELEMENT_START, CONVERT_CDATA_ELEMENT_END);
    }

    public static String convertCdataTranslation(String xmlPage, String startTag, String endTag) throws Exception {
        Pattern p1 = Pattern.compile("<!\\[CDATA\\[((?!\\]\\]>).)*\\]\\]>");
        Pattern p2 = Pattern.compile("<!--\\[CDATA\\[((?!\\]\\]-->).)*\\]\\]-->");
        Matcher m1 = p1.matcher(xmlPage);
        Matcher m2 = p2.matcher(xmlPage);
        if (m1.groupCount() > 0) {
            while (m1.find()) {
                String value = m1.group();
                value = value.replace("<![CDATA[", "");
                value = value.replace("]]>", "");
                value = unescapeJava(escapeXml(escapeJava(value)));
                value = startTag + value + endTag;
                xmlPage = xmlPage.replace(m1.group(), value);
            }
        }
        if (m2.groupCount() > 0) {
            while (m2.find()) {
                String value = m2.group();
                value = value.replace("<--![CDATA[", "");
                value = value.replace("]]-->", "");
                value = unescapeJava(escapeXml(escapeJava(value)));
                value = startTag + value + endTag;
                xmlPage = xmlPage.replace(m1.group(), value);
            }
        }

        return xmlPage;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="Synchronize">
    /**
     *
     * @param page
     * @param xmlPage
     * @param isUpdateTitle
     * @param args
     * @param isSaveContent
     * @return
     */
    public static String xmlSynchronizeProccess(String xmlPage, boolean importSourceLang, List<Integer> args, boolean isOnlyDesskMacro, String toLanguageKey) {
        try {
            //LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
            //Language language = languageService.getLanguageBySpaceKey(page.getSpaceKey());
            xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
            if (importSourceLang == false && isOnlyDesskMacro == false) {
                //spanタグを削除します。
                xmlPage = removeSpan(xmlPage);
                //ac:inline-comment-markerタグを削除します。
                xmlPage = removeInlineComment(xmlPage);
                //change font
                xmlPage = changeFont(xmlPage, toLanguageKey);
            }

            if (args == null || args.isEmpty()) {
                args = new ArrayList<Integer>();
                args.add(DocumentUtil.NEED_TRANSLATION);
                args.add(DocumentUtil.SKIP_TRANSLATION);
            }

            for (int param : args) {
                switch (param) {
                    case DocumentUtil.SKIP_TRANSLATION:
                        xmlPage = skipSynchronize(xmlPage);
                        break;
                    case DocumentUtil.CONVERT_CDATA_ELEMENT:
                        xmlPage = convertCdataSynchronize(xmlPage);
                    case DocumentUtil.REMOVE_GENGO_MACRO_TRANSLATION:
                        //xmlPage = removeGengoMacro(xmlPage, language.getFromLanguage(), toLanguageKey);
                        break;
                    case DocumentUtil.NEED_TRANSLATION:
                    //なし
                    default:
                    //なし
                }
            }

            xmlPage = DocumentUtil.skipProcessChar(xmlPage, true);
            return xmlPage;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }
    }

    /**
     *
     * @param xmlPage
     * @return
     */
    private static String skipSynchronize(String xmlPage) throws Exception {
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression = DocumentUtil.ATTR_PARAMETER_XPATH;
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String attValue = eElement.getAttributeNode(DocumentUtil.ATTR_PARAMETER).getValue();
                    attValue = new String(Base64.decodeBase64(attValue));
                    Document child = DocumentUtil.convertStringToDocument(attValue, true);
                    Node root = child.getDocumentElement().cloneNode(true);
                    document.adoptNode(root);
                    eElement.getParentNode().insertBefore(root, eElement);
                    eElement.getParentNode().removeChild(eElement);
                }
            }
            xmlPage = DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, ex);
            return xmlPage;
        }

        return xmlPage;
    }

    private static String convertCdataSynchronize(String xmlPage) {
        return convertCdataSynchronize(xmlPage, CONVERT_CDATA_ELEMENT_START, CONVERT_CDATA_ELEMENT_END);
    }

    public static String convertCdataSynchronize(String xmlPage, String startTag, String endTag) {
        while (xmlPage.indexOf(startTag) > -1) {
            int startIndex = xmlPage.indexOf(startTag);
            int endIndex = xmlPage.indexOf(endTag);
            if (endIndex <= startIndex) {
                break;
            }

            String oldString = xmlPage.substring(startIndex, endIndex + endTag.length());
            String value = oldString.replace(startTag, "");
            value = value.replace(endTag, "");
            value = unescapeJava(unescapeXml(escapeJava(value)));
            value = "<![CDATA[" + value + "]]>";
            xmlPage = xmlPage.replace(oldString, value);
        }

        return xmlPage;
    }
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc="パブリック">
    public static Document convertStringToDocument(String content, boolean hasRoot) throws Exception {
        String xmlString = content.replaceFirst("^(<\\?xml([^?>])*\\?>)", "");
        if (xmlString != null) {
            xmlString = StringHtmlEscapeUtil.encodeCdataToBase64(xmlString);
            xmlString = xmlString.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "");
            xmlString = StringHtmlEscapeUtil.decodeCdataToBase64(xmlString);
            xmlString = StringHtmlEscapeUtil.unEscapeHtmlEntities(xmlString);
            if (!hasRoot) {
                xmlString = "<kod>" + xmlString + "</kod>";
            }
        } else if (hasRoot) {
            xmlString = "<kod></kod>";
        }

        // html document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document document = null;
        try {
            builder = factory.newDocumentBuilder();
            xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlString;
            document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
        } catch (Exception ex) {
            try {
                if (!hasRoot) {
                    xmlString = xmlString = xmlString.replaceFirst("^(<\\?xml([^?>])*\\?>)", "");
                    xmlString = "<kod>" + xmlString + "</kod>";
                    builder = factory.newDocumentBuilder();
                    xmlString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlString;
                    xmlString = StringHtmlEscapeUtil.unEscapeHtmlEntities(xmlString);
                    document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
                } else {
                    throw ex;
                }
            } catch (Exception e) {
                try {
                    org.jsoup.nodes.Document workdocument = org.jsoup.Jsoup.parse(xmlString);
                    org.jsoup.helper.W3CDom w3cDom = new org.jsoup.helper.W3CDom();
                    document = w3cDom.fromJsoup(workdocument);
                } catch (Exception e1) {
                    throw e1;
                }
            }
        }
        // パーサー内のDOMツリーのドキュメントノードをdocumentにセット
        return document;
    }

    public static Document convertStringToDocumentJsoup(String content, boolean hasRoot) throws Exception {
        if (content != null) {
            content = content.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "");
            content = StringHtmlEscapeUtil.unEscapeHtmlEntities(content, true);
            if (!hasRoot) {
                content = "<kod>" + content + "</kod>";
            }
        } else {
            content = "<kod></kod>";
        }

        Document document = null;
        try {
            org.jsoup.nodes.Document workdocument = org.jsoup.Jsoup.parse(content);
            org.jsoup.helper.W3CDom w3cDom = new org.jsoup.helper.W3CDom();
            document = w3cDom.fromJsoup(workdocument);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        // パーサー内のDOMツリーのドキュメントノードをdocumentにセット
        return document;
    }

    /**
     *
     * @param document
     * @return
     */
    public static String convertXmlToString(Node document, Boolean isRemoveRoot, Boolean isXmlDeclaration) throws Exception {
        StringWriter sw = new StringWriter();
        try {
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            if (isXmlDeclaration == true) {
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            } else {
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            }
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "no");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.transform(new DOMSource(document), new StreamResult(sw));
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }

        sw.flush();
        String result = sw.toString().replace("<kod>", "").replace("</kod>", "").replace("<kod/>", "").replace("<kod />", "");
        result = StringHtmlEscapeUtil.unEscapeHtmlEntities(result);
        return result;
    }

    /**
     *
     * @param listPageSrc
     * @param listPageDest
     * @return
     */
    public static Map<String, List<String>> getListTextOfHeadingAndAnchor(List<Page> listPageSrc, List<Page> listPageDest, String fromLanguage, String toLanguage) throws Exception {
        Map<String, List<String>> mapAnchorTxt = new LinkedHashMap<String, List<String>>();
        for (int h = 0; h < listPageDest.size(); h++) {
            List<String> lstAnchorTxt = new ArrayList<String>();
            Page page = listPageSrc.get(h);
            if (page == null || listPageDest.get(h) == null) {
                continue;
            }
            MemoryLogUtil.outputLog("BEGIN GET LIST TEXT OF HEADING ANCHOR : " + page.getIdAsString());
            try {
                String xmlPage = ImportExportUtil.exportXmlString(page);
                xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
                xmlPage = removeGengoMacro(xmlPage, fromLanguage, toLanguage);
                if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
                    continue;
                }

                List<String> listTextContentOfElement = new ArrayList<String>();
                listTextContentOfElement = getListTextContentOfElement(ANCHOR_PARAMETER, xmlPage, false);
                listTextContentOfElement.addAll(getListTextContentOfElement(ANCHOR_HEADING, xmlPage, true));
                for (String str : listTextContentOfElement) {
                    lstAnchorTxt.add(listPageDest.get(h).getIdAsString() + "_" + str);
                }

                mapAnchorTxt.put(listPageDest.get(h).getIdAsString(), lstAnchorTxt);
            } catch (Exception ex) {
                MemoryLogUtil.outputLog("EXCEPTION GET LIST TEXT OF HEADING ANCHOR : " + ex.getMessage());
                continue;
            }
        }

        return mapAnchorTxt;
    }

    /**
     *
     * @param listPageDest
     * @return
     */
    public static Map<String, List<String>> getDicIncludePage(List<Page> listPageDest) throws Exception {
        MemoryLogUtil.outputLog("BEGIN GET DIC INCLUDE :");
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        Map<String, List<String>> dic = new HashMap<String, List<String>>();
        for (int h = 0; h < listPageDest.size(); h++) {
            Page page = listPageDest.get(h);
            if (page == null) {
                continue;
            }
            MemoryLogUtil.outputLog("GET DIC INCLUDE :" + page.getIdAsString());
            String xmlPage = ImportExportUtil.exportXmlString(page);
            xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
            if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
                continue;
            }
            List<String> listTextIncludePage = new ArrayList<String>();
            listTextIncludePage = getListTextContentOfElement(INCLUDE_PAGE, xmlPage, true);
            for (String strTitle : listTextIncludePage) {
                Page pageInclude = pageManager.getPage(page.getSpaceKey(), strTitle);
                if (pageInclude == null || pageInclude.isHomePage()) {
                    continue;
                }
                List<String> listTextIncludeRootPageId = new ArrayList<String>();
                List<String> listTextIncludePageId = new ArrayList<String>();
                if (dic.containsKey(pageInclude.getIdAsString())) {
                    listTextIncludeRootPageId.addAll(dic.get(pageInclude.getIdAsString()));
                    listTextIncludeRootPageId.add(page.getIdAsString());
                    dic.remove(pageInclude.getIdAsString());
                    dic.put(pageInclude.getIdAsString(), listTextIncludeRootPageId);
                } else {
                    listTextIncludeRootPageId.add(page.getIdAsString());
                    dic.put(pageInclude.getIdAsString(), listTextIncludeRootPageId);
                }
                if (dic.containsKey(page.getIdAsString())) {
                    listTextIncludePageId.addAll(dic.get(page.getIdAsString()));
                    listTextIncludePageId.add(pageInclude.getIdAsString());
                    dic.remove(page.getIdAsString());
                    dic.put(page.getIdAsString(), listTextIncludePageId);
                } else {
                    dic.put(page.getIdAsString(), Arrays.asList(pageInclude.getIdAsString()));
                }
            }
        }
        Set<String> setKey = dic.keySet();
        for (String strKey : setKey.toArray(new String[setKey.size()])) {
            List<String> listTextIncludePageId = dic.get(strKey);
            for (String strPageId : listTextIncludePageId) {
                List<String> listTextIncludePageIdNext = new ArrayList<String>();
                listTextIncludePageIdNext.addAll(dic.get(strPageId));
                for (String strPageIdNext : listTextIncludePageId) {
                    if (!strPageIdNext.equals(strPageId) && !listTextIncludePageIdNext.contains(strPageIdNext)) {
                        listTextIncludePageIdNext.add(strPageIdNext);
                    }
                }
                dic.remove(strPageId);
                dic.put(strPageId, listTextIncludePageIdNext);
            }
        }
        return dic;
    }

    /**
     *
     * @param page
     * @param xmlPage
     * @param dictionaryTitle
     * @param dictionaryMacro
     * @param mapIncludePage
     * @return
     */
    public static String replaceAnchor(Page page, String xmlPage, Map<String, String> dictionaryTitle, Map<String, String> dictionaryMacro, Map<String, List<String>> mapIncludePage) throws Exception {
        try {
            if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
                return xmlPage;
            }

            xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            xmlPage = DocumentUtil.skipProcessChar(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression = ANCHOR_NODE;
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String eValue = eElement.getAttributeNode(ANCHOR_ATTRIBUTE).getValue();

                    // get ri:content-title
                    NodeList nodeChildList = nNode.getChildNodes();
                    String strPageTitle = "";
                    for (int j = 0; j < nodeChildList.getLength(); j++) {
                        Node nChildNode = nodeChildList.item(j);
                        if (nChildNode != null && nChildNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element eChildElement = (Element) nChildNode;
                            Attr attrSpace = eChildElement.getAttributeNode(SPACE_ATTRIBUTE);
                            if (attrSpace != null && !attrSpace.getValue().equals(page.getSpaceKey())) {
                                break;
                            }
                            Attr attrTitle = eChildElement.getAttributeNode(TITLE_ATTRIBUTE);
                            if (attrTitle != null) {
                                strPageTitle = attrTitle.getValue();
                                break;
                            }
                        }
                    }

                    String strPageId = "";
                    if (dictionaryTitle.containsKey(strPageTitle)) {
                        strPageId = dictionaryTitle.get(strPageTitle);
                    } else {
                        strPageId = page.getIdAsString();
                    }

                    String strKey = strPageId + "_" + eValue;
                    if (dictionaryMacro.containsKey(strKey)) {
                        String strNewValue = dictionaryMacro.get(strKey);
                        eElement.setAttribute(ANCHOR_ATTRIBUTE, strNewValue.replaceFirst(strPageId + "_", ""));
                    } else {
                        strKey = strPageId + "_" + removeAllSpace(eValue);
                        if (dictionaryMacro.containsKey(strKey)) {
                            String strNewValue = dictionaryMacro.get(strKey);
                            eElement.setAttribute(ANCHOR_ATTRIBUTE, strNewValue.replaceFirst(strPageId + "_", ""));
                        }
                    }
                    if (mapIncludePage.containsKey(strPageId)) {
                        List<String> listPage = mapIncludePage.get(strPageId);
                        for (String pageId : listPage) {
                            strKey = pageId + "_" + eValue;
                            if (dictionaryMacro.containsKey(strKey)) {
                                String strNewValue = dictionaryMacro.get(strKey);
                                eElement.setAttribute(ANCHOR_ATTRIBUTE, strNewValue.replaceFirst(pageId + "_", ""));
                            }
                        }
                    }
                }
            }
            xmlPage = convertXmlToString(document, false, true);
            return xmlPage;
        } catch (Exception e) {
            MemoryLogUtil.outputLog("EXCEPTION REPLACE ANCHOR PAGE : " + e.getMessage());
            return xmlPage;
        }
    }

    public static String replaceLink(String spaceKey, String xmlPage, List<Page> listPageSource, List<Page> listPageDes) throws Exception {
        String xmlPageRpl = replaceTitle(spaceKey, xmlPage, listPageSource, listPageDes);
        return xmlPageRpl;
    }

    public static String replaceLinkImageDirectory(String toSpaceKey, String fromSpaceKey, String xmlPage, List<Page> listPageSource, List<Page> listPageDes) throws Exception {
        xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ri:attachment", "ri:filename");
                if (!nodeList.isEmpty()) {
                    for (Element element : nodeList) {
                        List<Element> listRiPage = XMLUtils.getElementsByTagNameAndAttr(element, "ri:page", "ri:content-title");
                        if (!listRiPage.isEmpty()) {
                            Element eRiPage = listRiPage.get(0);
                            if (eRiPage.hasAttribute("ri:space-key")) {
                                continue;
                            }
                            eRiPage.setAttribute("ri:space-key", fromSpaceKey);
                        } else {
                            Element eRiPage = document.createElement("ri:page");
                            eRiPage.setAttribute("ri:content-title", Constants.IMAGE_DIRECTORY_PAGE);
                            eRiPage.setAttribute("ri:space-key", fromSpaceKey);
                            element.appendChild(eRiPage);
                        }
                    }
                }
            }

            String result = DocumentUtil.convertXmlToString(document, false, true);
            result = DocumentUtil.skipProcessChar(result, true);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Error converting to String", e);
        }
    }

    public static String replaceTitle(String spaceKey, String xmlPage, List<Page> listPageSource, List<Page> listPageDes) throws Exception {
        xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document != null) {
                NodeList allElementAcLink = document.getElementsByTagName(AC_LINK_VALUE);
                for (int i = allElementAcLink.getLength() - 1; i >= 0; i--) {
                    Element acLink = (Element) allElementAcLink.item(i);
                    if (acLink.hasAttribute(A_HREF_VALUE) == false) {
                        if (acLink.hasAttribute(ANCHOR_ATTRIBUTE) == false) {
                            Element kodtrTilte = XMLUtils.getFirstElementByTagName(acLink, AC_LINK_KODTR_TITLE);
                            if (kodtrTilte != null) {
                                String titleKodtr = kodtrTilte.getTextContent();
                                if (titleKodtr != null && acLink.hasAttribute(TITLE_ATTRIBUTE)) {
                                    acLink.setAttribute(TITLE_ATTRIBUTE, titleKodtr);
                                }
                                Element riPage = XMLUtils.getFirstElementByTagName(acLink, RI_PAGE_VALUE);
                                if (riPage != null) {
                                    riPage.setAttribute(TITLE_ATTRIBUTE, titleKodtr);
                                }
                                kodtrTilte.getParentNode().removeChild(kodtrTilte);
                            }
                        }
                        continue;
                    }
                    String href = acLink.getAttribute(A_HREF_VALUE);
                    Element kodtr = XMLUtils.getFirstElementByTagName(acLink, AC_LINK_KODTR);
                    if (kodtr != null) {
                        href = kodtr.getTextContent();
                        kodtr.getParentNode().removeChild(kodtr);
                    }
                    Element kodtrLink = XMLUtils.getFirstElementByTagName(acLink, AC_LINK_KODTR_LINK);
                    if (kodtrLink != null) {
                        href = kodtrLink.getTextContent();
                        kodtrLink.getParentNode().removeChild(kodtrLink);
                    }
                    String text = acLink.getTextContent();
                    Element aHref = document.createElement("a");
                    aHref.setAttribute("href", href);
                    aHref.setTextContent(text);
                    acLink.getParentNode().insertBefore(aHref, acLink);
                    acLink.getParentNode().removeChild(acLink);
                }

                Element root = document.getDocumentElement();
                List<Element> linkAnchor = XMLUtils.getElementsByTagNameAndAttr(root, AC_LINK_VALUE, ANCHOR_ATTRIBUTE);
                for (Element element : linkAnchor) {
                    Element kodtrAnchor = XMLUtils.getFirstElementByTagName(element, AC_LINK_KODTR_ANCHOR);
                    if (kodtrAnchor != null) {
                        String anchorKodtr = kodtrAnchor.getTextContent();
                        if (element.hasAttribute(ANCHOR_ATTRIBUTE)) {
                            element.setAttribute(ANCHOR_ATTRIBUTE, anchorKodtr);
                        }
                        kodtrAnchor.getParentNode().removeChild(kodtrAnchor);
                    }

                    Element kodtrTitle = XMLUtils.getFirstElementByTagName(element, AC_LINK_KODTR_TITLE);
                    if (kodtrTitle != null) {
                        String titleKodtr = kodtrTitle.getTextContent();
                        Element riPageTitle = XMLUtils.getFirstElementByTagNameAndAttr(element, RI_PAGE_VALUE, TITLE_ATTRIBUTE);
                        if (riPageTitle != null) {
                            riPageTitle.setAttribute(TITLE_ATTRIBUTE, titleKodtr);
                        }
                        kodtrTitle.getParentNode().removeChild(kodtrTitle);
                    }
                }

                NodeList allElement = document.getElementsByTagName("ri:page");
                for (int i = 0; i < allElement.getLength(); i++) {
                    Node riPage = allElement.item(i);
                    Node node = riPage.getAttributes().getNamedItem("ri:content-title");
                    //Check page of is translated to get page title new set to link
                    Node nodeSpaceKey = riPage.getAttributes().getNamedItem("ri:space-key");
                    if (nodeSpaceKey != null && nodeSpaceKey.getNodeValue().equals(spaceKey)) {
                        if (spaceKey != null) {
                            nodeSpaceKey.setNodeValue(spaceKey);
                        }
                    }

                    String pageTitle = node.getNodeValue();
                    for (int j = 0; j < listPageSource.size(); j++) {
                        if (listPageSource.get(j).getDisplayTitle().equals(pageTitle)) {
                            if (listPageDes.get(j) != null) {
                                if (listPageDes.get(j).getDisplayTitle() != null) {
                                    pageTitle = listPageDes.get(j).getDisplayTitle();
                                }
                            }
                            break;
                        }
                    }

                    if (nodeSpaceKey == null || (nodeSpaceKey != null && nodeSpaceKey.getNodeValue().equals(spaceKey))) {
                        node.setNodeValue(pageTitle);
                    }
                }
            }

            String result = DocumentUtil.convertXmlToString(document, false, true);
            result = DocumentUtil.skipProcessChar(result, true);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Error converting to String", e);
        }
    }

    public static void replaceDrawIO(Map<String, Page> mapPagesDrawIO) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        for (Map.Entry<String, Page> entry : mapPagesDrawIO.entrySet()) {
            String fromPageId = entry.getKey();
            Page toPage = entry.getValue();
            if (toPage == null) {
                continue;
            }

            String xmlPage = ImportExportUtil.exportXmlString(toPage);
            if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
                continue;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                continue;
            }

            XPath xPath = XPathFactory.newInstance().newXPath();
            xPath.setNamespaceContext(getNSContext());

            String expression = DRAWIO_PARAMETER;
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                if (nNode.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }

                Element element = (Element) nNode;
                String fileName = element.getTextContent();
                if (CommonFunction.isNullOrEmpty(xmlPage)) {
                    continue;
                }

                Attachment attachment = attachmentManager.getAttachmentDao().getLatestAttachment(toPage, fileName);
                if (attachment == null) {
                    return;
                }

                replaceLinkDrawIo(attachment, mapPagesDrawIO);
            }
        }
    }

    private static void replaceLinkDrawIo(Attachment attachment, Map<String, Page> mapPagesDrawIO) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        if (attachment == null) {
            return;
        }

        InputStream in = attachmentManager.getAttachmentData(attachment);
        String drowIoData = CommonFunction.convertInputStreamToString(in);
        if (drowIoData.isEmpty()) {
            return;
        }

        Document docDrawIo = DocumentUtil.convertStringToDocument(drowIoData, false);
        NodeList nodes = docDrawIo.getElementsByTagName("diagram");
        if (nodes.getLength() <= 0) {
            return;
        }

        Element diagram = (Element) nodes.item(0);
        String diagramData = diagram.getTextContent();
        diagramData = MacroDrawIoProcessor.decodeDrawIO(diagramData);
        Document docDiagram = DocumentUtil.convertStringToDocument(diagramData, false);
        nodes = docDiagram.getElementsByTagName("mxCell");
        for (int i = 0; i < nodes.getLength(); i++) {
            if (nodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            Element mxCell = (Element) nodes.item(i);
            if (!mxCell.hasAttribute("style")) {
                continue;
            }

            String styleString = mxCell.getAttribute("style");
            if (!styleString.contains("image=")) {
                continue;
            }

            String url = styleString.substring(styleString.indexOf("image=") + 6);
            if (url.contains(";")) {
                url = url.substring(0, url.indexOf(";"));
            }

            Maybe<Attachment> fromAtts = attachmentManager.getAttachmentForDownloadPath(url);
            if ((fromAtts == null || fromAtts.isEmpty()) && !url.contains("?")) {
                fromAtts = attachmentManager.getAttachmentForDownloadPath(url + "?");
            }

            if (fromAtts == null || fromAtts.isEmpty()) {
                continue;
            }

            Attachment fromAtt = fromAtts.get();
            if (fromAtt.getContainer() == null) {
                continue;
            }

            Page fromPageAtt = (Page) fromAtt.getContainer();
            if (!mapPagesDrawIO.containsKey(fromPageAtt.getIdAsString())) {
                continue;
            }

            Page toPageAtt = mapPagesDrawIO.get(fromPageAtt.getIdAsString());
            if (toPageAtt == null) {
                continue;
            }
            Attachment toAtt = attachmentManager.getAttachmentDao().getLatestAttachment(toPageAtt, fromAtt.getTitle());
            if (toAtt == null) {
                continue;
            }

            diagramData = diagramData.replace("image=" + url, "image=" + toAtt.getDownloadPathWithoutVersion());
        }

        diagramData = MacroDrawIoProcessor.encodeDrawIO(diagramData);
        drowIoData = drowIoData.replace(diagram.getTextContent(), diagramData);
        byte[] dataByte = drowIoData.getBytes("utf-8");
        InputStream is = new ByteArrayInputStream(dataByte);
        attachment.setFileSize(dataByte.length);
        attachmentManager.getAttachmentDao().replaceAttachmentData(attachment, is);
    }

    public static String replacePageOutgoingLinks(String toSpaceKey, Page toPage, String xmlPage, String fromLangKey, String toLanguageKey) throws Exception {
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        try {
            xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> lstMacro = getIncludeMacro(document);
                int index = 0;
                for (Element element : lstMacro) {
                    index++;
                    Node attrSpaceKey = element.getAttributes().getNamedItem(SPACE_ATTRIBUTE);
                    if (attrSpaceKey == null) {
                        continue;
                    }

                    String fromSpaceKey = attrSpaceKey.getNodeValue();
                    Node attrContentTitle = element.getAttributes().getNamedItem(TITLE_ATTRIBUTE);
                    if (attrContentTitle == null) {
                        continue;
                    }

                    String fromTitle = attrContentTitle.getNodeValue();

                    Page fromPage = pageManager.getPage(fromSpaceKey, fromTitle);
                    if (fromPage == null) {
                        String pageSaveId = toPage.getProperties().getStringProperty(fromSpaceKey + ":" + index);
                        if (pageSaveId == null) {
                            continue;
                        }

                        fromPage = pageManager.getPage(Long.valueOf(pageSaveId));
                        if (fromPage == null) {
                            continue;
                        }
                    }

                    if (toSpaceKey.equals(fromSpaceKey)) {
                        continue;
                    }

                    String pageIdReplace = DocumentUtil.getPageInclude(fromSpaceKey, fromPage, fromLangKey, toLanguageKey);
                    if (CommonFunction.isNullOrEmpty(pageIdReplace)) {
                        continue;
                    }

                    Page replacePage = pageManager.getPage(Long.valueOf(pageIdReplace));
                    if (replacePage == null) {
                        continue;
                    }

                    attrSpaceKey.setNodeValue(replacePage.getSpaceKey());
                    attrContentTitle.setNodeValue(replacePage.getDisplayTitle());
                    toPage.getProperties().setStringProperty(fromSpaceKey + ":" + index, pageIdReplace);
                }
                replaceLinkToPageTarget(root, toSpaceKey, fromLangKey, toLanguageKey);
            }

            String result = DocumentUtil.convertXmlToString(document, false, true);
            result = DocumentUtil.skipProcessChar(result, true);
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Error converting to String", e);
        }
    }

    public static void replaceLinkToPageTarget(Element root, String rootSpaceKey, String fromLangKey, String toLangKey) throws Exception {
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        try {
            List<Element> links = XMLUtils.getElementByTagName(root, AC_LINK_VALUE);
            for (Element link : links) {
                Element riPage = XMLUtils.getFirstElementByTagNameAndAttr(link, RI_PAGE_VALUE, TITLE_ATTRIBUTE);
                if (riPage == null) {
                    continue;
                }
                String title = riPage.getAttribute(TITLE_ATTRIBUTE);
                if (!riPage.hasAttribute(SPACE_ATTRIBUTE)) {
                    continue;
                }
                String spaceKey = riPage.getAttribute(SPACE_ATTRIBUTE);
                if (spaceKey.equals(rootSpaceKey)) {
                    continue;
                }
                Page fromPage = pageManager.getPage(spaceKey, title);
                if (fromPage == null) {
                    continue;
                }
                String pageIdReplace = DocumentUtil.getPageInclude(spaceKey, fromPage, fromLangKey, toLangKey);
                if (CommonFunction.isNullOrEmpty(pageIdReplace)) {
                    continue;
                }
                Page replacePage = pageManager.getPage(Long.valueOf(pageIdReplace));
                if (replacePage == null) {
                    continue;
                }
                riPage.setAttribute(TITLE_ATTRIBUTE, replacePage.getTitle());
                riPage.setAttribute(SPACE_ATTRIBUTE, spaceKey);
            }
        } catch (Exception e) {
            throw new RuntimeException("Error Replace Link To PageTarget", e);
        }
    }
    
    public static String removeJiraComment(String xmlPage) throws Exception {
        if (CommonFunction.isNullOrEmpty(xmlPage) == true) {
            return xmlPage;
        }

        try {
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }
            Element root = document.getDocumentElement();
            List<Element> elements = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", XML_AC_NAME_JIRA);
            for (Element element : elements) {
                element.getParentNode().removeChild(element);
            }
            return DocumentUtil.convertXmlToString(document, false, true);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(XMLUtils.class, ex);
            return xmlPage;
        }
    }

    public static List<Element> getIncludeMacro(Document doc) throws Exception {
        NodeList nodeMacro = doc.getElementsByTagName(AC_STRUC_MACRO_VALUE);
        List<Element> result = new ArrayList<Element>();
        for (int i = 0; i < nodeMacro.getLength(); i++) {
            Node nNode = nodeMacro.item(i);
            if (nNode.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            Element element = (Element) nNode;
            if (!element.hasAttribute(AC_NAME_VALUE)) {
                continue;
            }

            String nameValue = element.getAttribute(AC_NAME_VALUE);
            if (!ATTR_INCLUDE_VALUE.equals(nameValue)) {
                continue;
            }

            NodeList nodeLink = element.getElementsByTagName(AC_LINK_VALUE);
            if (nodeLink.getLength() == 0) {
                continue;
            }

            element = (Element) nodeLink.item(0);
            NodeList nodeRiPage = element.getElementsByTagName(RI_PAGE_VALUE);
            if (nodeRiPage.getLength() == 0) {
                continue;
            }

            element = (Element) nodeRiPage.item(0);
            result.add(element);
        }

        return result;
    }

    public static String getPageInclude(String fromSpaceKey, Page fromPage, String fromLangKey, String toLangKey) throws Exception {
        TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
        TranslationJob translationJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, fromPage.getId());
        if (translationJob == null) {
            return null;
        }
        Long toPageId = translationJob.getToPageId();
        if (toPageId == null) {
            return null;
        }
        return String.valueOf(toPageId);
    }

    public static void replacePageIncomingLinks(String spaceKey, Page fromPage, Page toPage, String fromLangKey, String toLangKey) throws Exception {
        MemoryLogUtil.outputLog("BEGIN REPLACE PAGE INCOME LINK : from page " + fromPage.getIdAsString() + " to page " + toPage.getIdAsString());
        LinkManager linkManager = TranslationMemoryToolFactory.getLinkManager();
        PermissionManager permissionManager = TranslationMemoryToolFactory.getPermissionManager();
        PageIncomingLinks pil = new PageIncomingLinks(linkManager, permissionManager);
        ConfluenceUser remoteUser = AuthenticatedUserThreadLocal.get();
        List<OutgoingLink> listOutgoingLink = pil.getIncomingLinks(fromPage, remoteUser);
        String titleFrom = fromPage.getTitle();
        String titleTo = toPage.getTitle();
        TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        for (OutgoingLink outgoingLink : listOutgoingLink) {
            if (!(outgoingLink.getSourceContent() instanceof Page)) {
                continue;
            }

            Page pageFrom = (Page) outgoingLink.getSourceContent();
            if (pageFrom == null) {
                continue;
            }

            TranslationJob translationJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, pageFrom.getId());
            if (translationJob == null) {
                continue;
            }

            Long pageLinkId = translationJob.getToPageId();
            if (pageLinkId == null) {
                continue;
            }
            Page page = pageManager.getPage(pageLinkId);
            String xmlString = ImportExportUtil.exportXmlString(page);
            xmlString = DocumentUtil.skipProcessChar(xmlString, false);

            try {
                Document document = DocumentUtil.convertStringToDocument(xmlString, true);
                if (document != null) {
                    Element root = document.getDocumentElement();

                    List<Element> acLinks = XMLUtils.getElementByTagName(root, "ac:link");
                    for (Element acLink : acLinks) {
                        Element riPage = XMLUtils.getFirstElementByTagNameAndAttr(acLink, "ri:page", "ri:content-title");
                        if (riPage == null) {
                            continue;
                        }
                        String contentTitle = riPage.getAttribute("ri:content-title");
                        if (!(titleFrom).equals(contentTitle)) {
                            continue;
                        }

                        if (riPage.hasAttribute("ri:space-key")) {
                            String spaceKeyLink = riPage.getAttribute("ri:space-key");
                            if (!spaceKeyLink.equals(spaceKey)) {
                                continue;
                            }
                        }
                        riPage.setAttribute("ri:content-title", titleTo);
                    }

                    xmlString = DocumentUtil.convertXmlToString(document, false, true);
                    xmlString = DocumentUtil.skipProcessChar(xmlString, true);
                    ImportExportUtil.importXmlString(page, xmlString, false, true);
                }
            } catch (Exception ex) {
                throw new RuntimeException("Replace Faild", ex);
            }
        }
    }

    public static void replacePageAnchorIncomingLinks(String spaceKey, Page fromPage, Page toPage, String fromLangKey, String toLangKey, Map<String, String> maplinkAnchor, Map<String, String> mapLinkHeader) throws Exception {
        MemoryLogUtil.outputLog("BEGIN REPLACE ANCHOR INCOME LINK : from page " + fromPage.getIdAsString() + " to page " + toPage.getIdAsString());
        LinkManager linkManager = TranslationMemoryToolFactory.getLinkManager();
        PermissionManager permissionManager = TranslationMemoryToolFactory.getPermissionManager();
        PageIncomingLinks pil = new PageIncomingLinks(linkManager, permissionManager);
        ConfluenceUser remoteUser = AuthenticatedUserThreadLocal.get();
        List<OutgoingLink> listOutgoingLink = pil.getIncomingLinks(fromPage, remoteUser);
        String titleFrom = fromPage.getTitle();
        TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        for (OutgoingLink outgoingLink : listOutgoingLink) {
            if (!(outgoingLink.getSourceContent() instanceof Page)) {
                continue;
            }

            Page pageFrom = (Page) outgoingLink.getSourceContent();
            if (pageFrom == null) {
                continue;
            }

            TranslationJob translationJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, pageFrom.getId());
            if (translationJob == null) {
                continue;
            }

            Long pageLinkId = translationJob.getToPageId();
            if (pageLinkId == null) {
                continue;
            }
            Page page = pageManager.getPage(pageLinkId);
            String xmlString = ImportExportUtil.exportXmlString(page);
            xmlString = DocumentUtil.skipProcessChar(xmlString, false);

            try {
                Document document = DocumentUtil.convertStringToDocument(xmlString, true);
                if (document != null) {
                    Element root = document.getDocumentElement();
                    List<Element> linkAnchor = XMLUtils.getElementsByTagNameAndAttr(root, AC_LINK_VALUE, ANCHOR_ATTRIBUTE);
                    for (Element element : linkAnchor) {
                        Element riPage = XMLUtils.getFirstElementByTagNameAndAttr(element, RI_PAGE_VALUE, TITLE_ATTRIBUTE, titleFrom);
                        if (riPage == null) {
                            continue;
                        }
                        if (riPage.hasAttribute("ri:space-key")) {
                            String spaceKeyLink = riPage.getAttribute("ri:space-key");
                            if (!spaceKeyLink.equals(spaceKey)) {
                                continue;
                            }
                        }

                        String anchor = element.getAttribute(ANCHOR_ATTRIBUTE);
                        if (maplinkAnchor.containsKey(anchor)) {
                            element.setAttribute(ANCHOR_ATTRIBUTE, maplinkAnchor.get(anchor));
                        }
                        if (mapLinkHeader.containsKey(anchor)) {
                            element.setAttribute(ANCHOR_ATTRIBUTE, mapLinkHeader.get(anchor));
                        }
                    }
                    List<Element> anchors = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "anchor");
                    for (Element anchor : anchors) {
                        Element e = XMLUtils.getFirstElementByTagName(anchor, "ac:parameter");
                        if (e == null) {
                            continue;
                        }
                        if (!e.hasAttribute(TEXT_ANCHOR_NOTRANS)) {
                            continue;
                        }
                        e.removeAttribute(TEXT_ANCHOR_NOTRANS);
                    }
                    xmlString = DocumentUtil.convertXmlToString(document, false, true);
                    xmlString = DocumentUtil.skipProcessChar(xmlString, true);
                    ImportExportUtil.importXmlString(page, xmlString, false, true);
                }
            } catch (Exception ex) {
                throw new RuntimeException("Replace Anchor Faild", ex);
            }
        }
    }

    public static String replaceAttributeAnchor(String spaceKey, Page fromPage, String xmlString, String toLangKey, Map<String, String> maplinkAnchor, Map<String, String> mapLinkHeader) throws Exception {
        String titleFrom = fromPage.getTitle();
        try {
            Document document = DocumentUtil.convertStringToDocument(xmlString, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> linkAnchor = XMLUtils.getElementsByTagNameAndAttr(root, AC_LINK_VALUE, ANCHOR_ATTRIBUTE);
                for (Element element : linkAnchor) {   
                    Element riPage = XMLUtils.getFirstElementByTagNameAndAttr(element, RI_PAGE_VALUE, TITLE_ATTRIBUTE, titleFrom);
                    if (riPage != null) {
                        if (riPage.hasAttribute("ri:space-key")) {
                            String spaceKeyLink = riPage.getAttribute("ri:space-key");
                            if (!spaceKeyLink.equals(spaceKey)) {
                                continue;
                            }
                        }
                    }

                    String anchor = element.getAttribute(ANCHOR_ATTRIBUTE);
                    if (maplinkAnchor.containsKey(anchor)) {
                        element.setAttribute(ANCHOR_ATTRIBUTE, maplinkAnchor.get(anchor));
                    }
                    if (mapLinkHeader.containsKey(anchor)) {
                        element.setAttribute(ANCHOR_ATTRIBUTE, mapLinkHeader.get(anchor));
                    }
                }
                List<Element> anchors = XMLUtils.getElementsByTagNameAndAttr(root, AC_MACRO_VALUE, AC_NAME_VALUE, "anchor");
                for (Element anchor : anchors) {
                    Element e = XMLUtils.getFirstElementByTagName(anchor, "ac:parameter");
                    if (e == null) {
                        continue;
                    }
                    if (!e.hasAttribute(TEXT_ANCHOR_NOTRANS)) {
                        continue;
                    }
                    e.removeAttribute(TEXT_ANCHOR_NOTRANS);
                }
                xmlString = DocumentUtil.convertXmlToString(document, false, true);
            }
            return xmlString;
        } catch (Exception ex) {
            MemoryLogUtil.outputLog("EXCEPTION REPLACE ANCHOR : " + ex.getMessage());
            return xmlString;
        }
    }

    public static String replaceAttributeIncludeMacro(String xmlData) throws Exception {
        MemoryLogUtil.outputLog("BEGIN REPLACE INCLUDE ATTRIBUTE");
        try {
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlData, false);
            if (document == null) {
                return xmlData;
            }

            Element root = document.getDocumentElement();
            List<Element> macroIncludes = XMLUtils.getElementsByTagNameAndAttr(root, AC_STRUC_MACRO_VALUE, AC_NAME_VALUE, ATTR_INCLUDE_VALUE);
            for (Element macroInclude : macroIncludes) {
                Element acLink = XMLUtils.getFirstElementByTagName(macroInclude, AC_LINK_VALUE);
                if (acLink == null) {
                    continue;
                }
                Element riPage = XMLUtils.getFirstElementByTagName(acLink, RI_PAGE_VALUE);
                if (riPage == null) {
                    continue;
                }
                
                Element kodtrIncludeTitle = XMLUtils.getFirstElementByTagNameAndAttr(acLink, AC_LINK_KODTR_INCLUDE, ATTR_KODTR_INCLUDE_TITLE);
                if(kodtrIncludeTitle == null){
                    continue;
                }
                String titleKodtr = kodtrIncludeTitle.getTextContent();
                if(acLink.hasAttribute(TITLE_ATTRIBUTE)){
                    acLink.setAttribute(TITLE_ATTRIBUTE, titleKodtr);
                }
                riPage.setAttribute(TITLE_ATTRIBUTE, titleKodtr);
                kodtrIncludeTitle.getParentNode().removeChild(kodtrIncludeTitle);
                Element kodtrIncludeSpace = XMLUtils.getFirstElementByTagNameAndAttr(acLink, AC_LINK_KODTR_INCLUDE, ATTR_KODTR_INCLUDE_SPACE_KEY);
                if(kodtrIncludeSpace == null){
                    continue;
                }
                String spaceKeyInclude = kodtrIncludeSpace.getTextContent();
                riPage.setAttribute(SPACE_ATTRIBUTE, spaceKeyInclude);
                kodtrIncludeSpace.getParentNode().removeChild(kodtrIncludeSpace);
            }
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            MemoryLogUtil.outputLog("END REPLACE INCLUDE ATTRIBUTE");
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }
    /**
     *
     * @param expression
     * @param xmlPage
     * @return
     */
    public static List<String> getListTextContentOfElement(String expression, String xmlPage, boolean hasRemoveSpace) throws Exception {
        List<String> listTextContentOfElement = new ArrayList<String>();
        if (xmlPage.isEmpty()) {
            return listTextContentOfElement;
        }

        xmlPage = DocumentUtil.skipProcessChar(xmlPage, false);
        Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
        xmlPage = DocumentUtil.skipProcessChar(xmlPage, true);
        if (document == null) {
            return listTextContentOfElement;
        }

        XPath xPath = XPathFactory.newInstance().newXPath();
        xPath.setNamespaceContext(getNSContext());

        NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String eValue = eElement.getTextContent();
                if (hasRemoveSpace == true) {
                    listTextContentOfElement.add(removeAllSpace(eValue));
                } else {
                    listTextContentOfElement.add(eValue);
                }
            } else if (nNode.getNodeType() == Node.ATTRIBUTE_NODE) {
                listTextContentOfElement.add(nNode.getNodeValue());
            }
        }
        return listTextContentOfElement;
    }

    /**
     *
     * @param String
     * @return String
     */
    public static String removeAllSpace(String str) {
        str = str.replaceAll(" ", "");
        str = str.replaceAll(" ", "");
        str = str.replaceAll("　", "");
        return str;
    }

    public static String removeGengoMacro(String xmlPage, String fromLanguage, String toLanguage) throws Exception {
        if (xmlPage.isEmpty()) {
            return xmlPage;
        }

        Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
        if (document == null) {
            return xmlPage;
        }

        List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(document.getDocumentElement(), "ac:structured-macro", "ac:name", "wikiwork-base-gengo-macro");
        List<Node> nodeNoRemoves = new ArrayList<Node>();
        for (int i = nodeList.size() - 1; i >= 0; i--) {
            Element eParent = nodeList.get(i);
            Element eElement = XMLUtils.getFirstElementByTagNameAndAttr(eParent, "ac:parameter", "ac:name", "gengo");
            if (eElement == null) {
                continue;
            }

            String valueLanguage = eElement.getTextContent();
            List<Element> nodes = XMLUtils.getElementsByTagNameAndAttr(eParent, "ac:structured-macro", "ac:name", "wikiwork-gengo-macro");
            if (valueLanguage.equalsIgnoreCase(toLanguage)) {
                for (int j = nodes.size() - 1; j >= 0; j--) {
                    nodeNoRemoves.add(nodes.get(j));
                }

                continue;
            }

            for (int j = 0; j < nodes.size(); j++) {
                eParent.getParentNode().insertBefore(nodes.get(j), eParent);
            }

            eParent.getParentNode().removeChild(eParent);
        }

        nodeList = XMLUtils.getElementsByTagNameAndAttr(document.getDocumentElement(), "ac:structured-macro", "ac:name", "wikiwork-gengo-macro");
        for (int i = 0; i < nodeList.size(); i++) {
            Element eParent = nodeList.get(i);
            if (nodeNoRemoves.contains(eParent)) {
                continue;
            }

            Element eElement = XMLUtils.getFirstElementByTagNameAndAttr(eParent, "ac:parameter", "ac:name", "gengo");
            if (eElement == null) {
                continue;
            }

            String valueLanguages = eElement.getTextContent();
            String[] valuesLanguage = valueLanguages.split(",");
            boolean flagHasGengo = false;
            for (String valueLanguage : valuesLanguage) {
                if (valueLanguage.equalsIgnoreCase(toLanguage)) {
                    flagHasGengo = true;
                    break;
                }
            }
            Element elementGengoIgai = XMLUtils.getFirstElementByTagNameAndAttr(eParent, "ac:parameter", "ac:name", "gengoIgai");
            String gengoIgai = "";
            if (elementGengoIgai != null) {
                gengoIgai = elementGengoIgai.getTextContent();
            } else {
                gengoIgai = "false";
            }
            if (gengoIgai.equalsIgnoreCase("true")) {
                flagHasGengo = !flagHasGengo;
            }
            if (flagHasGengo) {
                continue;
            }
            eParent.getParentNode().removeChild(eParent);
        }

        xmlPage = convertXmlToString(document, false, true);
        return xmlPage;
    }

    public static boolean checkSpaceScrollTranslation(String xmlPage) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return false;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return false;
            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "sv-translation");
            return !nodeList.isEmpty();
        } catch (Exception e) {
            MemoryLogUtil.outputLog("check is space scroll translation: " + e.getMessage());
            return false;
        }
    }

    public static String getBaseLangFromScrollTranslation(String spaceKey) throws Exception {
        try {
            SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            Space space = spaceManager.getSpace(spaceKey);
            if (space == null) {
                return "";
            }
            List<Page> pages = pageManager.getPages(space, true);
            Map<String, String> supportedLanguages = getLanguageSupport();
            for (Page page : pages) {
                String xmlPage = page.getBodyAsString();
                Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
                if (document == null) {
                    continue;
                }
                Element root = document.getDocumentElement();
                List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "sv-translation");
                if (!nodeList.isEmpty()) {
                    for (Element element : nodeList) {
                        List<Element> elementLanguage = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "language");
                        if (!elementLanguage.isEmpty()) {
                            String key = elementLanguage.get(0).getTextContent();
                            key = key.toLowerCase();
                            String key_name = key + "$$$" + supportedLanguages.get(key);
                            return key_name;
                        }
                    }
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("Get Base Language From ScrollTranslation: " + e.getMessage());
            return "";
        }
        return "";
    }

    public static Map<String, String> getTagetLangsFromScrollTranslation(String xmlPage) throws Exception {
        Map<String, String> lstLangs = new HashMap<>();
        try {
            if (xmlPage.isEmpty()) {
                return lstLangs;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return lstLangs;
            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "sv-translation");
            if (nodeList.isEmpty()) {
                return lstLangs;
            } else {
                Map<String, String> supportedLanguages = getLanguageSupport();
                int i = 0;
                for (Element element : nodeList) {
                    if (i == 0) {
                        i++;
                        continue;
                    }
                    List<Element> elementLanguage = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "language");
                    String keyLang = elementLanguage.get(0).getTextContent();
                    if (!elementLanguage.isEmpty()) {
                        keyLang = keyLang.toLowerCase();
                        lstLangs.put(keyLang, supportedLanguages.get(keyLang));
                    }
                    i++;
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("getTagetLangsFromScrollTranslation: " + e.getMessage());
            return lstLangs;
        }
        return lstLangs;
    }

    public static String[] getBodyScrollTranslation(String xmlPage, String languageKey) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return new String[]{"false", xmlPage};

            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return new String[]{"false", xmlPage};

            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "sv-translation");
            List<Element> nodeContentList = XMLUtils.getElementByTagName(root, "content");
            if (nodeList.isEmpty()) {
                return new String[]{"false", xmlPage};

            } else {
                boolean isChange = false;
                for (Element element : nodeList) {
                    List<Element> elementLanguage = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "language");
                    if (!elementLanguage.isEmpty() && elementLanguage.get(0).getTextContent().equalsIgnoreCase(languageKey) && !nodeContentList.isEmpty()) {
                        isChange = true;
                        List<Element> eBody = XMLUtils.getElementByTagName(element, "ac:rich-text-body");
                        if (!eBody.isEmpty()) {
                            xmlPage = DocumentUtil.convertXmlToString(eBody.get(0), false, true);
                            nodeContentList.get(0).setTextContent("");
                            nodeContentList.get(0).appendChild(eBody.get(0));
                            break;
                        } else {
                            nodeContentList.get(0).setTextContent("");
                            break;
                        }
                    }
                }
                if (isChange) {
                    xmlPage = convertXmlToString(document, false, true);
                    xmlPage = removeTag(xmlPage, "<content[^>]+><ac:rich-text-body>");
                    xmlPage = xmlPage.replace("</ac:rich-text-body></content>", "</content>");
                    return new String[]{"true", xmlPage};
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("getBodyScrollTranslation: " + e.getMessage());
            return new String[]{"false", xmlPage};

        }
        return new String[]{"false", xmlPage};

    }

    public static String[] getBodyBaseScrollTranslation(String xmlPage, String languageKey) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return new String[]{"false", xmlPage};
            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return new String[]{"false", xmlPage};
            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "sv-translation");
            if (nodeList.isEmpty()) {
                return new String[]{"false", xmlPage};
            } else {
                for (Element element : nodeList) {
                    List<Element> elementLanguage = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "language");
                    if (!elementLanguage.isEmpty() && elementLanguage.get(0).getTextContent().equalsIgnoreCase(languageKey)) {
                        List<Element> eBody = XMLUtils.getElementByTagName(element, "ac:rich-text-body");
                        if (!eBody.isEmpty()) {
                            xmlPage = DocumentUtil.convertXmlToString(eBody.get(0), false, true);
                            xmlPage = xmlPage.substring(19, xmlPage.length() - 20);
                            return new String[]{"true", xmlPage};
                        } else {
                            return new String[]{"true", ""};
                        }
                    }
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("getBodyScrollTranslation: " + e.getMessage());
            return new String[]{"false", xmlPage};
        }
        return new String[]{"false", xmlPage};
    }

    public static String removeTag(String targetBody, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(targetBody);
        List<String> lists = new ArrayList<String>();
        while (matcher.find()) {
            String str = matcher.group();
            lists.add(str);
        }
        for (String listStr : lists) {
            targetBody = targetBody.replace(listStr, "<content xmlns:atlassian-content=\"http://atlassian.com/content\" xmlns:ac=\"http://atlassian.com/content\" xmlns:ri=\"http://atlassian.com/resource/identifier\" xmlns:atlassian-template=\"http://atlassian.com/template\" xmlns:at=\"http://atlassian.com/template\" xmlns=\"http://www.w3.org/1999/xhtml\" >");
        }
        return targetBody;
    }

    public static String convertLinkImageInHomePage(String xmlPage, String baseSpaceKey) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return xmlPage;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ri:attachment", "ri:filename");
            if (nodeList.isEmpty()) {
                return xmlPage;
            } else {
                boolean isReplace = false;
                for (Element element : nodeList) {
                    List<Element> listRiPage = XMLUtils.getElementsByTagNameAndAttr(element, "ri:page", "ri:content-title");
                    if (!listRiPage.isEmpty()) {
                        Element eRiPage = listRiPage.get(0);
                        eRiPage.setAttribute("ri:content-title", Constants.IMAGE_DIRECTORY_PAGE);
                        //eRiPage.setAttribute("ri:space-key", baseSpaceKey);
                        isReplace = true;
                    }
                }
                if (isReplace) {
                    xmlPage = convertXmlToString(document, false, true);
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("convertLinkImageInHomePage: " + e.getMessage());
            return xmlPage;
        }
        return xmlPage;

    }
 
    public static String convertEmojiToHtmlHex(String xmlData) throws Exception {
        StringBuilder buffer = new StringBuilder();
        String str = xmlData;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (isEmoji(ch)) {
                char ch2 = str.charAt(i + 1);
                int cp = Character.toCodePoint(ch, ch2);
                if (cp < 128) {
                    buffer.append((char) cp);
                } else {
                    buffer.append("<tagemoji emoji=\"").append(cp).append("\"/>");
                }
                i++;
            } else {
                buffer.append(ch);
            }
        }
        return buffer.toString();
    }

    public static String convertTagEmoji(String xmlData, boolean isSync) throws Exception {
        String result = xmlData;
        String regex = "<tagemoji[^>]+emoji\\s*=\\s*['\\\"]([^'\\\"]+)['\\\"][^>]*>";
        if (isSync == true) {
            regex = "<tagemoji[^>]*emoji=\"[^\\\"](.+?)[^\\\"]\">(.*?)<\\/tagemoji>";
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher match = pattern.matcher(xmlData);
        while (match.find()) {
            String newTag = match.group();
            newTag = newTag.replace("<tagemoji emoji=\"", "");
            if (isSync == true) {
                newTag = newTag.replace("</tagemoji>", "");
                newTag = newTag.replace("\">", "").replace("\" >", "");
            } else {
                newTag = newTag.replace("\"/>", "").replace("\" />", "");
            }
            char[] chars = Character.toChars(Integer.parseInt(newTag));
            byte[] converttoBytes = new String(chars).getBytes("UTF-16");
            String str = new String(converttoBytes, "UTF-16");
            result = result.replace(match.group(), str);
        }
        return result;
    }

    public static boolean isEmoji(char c) {
        return Character.UnicodeBlock.of(c) == Character.UnicodeBlock.HIGH_SURROGATES;
    }

    public static String convertLinkPage(String titlePage, String xmlData) throws Exception {
        try {
            MemoryLogUtil.outputLog("BEGIN CONVERT LINK PAGE");
            if (CommonFunction.isNullOrEmpty(xmlData) == true) {
                return xmlData;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlData, false);
            if (document == null) {
                return xmlData;
            }

            Element root = document.getDocumentElement();
            List<Element> acLinks = XMLUtils.getElementByTagName(root, AC_LINK_VALUE);
            for (Element acLink : acLinks) {
                Element acPlainText = XMLUtils.getFirstElementByTagName(acLink, AC_PLAIN_TEXT_LINK);
                if (acPlainText != null) {
                    continue;
                }
                Element acImage = XMLUtils.getFirstElementByTagName(acLink, "ri:attachment");
                if (acImage != null) {
                    if (acImage.hasAttribute("ri:filename") == false) {
                        continue;
                    }
                    String fileName = acImage.getAttribute("ri:filename");
                    CDATASection cDataFilename = document.createCDATASection(fileName);
                    acPlainText = document.createElement(AC_PLAIN_TEXT_LINK);
                    acPlainText.appendChild(cDataFilename);
                    acLink.appendChild(acPlainText);
                    continue;
                }
                NodeList childNodes = acLink.getChildNodes();
                if (childNodes.getLength() == 0) {
                    Element newRiPage = document.createElement(RI_PAGE_VALUE);
                    newRiPage.setAttribute("ri:content-title", titlePage);
                    acLink.appendChild(newRiPage);
                }
                acPlainText = document.createElement(AC_PLAIN_TEXT_LINK);
                Element riPage = XMLUtils.getFirstElementByTagName(acLink, RI_PAGE_VALUE);
                if (riPage == null) {
                    continue;
                }

                String title = riPage.getAttribute("ri:content-title");
                if (title == null || title.isEmpty()) {
                    continue;
                }
                MemoryLogUtil.outputLog("ADD CDATA : " + title);
                CDATASection cData = document.createCDATASection(title);
                acPlainText.appendChild(cData);
                acLink.appendChild(acPlainText);
            }
            xmlData = DocumentUtil.convertXmlToString(document, false, true);
            MemoryLogUtil.outputLog("END CONVERT LINK PAGE");
            return xmlData;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(DocumentUtil.class, e);
            return xmlData;
        }
    }

    public static String convertLinkImageInHomePage(String xmlPage, String titleHomePage, String baseSpaceKey, boolean isBaseLang) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return xmlPage;
            }
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }
            Element root = document.getDocumentElement();
            List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ri:attachment", "ri:filename");
            if (nodeList.isEmpty()) {
                return xmlPage;
            } else {
                boolean isReplace = false;
                for (Element element : nodeList) {
                    List<Element> listRiPage = XMLUtils.getElementsByTagNameAndAttr(element, "ri:page", "ri:content-title");
                    if (!listRiPage.isEmpty()) {
                        Element eRiPage = listRiPage.get(0);
                        eRiPage.setAttribute("ri:content-title", titleHomePage);
                        if (isBaseLang == false) {
                            eRiPage.setAttribute("ri:space-key", baseSpaceKey);
                        } else {
                            if (eRiPage.hasAttribute("ri:space-key")) {
                                String eRiSpaceKey = eRiPage.getAttribute("ri:space-key");
                                if (eRiSpaceKey.equals(baseSpaceKey)) {
                                    eRiPage.removeAttribute("ri:space-key");
                                }
                            }
                        }
                        isReplace = true;
                    }
                }
                if (isReplace) {
                    xmlPage = convertXmlToString(document, false, true);
                }
            }
        } catch (Exception e) {
            MemoryLogUtil.outputLog("convertLinkImageInHomePage: " + e.getMessage());
            return xmlPage;
        }
        return xmlPage;

    }

//    public static String convertMxliffToXml(String xmlPage, String xmlOriginString) throws Exception {
//        try {
//            if (xmlPage.isEmpty()) {
//                return xmlPage;
//            }
//
//            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
//            if (document == null) {
//                return xmlPage;
//            }
//
//            Map<String, String> mapElemetContent = new LinkedHashMap<String, String>();
//            Element root = document.getDocumentElement();
//            List<Element> groups = XMLUtils.getElementByTagName(root, "group");
//            for (Element group : groups) {
//                Element context = XMLUtils.getFirstElementByTagName(group, "context");
//                if (context == null) {
//                    continue;
//                }
//
//                String tagXml = context.getTextContent();
//                if (tagXml == null || tagXml.isEmpty() == true) {
//                    continue;
//                }
//
//                Element source = XMLUtils.getFirstElementByTagName(group, "source");
//                if (source == null) {
//                    continue;
//                }
//
//                String content = source.getTextContent();
//                Element metadata = XMLUtils.getFirstElementByTagName(group, "m:tunit-metadata");
//                if (metadata != null) {
//                    List<Element> marks = XMLUtils.getElementByTagName(metadata, "m:mark");
//                    for (Element mark : marks) {
//                        Element mContent = XMLUtils.getFirstElementByTagName(mark, "m:content");
//                        if (mContent == null) {
//                            continue;
//                        }
//
//                        Element mType = XMLUtils.getFirstElementByTagName(mark, "m:type");
//                        if (mType != null && mType.getTextContent().equalsIgnoreCase("link") == true) {
//                            content = content.replace("{" + mark.getAttribute("id") + "}", "");
//                        } else {
//                            content = content.replace("{" + mark.getAttribute("id") + "}", mContent.getTextContent());
//                        }
//                    }
//                }
//
//                mapElemetContent.put(group.getAttribute("id") + "|" + tagXml, content);
//            }
//
//            Document doc = DocumentUtil.convertStringToDocument(xmlOriginString, true);
//            List<Text> listElemetOrgin = new ArrayList<Text>();
//            getMapElementText(doc.getDocumentElement(), listElemetOrgin);
//            int index = 0;
//            for (Map.Entry<String, String> entry : mapElemetContent.entrySet()) {
//                String key = entry.getKey();
//                String id = key.split("\\|")[0];
//                String tagXml = key.split("\\|")[1];
//                String value = entry.getValue();
//                Node node = listElemetOrgin.get(index);
//                boolean isEqual = equalElement(tagXml, listElemetOrgin.get(index));
//                if (isEqual == false) {
//                    if (listElemetOrgin.get(index - 1).getParentNode() != null && (listElemetOrgin.get(index - 1).getNodeName().equalsIgnoreCase("p") == true || listElemetOrgin.get(index - 1).getNodeName().equalsIgnoreCase("span") == true)) {
//                        node.setTextContent(node.getTextContent() + value);
//                    }
//                    continue;
//                }
//
//                if (index == listElemetOrgin.size()) {
//                    break;
//                }
//
//                node.setTextContent(value);
//                index++;
//            }
//
//            return DocumentUtil.convertXmlToString(doc, false, true);
//        } catch (Exception e) {
//            MemoryLogUtil.outputLog("EXCEPTION CONVERT MXLIFF TO XML : " + e.getMessage());
//            return xmlPage;
//        }
//    }
    public static void getMapElementText(Node element, List<Text> result) throws Exception {
        NodeList childs = element.getChildNodes();
        for (int i = 0; i < childs.getLength(); i++) {
            Node child = childs.item(i);
            if (child instanceof Text) {
                ((Text) child).setNodeValue("");
                result.add((Text) child);
            }

            if (child.hasChildNodes() == true) {
                getMapElementText(child, result);
            }
        }
    }

    public static Boolean equalElement(String key, Node node) {
        String[] keys = key.split("\\/");
        for (int i = keys.length - 1; i >= 0; i--) {
            Node parent = node.getParentNode();
            if (keys[i].isEmpty() == true) {
                return true;
            }

            if (parent == null || parent instanceof Document) {
                return false;
            }

            if (keys[i].startsWith("<" + parent.getNodeName()) == false) {
                return false;
            }

            node = parent;
            parent = null;
        }

        return true;
    }

    public static String convertMxliffToXml(String xmlPage, String xmlOriginString) throws Exception {
        try {
            if (xmlPage.isEmpty()) {
                return xmlPage;
            }

            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document == null) {
                return xmlPage;
            }

            Map<String, String> mapElemetContent = new LinkedHashMap<String, String>();
            Element root = document.getDocumentElement();
            List<Element> groups = XMLUtils.getElementByTagName(root, "group");
            for (Element group : groups) {
                Element context = XMLUtils.getFirstElementByTagName(group, "context");
                if (context == null) {
                    continue;
                }

                String tagXml = context.getTextContent();
                if (tagXml == null || tagXml.isEmpty() == true) {
                    continue;
                }

                tagXml = tagXml.replace("/<kod>", "").replace("<kod>/", "").replace("//", "/");
                if (tagXml.startsWith("/") == true) {
                    tagXml = tagXml.substring(1);
                }

                Element source = XMLUtils.getFirstElementByTagName(group, "source");
                if (source == null) {
                    continue;
                }

                String content = source.getTextContent();
                Element metadata = XMLUtils.getFirstElementByTagName(group, "m:tunit-metadata");
                if (metadata != null) {
                    List<Element> marks = XMLUtils.getElementByTagName(metadata, "m:mark");
                    for (Element mark : marks) {
                        Element mContent = XMLUtils.getFirstElementByTagName(mark, "m:content");
                        if (mContent == null) {
                            continue;
                        }

                        Element mType = XMLUtils.getFirstElementByTagName(mark, "m:type");
                        if (mType != null && mType.getTextContent().equalsIgnoreCase("link") == true) {
                            content = content.replace("{" + mark.getAttribute("id") + "}", "");
                        } else {
                            content = content.replace("{" + mark.getAttribute("id") + "}", mContent.getTextContent());
                        }
                    }
                }

                mapElemetContent.put(group.getAttribute("id") + "|" + tagXml, content);
            }

            xmlPage = convertToXml(mapElemetContent);
            Document doc = DocumentUtil.convertStringToDocument(xmlPage, false);
            Map<String, String> mapSourceIndex = getMapNodeSourceByTransIndex(doc.getDocumentElement(), new HashMap<String, String>());
            Document docOrigin = DocumentUtil.convertStringToDocument(xmlOriginString, true);
            if (docOrigin == null) {
                return xmlPage;
            }

            Element rootOrigin = docOrigin.getDocumentElement();
            replaceNodeSourceByTransIndex(rootOrigin, mapSourceIndex);
            return DocumentUtil.convertXmlToString(docOrigin, false, true);
        } catch (Exception e) {
            MemoryLogUtil.outputLog("EXCEPTION CONVERT MXLIFF TO XML : " + e.getMessage());
            return xmlPage;
        }
    }

    public static void replaceNodeSourceByTransIndex(Element root, Map<String, String> mapElement) throws Exception {
        NodeList nodes = root.getChildNodes();
        for (int i = nodes.getLength() - 1; i >= 0; i--) {
            Node node = nodes.item(i);
            if (node.getNodeType() != Node.ELEMENT_NODE || node.getNodeName().equals("#cdata-section") == false) {
                continue;
            }
            Element child = (Element) node;
            if (child.hasAttribute("trans-index")) {
                String transIndex = child.getAttribute("trans-index");
                String textNew = mapElement.get(transIndex);
                child.setTextContent(textNew);
            }
            if (child.hasChildNodes()) {
                replaceNodeSourceByTransIndex(child, mapElement);
            }
        }
    }
    
    public static Map<String, String> getMapNodeSourceByTransIndex(Element root, Map<String, String> mapElement) throws Exception {
        NodeList nodes = root.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            if (node.getNodeType() != Node.ELEMENT_NODE || node.getNodeName().equals("#cdata-section") == true) {
                continue;
            }
            Element child = (Element) node;
            if (child.hasAttribute("trans-index")) {
                String transIndex = child.getAttribute("trans-index");
                mapElement.put(transIndex, child.getTextContent());
            }
            if (child.hasChildNodes()) {
                mapElement = getMapNodeSourceByTransIndex(child, mapElement);
            }
        }
        return mapElement;
    }

    public static String convertToXml(Map<String, String> mapElemetContent) throws Exception {
        String parent = "";
        String result = "";
        String tagElement = "";
        String before = "";
        Map<String, String> childs = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : mapElemetContent.entrySet()) {
            String key = entry.getKey();
            String id = key.split("\\|")[0];
            String tagXml = key.split("\\|")[1];
            String content = entry.getValue();
            if (tagXml.contains("/") == false) {
                if (childs.isEmpty() == false && tagElement.isEmpty() == false) {
                    result += (tagElement + convertToXml(childs) + getEndElement(tagElement));
                    childs = new LinkedHashMap<String, String>();
                }

                parent = "";
                result += (tagXml + content + getEndElement(tagXml));
                continue;
            }

            tagElement = tagXml.substring(0, tagXml.indexOf("/"));
            String tagChild = tagXml.substring(tagXml.indexOf("/") + 1);
            if (tagElement.equalsIgnoreCase(parent) == false && parent.isEmpty() == false) {
                parent = "";
                result += (tagElement + convertToXml(childs) + getEndElement(tagElement));
            } else {
                if ((before.startsWith("<li") == true && tagElement.startsWith("<li") == true && childs.isEmpty() == false)
                        || (before.startsWith("<p") == true && tagElement.startsWith("<p") == true)) {
                    result += (tagElement + convertToXml(childs) + getEndElement(tagElement));
                    childs = new LinkedHashMap<String, String>();
                }

                parent = tagElement;
                childs.put(id + "|" + tagChild, content);
            }

            before = tagElement;
        }

        if (tagElement.isEmpty() == false) {
            result += (tagElement + convertToXml(childs) + getEndElement(tagElement));
        }

        return result;
    }

    public static String getEndElement(String startElement) {
        String endElement = startElement;
        if (endElement.contains(" ") == true) {
            endElement = endElement.substring(0, endElement.indexOf(" ")) + ">";
        }

        return endElement.replace("<", "</");
    }

    public static Map<String, String> getLanguageSupport() throws Exception {
//        SupportedLanguageService supportedLanguageService = TranslationMemoryToolFactory.getSupportedLanguageService();
        Map<String, String> result = new LinkedHashMap<String, String>();
//        List<SupportedLanguage> listSupportedLanguage = supportedLanguageService.getAll();
//        for (SupportedLanguage supportedLanguage : listSupportedLanguage) {
//            result.put(supportedLanguage.getLanguageCode(), supportedLanguage.getLanguageName());
//        }
        return result;
    }

    /**
     * NameSpaceのタグの取得
     *
     * @return
     */
    private static NamespaceContext getNSContext() {
        return new NamespaceContext() {
            @Override
            public String getNamespaceURI(String prefix) {
                return prefix.equals(AC_PREFIX) ? AC_CONTENT : null;
            }

            @Override
            public Iterator<?> getPrefixes(String val) {
                return null;
            }

            @Override
            public String getPrefix(String uri) {
                return null;
            }
        };
    }
    // </editor-fold> 
}
