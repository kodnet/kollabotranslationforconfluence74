package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.request;

import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import static jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil.LOG;
import static jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil.createResponseInstance;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseJsonModeBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;


/**
 *
 * @author TAMPT
 */
public class ZendeskRequestUtil {

    public static ResponseModelBase createRequest(String apiURL, ResponseJsonModeBase requestModel, Class responseType) throws Exception {
        try {

            String logText = "ZENDDESK POST=> URL:";

            String urlString = Constants.ZENDDESK_API_BASE_URL + apiURL;
            logText += urlString;

            URL url = new URL(urlString);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            urlCon.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlCon.setRequestProperty("Accept-Language", "UTF-8");
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            if (requestModel != null) {
                OutputStream os = urlCon.getOutputStream();
                PrintStream ps = new PrintStream(os);
                String parameterData = requestModel.toString();
                os.write(parameterData.getBytes("UTF-8"));
                os.close();

                logText += " DATA: " + parameterData;
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseType);

        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }
}
