package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import net.java.ao.Query;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import java.util.Date;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;

public class TranslationApiLoginInfoServiceImpl implements TranslationApiLoginInfoService {

    private final ActiveObjects ao;

    public TranslationApiLoginInfoServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<ApiLoginInfo> getAll() {
        List<ApiLoginInfo> translationApiLoginInfos= newArrayList(ao.find(ApiLoginInfo.class));
        return translationApiLoginInfos;
    }

    @Override
    public ApiLoginInfo getOne(String translationToolId) {
        List<ApiLoginInfo> translationApiLoginInfos = newArrayList(ao.find(ApiLoginInfo.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        if (translationApiLoginInfos.isEmpty()) {
            return null;
        }
        return translationApiLoginInfos.get(0);
    }

    @Override
    public ApiLoginInfo add(
            String translationToolId,
            String translationToolName,
            String userName,
            String password,
            String loggedToken,
            String ortherKey01,
            String OrtherKey02,
            Date dateExpires
            ){
        ApiLoginInfo translationApiLoginInfo = ao.create(ApiLoginInfo.class);
        translationApiLoginInfo.setTranslationToolId(translationToolId);
        translationApiLoginInfo.setTranslationToolName(translationToolName);
        translationApiLoginInfo.setUserName(userName);
        translationApiLoginInfo.setPassword(password);
        translationApiLoginInfo.setLoggedToken(loggedToken);
        translationApiLoginInfo.setOrtherKey01(ortherKey01);
        translationApiLoginInfo.setOrtherKey02(OrtherKey02);
        translationApiLoginInfo.setDateExpires(dateExpires);
        translationApiLoginInfo.save();
        return translationApiLoginInfo;
    }

    @Override
    public ApiLoginInfo update(
            String translationToolId,
            String translationToolName,
            String userName,
            String password,
            String loggedToken,
            String ortherKey01,
            String OrtherKey02,
            Date dateExpires
            ) {
        ApiLoginInfo translationApiLoginInfo = this.getOne(translationToolId);
        if (translationApiLoginInfo != null) {
            translationApiLoginInfo.setTranslationToolName(translationToolName);
            translationApiLoginInfo.setUserName(userName);
            translationApiLoginInfo.setPassword(password);
            translationApiLoginInfo.setLoggedToken(loggedToken);
            translationApiLoginInfo.setOrtherKey01(ortherKey01);
            translationApiLoginInfo.setOrtherKey02(OrtherKey02);
            translationApiLoginInfo.setDateExpires(dateExpires);
            translationApiLoginInfo.save();
        }
        return translationApiLoginInfo;
    }

    @Override
    public void delete(String translationToolId) {
        ApiLoginInfo translationApiLoginInfo = this.getOne(translationToolId);
        if (translationApiLoginInfo != null) {
            ao.delete(translationApiLoginInfo);
        }
    }

    @Override
    public void deleteAll() {
        List<ApiLoginInfo> ip = this.getAll();
        for (ApiLoginInfo item : ip) {
            ao.delete(item);
        }
    }
}
