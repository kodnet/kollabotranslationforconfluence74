package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.core.util.ProgressMeter;
import com.atlassian.sal.api.transaction.TransactionCallback;

public interface TranslationMemsourceCallback<T> extends TransactionCallback<T> {

    public void setProgress(ProgressMeter progress);

    public String getName();
    
}
