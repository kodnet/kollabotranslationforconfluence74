package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Client;

/**
 *
 * @author anhlq
 */
public class GetClientResponse extends ResponseModelBase {

    private Client[] content;

    public Client[] getContent() {
        return content;
    }

    public void setContent(Client[] content) {
        this.content = content;
    }
}
