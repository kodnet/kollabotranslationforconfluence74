package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public abstract class SpanProcessor implements ElementProcessor {

    public static final QName SPAN_XMLTAG = new QName("http://www.w3.org/1999/xhtml", "span");
    public static final QName CLASS_ATTRIBUTE = new QName("class");
    private String className;

    public SpanProcessor(String className) {
        this.className = className;
    }

    @Override
    public boolean shouldProcess(XMLEvent xmlEvent) {
        return (xmlEvent.isStartElement())
                && (xmlEvent.asStartElement().getName().getLocalPart().equalsIgnoreCase(SPAN_XMLTAG.getLocalPart()))
                && (hasClass(xmlEvent.asStartElement(), this.className));
    }

    protected boolean hasClass(StartElement startElement, String className) {
        Attribute attribute = startElement.getAttributeByName(CLASS_ATTRIBUTE);
        if (attribute != null) {
            return attribute.getValue().contains(className);
        }
        return false;
    }
}
