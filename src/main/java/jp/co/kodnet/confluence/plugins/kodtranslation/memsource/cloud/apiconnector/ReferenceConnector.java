package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import java.io.InputStream;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ReferenceCreateRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ReferenceCreateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;

/**
 *
 * @author anhlq
 */
public class ReferenceConnector {

    private final static String CREATE_API_URL = "api/v2/referenceFile/create";
    private final String token;

    public ReferenceConnector(String token) {
        this.token = token;
    }

    public ReferenceCreateResponse create(String project, InputStream inputStream, String filename) throws Exception {
        ReferenceCreateRequest request = new ReferenceCreateRequest();
        request.setToken(this.token);
        request.setProject(project);
        request.setFilename(filename);

        return (ReferenceCreateResponse) MemoryRequestUtil.createRequestUploadWithMultipart(Constants.MEMSOURCE_API_BASE_URL + CREATE_API_URL, inputStream, request, ReferenceCreateResponse.class);
    }
}
