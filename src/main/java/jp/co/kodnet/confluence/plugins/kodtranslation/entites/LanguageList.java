package jp.co.kodnet.confluence.plugins.kodtranslation.entites;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface LanguageList extends Entity {

    public String getTranslationToolId();

    public void setTranslationToolId(String translationToolId);

    public String getLanguageKey();

    public void setLanguageKey(String languageKey);

    public String getLanguageDisplay();

    public void setLanguageDisplay(String languageDisplay);

}
