package jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base;

import com.atlassian.json.jsonorg.JSONObject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author TAMPT
 */
public class ResponseJsonModeBase extends ResponseModelBase {

    @Override
    public String toString() {
        return StringUtils.EMPTY;
    }

    public JSONObject toJson() {
        return new JSONObject();
    }
}
