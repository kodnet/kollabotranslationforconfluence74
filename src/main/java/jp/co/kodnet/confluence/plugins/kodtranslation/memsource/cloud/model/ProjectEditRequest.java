package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author hailc
 */
public class ProjectEditRequest extends RequestModelBase {

    private String[] targetLang;
    private String project;

    public String[] getTargetLang() {
        return targetLang;
    }

    public void setTargetLang(String[] targetLang) {
        this.targetLang = targetLang;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String projectUid) {
        this.project = projectUid;
    }

}
