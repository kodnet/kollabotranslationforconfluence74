package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author nhatnq
 */
public class GetJobListRequest extends RequestModelBase {

    private Integer pageNumber;
    private Integer workflowLevel;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getWorkflowLevel() {
        return workflowLevel;
    }

    public void setWorkflowLevel(Integer workflowLevel) {
        this.workflowLevel = workflowLevel;
    }
}
