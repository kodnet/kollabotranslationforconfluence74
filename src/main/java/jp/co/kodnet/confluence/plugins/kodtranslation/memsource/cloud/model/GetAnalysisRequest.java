package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author baoph
 */
public class GetAnalysisRequest extends RequestModelBase {

    private String analyse;

    public String getAnalyse() {
        return analyse;
    }

    public void setAnalyse(String analyse) {
        this.analyse = analyse;
    }
}
