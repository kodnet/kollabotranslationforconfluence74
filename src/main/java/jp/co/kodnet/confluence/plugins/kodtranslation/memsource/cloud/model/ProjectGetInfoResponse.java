package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;

/**
 *
 * @author hailc
 */
public class ProjectGetInfoResponse extends ResponseModelBase {

    private String id;
    private String name;
    private String status;
    private String sourceLang;
    private String[] targetLangs;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String[] getTargetLangs() {
        return targetLangs;
    }

    public void setTargetLangs(String[] targetLangs) {
        this.targetLangs = targetLangs;
    }

}
