package jp.co.kodnet.confluence.plugins.kodtranslation.entites;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface GroupInfo extends Entity {

    public String getGroupName();

    public void setGroupName(String groupName);

}
