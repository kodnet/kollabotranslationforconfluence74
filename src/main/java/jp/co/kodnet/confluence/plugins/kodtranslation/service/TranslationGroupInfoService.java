package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.GroupInfo;

@Transactional
public interface TranslationGroupInfoService {
    
    GroupInfo get();
    
    GroupInfo add(String groupName);
    
    GroupInfo update(String groupName);
    
    void delete();
  
}
