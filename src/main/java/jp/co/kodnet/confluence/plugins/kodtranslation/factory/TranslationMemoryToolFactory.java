package jp.co.kodnet.confluence.plugins.kodtranslation.factory;
													
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.pages.AttachmentManager;														
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.FileUploadManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import javax.xml.stream.XMLEventFactory;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import java.util.Locale;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationFavoriteLanguageService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationGroupInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;

public class TranslationMemoryToolFactory {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private static SpaceManager spaceManager;
    private static PageManager pageManager;
//    private static SpaceLabelManager spaceLabelManager;
    private static LabelManager labelManager;
    private static AttachmentManager attachmentManager;
    private static BootstrapManager bootstrapManager;
    private static SettingsManager settingsManager;
    private static LongRunningTaskManager longRunningTaskManager;
    private static LinkManager linkManager;
    private static PermissionManager permissionManager;
    private static BandanaManager bandanaManager;
//    private static StylesheetManager stylesheetManager;
    private static FileUploadManager fileUploadManager;
    private static PluginAccessor pluginAccessor;
    private static UserAccessor userAccessor;
    private static LocaleManager localeManager;
    private static I18NBeanFactory i18NBeanFactory;
    private static I18NBean i18NBean;
    private static TransactionTemplate transactionTemplate;
    private static SoyTemplateRenderer soyTemplateRenderer;
    private static XmlEventReaderFactory xmlEventReaderFactory;
    private static XmlOutputFactory xmlOutputFactory;
//    private static XhtmlContent xhtmlContent;
    private static XMLEventFactory xmlEventFactory;
    private static PluginHibernateSessionFactory pluginHibernateSessionFactory;
    private static CommentManager commentManager;
    private static SpacePermissionManager spacePermissionManager;
    private static ContentPermissionManager contentPermissionManager;
    private static TranslationApiLoginInfoService translationApiLoginInfoService;
    private static TranslationJobService translationJobService;
    private static TranslationLanguageListService translationLanguageListService;
    private static TranslationFavoriteLanguageService translationFavoriteLanguageService;
    private static TranslationLabelTemplateService translationLabelTemplateService;
    private static TranslationGroupInfoService translationGroupInfoService;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="constructor">
    public TranslationMemoryToolFactory(
            SpaceManager spaceManager,
            PageManager pageManager,
            LabelManager labelManager,
            AttachmentManager attachmentManager,
            BootstrapManager bootstrapManager,
            SettingsManager settingsManager,
            LongRunningTaskManager longRunningTaskManager,
            LinkManager linkManager,
            PermissionManager permissionManager,
            CommentManager commentManager,
            BandanaManager bandanaManager,
            SpacePermissionManager spacePermissionManager,
            ContentPermissionManager contentPermissionManager,
            PluginAccessor pluginAccessor,
            UserAccessor userAccessor,
            LocaleManager localeManager,
            I18NBeanFactory i18NBeanFactory,
            TransactionTemplate transactionTemplate,
            SoyTemplateRenderer soyTemplateRenderer,
            FileUploadManager fileUploadManager,
            PluginHibernateSessionFactory pluginHibernateSessionFactory,
            TranslationApiLoginInfoService translationApiLoginInfoService,
            TranslationJobService translationJobService,
            TranslationLanguageListService translationLanguageListService,
            TranslationFavoriteLanguageService translationFavoriteLanguageService,
            TranslationLabelTemplateService translationLabelTemplateService,
            TranslationGroupInfoService translationGroupInfoService
    ) {
        TranslationMemoryToolFactory.spaceManager = spaceManager;
        TranslationMemoryToolFactory.pageManager = pageManager;
        TranslationMemoryToolFactory.labelManager = labelManager;
        TranslationMemoryToolFactory.attachmentManager = attachmentManager;
        TranslationMemoryToolFactory.bootstrapManager = bootstrapManager;
        TranslationMemoryToolFactory.settingsManager = settingsManager;
        TranslationMemoryToolFactory.longRunningTaskManager = longRunningTaskManager;
        TranslationMemoryToolFactory.linkManager = linkManager;
        TranslationMemoryToolFactory.permissionManager = permissionManager;
        TranslationMemoryToolFactory.commentManager = commentManager;
        TranslationMemoryToolFactory.bandanaManager = bandanaManager;
        TranslationMemoryToolFactory.spacePermissionManager = spacePermissionManager;
        TranslationMemoryToolFactory.contentPermissionManager = contentPermissionManager;
        TranslationMemoryToolFactory.pluginAccessor = pluginAccessor;
        TranslationMemoryToolFactory.userAccessor = userAccessor;
        TranslationMemoryToolFactory.localeManager = localeManager;
        TranslationMemoryToolFactory.i18NBeanFactory = i18NBeanFactory;
        TranslationMemoryToolFactory.transactionTemplate = transactionTemplate;
        TranslationMemoryToolFactory.soyTemplateRenderer = soyTemplateRenderer;
        TranslationMemoryToolFactory.fileUploadManager = fileUploadManager;
        TranslationMemoryToolFactory.pluginHibernateSessionFactory = pluginHibernateSessionFactory;
        TranslationMemoryToolFactory.translationApiLoginInfoService = translationApiLoginInfoService;
        TranslationMemoryToolFactory.translationJobService = translationJobService;
        TranslationMemoryToolFactory.translationLanguageListService = translationLanguageListService;
        TranslationMemoryToolFactory.translationFavoriteLanguageService = translationFavoriteLanguageService;
        TranslationMemoryToolFactory.translationLabelTemplateService = translationLabelTemplateService;
        TranslationMemoryToolFactory.translationGroupInfoService = translationGroupInfoService;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="set/get">
    public static SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public static void setSpaceManager(SpaceManager spaceManager) {
        TranslationMemoryToolFactory.spaceManager = spaceManager;
    }

    public static PageManager getPageManager() {
        return pageManager;
    }

    public static void setPageManager(PageManager pageManager) {
        TranslationMemoryToolFactory.pageManager = pageManager;
    }
//
//    public static SpaceLabelManager getSpaceLabelManager() {
//        return spaceLabelManager;
//    }
//
//    public static void setSpaceLabelManager(SpaceLabelManager spaceLabelManager) {
//        InputPattenFactory.spaceLabelManager = spaceLabelManager;
//    }
//
    public static LabelManager getLabelManager() {
        return labelManager;
    }

    public static void setLabelManager(LabelManager labelManager) {
        TranslationMemoryToolFactory.labelManager = labelManager;
    }

    public static AttachmentManager getAttachmentManager() {
        return attachmentManager;
    }

    public static void setAttachmentManager(AttachmentManager attachmentManager) {
        TranslationMemoryToolFactory.attachmentManager = attachmentManager;
    }

    public static BootstrapManager getBootstrapManager() {
        return bootstrapManager;
    }

    public static void setBootstrapManager(BootstrapManager bootstrapManager) {
        TranslationMemoryToolFactory.bootstrapManager = bootstrapManager;
    }

    public static SettingsManager getSettingsManager() {
        return settingsManager;
    }

    public static void setSettingsManager(SettingsManager settingsManager) {
        TranslationMemoryToolFactory.settingsManager = settingsManager;
    }
//
    public static LongRunningTaskManager getLongRunningTaskManager() {
        return longRunningTaskManager;
    }

    public static void setLongRunningTaskManager(LongRunningTaskManager longRunningTaskManager) {
        TranslationMemoryToolFactory.longRunningTaskManager = longRunningTaskManager;
    }

    public static LinkManager getLinkManager() {
        return linkManager;
    }

    public static void setLinkManager(LinkManager linkManager) {
        TranslationMemoryToolFactory.linkManager = linkManager;
    }

    public static PermissionManager getPermissionManager() {
        return permissionManager;
    }

    public static void setPermissionManager(PermissionManager permissionManager) {
        TranslationMemoryToolFactory.permissionManager = permissionManager;
    }

    public static CommentManager getCommentManager() {
        return commentManager;
    }

    public static void setCommentManager(CommentManager commentManager) {
        TranslationMemoryToolFactory.commentManager = commentManager;
    }

    public static SpacePermissionManager getSpacePermissionManager() {
        return spacePermissionManager;
    }

    public static void setSpacePermissionManager(SpacePermissionManager spacePermissionManager) {
        TranslationMemoryToolFactory.spacePermissionManager = spacePermissionManager;
    }

    public static ContentPermissionManager getContentPermissionManager() {
        return contentPermissionManager;
    }

    public static void setContentPermissionManager(ContentPermissionManager contentPermissionManager) {
        TranslationMemoryToolFactory.contentPermissionManager = contentPermissionManager;
    }
//
//    public static StylesheetManager getStylesheetManager() {
//        return stylesheetManager;
//    }
//
//    public static void setStylesheetManager(StylesheetManager stylesheetManager) {
//        InputPattenFactory.stylesheetManager = stylesheetManager;
//    }

    public static PluginAccessor getPluginAccessor() {
        return pluginAccessor;
    }

    public static void setPluginAccessor(PluginAccessor pluginAccessor) {
        TranslationMemoryToolFactory.pluginAccessor = pluginAccessor;
    }

    public static UserAccessor getUserAccessor() {
        return userAccessor;
    }

    public static void setUserAccessor(UserAccessor userAccessor) {
        TranslationMemoryToolFactory.userAccessor = userAccessor;
    }

    public static LocaleManager getLocaleManager() {
        return localeManager;
    }

    public static void setLocaleManager(LocaleManager localeManager) {
        TranslationMemoryToolFactory.localeManager = localeManager;
    }

    public static I18NBeanFactory getI18NBeanFactory() {
        return i18NBeanFactory;
    }

    public static void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        TranslationMemoryToolFactory.i18NBeanFactory = i18NBeanFactory;
    }

    public static I18NBean getI18NBean() {
        User user = AuthenticatedUserThreadLocal.get();
        if (user == null) {
            i18NBean = (I18NBean) ContainerManager.getInstance().getContainerContext().getComponent("i18NBean");
        } else {
            Locale locale = TranslationMemoryToolFactory.getLocaleManager().getLocale(user);
            i18NBean = i18NBeanFactory.getI18NBean(locale);
        }
        return i18NBean;
    }

    public static void setI18NBean(I18NBean i18NBean) {
        TranslationMemoryToolFactory.i18NBean = i18NBean;
    }

    public static TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    public static void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        TranslationMemoryToolFactory.transactionTemplate = transactionTemplate;
    }

    public static BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    public static void setBandanaManager(BandanaManager bandanaManager) {
        TranslationMemoryToolFactory.bandanaManager = bandanaManager;
    }

    public static SoyTemplateRenderer getSoyTemplateRenderer() {
        return soyTemplateRenderer;
    }

    public static void setSoyTemplateRenderer(SoyTemplateRenderer soyTemplateRenderer) {
        TranslationMemoryToolFactory.soyTemplateRenderer = soyTemplateRenderer;
    }
    
    public static void setFileUploadManager(FileUploadManager fileUploadManager) {
        TranslationMemoryToolFactory.fileUploadManager = fileUploadManager;
    }

    public static PluginHibernateSessionFactory getPluginHibernateSessionFactory() {
        return pluginHibernateSessionFactory;
    }

    public static void setPluginHibernateSessionFactory(PluginHibernateSessionFactory pluginHibernateSessionFactory) {
        TranslationMemoryToolFactory.pluginHibernateSessionFactory = pluginHibernateSessionFactory;
    }

    public static XMLEventFactory getXmlEventFactory() {
        if (xmlEventFactory == null) {
            xmlEventFactory = (XMLEventFactory) ContainerManager.getComponent("xmlEventFactory");
        }
        return xmlEventFactory;
    }

    public static void setXmlEventFactory(XMLEventFactory aXmlEventFactory) {
        xmlEventFactory = aXmlEventFactory;
    }

    public static XmlEventReaderFactory getXmlEventReaderFactory() {
        if (xmlEventReaderFactory == null) {
            xmlEventReaderFactory = (XmlEventReaderFactory) ContainerManager.getComponent("xmlEventReaderFactory");
        }
        return xmlEventReaderFactory;
    }

    public static void setXmlEventReaderFactory(XmlEventReaderFactory aXmlEventReaderFactory) {
        xmlEventReaderFactory = aXmlEventReaderFactory;
    }

    public static XmlOutputFactory getXmlOutputFactory() {
        if (xmlOutputFactory == null) {
            xmlOutputFactory = (XmlOutputFactory) ContainerManager.getComponent("xmlFragmentOutputFactory");
        }
        return xmlOutputFactory;
    }

    public static void setXmlOutputFactory(XmlOutputFactory aXmlOutputFactory) {
        xmlOutputFactory = aXmlOutputFactory;
    }

    public static TranslationApiLoginInfoService getTranslationApiLoginInfoService() {
        return translationApiLoginInfoService;
    }

    public static void setTranslationApiLoginInfoService(TranslationApiLoginInfoService translationApiLoginInfoService) {
        TranslationMemoryToolFactory.translationApiLoginInfoService = translationApiLoginInfoService;
    }

    public static TranslationJobService getTranslationJobService() {
        return translationJobService;
    }

    public static void setTranslationJobService(TranslationJobService translationJobService) {
        TranslationMemoryToolFactory.translationJobService = translationJobService;
    }

    public static TranslationLanguageListService getTranslationLanguageListService() {
        return translationLanguageListService;
    }

    public static void setTranslationLanguageListService(TranslationLanguageListService translationLanguageListService) {
        TranslationMemoryToolFactory.translationLanguageListService = translationLanguageListService;
    }

    public static TranslationFavoriteLanguageService getTranslationFavoriteLanguageService() {
        return translationFavoriteLanguageService;
    }

    public static void setTranslationFavoriteLanguageService(TranslationFavoriteLanguageService translationFavoriteLanguageService) {
        TranslationMemoryToolFactory.translationFavoriteLanguageService = translationFavoriteLanguageService;
    }

    public static TranslationLabelTemplateService getTranslationLabelTemplateService() {
        return translationLabelTemplateService;
    }

    public static void setTranslationLabelTemplateService(TranslationLabelTemplateService translationLabelTemplateService) {
        TranslationMemoryToolFactory.translationLabelTemplateService = translationLabelTemplateService;
    }

    public static TranslationGroupInfoService getTranslationGroupInfoService() {
        return translationGroupInfoService;
    }

    public static void setTranslationGroupInfoService(TranslationGroupInfoService translationGroupInfoService) {
        TranslationMemoryToolFactory.translationGroupInfoService = translationGroupInfoService;
    }
}
