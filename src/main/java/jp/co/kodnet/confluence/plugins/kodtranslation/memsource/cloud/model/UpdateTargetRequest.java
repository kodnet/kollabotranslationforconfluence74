package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

public class UpdateTargetRequest extends RequestModelBase {

    protected JobInfo[] jobs;
    protected Boolean propagateConfirmedToTm;

    public JobInfo[] getJobs() {
        return jobs;
    }

    public void setJobs(JobInfo[] jobs) {
        this.jobs = jobs;
    }

    public Boolean getPropagateConfirmedToTm() {
        return propagateConfirmedToTm;
    }

    public void setPropagateConfirmedToTm(Boolean propagateConfirmedToTm) {
        this.propagateConfirmedToTm = propagateConfirmedToTm;
    }
 
}
