package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 *
 * @author kohei sato
 */
public class AsynchronousThread extends Thread {

    private final String projectUid;
    private final String zipFilePath;
    private final String[] targetLanguage;
    private final JobConnector jobConnector;

    public AsynchronousThread(String projectUid, String zipFilePath, String[] targetLanguage, JobConnector jobConnector) {
        this.projectUid = projectUid;
        this.zipFilePath = zipFilePath;
        this.targetLanguage = targetLanguage;
        this.jobConnector = jobConnector;
    }

    @Override
    public void run() {
        try {
            File file = new File(this.zipFilePath);
            if (file.isFile() == false || file.exists() == false) {
                return;
            }

            InputStream inputStream = new FileInputStream(file);
            this.jobConnector.createAsync(this.projectUid, file.getName(), inputStream, this.targetLanguage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
