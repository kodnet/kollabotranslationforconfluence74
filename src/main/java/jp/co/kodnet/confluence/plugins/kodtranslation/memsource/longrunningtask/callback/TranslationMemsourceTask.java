package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;


public class TranslationMemsourceTask extends ConfluenceAbstractLongRunningTask {

    protected TranslationMemsourceJobCallBack createProjectMemsourceCallBack;

    public TranslationMemsourceTask(TranslationMemsourceJobCallBack createProjectMemsourceCallBack) {
        this.createProjectMemsourceCallBack = createProjectMemsourceCallBack;
    }

    @Override
    protected void runInternal() {
        try {
            TransactionTemplate transactionTemplate = TranslationMemoryToolFactory.getTransactionTemplate();

            createProjectMemsourceCallBack.setProgress(progress);
            Object result = transactionTemplate.execute(createProjectMemsourceCallBack);
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            this.progress.setPercentage(100);
            this.progress.setCompletedSuccessfully(true);
        }
    }

    @Override
    public String getName() {
        if (this.createProjectMemsourceCallBack == null) {
            return this.getClass().getName();
        }
        return this.createProjectMemsourceCallBack.getName();
    }
}
