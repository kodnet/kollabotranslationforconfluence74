package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.LoginUserRequest;

/**
 *
 * @author lucvd
 */
public class WhoIsAmResponse extends ResponseModelBase {

    private LoginUserRequest user;

    /**
     * @return the user
     */
    public LoginUserRequest getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(LoginUserRequest user) {
        this.user = user;
    }
}
