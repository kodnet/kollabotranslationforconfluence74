package jp.co.kodnet.confluence.plugins.kodtranslation.common;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.util.ConfluenceHomeGlobalConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author huetq
 */
public class MemoryLogUtil {

    public static final String MEMSOURCE_FOLDER_NAME = "TranlationDirectoryLog";
    public static final String MEMSOURCE_FILE_NAME = "TranlationDirectory";
    public static final String MEMSOURCE_FILE_EXTENSION = "txt";
    private static String resultPath = "";
    private static WriteLog writeLog;

    public static void outputMemoryLog(String apiURL, String method, String resultCode, String errorCode, String errorDescription) {
        String mes = "";

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        String strDate = null;
        try {
            strDate = sdf.format(CommonFunction.getCurentDate());
        } catch (Exception ex) {
            outputLogException(MemoryLogUtil.class, ex);
        }
        resultCode = !StringUtils.isEmpty(resultCode) ? resultCode : "-";
        errorCode = !StringUtils.isEmpty(errorCode) ? errorCode : "-";
        errorDescription = !StringUtils.isEmpty(errorDescription) ? errorDescription : "-";
        mes += strDate + " " + method + " " + apiURL + " " + resultCode + " " + errorCode + " " + errorDescription;
        try {
            outputLog(mes);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MemoryLogUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void outputLogException(Class className, Throwable ex) {
        try {
            outputLog(getStackTrace(ex));
        } catch (Exception ex1) {
            java.util.logging.Logger.getLogger(MemoryLogUtil.class.getName()).log(Level.SEVERE, null, ex1);
        }
        Logger logger = LoggerFactory.getLogger(className);
        logger.error(ex.getMessage(), ex);
    }

    public static Map<String, Object> getBeanFaild(Exception ex) {
        Map<String, Object> mapBean = new HashMap<>();
        mapBean.put("result", false);
        mapBean.put("message", ex.getMessage());
        return mapBean;
    }

    public static void outputLog(String mes) {
        int result = MemoryLogUtil.getWriteLog().outputLog(mes);
    }

    public static String getStackTrace(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static String getDefaultLogPath() {
        BootstrapManager bootstrapManager = TranslationMemoryToolFactory.getBootstrapManager();
        return bootstrapManager.getLocalHome() + System.getProperty("file.separator") + ConfluenceHomeGlobalConstants.LOGS_DIR;
    }

    public static void outputStartTime(String actionName) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        outputLog("START ACTION: " + actionName);
        outputLog("DATE START: " + strDate);
        outputLog("TIME START: " + strSecond);
    }

    public static void outputEndTime(String actionName) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        outputLog("END ACTION: " + actionName);
        outputLog("DATE END: " + strDate);
        outputLog("TIME END: " + strSecond);
    }

    public static void outputTime(Boolean isStart, String... actionNames) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat ssf = new SimpleDateFormat("HH:mm:ss");
        String strDate = sdf.format(CommonFunction.getCurentDate());
        String strSecond = ssf.format(CommonFunction.getCurentDate());
        if (actionNames.length > 0) {
            outputLog("=================================");
            if (isStart) {
                outputLog("START ACTION: " + actionNames[0]);
            } else {
                outputLog("END ACTION: " + actionNames[0]);
            }

            for (int i = 1; i < actionNames.length; i++) {
                outputLog("POSITION: " + actionNames[i]);
            }
            outputLog("DATE START: " + strDate);
            outputLog("TIME START: " + strSecond);
            outputLog("----------------------------------");
        }
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String aResultPath) {
        resultPath = aResultPath;
    }

    public static WriteLog getWriteLog() {
        if (MemoryLogUtil.writeLog == null) {
            MemoryLogUtil.writeLog = new WriteLog();
        }

        return writeLog;
    }

    public static void setWriteLog(WriteLog writeLog) {
        MemoryLogUtil.writeLog = writeLog;
    }

    public static class WriteLog {

        public int outputLog(String mes) {
            FileWriter fstream;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                String strDate = sdf.format(CommonFunction.getCurentDate());
                SimpleDateFormat sdfFile = new SimpleDateFormat("YYYY-MM-dd");
                String strDateFileLog = sdfFile.format(CommonFunction.getCurentDate());
                resultPath = getDefaultLogPath() + "\\" + MEMSOURCE_FOLDER_NAME + "\\" + MEMSOURCE_FILE_NAME + "." + strDateFileLog + "." + MEMSOURCE_FILE_EXTENSION;
                File file = new File(resultPath);
                File dir = file.getParentFile();
                if (!dir.exists()) {
                    dir.mkdirs();
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                }
                fstream = new FileWriter(resultPath, true);
                BufferedWriter fbw = new BufferedWriter(fstream);
                fbw.write(mes);
                fbw.newLine();
                fbw.close();
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(MemoryLogUtil.class.getName()).log(Level.SEVERE, null, ex);
            }

            return -1;
        }
    }
}
