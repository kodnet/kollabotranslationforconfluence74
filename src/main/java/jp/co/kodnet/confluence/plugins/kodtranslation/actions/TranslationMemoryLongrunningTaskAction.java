package jp.co.kodnet.confluence.plugins.kodtranslation.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.core.task.longrunning.LongRunningTask;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import static jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil.MEMSOURCE_FILE_EXTENSION;
import static jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil.MEMSOURCE_FILE_NAME;
import static jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil.MEMSOURCE_FOLDER_NAME;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import org.apache.commons.lang.StringUtils;

public class TranslationMemoryLongrunningTaskAction extends ConfluenceActionSupport implements Beanable {

    private Map<String, Object> resultBean = new HashMap<String, Object>();

    @Override
    public Object getBean() {
        return resultBean;
    }

    public String doGetTaskPercentage() {
        try {
            ActionContext context = ActionContext.getContext();
            resultBean = new HashMap<String, Object>();
            String taskId = ActionContextHelper.getFirstParameterValueAsString(context, "taskId");
            int percentage = 0;
            if (taskId == null || taskId.isEmpty() == true) {
                percentage = 100;
                resultBean.put("result", true);
                resultBean.put("percentage", percentage);
                return SUCCESS;
            }

            LongRunningTask task = TranslationMemoryToolFactory.getLongRunningTaskManager().getLongRunningTask(getAuthenticatedUser(), LongRunningTaskId.valueOf(taskId));
            percentage = task.getPercentageComplete();
            String message = task.getCurrentStatus();
            String html = task.getCurrentStatus();
            String json = task.getCurrentStatus();
            String projectId = task.getCurrentStatus();
            String urlPath = task.getCurrentStatus();
            String error = StringUtils.EMPTY;
            Boolean isComplete = false;
            if (message.contains(Constants.ERROR_PREFIX) == true) {
                SettingsManager settingsManager = TranslationMemoryToolFactory.getSettingsManager();
                error = message.substring(Constants.ERROR_PREFIX.length());
                message = StringUtils.EMPTY;
                isComplete = true;
                SimpleDateFormat sdfFile = new SimpleDateFormat("YYYY-MM-dd");
                String strDateFileLog = sdfFile.format(CommonFunction.getCurentDate());
                String path = MemoryLogUtil.getDefaultLogPath() + "\\" + MEMSOURCE_FOLDER_NAME + "\\" + MEMSOURCE_FILE_NAME + "." + strDateFileLog + "." + MEMSOURCE_FILE_EXTENSION;
                urlPath = settingsManager.getGlobalSettings().getBaseUrl() + "/translationdirectory/dodownload.action?uri=" + URLEncoder.encode(path, "UTF-8");
                resultBean.put("result", false);
            } else if (message.contains(Constants.WAITTING_PREFIX) == true) {
                error = StringUtils.EMPTY;
                message = message.substring(Constants.WAITTING_PREFIX.length());
                resultBean.put("result", false);
            } else if (message.contains(Constants.INFO_PREFIX) == true) {
                error = StringUtils.EMPTY;
                message = message.substring(Constants.INFO_PREFIX.length());
                resultBean.put("result", false);
                resultBean.put("info", true);
            } else if (message.contains(Constants.STRING_DISTINS_SYNC) == true) {
                error = StringUtils.EMPTY;
                isComplete = isComplete || (percentage > 99);
                resultBean.put("result", true);
                String[] str = message.split(Constants.SPLIT_STRING_SYNC_LANG);
                html = str[0];
                if (str.length > 1) {
                    urlPath = str[1];
                } else {
                    urlPath = "";
                }
            } else {
                error = StringUtils.EMPTY;
                isComplete = isComplete || (percentage > 99);
                resultBean.put("result", true);
                if (message.contains(Constants.STRING_DISTINS) == true) {
                    String[] str = message.split(Constants.SPLIT_STRING_CEATE_LANG);
                    projectId = str[0];
                    html = str[1];
                }
            }

            if (isComplete) {
                TranslationMemoryToolFactory.getLongRunningTaskManager().stopTrackingLongRunningTask(LongRunningTaskId.valueOf(taskId));
                taskId = StringUtils.EMPTY;
                message = StringUtils.EMPTY;
            }

            resultBean.put("isComplete", isComplete);
            resultBean.put("percentage", percentage);
            resultBean.put("message", message);
            resultBean.put("urlPath", urlPath);
            resultBean.put("json", json);
            resultBean.put("projectId", projectId);
            resultBean.put("html", html);
            resultBean.put("error", error);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
        }

        return SUCCESS;
    }
    
}
