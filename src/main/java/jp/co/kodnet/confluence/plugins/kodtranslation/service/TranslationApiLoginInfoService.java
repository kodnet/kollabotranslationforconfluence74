package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.Date;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;

@Transactional
public interface TranslationApiLoginInfoService {
    
    List<ApiLoginInfo> getAll();
    
    ApiLoginInfo getOne(String translationToolId);
      
    ApiLoginInfo add(
            String translationToolId,
            String translationToolName,
            String userName,
            String password,
            String loggedToken,
            String ortherKey01,
            String OrtherKey02,
            Date dateExpires
            );
    
    ApiLoginInfo update(
            String translationToolId,
            String translationToolName,
            String userName,
            String password,
            String loggedToken,
            String ortherKey01,
            String OrtherKey02,
            Date dateExpires
            );
    
    void delete(String translationToolId);
    
    void deleteAll();  
}
