package jp.co.kodnet.confluence.plugins.kodtranslation.common;

import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.json.json.Json;
import com.atlassian.confluence.json.json.JsonObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.security.ContentPermissionSet;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.opensymphony.webwork.ServletActionContext;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.DocumentUtil;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author TamPT
 */
public class TranslationMemoryFunction {

    // <editor-fold defaultstate="collapsed" desc="Page">
    public static Long getPageId(HttpServletRequest request, PageManager pageManager) throws Exception {
        // リクエストを取得する。
        if (request == null) {
            return null;
        }
        URL url = new URL(request.getRequestURL().toString());
        String[] pathComponents = url.getPath().split("/");
        if (url.getPath().contains("/display/") && pathComponents.length >= 3) {
            String pageIdFromUrl = getPageIdFromUrl(pageManager);
            if (pageIdFromUrl == null) {
                return null;
            }
            return Long.parseLong(pageIdFromUrl);
        } else if (url.getPath().endsWith("/viewpage.action")) {
            // ページIDを取得する。
            String pageIds[] = request.getParameterValues("pageId");
            if (pageIds != null && pageIds.length >= 1) {
                // ページタイトルに日本語が含まれている場合（requestがパラメータとしてpageIdを持っていた場合）
                return Long.parseLong(pageIds[0]);
            }

            String spaceKeys[] = request.getParameterValues("spaceKey");
            String titles[] = request.getParameterValues("title");

            if (spaceKeys != null && spaceKeys.length >= 1
                    && titles != null && titles.length >= 1) {
                // スペースキーとページタイトルで指定されてた場合
                Page page = pageManager.getPage(spaceKeys[0], titles[0]);
                if (page != null) {
                    return page.getId();
                }
            }

            // その他（ページ不明）
            return null;
        } else {
            // 対象外のページ
            return null;
        }
    }

    public static String getPageIdFromUrl(PageManager pageManager) {
        HttpServletRequest request = ServletActionContext.getRequest();
        String url = request.getRequestURL().toString();
        String[] urlArray = url.split("/");
        if (urlArray.length < 2) {
            return null;
        }

        String spaceKey = GeneralUtil.urlDecode(urlArray[urlArray.length - 2]);
        String pageTitle = GeneralUtil.urlDecode(urlArray[urlArray.length - 1]);
        Page page = pageManager.getPage(spaceKey, pageTitle);
        if (page != null) {
            return page.getIdAsString();
        }

        if (spaceKey.equalsIgnoreCase("display")) {
            SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
            Space space = spaceManager.getSpace(pageTitle);
            if (space != null) {
                return space.getHomePage().getIdAsString();
            }
        }
        return null;
    }

    public static String getPageTitleDecode(String pageTitle) {
        pageTitle = pageTitle.replaceAll("\\\\0", "");
        return pageTitle.replaceAll("[\\\\\\\\/:*?\\\"<>|]", "");
    }

    /**
     * スペースキーで翻訳を取得する。
     *
     * @param spaceKey
     * @return
     */
    public static String getTranslateKeyFromSpaceKey(String spaceKey) throws Exception {
        int indexOfLang = 0;
        String translateKey = "";
        String regex = "V\\d{3}L[a-z]{2,12}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(spaceKey);
        while (matcher.find() == true) {
            indexOfLang = matcher.start();
        }

        if (indexOfLang > 0) {
            translateKey = spaceKey.substring(0, indexOfLang);
        }

        return translateKey;
    }

    public static String getSpaceNameOriginal(String spaceName) throws Exception {
        String spaceNameResult = "";
        Pattern regex = Pattern.compile("_v[0-9]{1,}(_\\w{1,}){1,}$");
        Matcher matcher = regex.matcher(spaceName);
        if (matcher.find() == true) {
            spaceNameResult = matcher.replaceAll("");
        } else {
            spaceNameResult = spaceName;
        }
        return spaceNameResult;
    }

    public static String getSpaceNameOriginalMarketPlace(Space space) throws Exception {
//        LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
//        String spaceNameResult = "";
//        Language lang = languageService.getLanguageBySpaceKey(space.getKey());
//        if (lang == null) {
//            return space.getName();
//        }
//
//        if (space.getName().endsWith("_" + lang.getLanguageKey())) {
//            spaceNameResult = replaceLast(space.getName(), "_" + lang.getLanguageKey(), "");
//            return spaceNameResult;
//        }

        return space.getName();
    }

    private static String replaceLast(String string, String toReplace, String replacement) throws Exception {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }

    /**
     * ページ順
     *
     * @param translationKey
     * @param version
     */
    public static void proccessSetPagePosition(String translationKey, String version) throws Exception {
//        LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
//        SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
//        //ベース言語の取得
//        Language base = languageService.getBaseLanguage(translationKey, version);
//        //スペース取得
//        Space baseSpace = spaceManager.getSpace(base.getSpaceKey());
//        //ホームページ取得
//        Page homePage = baseSpace.getHomePage();
//
//        //ペース言語のページ位置を定義する。
//        proccessSetPagePositionBaseLanguage(homePage);
//
//        //先言語のページ位置を定義する。
//        proccessSetPagePositionLanguage(translationKey, version, homePage);
    }

    /**
     * ペース言語のページ位置を定義する。
     *
     * @param homePage
     */
    public static void proccessSetPagePositionBaseLanguage(Page homePage) throws Exception {
        //子ページ存在のチェック
        if (homePage.hasChildren() == false) {
            return;
        }

        //子ページ定義
        int indexPosition = 1;
        for (Page page : homePage.getSortedChildren()) {
            page.setPosition(indexPosition);
            //孫ページ位置を定義する。
            proccessSetPagePositionBaseLanguage(page);
            indexPosition++;
        }
    }

    /**
     * 先言語のページ位置を定義する。
     *
     * @param translationKey
     * @param ver
     * @param homePage
     */
    public static void proccessSetPagePositionLanguage(String translationKey, String ver, Page homePage) throws Exception {
        //子ページ存在のチェック
        if (homePage.hasChildren() == false) {
            return;
        }

        //子ページ位置を定義する。
        //TranslationPageHistoryService translationPageHistoryService = TranslationMemoryToolFactory.getTranslationPageHistoryService();
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        //List<TranslationHistory> tranHis = null;
        for (Page page : homePage.getChildren()) {
            //tranHis = translationPageHistoryService.getListTranslationPageHistoryByBasePageId(translationKey, ver, page.getIdAsString());
            //for (TranslationHistory his : tranHis) {
            //    Page curPage = pageManager.getPage(Long.valueOf(his.getPageId()));
            //    curPage.setPosition(page.getPosition());
            //}
        }

        //孫ページ位置を定義する。
        for (Page page : homePage.getChildren()) {
            proccessSetPagePositionLanguage(translationKey, ver, page);
        }
    }

    /**
     * ホームページからページツリーを取得する
     *
     * @param rootPage ホームページ
     * @return
     */
    public static List<Page> getPageTreeFromHome(Page rootPage) {
        List<Page> result = new ArrayList<Page>();
        if (rootPage == null) {
            return result;
        }
        List<Page> pages = rootPage.getSortedChildren();

        for (Page p : pages) {
            result.add(p);
            result.addAll(getPageTreeFromHome(p));
        }
        return result;
    }

    /**
     * ホームページからページツリーを取得する
     *
     * @param rootPage ホームページ
     * @return
     */
    public static List<Page> getPageTreeFromSpace(String translationKey, String spaceKey) {
        List<Page> result = new ArrayList<Page>();
        SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        Space space = spaceManager.getSpace(spaceKey);
        if (space == null) {
            return result;
        }

        //TranslationService translationService = TranslationMemoryToolFactory.getTranslationService();
        //Translation translation = translationService.getTranslationByKey(translationKey);
        boolean translationHome = false;
//        if (translation != null && translation.getTranslationHome() != null) {
//            translationHome = translation.getTranslationHome();
//        }

        List<Page> pages = pageManager.getTopLevelPages(space);
        for (Page page : pages) {
            if ((page.isHomePage() && translationHome) || page.isHomePage() == false) {
                result.add(page);
            }

            result.addAll(getPageTreeFromHome(page));
        }

        return result;
    }

    /**
     * タイトルの変更
     *
     * @param lstPage
     * @param title
     * @param l
     * @param pageId
     * @return
     */
    public static String changeTitle(List<Page> lstPage, String title, long l, String pageId) {
        String suffix = "_";
        if (!title.contains(suffix)) {
            for (Page pages : lstPage) {
                try {
                    if (pages.getTitle().equalsIgnoreCase(title) && !pages.getIdAsString().equals(pageId)) {
                        title = title + suffix + l;
                        return changeTitle(lstPage, title, l++, pageId);
                    }
                } catch (Exception ex) {
                    MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + pages.getIdAsString());
                    continue;
                }
            }
        } else {
            int index = title.lastIndexOf(suffix);
            String titleAfter = title.substring(0, index);
            String fix = title.substring(index + 1);
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(fix);
            if (matcher.matches()) {
                l = Long.parseLong(fix);
                for (Page pages : lstPage) {
                    try {
                        if (pages.getTitle().equalsIgnoreCase(title) && !pages.getIdAsString().equals(pageId)) {
                            l = l + 1;
                            title = titleAfter + suffix + l;
                            return changeTitle(lstPage, title, l, pageId);
                        }
                    } catch (Exception ex) {
                        MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + pages.getIdAsString());
                        continue;
                    }
                }
            } else {
                for (Page pages : lstPage) {
                    try {
                        if (pages.getTitle().equalsIgnoreCase(title) && !pages.getIdAsString().equals(pageId)) {
                            title = title + suffix + l;
                            return changeTitle(lstPage, title, l++, pageId);
                        }
                    } catch (Exception ex) {
                        MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + pages.getIdAsString());
                        continue;
                    }
                }
            }
        }
        return title;
    }

    public static String getLanguageStatus(int status) {
        String strStatus = "";
        switch (status) {
            case Constants.STATUS_BASE:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.base";
                break;
            case Constants.STATUS_COPPYED:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.copy";
                break;
            case Constants.STATUS_NOT_TRANSLATE:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.not.translate";
                break;
            case Constants.STATUS_TRANSLATING:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.translating";
                break;
            case Constants.STATUS_TRANSLATTED:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.translated";
                break;
            case Constants.STATUS_ADDNEW:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.addnew";
                break;
            case Constants.STATUS_DELETE:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.delete";
                break;
            case Constants.STATUS_COMPLETED_BY_LINGUIST:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.imported";
                break;
            case Constants.STATUS_ASSIGNED_BY_LINGUIST:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.translating";
                break;
            case Constants.STATUS_CAN_NOT_TRANSLATE:
                strStatus = "kod.plugins.kodtranslationdirectory.translation.status.can.not.translate";
                break;
        }
        return strStatus;
    }

    /**
     *
     * @param pages
     * @param toLangSpaceKey
     * @return
     */
    public static List<Page> getPageRefs(List<Page> pages, String toLangSpaceKey) throws Exception {
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        //TranslationPageHistoryService translationPageHistoryService = TranslationMemoryToolFactory.getTranslationPageHistoryService();

        //List<PageRefInfoBean> listPageRef = new ArrayList<PageRefInfoBean>();
        List<Page> listPage = new ArrayList<Page>();
        List<Page> listTemp = new ArrayList<Page>();
//        for (Page page : pages) {
//            listPageRef = DocumentUtil.getReferencePageInPage(page);
//            if (!listPageRef.isEmpty()) {
//                //loop change page
//                for (PageRefInfoBean pageRefInfo : listPageRef) {
//                    if (pageRefInfo.getSpaceKey().equals(page.getSpaceKey())) {
//                        Page pageTemp = pageManager.getPage(pageRefInfo.getSpaceKey(), pageRefInfo.getPageTitle());
//                        if (pageTemp == null) {
//                            continue;
//                        }
//                        TranslationHistory translationHistorySec = translationPageHistoryService.getTransPageHistoryByFromPageId(pageTemp.getIdAsString(), toLangSpaceKey);
//                        if (translationHistorySec == null) {
//                            listTemp.add(pageTemp);
//                        }
//                    }
//                }
//
//                if (!listTemp.isEmpty()) {
//                    listPage = TranslationMemoryFunction.getPageRefs(listTemp, toLangSpaceKey);
//                    for (Page pageTT : listPage) {
//                        if (!listTemp.contains(pageTT)) {
//                            listTemp.add(pageTT);
//                        }
//                    }
//                }
//            }
//        }

        return listTemp;
    }

    public static Boolean isReferencePage(String pageId) {
        //TranslationVersionHistoryService historyService = TranslationMemoryToolFactory.getTranslationVersionHistoryService();
        Boolean isReferenceTarget = false;
        //VersionPageHistory versionHistory = historyService.getVesionByPageId(pageId);
        //if (versionHistory != null) {
         //   isReferenceTarget = (versionHistory.getActionStatus() == Constants.VERSION_STATUS_REFERENCE);
        //}

        return isReferenceTarget;
    }
    
    public static Boolean isGoogleTool() {
        //TranslationMemoryStatusService translationMemoryStatusService = TranslationMemoryToolFactory.getTranslationMemoryStatusService();
        //TransMemoryStatus transMemoryStatus = translationMemoryStatusService.get();
        //return transMemoryStatus.getInusetmemID() == 4;
        
        return true;
    }
    
    public static Boolean isMemsourceTool() {
//        TranslationMemoryStatusService translationMemoryStatusService = TranslationMemoryToolFactory.getTranslationMemoryStatusService();
//        TransMemoryStatus transMemoryStatus = translationMemoryStatusService.get();
//        return transMemoryStatus.getInusetmemID() == 1;
        return true;
    }

//    public static MemsourceJob getMemsourceJobReference(String pageId) {
//        MemsourceJobService memsourceJobService = TranslationMemoryToolFactory.getMemsourceJobService();
//        TranslationVersionHistoryService historyService = TranslationMemoryToolFactory.getTranslationVersionHistoryService();
//        VersionPageHistory versionHistory = historyService.getVesionByPageId(pageId);
//        if (versionHistory != null && versionHistory.getActionStatus() == Constants.VERSION_STATUS_REFERENCE) {
//            return getMemsourceJobReference(versionHistory.getFromPageId());
//        }
//
//        MemsourceJob lastJobsOfPage = memsourceJobService.getLastJobByPageId(pageId);
//        if (lastJobsOfPage != null) {
//            return lastJobsOfPage;
//        }
//
//        return null;
//    }

//    public static int getStatusOfPage(String conPageId) {
//        MemsourceJob lastJobsOfPage = getMemsourceJobReference(conPageId);
//        if (lastJobsOfPage == null) {
//            return Constants.STATUS_NOT_TRANSLATE;
//        }
//
//        return Integer.valueOf(lastJobsOfPage.getStatus());
//    }

    public static void copyComment(Page page, Comment fromCmt, Comment parentCmt) throws Exception {
        if (fromCmt == null) {
            return;
        }
        CommentManager commentManager = TranslationMemoryToolFactory.getCommentManager();
        Comment comment = commentManager.addCommentToObject(page, parentCmt, fromCmt.getBodyAsString());
        comment.setCreator(fromCmt.getCreator());
        comment.setCreationDate(fromCmt.getLastModificationDate());
        commentManager.saveContentEntity(comment, null);
        if (!fromCmt.getChildren().isEmpty()) {
            for (Comment child : fromCmt.getChildren()) {
                copyComment(page, child, comment);
            }
        }
    }

    public static void deleteComment(Comment comment) throws Exception {
        if (comment == null) {
            return;
        }
        if (!comment.getChildren().isEmpty()) {
            for (Comment c : comment.getChildren()) {
                deleteComment(c);
            }
        }

        CommentManager commentManager = TranslationMemoryToolFactory.getCommentManager();
        commentManager.removeContentEntity(comment);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Translation">
    public static List<Long> getListExcute(String spaceKey, String fromLanguageKey, String toLanguageKey) throws Exception {
        SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
        Space space = spaceManager.getSpace(spaceKey);
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        List<Page> pages = pageManager.getTopLevelPages(space);
        return getListPageExcute(pages, fromLanguageKey, toLanguageKey, true);
    }

    public static List<Long> getListPageExcute(List<Page> pages, String fromLanguageKey, String toLanguageKey, boolean canExecute) throws Exception {
        List<Long> result = new ArrayList<Long>();
        for (Page page : pages) {
            if (page == null) {
                continue;
            }

            boolean isExcute = TranslationMemoryFunction.isExcuteLanguage(page.getId(), toLanguageKey, toLanguageKey);
            if (canExecute && isExcute) {
                result.add(page.getId());
            }

            if (page.hasChildren()) {
                result.addAll(getListPageExcute(page.getSortedChildren(), fromLanguageKey, toLanguageKey, canExecute && isExcute));
            }
        }

        return result;
    }

    public static boolean isExcuteLanguage(Long pageId, String fromLanguage, String toLanguage) throws Exception {
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        Page page = pageManager.getPage(pageId);
        if (page == null) {
            return false;
        }

        //DetailsTranslationMemoryToolUtil util = TranslationMemoryToolFactory.getDetailsTranslationMemoryUtil();
        //Map<String, String> mapLanguage = util.getLanguageSupport();
        boolean result = true;
        for (Label label : page.getLabels()) {
            String key = label.getName();
            if (key.toLowerCase().endsWith(Constants.BASE_SUBFIX)) {
                key = key.substring(0, key.length() - Constants.BASE_SUBFIX.length());
//                if (!mapLanguage.containsKey(key)) {
//                    continue;
//                }

                if (key.equalsIgnoreCase(toLanguage)) {
                    return true;
                }

                result = false;
            }
        }

        for (Label label : page.getLabels()) {
            String key = label.getName();
//            if (!mapLanguage.containsKey(key)) {
//                continue;
//            }

            if (key.equalsIgnoreCase(toLanguage)) {
                return true;
            }

            result = false;
        }

        return result;
    }
    
    public static boolean isNoTranslation(Long pageId) {
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        Page page = pageManager.getPage(pageId);
        if (page == null) {
            return true;
        }

        for (Label label : page.getLabels()) {
            String key = label.getName();
            if (key.equalsIgnoreCase(Constants.NO_TRANSLATION)) {
                return true;
            }
        }

        return false;
    }

    public static void updateLanguageStatus(String translationKey, String version, String toLanguageKey) {
//        TranslationPageHistoryService translationPageHistoryService = TranslationMemoryToolFactory.getTranslationPageHistoryService();
//        LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
//        Language language = languageService.getLanguage(translationKey, version, toLanguageKey);
//        if (language == null || language.getStatus() == Constants.STATUS_BASE) {
//            return;
//        }
//        List<TranslationHistory> listHistory = translationPageHistoryService.getListTransPageHistoryBySpaceKey(language.getSpaceKey());
//        int countTranslated = 0;
//        int countImported = 0;
//        int countTranslating = 0;
//        for (TranslationHistory translationHistory : listHistory) {
//            String pageId = translationHistory.getPageId();
//            MemsourceJob lastJob = getMemsourceJobReference(pageId);
//            if (lastJob == null) {
//                boolean isNoTranslation = TranslationMemoryFunction.isNoTranslation(Long.valueOf(pageId));
//                if (isNoTranslation) {
//                    countTranslated++;
//                }
//
//                continue;
//            }
//
//            switch (Integer.valueOf(lastJob.getStatus())) {
//                case Constants.STATUS_TRANSLATTED:
//                    countTranslated++;
//                    break;
//                case Constants.STATUS_COMPLETED_BY_LINGUIST:
//                    countImported++;
//                    break;
//                case Constants.STATUS_TRANSLATING:
//                    countTranslating++;
//                    break;
//            }
//        }
//
//        if (countTranslated == listHistory.size()) {
//            languageService.updateStatus(translationKey, version, toLanguageKey, Constants.STATUS_TRANSLATTED);
//        } else if (countTranslated == 0 && countTranslating == 0 && countImported == 0) {
//            languageService.updateStatus(translationKey, version, toLanguageKey, Constants.STATUS_NOT_TRANSLATE);
//        } else {
//            languageService.updateStatus(translationKey, version, toLanguageKey, Constants.STATUS_TRANSLATING);
//        }
    }

    public static void updateSpacePermission(String spaceKey) {
//        LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
//        SpacePermissionManager spacePermissonManager = TranslationMemoryToolFactory.getSpacePermissionManager();
//        SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
//        Language language = languageService.getLanguageBySpaceKey(spaceKey);
//        if (language == null) {
//            return;
//        }
//
//        Space sourceSpace = spaceManager.getSpace(spaceKey);
//        List<Language> languages = languageService.getListTargetLanguageByFromLanguage(language.getTranslationKey(), language.getVersion(), language.getLanguageKey());
//        if (languages.isEmpty()) {
//            return;
//        }
//
//        List<SpacePermission> permissions = sourceSpace.getPermissions();
//        for (Language lang : languages) {
//            String key = lang.getSpaceKey();
//            Space space = spaceManager.getSpace(key);
//            spacePermissonManager.removeAllPermissions(space);
//            for (SpacePermission permission : permissions) {
//                SpacePermission copyPermission = new SpacePermission(permission);
//                if (copyPermission != null) {
//                    space.addPermission(copyPermission);
//                    spacePermissonManager.savePermission(copyPermission);
//                }
//            }
//            updateSpacePermission(key);
//        }
    }

    public static void updatePagePermission(String fromPageId) {
//        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
//        TranslationPageHistoryService translationPageHistoryService = TranslationMemoryToolFactory.getTranslationPageHistoryService();
//        List<TranslationHistory> translationHistorys = translationPageHistoryService.getListTransPageHistoryByFromPageId(fromPageId);
//        if (translationHistorys.isEmpty()) {
//            return;
//        }
//
//        Page fromPage = pageManager.getPage(Long.valueOf(fromPageId));
//        List<ContentPermission> permissions = getAllPermissionFromPage(fromPage);
//
//        for (TranslationHistory translationHistory : translationHistorys) {
//            Page toPage = pageManager.getPage(Long.valueOf(translationHistory.getPageId()));
//            if (toPage == null) {
//                continue;
//            }
//
////            List<ContentPermission> permissionToPages = getAllPermissionFromPage(toPage);
////            for (ContentPermission permissionToPage : permissionToPages) {
////                contentPermissionManager.removeContentPermission(permissionToPage);
////            }
//            updateContentPermission(permissions, toPage, false);
//        }
    }

    public static List<ContentPermission> getAllPermissionFromPage(Page page) {
        ContentPermissionManager contentPermissionManager = TranslationMemoryToolFactory.getContentPermissionManager();
        List<ContentPermissionSet> editSets = contentPermissionManager.getContentPermissionSets(page, ContentPermission.EDIT_PERMISSION);
        List<ContentPermissionSet> viewSets = contentPermissionManager.getContentPermissionSets(page, ContentPermission.VIEW_PERMISSION);
        List<ContentPermissionSet> shareSets = contentPermissionManager.getContentPermissionSets(page, ContentPermission.SHARED_PERMISSION);
        List<ContentPermission> result = new ArrayList<ContentPermission>();
        for (ContentPermissionSet cps : editSets) {
            for (ContentPermission cp : cps) {
                result.add(cp);
            }
        }
        for (ContentPermissionSet cps : viewSets) {
            for (ContentPermission cp : cps) {
                result.add(cp);
            }
        }
        for (ContentPermissionSet cps : shareSets) {
            for (ContentPermission cp : cps) {
                result.add(cp);
            }
        }
        return result;
    }

    public static void updateContentPermission(List<ContentPermission> contentPermissions, Page toPage, boolean isNewPage) {
        ContentPermissionManager contentPermissionManager = TranslationMemoryToolFactory.getContentPermissionManager();
        List<ContentPermission> viewList = new ArrayList<ContentPermission>();
        List<ContentPermission> editList = new ArrayList<ContentPermission>();
        List<ContentPermission> shareList = new ArrayList<ContentPermission>();
        for (ContentPermission permission : contentPermissions) {
            ContentPermission contentPermission = new ContentPermission(permission);
            if (contentPermission != null) {
                if (isNewPage) {
                    toPage.addPermission(contentPermission);
                } else {
                    if (contentPermission.getType().equals(ContentPermission.VIEW_PERMISSION)) {
                        viewList.add(contentPermission);
                    } else if (contentPermission.getType().equals(ContentPermission.EDIT_PERMISSION)) {
                        editList.add(contentPermission);
                    } else if (contentPermission.getType().equals(ContentPermission.SHARED_PERMISSION)) {
                        shareList.add(contentPermission);
                    }
                }
            }
        }
        if (!isNewPage) {
            contentPermissionManager.setContentPermissions(viewList, toPage, ContentPermission.VIEW_PERMISSION);
            contentPermissionManager.setContentPermissions(editList, toPage, ContentPermission.EDIT_PERMISSION);
            contentPermissionManager.setContentPermissions(shareList, toPage, ContentPermission.SHARED_PERMISSION);
        }
    }

    public static boolean hasDrawIoMacros(Page page) throws Exception {
        try {
            String bodyAsString = page.getBodyAsString();
            Document document = DocumentUtil.convertStringToDocument(bodyAsString, false);
            NodeList nodes = document.getElementsByTagName(Constants.AC_STRUCTURED_MACRO);
            for (int i = 0; i < nodes.getLength(); i++) {
                Element drawio = (Element) nodes.item(i);
                String acName = drawio.getAttribute(Constants.AC_NAME);
                if (acName == null || acName.isEmpty()) {
                    continue;
                }

                if (acName.equals(Constants.DRAWIO)) {
                    return true;
                }
            }

            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static List<Json> getListInfoDrawIOs(Page page) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        List<Json> replaceDiagrams = new ArrayList<Json>();
        try {
            String bodyAsString = page.getBodyAsString();
            Document document = DocumentUtil.convertStringToDocument(bodyAsString, false);
            NodeList nodes = document.getElementsByTagName(Constants.AC_STRUCTURED_MACRO);
            for (int i = 0; i < nodes.getLength(); i++) {
                Element drawio = (Element) nodes.item(i);
                String acName = drawio.getAttribute(Constants.AC_NAME);
                if (acName == null || acName.isEmpty()) {
                    continue;
                }

                if (!acName.equals(Constants.DRAWIO)) {
                    continue;
                }

                NodeList parameters = drawio.getElementsByTagName(Constants.AC_PARAMETER);
                if (parameters.getLength() <= 0) {
                    continue;
                }

                String diagramName = "";
                JsonObject drawioObject = new JsonObject();
                for (int j = 0; j < parameters.getLength(); j++) {
                    Element currentParam = (Element) parameters.item(j);
                    String acNameParam = currentParam.getAttribute(Constants.AC_NAME);
                    if (acNameParam == null || acNameParam.isEmpty()) {
                        continue;
                    }

                    if (acNameParam.equals(Constants.MACRO_DIAGRAM_NAME)) {
                        diagramName = currentParam.getTextContent();
                    }
                }
                if (diagramName.equals("")) {
                    continue;
                }

                // Background
                Attachment backgroundImg = attachmentManager.getAttachmentDao().getLatestAttachment(page, diagramName + Constants.EXT_PNG);
                if (backgroundImg == null) {
                    continue;
                }
                int latestRevision = backgroundImg.getVersion();

                // DrawIO File (non extension file)
                Attachment diagramFile = attachmentManager.getAttachmentDao().getLatestAttachment(page, diagramName);
                if (diagramFile == null) {
                    continue;
                }

                drawioObject.setProperty(Constants.IMG_CURRENT_VER, latestRevision);
                drawioObject.setProperty(Constants.DIAGRAM_CURRENT_VER, diagramFile.getVersion());
                drawioObject.setProperty(Constants.DRAWIO_IMAGE_NAME, diagramName + Constants.EXT_PNG);
                drawioObject.setProperty(Constants.PAGE_ID, page.getIdAsString());
                drawioObject.setProperty(Constants.DIAGRAM_FILE_NAME, diagramName);
                drawioObject.setProperty(Constants.REVISION, diagramFile.getVersion());
                drawioObject.setProperty(Constants.ATTACHMENT_ID, diagramFile.getId());
                replaceDiagrams.add(drawioObject);
            }

            return replaceDiagrams;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(TranslationMemoryFunction.class, e);
            return replaceDiagrams;
        }
    }

    public static String updateDrawIoMacroRevision(String body, String diagramName, int latestRevision) throws Exception {
        if (body.isEmpty()) {
            return body;
        }

        Document document = DocumentUtil.convertStringToDocument(body, false);
        if (document == null) {
            return body;
        }

        NodeList nodes = document.getElementsByTagName(Constants.AC_STRUCTURED_MACRO);
        if (nodes.getLength() <= 0) {
            return body;
        }

        for (int i = 0; i < nodes.getLength(); i++) {
            Element drawio = (Element) nodes.item(i);
            String acName = drawio.getAttribute(Constants.AC_NAME);
            if (acName == null || acName.isEmpty()) {
                continue;
            }

            if (!acName.equals(Constants.DRAWIO)) {
                continue;
            }

            NodeList parameters = drawio.getElementsByTagName(Constants.AC_PARAMETER);
            if (parameters.getLength() <= 0) {
                continue;
            }

            for (int j = 0; j < parameters.getLength(); j++) {
                Element currentParam = (Element) parameters.item(j);
                String acNameParam = currentParam.getAttribute(Constants.AC_NAME);
                if (acNameParam == null || acNameParam.isEmpty()) {
                    continue;
                }

                if (acNameParam.equals(Constants.MACRO_DIAGRAM_NAME)) {
                    if (!currentParam.getTextContent().equals(diagramName)) {
                        break;
                    }
                }

                if (acNameParam.equals(Constants.REVISION)) {
                    currentParam.setTextContent(String.valueOf(latestRevision));
                    body = DocumentUtil.convertXmlToString(document, false, true);
                    body = body.replace("<kod>", "").replace("</kod>", "");
                    return body;
                }
            }
        }
        return body;
    }
    
    public static List<String> getListLangChild (String translationKey, String version, String fromLanguage){
        List<String> lstLangKey = new ArrayList<String>();
//        LanguageService languageService = TranslationMemoryToolFactory.getLanguageService();
//        List<Language> langs = languageService.getListTargetLanguageByFromLanguage(translationKey, version, fromLanguage);
//        for (Language lang : langs) {
//            lstLangKey.add(lang.getLanguageKey());
//            lstLangKey.addAll(getListLangChild(translationKey, version, lang.getLanguageKey()));
//        }
        return lstLangKey;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Attachment">
    /**
     * 最新バージョンの添付ファイルを取得する
     *
     * @param page 添付のページ
     * @param fileName ファイル名
     * @return
     */
    public static Attachment getLastVersionAttachment(Page page, String fileName) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        Attachment attach = attachmentManager.getAttachment(page, fileName);
        if (attach == null) {
            return null;
        }

        List<Attachment> listAtt = attachmentManager.getAllVersions(attach);
        for (Attachment attachment : listAtt) {
            if (attachment.getVersion() > attach.getVersion()) {
                attach = attachment;
            }
        }

        return attach;
    }

    /**
     * 添付ファイルのコピーを処理する
     *
     * @param page 新しページ
     * @param oldPage 古いページ
     */
    public static void copyAttachment(Page page, Page oldPage) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        List<Attachment> listAttachment = oldPage.getLatestVersionsOfAttachments();
        if (listAttachment.size() > 0) {
            for (int i = 0; i < listAttachment.size(); i++) {
                Attachment baseAttachment = listAttachment.get(i);
                try {
                    List<Attachment> listBaseAttachment = attachmentManager.getAllVersions(baseAttachment);
                    Collections.sort(listBaseAttachment, new Comparator<Attachment>() {
                        @Override
                        public int compare(Attachment o1, Attachment o2) {
                            if (o1 == null) {
                                return 1;
                            }

                            if (o2 == null) {
                                return -1;
                            }

                            return Integer.compare(o1.getVersion(), o2.getVersion());
                        }
                    });

                    int currentIndex = -1;
                    for (int j = 0; j < listBaseAttachment.size(); j++) {
                        baseAttachment = listBaseAttachment.get(j);
                        if (baseAttachment == null) {
                            continue;
                        }

                        if (baseAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        Attachment newAttachment = new Attachment(baseAttachment.getFileName(), baseAttachment.getContentType(), baseAttachment.getFileSize(), null);
                        Attachment oldAttachment = null;
                        int index = baseAttachment.getVersion();
                        if (currentIndex == -1) {
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setVersion(index);
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            page.addAttachment(newAttachment);
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e1) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            currentIndex = newAttachment.getVersion();
                            continue;
                        }

                        index = currentIndex;
                        newAttachment = null;
                        newAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), currentIndex);
                        if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        for (int k = index; k < baseAttachment.getVersion(); k++) {
                            if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            oldAttachment = (Attachment) newAttachment.clone();
                            oldAttachment.convertToHistoricalVersion();
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e2) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            currentIndex = newAttachment.getVersion();
                        }

                        for (int k = index + 1; k < baseAttachment.getVersion(); k++) {
                            oldAttachment = null;
                            oldAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), k);
                            if (oldAttachment == null || oldAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            attachmentManager.removeAttachmentVersionFromServer(oldAttachment);
                        }
                    }
                } catch (Exception ex) {
                    throw new RuntimeException("Copy attachment fail", ex);
                }
            }
        }
    }

    public static void copyAttachmentAddLanguage(Page page, Page oldPage, boolean isImagedirectory) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        List<Attachment> listAttachment = oldPage.getLatestVersionsOfAttachments();
        List<String> listImagesDrawio = new ArrayList<String>();
        List<String> listAttrDrawios = new ArrayList<String>();
        if (isImagedirectory) {
            listImagesDrawio = CommonFunction.getListImageNeedInPage(oldPage, true);
            listAttrDrawios = CommonFunction.getListAttrDrawioInPage(oldPage, true);
        }
        if (listAttachment.size() > 0) {
            for (int i = 0; i < listAttachment.size(); i++) {
                Attachment baseAttachment = listAttachment.get(i);
                String attrName = baseAttachment.getFileName();
//                if (checkCanCopyAttachmentIsImageDirectory(isImagedirectory, listImagesDrawio, listAttrDrawios, oldPage, attrName) == false) {
//                    continue;
//                }
                try {
                    List<Attachment> listBaseAttachment = attachmentManager.getAllVersions(baseAttachment);
                    Collections.sort(listBaseAttachment, new Comparator<Attachment>() {
                        @Override
                        public int compare(Attachment o1, Attachment o2) {
                            if (o1 == null) {
                                return 1;
                            }

                            if (o2 == null) {
                                return -1;
                            }

                            return Integer.compare(o1.getVersion(), o2.getVersion());
                        }
                    });

                    int currentIndex = -1;
                    for (int j = 0; j < listBaseAttachment.size(); j++) {
                        baseAttachment = listBaseAttachment.get(j);
                        if (baseAttachment == null) {
                            continue;
                        }

                        if (baseAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        Attachment newAttachment = new Attachment(baseAttachment.getFileName(), baseAttachment.getContentType(), baseAttachment.getFileSize(), null);
                        Attachment oldAttachment = null;
                        int index = baseAttachment.getVersion();
                        if (currentIndex == -1) {
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setVersion(index);
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            page.addAttachment(newAttachment);
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e1) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            currentIndex = newAttachment.getVersion();
                            continue;
                        }

                        index = currentIndex;
                        newAttachment = null;
                        newAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), currentIndex);
                        if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        for (int k = index; k < baseAttachment.getVersion(); k++) {
                            if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            oldAttachment = (Attachment) newAttachment.clone();
                            oldAttachment.convertToHistoricalVersion();
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e2) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            currentIndex = newAttachment.getVersion();
                        }

                        for (int k = index + 1; k < baseAttachment.getVersion(); k++) {
                            oldAttachment = null;
                            oldAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), k);
                            if (oldAttachment == null || oldAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            attachmentManager.removeAttachmentVersionFromServer(oldAttachment);
                        }
                    }
                } catch (Exception ex) {
                    throw new RuntimeException("Copy attachment fail", ex);
                }
            }
        }
    }
    
    public static boolean checkCanCopyAttachmentIsImageDirectory(boolean isImagedirectory, List<String> listImagesDrawio, List<String> listAttrDrawios, Page page, String attrName){
        if (!isImagedirectory){
            return true;
        }
        if (listImagesDrawio.contains(attrName)) {
            return true;
        }
        if (listAttrDrawios.contains(attrName)) {
            return true;
        }
        if (CommonFunction.checkHaveVisualIndex(page) == true) {
            return true;
        }

        return false;
    }
    
    /**
     * 添付の編集を処理する
     *
     * @param page
     * @param attachment
     * @param dataText
     * @throws Exception
     */
    public static void updateAttachment(Page page, Attachment attachment, String dataText) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        byte[] dataByte = dataText.getBytes("utf-8");
        InputStream is = new ByteArrayInputStream(dataByte);
        attachment.setFileSize(dataByte.length);
        attachmentManager.getAttachmentDao().replaceAttachmentData(attachment, is);
    }

    public static Integer getSizeInputStream(InputStream in) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int b;
        while ((b = in.read()) != -1) {
            os.write(b);
        }

        return os.size();
    }
    // </editor-fold>
}
