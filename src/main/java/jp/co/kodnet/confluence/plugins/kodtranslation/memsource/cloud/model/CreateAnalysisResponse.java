package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.AsyncMutilInfo;

/**
 *
 * @author baoph
 */
public class CreateAnalysisResponse extends ResponseModelBase {

    private AsyncMutilInfo[] asyncRequests;

    public AsyncMutilInfo[] getAsyncRequests() {
        return asyncRequests;
    }

    public void setAsyncRequests(AsyncMutilInfo[] asyncRequests) {
        this.asyncRequests = asyncRequests;
    }

}
