package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author hailc
 */
public class ProjectGetRequest extends RequestModelBase {

    private String project;

    public String getProject() {
        return project;
    }

    public void setProject(String projectUid) {
        this.project = projectUid;
    }

}
