package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author anhlq
 */
public class WorkflowStep {

    private String id;
    private String name;
    private String abbr;
    private String order;
    private String workflowLevel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getWorkflowLevel() {
        return workflowLevel;
    }

    public void setWorkflowLevel(String workflowLevel) {
        this.workflowLevel = workflowLevel;
    }
}
