package jp.co.kodnet.confluence.plugins.kodtranslation.xml;

import com.atlassian.confluence.content.render.xhtml.Namespace;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author lucvd
 */
public class XMLUtils {

    private static final String ENCODING_UTF8 = "UTF-8";

    public static void writeStartDocument(OutputStream out) throws Exception {
        out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>".getBytes(ENCODING_UTF8));
    }

    public static void writeEndDocument(OutputStream out)
            throws Exception {
    }

    public static void writeEndElement(String elementName, OutputStream out) throws Exception {
        out.write(("</" + elementName + ">").getBytes(ENCODING_UTF8));
    }

    public static void writeStartElement(String elementName, OutputStream out) throws Exception {
        out.write(("<" + elementName + ">").getBytes(ENCODING_UTF8));
    }

    public static void writeStartElement(String elementName, OutputStream out, CustomAttribute... attributes) throws Exception {
        out.write(("<" + elementName).getBytes(ENCODING_UTF8));
        for (CustomAttribute attribute : attributes) {
            out.write((" " + attribute.attributeName + "=\"" + attribute.attributeValue + "\"").getBytes(ENCODING_UTF8));
        }
        out.write(">".getBytes(ENCODING_UTF8));
    }

    public static void writeStartElement(String elementName, List<Namespace> namespaces, OutputStream out) throws Exception {
        out.write(("<" + elementName + " ").getBytes(ENCODING_UTF8));
        for (Namespace ns : namespaces) {
            if (ns.getPrefix() != null) {
                out.write(("xmlns:" + ns.getPrefix() + "=\"" + ns.getUri() + "\" ").getBytes(ENCODING_UTF8));
            } else {
                out.write(("xmlns=\"" + ns.getUri() + "\" ").getBytes(ENCODING_UTF8));
            }
        }
        out.write(">".getBytes(ENCODING_UTF8));
    }

    public static void writeContent(String content, OutputStream outputStream) throws Exception {
        outputStream.write(content.getBytes(ENCODING_UTF8));
    }

    public static void writeSimpleElement(String elementName, String data, OutputStream out) throws Exception {
        out.write(("<" + elementName + ">" + data + "</" + elementName + ">").getBytes(ENCODING_UTF8));
    }

    public static String getAttribute(StartElement startElement, String attributeName) throws Exception {
        Attribute attribue = startElement.getAttributeByName(new QName(attributeName));
        if (attribue != null) {
            return attribue.getValue();
        }
        throw new XMLStreamException("No xml attribute found with name '" + attributeName);
    }

    public static String getAttributeOrNull(StartElement startElement, String attributeName) throws Exception {
        Attribute attribute = startElement.getAttributeByName(new QName(attributeName));
        return attribute != null ? attribute.getValue() : null;
    }

    public static void writeSimpleCDataElement(String elementName, String content, OutputStream out) throws Exception {
        out.write(("<" + elementName + "><![CDATA[" + content + "]]></" + elementName + ">").getBytes(ENCODING_UTF8));
    }

    public static class CustomAttribute {

        private String attributeName;
        private String attributeValue;

        public CustomAttribute(String attributeName, String attributeValue) {
            this.attributeName = attributeName;
            this.attributeValue = attributeValue;
        }
    }

    public static List<Element> getElementByTagName(Element e, String tagName) throws Exception {
        NodeList nodeList = e.getElementsByTagName(tagName);
        List<Element> lstResult = new ArrayList<Element>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node dataNode = nodeList.item(i);
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagName(Element e, String tagName) throws Exception {
        List<Element> lstResult = getElementByTagName(e, tagName);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }

    public static List<Element> getElemetByAttrName(Element e, String attrName) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getChildNodes();
        for (int y = 0; y < childNodes.getLength(); y++) {
            Node data = childNodes.item(y);
            Node attrFileName = data.getAttributes().getNamedItem(attrName);
            if (attrFileName == null || data.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }

            lstResult.add((Element) data);
        }
        return lstResult;
    }

    public static List<Element> getElementsByTagNameAndAttr(Element e, String tagName, String attrName) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getElementsByTagName(tagName);
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node dataNode = childNodes.item(i);
            if (attrName != null) {
                Node attrNode = dataNode.getAttributes().getNamedItem(attrName);
                if (attrNode == null) {
                    continue;
                }
            }
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagNameAndAttr(Element e, String tagName, String attrName) throws Exception {
        List<Element> lstResult = getElementsByTagNameAndAttr(e, tagName, attrName);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }

    public static List<Element> getElementsByTagNameAndAttr(Element e, String tagName, String attrName, String attrValue) throws Exception {
        List<Element> lstResult = new ArrayList<Element>();
        NodeList childNodes = e.getElementsByTagName(tagName);
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node dataNode = childNodes.item(i);
            if (attrName != null && attrValue != null) {
                Node attrNode = dataNode.getAttributes().getNamedItem(attrName);
                if (attrNode == null) {
                    continue;
                }
                String attrNodeValue = attrNode.getNodeValue();
                if (attrNodeValue == null || attrNodeValue.isEmpty() || !attrNodeValue.equals(attrValue)) {
                    continue;
                }
            }
            lstResult.add((Element) dataNode);
        }
        return lstResult;
    }

    public static Element getFirstElementByTagNameAndAttr(Element e, String tagName, String attrName, String attrValue) throws Exception {
        List<Element> lstResult = getElementsByTagNameAndAttr(e, tagName, attrName, attrValue);
        if (lstResult.isEmpty()) {
            return null;
        }

        return lstResult.get(0);
    }

    public static Document newDocument(DocumentBuilder builder, Document src) {
        Element srcRoot = src.getDocumentElement();

        Document newDoc = builder.newDocument();
        Element root = newDoc.createElement(srcRoot.getTagName());
        newDoc.appendChild(root);

        NamedNodeMap attrList = srcRoot.getAttributes();
        for (int i = 0; i < attrList.getLength(); i++) {
            Attr rootAttr = (Attr) attrList.item(i);
            root.setAttribute(rootAttr.getName(), rootAttr.getValue());
        }

        return newDoc;
    }

    public static Document makeTranslationDocument(Document docSrc, Map<String, Object> mapAttr) throws Exception {
        DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        //translation.xml
        Document docTranslation = dBuilder.newDocument();
        Element root = docTranslation.createElement("translation");
        root.setAttribute("spaceKey", (String) mapAttr.get("spaceKey"));
        root.setAttribute("originalLanguage", (String) mapAttr.get("originalLanguage"));
        root.setAttribute("targetLanguage", (String) mapAttr.get("targetLanguage"));
        docTranslation.appendChild(root);

        return docTranslation;
    }
}
