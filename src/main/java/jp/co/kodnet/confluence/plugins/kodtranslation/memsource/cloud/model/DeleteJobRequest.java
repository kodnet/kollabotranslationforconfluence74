package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

public class DeleteJobRequest extends RequestModelBase {

    protected JobInfo[] jobs;

    public JobInfo[] getJobs() {
        return jobs;
    }

    public void setJobs(JobInfo[] jobs) {
        this.jobs = jobs;
    }

}
