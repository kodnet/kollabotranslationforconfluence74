package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetLangguageRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetLanguageResponse;

/**
 *
 * @author anhlq
 */
public class LanguageConnector {

    private final static String GET_API_URL = "api2/v1/languages";
    private final String token;

    public LanguageConnector(String token) {
        this.token = token;
    }

    public GetLanguageResponse get() throws Exception {
        GetLangguageRequest request = new GetLangguageRequest();
        request.setToken(this.token);

        return (GetLanguageResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + GET_API_URL, request, GetLanguageResponse.class, Constants.GET_METHOD);
    }
}
