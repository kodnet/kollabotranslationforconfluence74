package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author lucvd
 */
public class DownloadFileRequest extends RequestModelBase {

    private String uploadedFile;

    /**
     * @return the uploadedFile
     */
    public String getUploadedFile() {
        return uploadedFile;
    }

    /**
     * @param uploadedFile the uploadedFile to set
     */
    public void setUploadedFile(String uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
}
