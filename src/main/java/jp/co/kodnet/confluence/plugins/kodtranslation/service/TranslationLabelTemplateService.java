package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LabelTemplate;

@Transactional
public interface TranslationLabelTemplateService {
    
    List<LabelTemplate> getAll();
    
    List<LabelTemplate> getByLabel(String label);
    
    LabelTemplate getByLabelAndLangKey(String label, String langKey);
      
    LabelTemplate add(
            String label,
            String uid,
            String templateName,
            String langKey
            );
    
    LabelTemplate update(
            String label,
            String uid,
            String templateName,
            String langKey
            );
    
    void delete(String label, String langKey);
    
    void deleteAll();  
}
