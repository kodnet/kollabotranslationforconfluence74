package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author anhlq
 */
public class SegmentsCount {

    private String jobPartUid;
    private Counts counts;

    public Counts getCounts() {
        return counts;
    }

    public void setCounts(Counts counts) {
        this.counts = counts;
    }

    public String getJobPartUid() {
        return jobPartUid;
    }

    public void setJobPartUid(String jobPartUid) {
        this.jobPartUid = jobPartUid;
    }
}
