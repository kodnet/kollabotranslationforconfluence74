package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.apache.commons.lang.StringUtils;

public class PageLinkBodyProcessor implements ElementProcessor {

    public static final QName QNAME_TEXT_LINK_BODY = new QName("http://atlassian.com/content", "plain-text-link-body");
    public static final QName QNAME_TEXT_BODY = new QName("http://atlassian.com/content", "plain-text-body");

    public static final QName SPAN_XMLTAG = new QName("http://www.w3.org/1999/xhtml", "span");
    public static final QName SPAN_NBSPTAG = new QName("http://www.w3.org/1999/xhtml", "spannbsp");
    public static final QName CLASS_ATTRIBUTE = new QName("http://www.w3.org/1999/xhtml", "class");

    public static final String CLASS_VALUE = "sv-cdata-translate";
    public static final QName PRE_XML_TAG = new QName("http://www.w3.org/1999/xhtml", "pre");

    private final XMLEventFactory xmlEventFactory;

    public PageLinkBodyProcessor(XMLEventFactory xmlEventFactory) {
        this.xmlEventFactory = xmlEventFactory;
    }

    @Override
    public boolean shouldProcess(XMLEvent event) {
        boolean isTextBody = (event.isStartElement()) && event.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_BODY.getLocalPart());
        boolean isTextLinkBody = (event.isStartElement()) && event.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_LINK_BODY.getLocalPart());
        return isTextBody || isTextLinkBody;
    }

    @Override
    public void marshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter) throws Exception {
        xmlEventWriter.add(currentEvent);

        boolean isTextBody = false;
        boolean isTextLinkBody = false;
        if (currentEvent.isStartElement()) {
            isTextBody = currentEvent.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_BODY.getLocalPart());
            isTextLinkBody = currentEvent.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_LINK_BODY.getLocalPart());
        }

        while (reader.hasNext() && (isTextBody || isTextLinkBody)) {
            XMLEvent xmlEvent = reader.nextEvent();
            if (xmlEvent.isEndElement()) {
                if (xmlEvent.asEndElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_BODY.getLocalPart())
                        || xmlEvent.asEndElement().getName().getLocalPart().equalsIgnoreCase(QNAME_TEXT_LINK_BODY.getLocalPart())) {
                    xmlEventWriter.add(xmlEvent);
                    return;
                }
            }
            if (xmlEvent.isCharacters() && (isTextBody || isTextLinkBody)) {
                List<Attribute> attributes = new ArrayList(1);
                attributes.add(this.xmlEventFactory.createAttribute(CLASS_ATTRIBUTE, "sv-cdata-translate"));
                xmlEventWriter.add(this.xmlEventFactory.createStartElement(SPAN_XMLTAG, attributes.iterator(), new ArrayList().iterator()));
                if (isTextLinkBody) {
                    this.convertCdataNbspEvent(xmlEventWriter, xmlEvent.asCharacters().getData());
                } else if (isTextBody) {
                    String content = xmlEvent.asCharacters().getData();
                    content = content.replace("\r\n", "\n").replace("\r", "\n");
                    content = content + "\n";
                    int index = content.indexOf("\n");
                    StartElement startElement = this.xmlEventFactory.createStartElement(PRE_XML_TAG, null, null);
                    EndElement endElement = this.xmlEventFactory.createEndElement(PRE_XML_TAG, null);
                    while (index > -1) {
                        xmlEventWriter.add(startElement);
                        this.convertCdataNbspEvent(xmlEventWriter, content.substring(0, index));
                        xmlEventWriter.add(endElement);
                        content = content.substring(index + 1, content.length());
                        index = content.indexOf("\n");
                    }
                }
                xmlEventWriter.add(this.xmlEventFactory.createEndElement(SPAN_XMLTAG, new ArrayList().iterator()));
            }
        }
    }

    public void convertCdataNbspEvent(XMLEventWriter xmlEventWriter, String xmlData) throws XMLStreamException {
        if (xmlData.isEmpty() || xmlData.equals("\n")) {
            xmlEventWriter.add(this.xmlEventFactory.createCharacters(xmlData));
            return;
        }
        List<XMLEvent> lstEvent = new ArrayList<XMLEvent>();
        String result = StringUtils.EMPTY;
        StartElement startElement = this.xmlEventFactory.createStartElement(SPAN_NBSPTAG, null, null);
        EndElement endElement = this.xmlEventFactory.createEndElement(SPAN_NBSPTAG, null);
        for (int i = xmlData.length() - 1; i > 0; i--) {
            Character ch = xmlData.charAt(i);
            Character preCh = xmlData.charAt(i - 1);
            if (ch.equals(' ') && preCh.equals(' ')) {
                if (!result.isEmpty()) {
                    lstEvent.add(0, this.xmlEventFactory.createCharacters(result));
                    result = StringUtils.EMPTY;
                }

                lstEvent.add(0, endElement);
                lstEvent.add(0, startElement);
            } else {
                result = ch + result;
            }
        }

        result = xmlData.charAt(0) + result;
        if (!result.isEmpty()) {
            lstEvent.add(0, this.xmlEventFactory.createCharacters(result));
        }

        for (int i = 0; i < lstEvent.size(); i++) {
            xmlEventWriter.add(lstEvent.get(i));
        }
    }

    @Override
    public void unmarshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter) throws Exception {
    }
}
