package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.core.util.ProgressMeter;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.JobConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.ProjectConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.WorkflowStepConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetWorkflowStepResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectCreateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectGetInfoResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.WorkflowStep;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.DocumentUtil;

/**
 *
 * @param <T>
 */
public class TranslationMemsourceJobCallBack<T> implements TranslationMemsourceCallback<T> {

    protected ProgressMeter progress;
    protected Map<String, Map<String, String>> map = new HashMap<>();
    protected JsonObject resultList = new JsonObject();
    protected Map<String, Object> mapInput;
    protected TranslationApiLoginInfoService translationApiLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
    protected SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
    protected PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
    protected LabelManager labelManager = TranslationMemoryToolFactory.getLabelManager();
    protected TranslationLabelTemplateService labelTemplateService = TranslationMemoryToolFactory.getTranslationLabelTemplateService();
    protected TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
    protected I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
    protected String jobTitle = "";

    public TranslationMemsourceJobCallBack(Map<String, Object> mapInput) {
        this.mapInput = mapInput;
    }

    @Override
    public void setProgress(ProgressMeter progress) {
        this.progress = progress;
    }

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public T doInTransaction() {
        try {
            progress.setStatus(Constants.WAITTING_PREFIX);
            Long fromPageId = (Long) mapInput.get("pageId");
            String spaceKey = (String) mapInput.get("spaceKey");
            String fromLangKey = (String) mapInput.get("fromLangKey");
            String fromLangName = (String) mapInput.get("fromLangName");
            String toLangKey = (String) mapInput.get("toLangKey");
            String toLangName = (String) mapInput.get("toLangName");
            TranslationJob translationJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, fromPageId);

            //Get token
            ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
            if (apiLoginInfo == null) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
                return null;
            }

            Date dateExpires = apiLoginInfo.getDateExpires();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if(dateExpires != null && (sdf.parse(sdf.format(dateExpires)).before(sdf.parse(sdf.format(new Date()))) || sdf.parse(sdf.format(dateExpires)).equals(sdf.parse(sdf.format(new Date()))))){
                CommonFunction.doLoginMemsource(apiLoginInfo.getUserName(), apiLoginInfo.getPassword(),String.valueOf(Constants.MEMSOURCE_ID), "", "", "");
                apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
            }     
            String token = apiLoginInfo.getLoggedToken();
            if (token == null) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
                return null;
            }
            
            String[] workflow = this.getWorkflow(token); 
            if (workflow == null) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
                return null;
            }
            //Project existed
            String projectUid = "";
            if (translationJob == null) {
                progress.setStatus("Create project memsource");
                projectUid = createProjectMemsource(token, workflow, fromLangKey, fromLangName, toLangKey, toLangName, fromPageId);
            } else {
                projectUid = translationJob.getApiProjectId();
                Long toPageId = translationJob.getToPageId();
                boolean result = deleteJobMemsource(token, projectUid, workflow, fromLangKey, toLangKey, spaceKey, fromPageId, toPageId);
                if(result == false){
                    this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.language.space.select.not.found.error"));
                    return null;
                }
            }
            //Create Job
            if (projectUid != null) {
                progress.setStatus("Create job memsource");
                createJobMemsource(token, projectUid, fromPageId, fromLangKey, toLangKey, spaceKey);
            }

            this.progress.setPercentage(99);
            this.progress.setStatus(projectUid + Constants.STRING_DISTINS + jobTitle + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.excute.excute.success"));
        } catch (Exception ex) {
            progress.setStatus(Constants.ERROR_PREFIX + ex.getMessage());
            MemoryLogUtil.outputLog(ex.getMessage());
        }
        return null;
    }

    private String[] getWorkflow(String token) throws Exception {
        //Get worlflow
        WorkflowStepConnector workflowStepConnector = new WorkflowStepConnector(token);
        GetWorkflowStepResponse lstWorkflowStep = workflowStepConnector.get();
        if (!CommonFunction.isNullOrEmpty(lstWorkflowStep.getErrorCode())) {
            this.progress.setStatus(Constants.ERROR_PREFIX + lstWorkflowStep.getErrorCode());
            return null;
        }
        List<String> workflow = new ArrayList<String>();
        for (String workflowName : Constants.WORKFLOWS) {
            for (WorkflowStep workflowStep : lstWorkflowStep.getContent()) {
                if (workflowStep.getAbbr().equals(workflowName)) {
                    workflow.add(workflowStep.getId());
                }
            }
        }
        return (String[]) workflow.toArray(new String[workflow.size()]);
    }

    private String createProjectMemsource(String token, String[] workflow, String fromLangKey,String fromLangName, String toLangKey, String toLangName,Long fromPageId) throws Exception {
        Page page = pageManager.getPage(fromPageId);
        if (page == null) {
            this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.language.space.select.not.found.error"));
            return null;
        }

        //ClientId
        ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        if(apiLoginInfo == null){
            this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.none.message"));
            return null;
        }

        String clientId = apiLoginInfo.getOrtherKey01();
        //Target language
        String[] targetLangs = new String[1];
        targetLangs[0] = toLangKey;
        String pageTitle = page.getTitle();
        //「PageTitle」＋「From 」＋「From language display」＋「To」＋「To  language display」
        String projectName = pageTitle + " From " + fromLangName + " To " + toLangName;
        //TemplateId
        String templateId = CommonFunction.getTemplateId(page, fromLangKey);
        ProjectConnector projectConnector = new ProjectConnector(token);
        ProjectCreateResponse projectCreateResponse = projectConnector.create(projectName, fromLangKey, targetLangs, clientId, workflow, templateId);
        if (!CommonFunction.isNullOrEmpty(projectCreateResponse.getErrorCode())) {
            this.progress.setStatus(Constants.ERROR_PREFIX + projectCreateResponse.getErrorCode());
            return null;
        }
        jobTitle = projectName;
        String projectUId = projectCreateResponse.getUid();
        return projectUId;
    }

    private void createJobMemsource(String token, String projectUid, Long fromPageId, String fromLangKey, String toLangKey, String spaceKey) throws Exception {
        Page page = pageManager.getPage(fromPageId);
        if (page == null) {
            this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
            return;
        }
        List<Integer> listTranslationProcess = new ArrayList<Integer>();
        listTranslationProcess.add(DocumentUtil.REMOVE_GENGO_MACRO_TRANSLATION);
        listTranslationProcess.add(DocumentUtil.SKIP_TRANSLATION);
        String xml = DocumentUtil.xmlTranslationProccess(page, listTranslationProcess);
        xml = DocumentUtil.addAttribueForLinkAnchor(xml);
        xml = DocumentUtil.convertMacroDeckTranslation(xml, fromLangKey, toLangKey);
        xml = DocumentUtil.convertLinkTranslation(xml);
        xml = DocumentUtil.convertTagEmoji(xml, false);
        xml = DocumentUtil.convertIncludeMacro(xml);
        InputStream inputStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        JobConnector jobConnector = new JobConnector(token);
        String[] targetLangs = new String[1];
        targetLangs[0] = toLangKey;
        String jobName = page.getIdAsString() + "_" + page.getDisplayTitle() + ".xml";
        CreateJobResponse createJobResponse = jobConnector.create(projectUid, jobName, inputStream, targetLangs);
        if (!CommonFunction.isNullOrEmpty(createJobResponse.getErrorCode())) {
            this.progress.setStatus(Constants.ERROR_PREFIX + createJobResponse.getErrorDescription());
            return;
        }

        String baseLang = fromLangKey;
        TranslationJob trans = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, fromPageId);
        if (trans != null && trans.getTranslationJobId() != null) {
            return;
        }
        
        JobInfo[] jobInfo = createJobResponse.getJobs();
        int lastIndex = jobInfo.length;
        if (lastIndex == 0) {
            this.progress.setStatus(Constants.ERROR_PREFIX + createJobResponse.getErrorDescription());
            return;
        }
        
        JobInfo job = jobInfo[lastIndex - 1];
        String jobId = job.getUid();
        translationJobService.add(fromLangKey, fromPageId, toLangKey, null, spaceKey, baseLang, projectUid, jobId, Constants.MEMSOURCE_STATUS_CREATE_NEW);
        ProjectConnector projectConnector = new ProjectConnector(token);
        ProjectGetInfoResponse projectCreateResponse = projectConnector.getInfo(projectUid);
        this.jobTitle = projectCreateResponse.getName();
    }
    
    private boolean deleteJobMemsource(String token, String projectUid, String[] workflow,
            String fromLangKey, String toLangKey, String spaceKey, Long fromPageId, Long toPageId) {
        try {
            TransactionTemplate transactionTemplate = TranslationMemoryToolFactory.getTransactionTemplate();
            TranslationMemsourceDeletePageCallBack<Boolean> task = new TranslationMemsourceDeletePageCallBack<Boolean>(
                    token, projectUid, workflow,
                    fromLangKey, toLangKey, spaceKey,
                    fromPageId, toPageId
            );
            Boolean result = transactionTemplate.execute(task);
            task = null;
            MemoryLogUtil.outputTime(false, "TranslationMemsourceJobCallBack", "deleteJobMemsource", "TranslationMemsourceJobCallBack");
            return result;
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
            return false;
        }
    }
}
