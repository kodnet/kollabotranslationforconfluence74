package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetSegmentsCountRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetSegmentsCountResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;

/**
 *
 * @author anhlq
 */
public class SegmentsCountConnector {

    //api2/v1/projects/{projectUid}/jobs/segmentsCount
    private final static String GET_API_URL = "api2/v1/projects/%s/jobs/segmentsCount";
    private final String token;

    public SegmentsCountConnector(String token) {
        this.token = token;
    }

    public GetSegmentsCountResponse get(String projectUid, String[] jobParts) throws Exception {
        if (jobParts == null || jobParts.length == 0) {
            return (GetSegmentsCountResponse) MemoryRequestUtil.createResponseInstance(GetSegmentsCountResponse.class, "EXCEPTION", "No jobpart", GET_API_URL, Constants.POST_METHOD);
        }

        JobInfo[] jobs = new JobInfo[jobParts.length];
        for (int i = 0; i < jobParts.length; i++) {
            jobs[i] = new JobInfo();
            jobs[i].setUid(jobParts[i]);
        }

        return get(projectUid, jobs);
    }

    public GetSegmentsCountResponse get(String projectUid, JobInfo[] jobParts) throws Exception {
        GetSegmentsCountRequest request = new GetSegmentsCountRequest();
        request.setJobs(jobParts);
        request.setToken(this.token);
        String url = String.format(GET_API_URL, projectUid);
        return (GetSegmentsCountResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetSegmentsCountResponse.class);
    }
}
