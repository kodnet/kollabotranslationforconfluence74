package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;

/**
 *
 * @author baoph
 */
public class GetAnalysisResponse extends ResponseModelBase {

    protected String id;
    protected String type;
    protected String name;
    protected Parts[] analyseLanguageParts;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Parts[] getAnalyseLanguageParts() {
        return analyseLanguageParts;
    }

    public void setAnalyseLanguageParts(Parts[] analyseLanguageParts) {
        this.analyseLanguageParts = analyseLanguageParts;
    }

    public class Parts {

        private String sourceLang;
        private String targetLang;
        private Data data;

        public String getSourceLang() {
            return sourceLang;
        }

        public void setSourceLang(String sourceLang) {
            this.sourceLang = sourceLang;
        }

        public String getTargetLang() {
            return targetLang;
        }

        public void setTargetLang(String targetLang) {
            this.targetLang = targetLang;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public class Data {

            private Details all;
            private Details repetitions;
            private TransMemoryMatches transMemoryMatches;

            public Details getAll() {
                return all;
            }

            public void setAll(Details all) {
                this.all = all;
            }

            public Details getRepetitions() {
                return repetitions;
            }

            public void setRepetitions(Details repetitions) {
                this.repetitions = repetitions;
            }

            public TransMemoryMatches getTransMemoryMatches() {
                return transMemoryMatches;
            }

            public void setTransMemoryMatches(TransMemoryMatches transMemoryMatches) {
                this.transMemoryMatches = transMemoryMatches;
            }

            public class TransMemoryMatches {

                private Details match0;
                private Details match50;
                private Details match75;
                private Details match85;
                private Details match95;
                private Details match100;
                private Details match101;

                public Details getMatch0() {
                    return match0;
                }

                public void setMatch0(Details match0) {
                    this.match0 = match0;
                }

                public Details getMatch50() {
                    return match50;
                }

                public void setMatch50(Details match50) {
                    this.match50 = match50;
                }

                public Details getMatch75() {
                    return match75;
                }

                public void setMatch75(Details match75) {
                    this.match75 = match75;
                }

                public Details getMatch85() {
                    return match85;
                }

                public void setMatch85(Details match85) {
                    this.match85 = match85;
                }

                public Details getMatch95() {
                    return match95;
                }

                public void setMatch95(Details match95) {
                    this.match95 = match95;
                }

                public Details getMatch100() {
                    return match100;
                }

                public void setMatch100(Details match100) {
                    this.match100 = match100;
                }

                public Details getMatch101() {
                    return match101;
                }

                public void setMatch101(Details match101) {
                    this.match101 = match101;
                }
            }

            public class Details {

                private double percent;
                private int words;
                private int segments;
                private int characters;
                private double normalizedPages;

                public double getPercent() {
                    return percent;
                }

                public void setPercent(double percent) {
                    this.percent = percent;
                }

                public int getWords() {
                    return words;
                }

                public void setWords(int words) {
                    this.words = words;
                }

                public int getSegments() {
                    return segments;
                }

                public void setSegments(int segments) {
                    this.segments = segments;
                }

                public int getCharacters() {
                    return characters;
                }

                public void setCharacters(int characters) {
                    this.characters = characters;
                }

                public double getNormalizedPages() {
                    return normalizedPages;
                }

                public void setNormalizedPages(double normalizedPages) {
                    this.normalizedPages = normalizedPages;
                }
            }
        }
    }
}
