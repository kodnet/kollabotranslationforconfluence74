package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.XMLEvent;

public class NoopProcessor implements ElementProcessor {

    @Override
    public boolean shouldProcess(XMLEvent event) {
        return true;
    }

    @Override
    public void marshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter)
            throws Exception {
        xmlEventWriter.add(currentEvent);
    }

    @Override
    public void unmarshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter)
            throws Exception {
        xmlEventWriter.add(currentEvent);
    }
}
