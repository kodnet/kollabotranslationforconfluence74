package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author baoph
 */
public class SetStatusJobRequest extends RequestModelBase {

    private String requestedStatus;

    public String getRequestedStatus() {
        return requestedStatus;
    }

    public void setRequestedStatus(String requestedStatus) {
        this.requestedStatus = requestedStatus;
    }
}
