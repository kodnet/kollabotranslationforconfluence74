package jp.co.kodnet.confluence.plugins.kodtranslation.cloud;

import com.atlassian.confluence.setup.settings.Settings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.FileRequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import static jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction.isNullOrEmpty;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author lucvd
 */
public class MemoryRequestUtil {

    public static final Logger LOG = Logger.getLogger(MemoryRequestUtil.class.getName());

    public static ResponseModelBase createRequest(String apiURL, RequestModelBase requestModel, Class responseType) throws Exception {
        return createRequest(apiURL, requestModel, responseType, responseType);
    }

    public static ResponseModelBase createRequest(String apiURL, RequestModelBase requestModel, Class responseSuccessType, Class responseErrorType) throws Exception {
        try {

            String logText = "MemSource POST=> URL:";
            logText += apiURL;
            URL url = new URL(apiURL);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }

            urlCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            if (requestModel != null) {
                OutputStream os = urlCon.getOutputStream();
                PrintStream ps = new PrintStream(os);
                String parameterData = MemoryRequestUtil.getParamerStringFromObject(requestModel);
                ps.print(parameterData);
                ps.close();
                os.close();

                logText += " DATA: " + parameterData;
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);

        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase createRequestJson(String apiURL, RequestModelBase requestModel, Class responseType) throws Exception {
        return createRequestJson(apiURL, requestModel, responseType, responseType, Constants.POST_METHOD);
    }

    public static ResponseModelBase createRequestJson(String apiURL, RequestModelBase requestModel, Class responseType, String methodType) throws Exception {
        return createRequestJson(apiURL, requestModel, responseType, responseType, methodType);
    }

    public static ResponseModelBase createRequestJson(String apiURL, RequestModelBase requestModel, Class responseSuccessType, Class responseErrorType, String methodType) throws Exception {
        try {
            String logText = String.format("MemSource %s=> URL:", methodType);
            logText += apiURL;
            HttpURLConnection urlCon = null;
            //POST method
            if (methodType.equalsIgnoreCase(Constants.POST_METHOD) == true || methodType.equalsIgnoreCase(Constants.DELETE_METHOD)) {
                URL url = new URL(apiURL);
                urlCon = (HttpURLConnection) url.openConnection();
                urlCon.setRequestMethod(methodType);
                urlCon.setAllowUserInteraction(false);
                urlCon.setDoOutput(true);
                urlCon.setRequestProperty("Content-Type", "application/json");
                if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                    urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
                }

                System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

                if (requestModel != null) {
                    OutputStream os = urlCon.getOutputStream();
                    PrintStream ps = new PrintStream(os);
                    String parameterData = MemoryRequestUtil.getParamerJsonStringFromObject(requestModel);
                    ps.write(parameterData.getBytes("UTF-8"));
                    ps.close();
                    os.close();

                    logText += " DATA: " + parameterData;
                }
            } else if (methodType.equalsIgnoreCase(Constants.GET_METHOD) == true) {
                String param = getParamerStringFromObject(requestModel);
                if (param.isEmpty() == false) {
                    apiURL = apiURL + "?" + param;
                }

                URL url = new URL(apiURL);
                urlCon = (HttpURLConnection) url.openConnection();
                urlCon.setRequestMethod(methodType);
                urlCon.setRequestProperty("Connection", "Keep-Alive");
                urlCon.setRequestProperty("Keep-Alive", "header");
                urlCon.setRequestProperty("Cache-Control", "no-cache");
                urlCon.setRequestProperty("Accept", "*");
                if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                    urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
                }
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase createPostRequest(
            String apiURL,
            RequestModelBase requestModel,
            Map<String, String> requestHeaders,
            Class responseSuccessType,
            Class responseErrorType
    ) throws Exception {
        try {
            String logText = "MemSource POST=> URL:";
            logText += apiURL;
            URL url = new URL(apiURL);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }

            urlCon.setRequestProperty("Content-Type", "application/json");

            for (String key : requestHeaders.keySet()) {
                urlCon.setRequestProperty(key, requestHeaders.get(key));
            }

            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            String jsonString = JsonUtil.toJsonString(requestModel);

            try ( OutputStream os = urlCon.getOutputStream()) {
                byte[] input = jsonString.getBytes(StandardCharsets.UTF_8);
                os.write(input, 0, input.length);
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);

        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase createGetRequest(
            String apiURL,
            RequestModelBase requestModel,
            Map<String, String> requestHeaders,
            Class responseSuccessType,
            Class responseErrorType
    ) throws Exception {
        try {
            String logText = "T400 GET => URL:";
            logText += apiURL;
            URL url = new URL(apiURL);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.GET_METHOD);
            urlCon.setRequestProperty("User-Agent", "Mozilla/5.0");
            for (String key : requestHeaders.keySet()) {
                urlCon.setRequestProperty(key, requestHeaders.get(key));
            }
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.GET_METHOD);
        }
    }

    public static ResponseModelBase createRequestUpload(String apiURL, InputStream inputStream, RequestModelBase requestModel, Class responseType) throws Exception {
        return createRequestUpload(apiURL, inputStream, requestModel, responseType, responseType);
    }

    public static ResponseModelBase createRequestUpload(String apiURL, InputStream inputStream, RequestModelBase requestModel, Class responseSuccessType, Class responseErrorType) throws Exception {
        try {
            String logText = "MemSource POST=> URL:";

            String urlString = apiURL + "?" + MemoryRequestUtil.getParamerStringFromObject(requestModel);
            logText += urlString;

            URL url = new URL(urlString);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            String boundary = Long.toHexString(System.currentTimeMillis());
            urlCon.setRequestProperty("Content-Type", "application/octet-stream");
            urlCon.setRequestProperty("Content-Disposition", "attachment; filename=\"abc.txt\"");
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            String charset = "UTF-8";

            if (requestModel != null) {

                OutputStream os = urlCon.getOutputStream();
                DataOutputStream output = new DataOutputStream(os);
                //PrintStream ps = new PrintStream(os);

                byte[] buffer = new byte[4096];
                ByteArrayOutputStream ous = new ByteArrayOutputStream();
                int read = 0;
                while ((read = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.close();
                os.flush();
                // os.close();
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase createRequestUploadWithMultipart(String apiURL, InputStream inputStream, FileRequestModelBase requestModel, Class responseType) throws Exception {
        return createRequestUploadWithMultipart(apiURL, inputStream, requestModel, responseType, responseType);
    }

    public static ResponseModelBase createRequestUploadWithMultipart(String apiURL, InputStream inputStream, FileRequestModelBase requestModel, Class responseSuccessType, Class responseErrorType) throws Exception {
        try {
            HttpURLConnection urlCon;
            String logText = "MemSource POST=> URL:";

            String urlString = apiURL;

            MultipartUtility multipartUtility = new MultipartUtility(urlString);
            multipartUtility.addFormField("token", requestModel.getToken());
            multipartUtility.addHeader("Authorization", String.format("ApiToken %s", requestModel.getToken()));

            Field[] fields = requestModel.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                if (field.getName().equals("filename")
                        || field.getName().equals("token")) {
                    continue;
                }

                field.setAccessible(true);
                Object value = field.get(requestModel);
                if (value == null || CommonFunction.isNullOrEmpty(value.toString())) {
                    continue;
                }
                multipartUtility.addFormField(field.getName(), value.toString());
            }

            multipartUtility.addFilePart("file", inputStream, requestModel.getFilename());
            System.out.println(logText);

            urlCon = multipartUtility.finishing();
            return MemoryRequestUtil.getResponseFile(urlCon, responseSuccessType, responseErrorType);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase createRequestDownload(String apiURL, OutputStream outputStream, RequestModelBase requestModel, Class responseType) throws Exception {
        return createRequestDownload(apiURL, outputStream, requestModel, responseType, responseType, Constants.POST_METHOD);
    }

    public static ResponseModelBase createRequestDownload(String apiURL, OutputStream outputStream, RequestModelBase requestModel, Class responseType, String methodType) throws Exception {
        return createRequestDownload(apiURL, outputStream, requestModel, responseType, responseType, methodType);
    }

    public static ResponseModelBase createRequestDownload(String apiURL, OutputStream outputStream, RequestModelBase requestModel, Class responseSuccessType, Class responseErrorType, String methodType) throws Exception {
        try {
            String logText = String.format("MemSource %s=> URL:", methodType);
            String urlString = apiURL;
            logText += urlString;
            HttpURLConnection urlCon = null;
            //POST method
            if (methodType.equalsIgnoreCase(Constants.POST_METHOD) == true || methodType.equalsIgnoreCase(Constants.DELETE_METHOD)) {
                URL url = new URL(apiURL);
                urlCon = (HttpURLConnection) url.openConnection();
                urlCon.setRequestMethod(methodType);
                urlCon.setAllowUserInteraction(false);
                urlCon.setDoOutput(true);
                urlCon.setRequestProperty("Content-Type", "application/json");
                if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                    urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
                }

                System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

                if (requestModel != null) {
                    OutputStream os = urlCon.getOutputStream();
                    PrintStream ps = new PrintStream(os);
                    String parameterData = MemoryRequestUtil.getParamerJsonStringFromObject(requestModel);
                    ps.write(parameterData.getBytes("UTF-8"));
                    ps.close();
                    os.close();

                    logText += " DATA: " + parameterData;
                }
            } else if (methodType.equalsIgnoreCase(Constants.GET_METHOD) == true) {
                String param = getParamerStringFromObject(requestModel);
                if (param.isEmpty() == false) {
                    apiURL = apiURL + "?" + param;
                }

                URL url = new URL(apiURL);
                urlCon = (HttpURLConnection) url.openConnection();
                urlCon.setRequestMethod(methodType);
                urlCon.setRequestProperty("Connection", "Keep-Alive");
                urlCon.setRequestProperty("Keep-Alive", "header");
                urlCon.setRequestProperty("Cache-Control", "no-cache");
                urlCon.setRequestProperty("Accept", "*");
                if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                    urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
                }
            }

            System.out.println(logText);
            return MemoryRequestUtil.getResponseDownloadFile(urlCon, outputStream, responseSuccessType);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return createResponseInstance(responseErrorType, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public static ResponseModelBase getResponseFile(HttpURLConnection urlCon, Class responseType) throws Exception {
        return getResponseFile(urlCon, responseType, responseType);
    }

    public static ResponseModelBase getResponseFile(HttpURLConnection urlCon, Class responseSuccessType, Class responseErrorType) throws Exception {
        try {
            int resCode = urlCon.getResponseCode();

            String id = null;
            Boolean isError = false;
            InputStream ist = null;
            if (resCode == 200 || resCode == 201 || resCode == 202 || resCode == 204) {
                ist = urlCon.getInputStream();
                isError = false;
            } else {
                ist = urlCon.getErrorStream();
                isError = true;
            }

            if (ist == null) {
                return createResponseInstance(responseErrorType, null, null, urlCon.getURL().toString(), Constants.POST_METHOD);
            }

            InputStreamReader isr = new InputStreamReader(ist, Settings.DEFAULT_DEFAULT_ENCODING);
            BufferedReader br = new BufferedReader(isr);

            StringBuilder body = new StringBuilder();
            String nextLine = br.readLine();
            while (nextLine != null) {
                body.append(nextLine);
                nextLine = br.readLine();
            }

            if (StringUtils.isEmpty(body.toString()) || StringUtils.isBlank(body.toString())) {
                return createResponseInstance(responseErrorType, null, null, urlCon.getURL().toString(), Constants.POST_METHOD);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            String strDate = sdf.format(CommonFunction.getCurentDate());

            MemoryLogUtil.outputLog(strDate + "START Parse JSON");
            Object responseObj = null;
            if (isError) {
                responseObj = JsonUtil.jsonStringToObject(body.toString(), responseErrorType);
            } else {
                responseObj = JsonUtil.jsonStringToObject(body.toString(), responseSuccessType);
            }
            MemoryLogUtil.outputLog(strDate + "Content Response Json " + body.toString());
            ResponseModelBase response = (ResponseModelBase) responseObj;
            strDate = sdf.format(CommonFunction.getCurentDate());
            MemoryLogUtil.outputLog(strDate + "END Parse JSON");

            // Write log request memsource.
            MemoryLogUtil.outputMemoryLog(urlCon.getURL().toString(), urlCon.getRequestMethod(), String.valueOf(resCode), response.getErrorCode(), response.getErrorDescription());
            return response;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static ResponseModelBase getResponseDownloadFile(HttpURLConnection urlCon, OutputStream outputStream, Class responseType) throws Exception {
        try {
            int resCode = urlCon.getResponseCode();

            String id = null;
            if (resCode == 200 || resCode == 202) {
                // ist = urlCon.getInputStream();
                InputStream ist = urlCon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(ist);
                byte[] buff = new byte[64 * 1024];
                int n = 0;
                while ((n = bis.read(buff)) >= 0) {
                    outputStream.write(buff, 0, n);
                }
                bis.close();
                ist.close();
                return createResponseInstance(responseType, null, null, urlCon.getURL().toString(), Constants.POST_METHOD);

            } else {

                InputStream ist = null;
                ist = urlCon.getErrorStream();

                InputStreamReader isr = new InputStreamReader(ist, Settings.DEFAULT_DEFAULT_ENCODING);
                BufferedReader br = new BufferedReader(isr);

                StringBuilder body = new StringBuilder();
                String nextLine = br.readLine();
                while (nextLine != null) {
                    body.append(nextLine);
                    nextLine = br.readLine();
                }
                br.close();
                ist.close();

                if (StringUtils.isEmpty(body.toString()) || StringUtils.isBlank(body.toString())) {
                    return createResponseInstance(responseType, null, null, urlCon.getURL().toString(), Constants.POST_METHOD);
                }

                Object responseObj = JsonUtil.jsonStringToObject(body.toString(), responseType);
                ResponseModelBase response = (ResponseModelBase) responseObj;
                // Write log request memsource.
                MemoryLogUtil.outputMemoryLog(urlCon.getURL().toString(), urlCon.getRequestMethod(), String.valueOf(resCode), response.getErrorCode(), response.getErrorDescription());
                return response;
            }
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static ResponseModelBase createResponseInstance(Class responseType, String errorCode, String message, String apiURL, String requestMethod) throws Exception {
        try {

            ResponseModelBase response = (ResponseModelBase) responseType.newInstance();
            response.setErrorCode(errorCode);
            response.setErrorDescription(message);
            MemoryLogUtil.outputMemoryLog(apiURL, requestMethod, null, response.getErrorCode(), response.getErrorDescription());
            return response;
        } catch (Exception ex1) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex1);
            LOG.log(Level.SEVERE, null, ex1);
            ex1.printStackTrace();
            return null;
        }
    }

    public static String getFileName(HttpURLConnection urlCon) throws Exception {
        String header = urlCon.getHeaderField("content-disposition");
        int pos1 = header.indexOf("filename");
        if (pos1 > 0) {
            pos1 = header.indexOf("=", pos1);
        }
        String downloadFilename = null;
        if (pos1 > 0) {
            downloadFilename = header.substring(pos1 + 1).trim();
        }

        return downloadFilename;
    }

    public static String getMessageFromResponse(ResponseModelBase response) throws Exception {
        String responseMsg = !CommonFunction.isNullOrEmpty(response.getErrorCode()) ? response.getErrorCode() : "";
        responseMsg += " " + (!CommonFunction.isNullOrEmpty(response.getErrorDescription()) ? response.getErrorDescription() : "");
        return response.getErrorCode() + " " + response.getErrorDescription();
    }

    public static String getParamerStringFromObject(Object request) throws Exception {
        if (request == null) {
            return "";
        }

        Class objClass = request.getClass();
        String resultString = "";
        List<Field> allFieldList = new ArrayList<Field>();
        allFieldList.addAll(Arrays.asList(objClass.getDeclaredFields()));
        allFieldList.addAll(Arrays.asList(objClass.getSuperclass().getDeclaredFields()));

        for (Field field : allFieldList) {
            try {

                field.setAccessible(true);
                String fieldName = field.getName();
                if (fieldName.equals("headers")) {
                    continue;
                }
                if (fieldName.equals("token")) {
                    continue;
                }
                Object value = field.get(request);
                if (value == null || isNullOrEmpty(value.toString())) {
                    continue;
                }

                if (field.getType().isArray()) {
                    int length = Array.getLength(value);
                    for (int i = 0; i < length; i++) {
                        Object arrayElement = Array.get(value, i);
                        if (arrayElement != null && !isNullOrEmpty(arrayElement.toString())) {
                            String valueString = arrayElement.toString();
                            if (!StringUtils.isEmpty(resultString)) {
                                resultString += "&";
                            }

                            resultString += fieldName;
                            resultString += "=";
                            resultString += URLEncoder.encode(valueString, "UTF-8");
                        }
                    }
                    continue;
                }

                String valueString = value.toString();
                if (!StringUtils.isEmpty(resultString)) {
                    resultString += "&";
                }

                resultString += fieldName;
                resultString += "=";
                resultString += URLEncoder.encode(valueString, "UTF-8");
            } catch (IllegalArgumentException ex) {
                MemoryLogUtil.outputLogException(CommonFunction.class, ex);
                Logger.getLogger(CommonFunction.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                MemoryLogUtil.outputLogException(CommonFunction.class, ex);
                Logger.getLogger(CommonFunction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return resultString;
    }

    public static String getParamerJsonStringFromObject(Object request) throws Exception {
        if (request == null) {
            return "{}";
        }

        try {
            Class objClass = request.getClass();
            Gson gson = new Gson();
            JsonElement result = gson.toJsonTree(request, objClass);
            return result.toString();
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            Logger.getLogger(CommonFunction.class.getName()).log(Level.SEVERE, null, ex);
            return "{}";
        }
    }
}
