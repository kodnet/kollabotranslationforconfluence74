package jp.co.kodnet.confluence.plugins.kodtranslation.cloud;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseArrayModelBase;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author lucvd
 */
public class JsonUtil {

    /**
     * Objectを文字列に変換する
     *
     * @param obj
     * @return
     */
    public static String toJsonString(Object obj) {
        if (obj == null) {
            return null;
        }

        Gson gson = new Gson();
        String jsonInString = gson.toJson(obj);
        return jsonInString;
    }

    /**
     * JSON形式文字列をパースする
     *
     * @param obj
     * @return
     */
    public static Object jsonStringToObject(String jsonString, Class targetClass) throws Exception {
        try {
            if (StringUtils.isEmpty(jsonString)) {
                return null;
            }

            if (targetClass.getSuperclass().equals(ResponseArrayModelBase.class)) {
                jsonString = "{array:" + jsonString + "}";
            }

            Gson gson = new Gson();
            Object obj = gson.fromJson(jsonString, targetClass);

            return obj;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static <T> T jsonStringToObject(JsonObject jsonObject, Class<T> targetClass) throws Exception {
        T obj = targetClass.newInstance();
        for (Field field : obj.getClass().getDeclaredFields()) {
            JsonElement value = jsonObject.get(field.getName());
            if (Modifier.isPrivate(field.getModifiers())) {
                field.setAccessible(true);
            }

            Class type = field.getType();
            if (value != null && !value.isJsonNull()) {
                if (field.getType().isArray() && value.isJsonArray()) {
                    JsonArray jsonArray = value.getAsJsonArray();
                    Class arrayClass = field.getType().getComponentType();
                    Object valueArray = jsonArrayToArray(jsonArray, arrayClass);
                    field.set(obj, valueArray);
                } else {
                    PropertyEditor editor = PropertyEditorManager.findEditor(type);
                    editor.setAsText(value.getAsString());
                    field.set(obj, editor.getValue());
                }
            }
        }
        return obj;
    }

    public static <T> Object jsonArrayToArray(JsonArray jsonArray, Class<T> targerClass) throws Exception {
        Object result = Array.newInstance(targerClass, jsonArray.size());
        int i = 0;
        for (JsonElement jsonElement : jsonArray) {
            PropertyEditor editor = PropertyEditorManager.findEditor(targerClass);
            editor.setAsText(jsonElement.getAsString());
            Array.set(result, i, targerClass.cast(editor.getValue()));
            i++;
        }

        return result;
    }
}
