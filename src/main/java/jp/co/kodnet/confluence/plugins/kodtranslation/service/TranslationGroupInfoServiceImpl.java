package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.GroupInfo;

public class TranslationGroupInfoServiceImpl implements TranslationGroupInfoService {

    private final ActiveObjects ao;

    public TranslationGroupInfoServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public GroupInfo get() {
        List<GroupInfo> translationGroupInfos= newArrayList(ao.find(GroupInfo.class));
        if(translationGroupInfos.isEmpty()){
            return null;
        }
        return translationGroupInfos.get(0);
    }

    @Override
    public GroupInfo add(String groupName){
        GroupInfo translationGroupInfo = ao.create(GroupInfo.class);
        translationGroupInfo.setGroupName(groupName);
        translationGroupInfo.save();
        return translationGroupInfo;
    }

    @Override
    public GroupInfo update(String groupName) {
        this.delete();
        GroupInfo translationGroupInfo = this.add(groupName);
        return translationGroupInfo;
    }

    @Override
    public void delete() {
        GroupInfo group = this.get();
        if (group != null) {
            ao.delete(group);
        }
    }
}
