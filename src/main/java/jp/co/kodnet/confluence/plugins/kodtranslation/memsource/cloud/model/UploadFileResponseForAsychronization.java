package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;

/**
 *
 * @author Kohei.Sato
 */
public class UploadFileResponseForAsychronization extends ResponseModelBase {

    private String[] uid;
    private String[] name;
    private int[] size;

    /**
     * @return the uid
     */
    public String[] getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String[] uid) {
        this.uid = uid;
    }

    /**
     * @return the name
     */
    public String[] getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String[] name) {
        this.name = name;
    }

    /**
     * @return the size
     */
    public int[] getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int[] size) {
        this.size = size;
    }
}
