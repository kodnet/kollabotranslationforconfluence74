package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import java.util.HashMap;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author TamPT
 */
public class FileImportSettingRequest extends RequestModelBase {

    public String name;
    public Map<String, Object> fileImportSettings;

    public FileImportSettingRequest() {
        this.name = "KodFileImportSeting";
        this.fileImportSettings = new HashMap<String, Object>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getFileImportSettings() {
        return fileImportSettings;
    }

    public void setFileImportSettings(Map<String, Object> fileImportSettings) {
        this.fileImportSettings = fileImportSettings;
    }

    public void setInlineTag(String value) {
        Map<String, String> mapXml = new HashMap<String, String>();
        if (this.fileImportSettings.containsKey("xml") == true) {
            mapXml = (Map<String, String>) this.fileImportSettings.get("xml");
        }

        mapXml.put("inlineElementsPlain", value);
        this.fileImportSettings.put("xml", mapXml);
    }

    public void setNotInlineTag(String value) {
        Map<String, String> mapXml = new HashMap<String, String>();
        if (this.fileImportSettings.containsKey("xml") == true) {
            mapXml = (Map<String, String>) this.fileImportSettings.get("xml");
        }

        mapXml.put("inlineElementsNonTranslatablePlain", value);
        this.fileImportSettings.put("xml", mapXml);
    }
}
