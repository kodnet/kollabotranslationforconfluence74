package jp.co.kodnet.confluence.plugins.kodtranslation.common;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TamPT
 */
public class Constants {

    // Plugin Key
    public static String PLUGIN_KEY = "";
    public static String DIRECTORY_RESOURCE_KEY = ":kodtranslationdirectory-resources";
    public static String CORE_SETTING_RESOURCE_KEY = ":translation-core-settings-resources";
    public static String HOME_RESOURCE_KEY = ":kodtranslationhome-resources";
    public static String DIALOG_RESOURCE_KEY = ":kodtranslationdirectory-dialog-resources";

    //Memory Const
    public static final int NONE_ID = -1;
    public static final int MEMSOURCE_ID = 1;
    public static final int TRADOS_ID = 2;
    public static final int OMEGAT_ID = 3;
    public static final int GOOGLE_ID = 4;
    public static final int RICOH_ID = 5;
    public static final int T400_ID = 6;
    public static final String TRANSLATION_MANAGEMENT_STRING = "kod.plugins.kodtranslationdirectory.translation.management.string";
    public static final String TRANSLATION_NAME = "Translation Management";
     public static final String NONE_LANG = "NONE_LANG";

    public static String MEMSOURCE_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_MEMSOURCE = "";
    public static String KOD_TRANSLATION_MEMSOURCE_PACKAGE = "";
    public static String MEMSOURCE_NAME = "";
    public static final String MEMSOURCE_STRING = "kod.plugins.kodtranslationdirectory.translation.memsource.string";
    public static String KOD_TRANSLATION_MEMSOURCE_UTIL = ".MemsourceUtilImpl";
    public static String KOD_TRANSLATION_HOME_MEMSOURCE_UTIL = ".HomeMemsourceUtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_MEMSOURCE_UTIL = ".DirectoryMemsourceUtilImpl";
    public static String KOD_TRANSLATION_DETAILS_MEMSOURCE_UTIL = ".DetailsMemsourceUtilImpl";
    public static String KOD_TRANSLATION_VERSION_MEMSOURCE_UTIL = ".version.VersionMemsourcelUtilImpl";

    public static String TRADOS_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_TRADOS = "";
    public static String KOD_TRANSLATION_TRADOS_PACKAGE = "";
    public static String TRADOS_NAME = "";
    public static final String TRADOS_STRING = "kod.plugins.kodtranslationdirectory.translation.trados.string";
    public static String KOD_TRANSLATION_TRADOS_UTIL = ".TradosUtilImpl";
    public static String KOD_TRANSLATION_HOME_TRADOS_UTIL = ".HomeTradosUtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_TRADOS_UTIL = ".DirectoryTradosUtilImpl";
    public static String KOD_TRANSLATION_DETAILS_TRADOS_UTIL = ".DetailsTradosUtilImpl";
    public static String KOD_TRANSLATION_VERSION_TRADOS_UTIL = ".version.VersionTradoslUtilImpl";

    public static String OMEGAT_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_OMEGAT = "";
    public static String KOD_TRANSLATION_OMEGAT_PACKAGE = "";
    public static String OMEGAT_NAME = "";
    public static final String OMEGAT_STRING = "kod.plugins.kodtranslationdirectory.translation.omegat.string";
    public static String KOD_TRANSLATION_OMEGAT_UTIL = ".OmegaTUtilImpl";
    public static String KOD_TRANSLATION_HOME_OMEGAT_UTIL = ".HomeOmegaTUtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_OMEGAT_UTIL = ".DirectoryOmegaTUtilImpl";
    public static String KOD_TRANSLATION_DETAILS_OMEGAT_UTIL = ".DetailsOmegaTUtilImpl";
    public static String KOD_TRANSLATION_VERSION_OMEGAT_UTIL = ".version.VersionOmegaTUtilImpl";

    public static String GOOGLE_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_GOOGLE = "";
    public static String KOD_TRANSLATION_GOOGLE_PACKAGE = "";
    public static String GOOGLE_NAME = "";
    public static final String GOOGLE_STRING = "kod.plugins.kodtranslationdirectory.translation.google.string";
    public static String KOD_TRANSLATION_GOOGLE_UTIL = ".GoogleUtilImpl";
    public static String KOD_TRANSLATION_HOME_GOOGLE_UTIL = ".HomeGoogleUtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_GOOGLE_UTIL = ".DirectoryGoogleUtilImpl";
    public static String KOD_TRANSLATION_DETAILS_GOOGLE_UTIL = ".DetailsGoogleUtilImpl";
    public static String KOD_TRANSLATION_VERSION_GOOGLE_UTIL = ".version.VersionGoogleUtilImpl";

    public static String T400_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_T400 = "";
    public static String KOD_TRANSLATION_T400_PACKAGE = "";
    public static String T400_NAME = "";
    public static final String T400_STRING = "kod.plugins.kodtranslationdirectory.translation.t400.string";
    public static String KOD_TRANSLATION_T400_UTIL = ".T400UtilImpl";
    public static String KOD_TRANSLATION_HOME_T400_UTIL = ".HomeT400UtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_T400_UTIL = ".DirectoryT400UtilImpl";
    public static String KOD_TRANSLATION_DETAILS_T400_UTIL = ".DetailsT400UtilImpl";
    public static String KOD_TRANSLATION_VERSION_T400_UTIL = ".version.VersionT400UtilImpl";

    public static String RICOH_NAME_LICENSE = "";
    public static String KOD_TRANSLATION_RICOH = "";
    public static String KOD_TRANSLATION_RICOH_PACKAGE = "";
    public static String RICOH_NAME = "";
    public static final String RICOH_STRING = "kod.plugins.kodtranslationdirectory.translation.trados.string";
    public static String KOD_TRANSLATION_RICOH_UTIL = ".TradosUtilImpl";
    public static String KOD_TRANSLATION_HOME_RICOH_UTIL = ".HomeTradosUtilImpl";
    public static String KOD_TRANSLATION_DIRECTORY_RICOH_UTIL = ".DirectoryTradosUtilImpl";
    public static String KOD_TRANSLATION_DETAILS_RICOH_UTIL = ".DetailsTradosUtilImpl";
    public static String KOD_TRANSLATION_VERSION_RICOH_UTIL = ".version.VersionTradoslUtilImpl";

    public static String KOD_TRANSLATION_VERSION = "";
    public static String KOD_TRANSLATION_VERSION_PACKAGE = "";
    public static String KOD_TRANSLATION_VERSION_HTML_UTIL = ".HtmlVersionUtilImpl";

    // SupportedLanguages.csv
    public static final int NUMBER_COLUMN_IN_CSV = 3;

    // SelectedModuleType.csv
    public static final int CSV_SELECTED_MODULE_TYPE_ALL_COLUMN = 4;
    public static final String CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_1_NAME = "ID";
    public static final String CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_2_NAME = "TypeName";
    public static final String CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_3_NAME = "Selected";
    public static final String CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_4_NAME = "License";
    public static final int MODULE_TYPE_NORMAL_ID = 1;
    public static final int MODULE_TYPE_MARKETPLACE_ID = 2;
    public static final int MODULE_TYPE_SELECTED = 1;

    // SelectedConnecterType.csv
    public static final int CSV_SELECTED_CONNECTOR_TYPE_ALL_COLUMN = 5;
    public static final String CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_1_NAME = "ID";
    public static final String CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_2_NAME = "TMEMNAME";
    public static final String CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_3_NAME = "TMEMNAME_EN";
    public static final String CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_4_NAME = "TMEM_ID";
    public static final String CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_5_NAME = "ACTIVE";

    // License 
    public static final int FREE_VERSION = 0;
    public static final int PAID_VERSION = 1;
    public static final int DEVELOPMENT_VERSION = 2;

    //Memory status
    public static final int STATUS_BASE = 0;
    public static final int STATUS_COPPYED = 1;
    public static final int STATUS_NOT_TRANSLATE = 2;
    public static final int STATUS_TRANSLATING = 3;
    public static final int STATUS_TRANSLATTED = 4;
    public static final int STATUS_CANCELLED = 5;
    public static final int STATUS_ADDNEW = 6;
    public static final int STATUS_TRANSLATING_PARTIAL = 7;
    public static final int STATUS_NOT_TRANSLATE_PARTIAL = 8;
    public static final int STATUS_TRANSLATING_NOT_TRANSLATE_PARTIAL = 9;
    public static final int STATUS_DELETE = 10;
    public static final int STATUS_COMPLETED_BY_LINGUIST = 11;
    public static final int STATUS_ASSIGNED_BY_LINGUIST = 12;
    public static final int STATUS_CAN_NOT_TRANSLATE = 13;

    //Action excuted
    public static final int ACTION_TRANSLATION = 1;
    public static final int ACTION_RECOPY = 2;
    public static final int ACTION_OUTDATE = 3;
    public static final int ACTION_SYNC = 4;
    public static final int ACTION_REEXPORT = 5;

    //Common
    public static final String SPACE_KEY_VERSION = "V";
    public static final String SPACE_KEY_LANG = "L";
    public static final String VERSION_PREFIX = "Ver";
    public static final String HOME = "Home";
    public static String SPLIT_WORK_FLOWS = "|";
    public static String SPLIT_STRING = "\\|";
    public static String SPLIT_STRING_CEATE_LANG = "(\\$\\$\\$)";
    public static String STRING_DISTINS = "$$$";
    public static String STRING_DISTINS_SYNC = "SYNC$$$";
    public static String SPLIT_STRING_SYNC_LANG = "(SYNC\\$\\$\\$)";
    public static String LABLE_REFERENCE = "references";
    public static final String TITLE_PROPERTY = "TITLE_FOR_TRANSLATION_MANAGEMENT";
    public static final String ERROR_PREFIX = "ERROR$$$";
    public static final String WAITTING_PREFIX = "WAITTING$$$";
    public static final String INFO_PREFIX = "INFO$$$";
    public static String PAGE_STATUS = "deleted";
    public static final String ERROR = "error";
    public static final String COPY = "copy";
    
    //Condition Group
    public static final int LIMIT_MEMBERS = 25;
    
    //XML execute
    public static final String EXTENSION_XML = ".xml";
    public static final int MAX_FILE_LENGTH = 240;
    public static final String BASE_SUBFIX = "_base";
    public static final String NO_TRANSLATION = "notrans";

    //Version page status
    public final static int EXECUTE_MODE_ADD_VERSION = 1;
    public final static int EXECUTE_MODE_ADD_REVISION = 2;
    public final static int EXECUTE_MODE_UPDATE = 3;

    public static final int VERSION_STATUS_COPY = 1;
    public static final int VERSION_STATUS_REFERENCE = 2;
    public static final int VERSION_STATUS_SKIP = 3;

    //MACRO_STRING
    public static final String MACRO_REFERENCE = "<p><ac:structured-macro ac:name=\"honyakureference\"><ac:parameter ac:name=\"page\"><ac:link><ri:page ri:space-key=\"%s\" ri:content-title=\"%s\" /></ac:link></ac:parameter></ac:structured-macro></p>";

    //DRAWIO
    public static final String DIAGRAMS = "diagrams";
    public static final String ATTACHMENT_ID = "attachmentId";
    public static final String NEW_IMG_VER = "isNewImgVersion";
    public static final String HAS_UPDATED = "hasUpdated";
    public static final String PAGE_ID = "pageId";
    public static final String DIAGRAM_FILE_NAME = "name";
    public static final String REVISION = "revision";
    public static final String DRAWIO_IMAGE_NAME = "imageName";
    public static final String IMG_CURRENT_VER = "imgCurrentVersion";
    public static final String DIAGRAM_CURRENT_VER = "diagramCurrentVersion";
    public static final String ONLY_UPDATE_DRAWIO = "onlyUpdate";
    public static final String AC_STRUCTURED_MACRO = "ac:structured-macro";
    public static final String AC_NAME = "ac:name";
    public static final String DRAWIO = "drawio";
    public static final String AC_PARAMETER = "ac:parameter";
    public static final String MACRO_DIAGRAM_NAME = "diagramName";
    public static final String EXT_PNG = ".png";

    //video
    public static final String LABEL_PAGE_VIDEO = "video";
    
    //API
    public static final String PUT_METHOD = "PUT";
    public static final String POST_METHOD = "POST";
    public static final String GET_METHOD = "GET";
    public static final String DELETE_METHOD = "DELETE";
    public static final int CONTENT_TYPE_BODY_FORM = 0;
    public static final int CONTENT_TYPE_BODY_JSON = 1;

    public static final String IMAGE_DIRECTORY_PAGE = "imagedirectory";
    public static final String IMAGE_DIRECTORY_LABEL = "imagedirectory";
    
    public static final String UI_CONTROL_TITLE_PAGE = "uicontrol";
    public static final String UI_CONTROL_TITLE_PAGE_LABEL = "uicontrol";
    public static final String UI_CONTROL_TITLE_PAGE_LABEL_TBD = "tbd";
    
    //Bandana Context favorite language
    public static final String BANDANA_CONTEXT_FAVORITELANGUAGE = "FAVORITELANGUAGE";
    public static final String BANDANA_KEY_TRADOS = "TradosFavoriteLanguage";
    public static final String BANDANA_KEY_MEMSOURCES = "MemsourcesFavoriteLanguage";
    public static final String BANDANA_KEY_OMEGAT = "OmegaTFavoriteLanguage";
    public static final String BANDANA_KEY_GOOGLE = "GoogleFavoriteLanguage";
    public static final String BANDANA_KEY_T400 = "T400FavoriteLanguage";
    public static final String BANDANA_KEY_RICOH = "RicohFavoriteLanguage";
    public static final String BANDANA_KEY_LANGUAGES_SUPPORT_SELECTED = "LangsSupportSelected";
    public static final String BANDANA_KEY_FIRST_LANG_DEFAULT = "LangSupportDefault";
    public static final String BANDANA_CONTEXT_INLINE_TAG = "BANDANA_CONTEXT_INLINE_TAG";
    public static final String BANDANA_CONTEXT_NOT_INLINE_TAG = "BANDANA_CONTEXT_NOT_INLINE_TAG";
    public static final String BANDANA_KEY_INLINE_TAG_CONFIG = "BANDANA_KEY_INLINE_TAG_CONFIG";
    public static final String BANDANA_KEY_NOT_INLINE_TAG_CONFIG = "BANDANA_KEY_NOT_INLINE_TAG_CONFIG";
    public final static String INLINE_TAG_DEFAULT_CONFIG = "a,b,big,i,small,tt,abbr,acronym,cite,code,dfn,em,kbd,strong,samp,time,var,bdo,img,map,object,q,script,span,sub,sup,label,ins,mark,del,image,attachment,link,page,structured-macro,parameter,plain-text-link-body,inline-comment-marker";
    
    //first lang default
    public static final String FIRST_LANG_KEY_DEFAULT = "ja";
    public static final String FIRST_LANG_NAME_DEFAULT = "Japanese";
    
    //Move attachment event
    public static final String PAGE_WAS_SAVED = "pagewassaved";
    
    //Memsource
     public static final String KOD_MEMSOURCE_PROJECT_NAME = "kod translation for ";
    public static final String MEMSOURCE_API_BASE_URL = "https://cloud.memsource.com/web/";
    public static final String MEMSOURCE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String CRLF = "\r\n";
    public static final String ZENDDESK_API_BASE_URL = "https://kodnet.zendesk.com/";
    public static final String ZENDDESK_REQUEST_SUBJECT = "Memsource Ultimateの購入申し込み：%s";
    public static final String ZENDDESK_REQUEST_BODY = "%s%n%s";
    public static final String ZENDDESK_BODY_DATE_FORMAT = "yyyy/MM/dd";
    public static final String ZENDDESK_TAGS = "Memsource_license";
    public static final int ZENDDESK_MAX_USER = 4;
    public static final int ZENDDESK_MAX_SEND_REQUEST = 2;
    public static final int MAX_JOB_NUMBER = 25;
    public static final String SUCCESS = "success";
    public static final int NUMJOB = 50;
    
    public static final String MEMSOURCE_CLIENT_NAME = "Confluence";
    public static final String MEMSOURCE_MAIL = "dummy@dummy.co.jp";
    public static final String MEMSOURCE_CONTACT_NAME = "dummy";
    public static final String MEMSOURCE_COMPANY_NAME = "dummy";

    public static final String MEMSOURCE_STATUS_NEW = "NEW";
    public static final String MEMSOURCE_STATUS_DELIVERED = "DELIVERED";
    public static final String MEMSOURCE_STATUS_COMPLETED = "COMPLETED";
    public static final String MEMSOURCE_STATUS_ACCEPTED = "ACCEPTED";
    public static final String MEMSOURCE_STATUS_CANCELLED = "CANCELLED";
    
    public static final int MEMSOURCE_STATUS_CREATE_NEW = 1;
    public static final int MEMSOURCE_STATUS_TRANSLATED = 2;
    
    public static final List<String> WORKFLOWS = new ArrayList<String>();

    public static final Boolean IS_RECAPTURE = true;
    public static final Boolean IS_NOT_RECAPTURE = true;
    
    public static final String  SAVE_TO_TRANS_MEMORY = "None";
    
    static {
        WORKFLOWS.add("T");
        WORKFLOWS.add("R");
    }
    
    public static final String BANDANA_KEY_FILE_SETTING_CONFIG = "BANDANA_KEY_FILE_SETTING_CONFIG";
}
