package jp.co.kodnet.confluence.plugins.kodtranslation.cloud;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;

/**
 * @author anhlq
 */
public class MultipartUtility {

    private HttpURLConnection httpConn;
    private DataOutputStream request;
    private final String boundary = "*****";
    private final String crlf = "\r\n";
    private final String twoHyphens = "--";
    private String requestMethod;
    private String requestURL;
    private int contentType;

    public MultipartUtility(String requestURL) throws Exception {
        this.init(requestURL, Constants.POST_METHOD, Constants.CONTENT_TYPE_BODY_FORM);
    }

    public MultipartUtility(String requestURL, String requestMethod) throws Exception {
        this.init(requestURL, requestMethod, Constants.CONTENT_TYPE_BODY_FORM);
    }

    public MultipartUtility(String requestURL, String requestMethod, int contentType) throws Exception {
        this.init(requestURL, requestMethod, contentType);
    }

    public void init(String requestURL, String requestMethod, int contentType) throws Exception {
        // creates a unique boundary based on time stamp
        this.requestURL = requestURL;
        this.requestMethod = requestMethod;
        this.contentType = contentType;
        URL url = new URL(requestURL);
        httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setUseCaches(false);
        httpConn.setDoInput(true);
        httpConn.setRequestProperty("Connection", "Keep-Alive");
        httpConn.setRequestProperty("Keep-Alive", "header");
        httpConn.setRequestProperty("Cache-Control", "no-cache");
        httpConn.setRequestProperty("X-Atlassian-Token", "no-check");

        if (requestMethod.equals(Constants.GET_METHOD)) {
            httpConn.setRequestMethod(Constants.GET_METHOD);
            httpConn.setRequestProperty("Accept", "*");
        } else if (requestMethod.equals(Constants.POST_METHOD)) {
            httpConn.setDoOutput(true); // indicates POST method
            httpConn.setRequestMethod(Constants.POST_METHOD);
            if (this.contentType == Constants.CONTENT_TYPE_BODY_FORM) {
                httpConn.setRequestProperty("Content-Type", "multipart/form-data; charset=UTF-8;boundary=" + this.boundary);
            } else if (this.contentType == Constants.CONTENT_TYPE_BODY_JSON) {
                httpConn.setRequestProperty("Content-Type", "application/json; charset=UTF-8;");
            }
            request = new DataOutputStream(httpConn.getOutputStream());
        }
    }

    public void addFormField(String name, String value) throws IOException {
        if (request == null) {
            return;
        }
        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
        request.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"" + this.crlf);
        request.writeBytes("Content-Type: text/plain; charset=UTF-8" + this.crlf);
        request.writeBytes(this.crlf);
        request.write(value.getBytes("UTF-8"));
        request.writeBytes(this.crlf);
        request.flush();
    }

    public void addJsonField(String value) throws IOException {
        if (request == null) {
            return;
        }

        request.write(value.getBytes("UTF-8"));
        request.flush();
    }

    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        if (request == null) {
            return;
        }
        String fileName = uploadFile.getName();
        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
        request.writeBytes("Content-Disposition: form-data; name=\""
                + fieldName + "\"; filename*=UTF-8''\""
                + URLEncoder.encode(fileName, "UTF-8") + "\"" + this.crlf);
        request.writeBytes(this.crlf);

        byte[] bytes = Files.readAllBytes(uploadFile.toPath());
        request.write(bytes);
        request.writeBytes(this.crlf);
    }

    public void addFilePart(String fieldName, InputStream inputStream, String fileName)
            throws IOException {
        if (request == null) {
            return;
        }
        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
        request.writeBytes("Content-Disposition: form-data; name=\""
                + fieldName + "\"; filename*=UTF-8''\""
                + URLEncoder.encode(fileName, "UTF-8") + "\"" + this.crlf);
        request.writeBytes(this.crlf);

        byte[] buffer = new byte[4096];
        int read = 0;
        while ((read = inputStream.read(buffer)) != -1) {
            request.write(buffer);
        }
        request.writeBytes(this.crlf);
    }

    public HttpURLConnection finishing() throws IOException {
        String response = "";
        if (request == null || this.contentType == Constants.CONTENT_TYPE_BODY_JSON) {
            return httpConn;
        }

        request.writeBytes(this.crlf);
        request.writeBytes(this.twoHyphens + this.boundary + this.twoHyphens + this.crlf);
        request.flush();
        request.close();
        return httpConn;
    }

    public String finish() throws IOException {
        String response = "";
        if (request != null && this.contentType == Constants.CONTENT_TYPE_BODY_FORM) {
            request.writeBytes(this.crlf);
            request.writeBytes(this.twoHyphens + this.boundary
                    + this.twoHyphens + this.crlf);

            request.flush();
            request.close();
        }

        // checks server's status code first
        int status = httpConn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            InputStream responseStream = new BufferedInputStream(httpConn.getInputStream());

            BufferedReader responseStreamReader
                    = new BufferedReader(new InputStreamReader(responseStream));

            String line = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = responseStreamReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            response = stringBuilder.toString();
            httpConn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return response;
    }

    public static String addParameterGet(String url, Map<String, String> parameter) throws Exception {
        if (!url.contains("?")) {
            url += "?";
        }

        int i = 1;
        for (Map.Entry<String, String> item : parameter.entrySet()) {
            url = url + item.getKey() + "=" + URLEncoder.encode(item.getValue(), "UTF-8");
            if (i < parameter.size()) {
                url += "&";
                i++;
            }
        }
        return url;
    }

    public void addParameter(Map<String, String> parameter) throws Exception {
        if (requestMethod.equals(Constants.GET_METHOD)) {
            if (!this.requestURL.contains("?")) {
                requestURL += "?";
            }

            int i = 1;
            for (Map.Entry<String, String> item : parameter.entrySet()) {
                requestURL = requestURL + item.getKey() + "=" + URLEncoder.encode(item.getValue(), "UTF-8");
                if (i < parameter.size()) {
                    requestURL += "&";
                    i++;
                }
            }
        } else if (requestMethod.equals(Constants.POST_METHOD)) {
            if (this.contentType == Constants.CONTENT_TYPE_BODY_FORM) {
                for (Map.Entry<String, String> item : parameter.entrySet()) {
                    this.addFormField(item.getKey(), item.getValue());
                }
            } else if (this.contentType == Constants.CONTENT_TYPE_BODY_JSON) {
                String value = parameter.get("data");
                this.addJsonField(value);
            }
        }
    }

    public void addHeader(String key, String value) throws Exception {
        httpConn.setRequestProperty(key, value);
    }

    public void addHeader(Map<String, String> header) throws Exception {
        for (Map.Entry<String, String> item : header.entrySet()) {
            httpConn.setRequestProperty(item.getKey(), item.getValue());
        }
    }
}
