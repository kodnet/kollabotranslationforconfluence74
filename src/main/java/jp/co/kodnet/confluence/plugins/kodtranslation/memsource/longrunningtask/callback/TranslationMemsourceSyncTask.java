package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.util.longrunning.ConfluenceAbstractLongRunningTask;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;


public class TranslationMemsourceSyncTask extends ConfluenceAbstractLongRunningTask {

        protected TranslationMemsourceSyncCallBack translationMemsourceSyncCallBack;

    public TranslationMemsourceSyncTask(TranslationMemsourceSyncCallBack translationMemsourceSyncCallBack) {
        this.translationMemsourceSyncCallBack = translationMemsourceSyncCallBack;
    }

    @Override
    protected void runInternal() {
        try {
            TransactionTemplate transactionTemplate = TranslationMemoryToolFactory.getTransactionTemplate();
            translationMemsourceSyncCallBack.setProgress(progress);
            Object result = transactionTemplate.execute(translationMemsourceSyncCallBack);
        } catch (Exception e) {

            throw new RuntimeException(e);
        } finally {
            this.progress.setPercentage(100);
            this.progress.setCompletedSuccessfully(true);
        }
    }

    @Override
    public String getName() {
        if (this.translationMemsourceSyncCallBack == null) {
            return this.getClass().getName();
        }
        return this.translationMemsourceSyncCallBack.getName();
    }
}
