package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.LoginUserRequest;

/**
 *
 * @author lucvd
 */
public class LoginResponse extends ResponseModelBase {

    private String token;
    private String expires;
    private LoginUserRequest user;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the expires
     */
    public String getExpires() {
        return expires;
    }

    /**
     * @param expires the expires to set
     */
    public void setExpires(String expires) {
        this.expires = expires;
    }

    /**
     * @return the user
     */
    public LoginUserRequest getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(LoginUserRequest user) {
        this.user = user;
    }

}
