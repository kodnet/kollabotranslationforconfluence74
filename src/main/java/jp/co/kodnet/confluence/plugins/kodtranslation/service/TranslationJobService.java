package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;

@Transactional
public interface TranslationJobService {

    List<TranslationJob> getAll();

    TranslationJob getOneByLang(String fromLanguageKey, String toLanguageKey, String spaceKey);
    
    TranslationJob getOneByLangAndFromPageId(String fromLanguageKey, String toLanguageKey, Long fromPageId);
    
    TranslationJob getOneByPageId(Long fromPageId, Long toPageId);
    
    List<TranslationJob> getBySpaceKey(String spaceKey);
    
    TranslationJob getByToLangKey(String spaceKey, Long fromPageId, String toLangKey);
    
    List<TranslationJob> getByPageIdAndStatus(Long fromPageId, int status);
    
    List<TranslationJob> getByTranslationJobByFromPageId(Long fromPageId);
    
    List<TranslationJob> getByPageIdIsTranlated(Long fromPageId, int status);
    
    TranslationJob getOneByFromPageId(Long fromPageId);
    
    TranslationJob getOneByToPageId(Long toPageId);
    
    TranslationJob getByProjectId(Long fromPageId, String projectId, String jobId);

    TranslationJob add(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
    );
    
    TranslationJob updateByLang(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
            );
    
    TranslationJob updateByPageId(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
    );

    void setToPageIdAndStatus(TranslationJob translationJob, Long toPageId, int status);

    void deleteByFromPageId(String fromLanguageKey, String toLanguageKey, String spaceKey, Long fromPageId);

    void deleteAll();  
}
