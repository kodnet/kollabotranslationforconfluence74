package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateClientRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateClientResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteClientRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteClientResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetClientRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetClientResponse;

/**
 *
 * @author anhlq
 */
public class ClientConnector {

    private final static String GET_LIST_API_URL = "api2/v1/clients";
    private final static String CREATE_CLIENT_API_URL = "api2/v1/clients";
    //api2/v1/clients/{clientId}
    private final static String DELETE_CLIENT_API_URL = "api2/v1/clients/%s";
    private final String token;

    public ClientConnector(String token) {
        this.token = token;
    }

    public GetClientResponse get() throws Exception {
        GetClientRequest request = new GetClientRequest();
        request.setToken(this.token);
        return (GetClientResponse) MemoryRequestUtil.createRequestJson(jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants.MEMSOURCE_API_BASE_URL + GET_LIST_API_URL, request, GetClientResponse.class, Constants.GET_METHOD);
    }

    public CreateClientResponse create(String clientId) throws Exception {
        CreateClientRequest request = new CreateClientRequest();
        request.setToken(this.token);
        request.setName(clientId);
        request.setExternalId(clientId);
        return (CreateClientResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + CREATE_CLIENT_API_URL, request, CreateClientResponse.class);
    }

    public DeleteClientResponse delete(String clientId) throws Exception {
        DeleteClientRequest request = new DeleteClientRequest();
        request.setToken(this.token);
        String url = String.format(DELETE_CLIENT_API_URL, clientId);
        return (DeleteClientResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, DeleteClientResponse.class, Constants.DELETE_METHOD);
    }
}
