package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import java.io.InputStream;
import java.io.OutputStream;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteFileRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteFileResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DownloadFileRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DownloadFileResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UploadFileRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UploadFileResponse;

/**
 *
 * @author lucvd
 */
public class FileConnector {

    private final static String UPLOAD_REQUEST_BODY_API_URL = "api2/v1/files";
    //api2/v1/files/{fileUid}
    private final static String DELETE_API_URL = "api2/v1/files/%s";
    private final static String DOWNLOAD_API_URL = "api/v2/file/download";

    private final String token;

    public FileConnector(String token) {
        this.token = token;
    }

    public UploadFileResponse uploadRequestBody(InputStream inputStream, String fileName) throws Exception {
        UploadFileRequest request = new UploadFileRequest();
        request.setName(fileName);
        request.setFilename(fileName);
        request.setToken(this.token);
        return (UploadFileResponse) MemoryRequestUtil.createRequestUploadWithMultipart(Constants.MEMSOURCE_API_BASE_URL + UPLOAD_REQUEST_BODY_API_URL,
                inputStream, request, UploadFileResponse.class);
    }

    public DeleteFileResponse delete(String uid) throws Exception {
        DeleteFileRequest request = new DeleteFileRequest();
        request.setToken(this.token);
        String url = String.format(DELETE_API_URL, uid);
        return (DeleteFileResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, DeleteFileResponse.class, Constants.GET_METHOD);
    }

    public DownloadFileResponse download(String uid, OutputStream outputStream) throws Exception {
        DownloadFileRequest request = new DownloadFileRequest();
        request.setUploadedFile(uid);
        request.setToken(this.token);

        return (DownloadFileResponse) MemoryRequestUtil.createRequestDownload(Constants.MEMSOURCE_API_BASE_URL + DOWNLOAD_API_URL, outputStream, request, DownloadFileResponse.class);
    }
}
