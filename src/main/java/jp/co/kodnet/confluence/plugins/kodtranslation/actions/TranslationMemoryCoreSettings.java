package jp.co.kodnet.confluence.plugins.kodtranslation.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.plugin.Plugin;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.opensymphony.xwork.Action.ERROR;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationFavoriteLanguageService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationGroupInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;
import org.apache.commons.lang.StringUtils;

public class TranslationMemoryCoreSettings extends ConfluenceActionSupport implements Beanable {

    private Map<String, Object> map;
    public Boolean result = true;
    public String options = "";
    public String message = "";
    public String favoriteLang = "";
    private String userName;
    private String password;

    @Override
    public Object getBean() {
        return map;
    }

    public String coreSetting() {
        try {
            TranslationApiLoginInfoService translationLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
            ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
            Boolean isSystemAd = permissionManager.isSystemAdministrator(confluenceUser);
            Boolean isConfluenceAd = permissionManager.isConfluenceAdministrator(confluenceUser);
            if (isSystemAd || isConfluenceAd) {
                //Info memsource
                ApiLoginInfo memsourceLogin = translationLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
                if (memsourceLogin == null) {
                    this.userName = StringUtils.EMPTY;
                    this.password = StringUtils.EMPTY;
                } else {
                    this.userName = memsourceLogin.getUserName();
                    this.password = memsourceLogin.getPassword();
                    Date dateExpires = memsourceLogin.getDateExpires();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    if (dateExpires != null && (sdf.parse(sdf.format(dateExpires)).before(sdf.parse(sdf.format(new Date()))) || sdf.parse(sdf.format(dateExpires)).equals(sdf.parse(sdf.format(new Date()))))) {
                        CommonFunction.doLoginMemsource(memsourceLogin.getUserName(), memsourceLogin.getPassword(), String.valueOf(Constants.MEMSOURCE_ID), "", "", "");
                    }
                }

                this.result = true;
                return SUCCESS;
            }
        } catch (Exception e) {
            this.result = false;
            this.message = e.getMessage();
            MemoryLogUtil.outputLogException(this.getClass(), e);
        }
        this.result = false;
        return ERROR;
    }

    public String doCoreSetting() {
        try {
            ActionContext context = ActionContext.getContext();
            String inusetmemId = ActionContextHelper.getFirstParameterValueAsString(context, "translationcore");
            if (inusetmemId.equals("-1")) {
                this.result = false;
                this.message = this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.save.success");
                return ERROR;
            }
            try {
                String userNameMem = ActionContextHelper.getFirstParameterValueAsString(context, "user-name");
                String passwordMem = ActionContextHelper.getFirstParameterValueAsString(context, "password");
                String email = ActionContextHelper.getFirstParameterValueAsString(context, "email");
                String inchargedName = ActionContextHelper.getFirstParameterValueAsString(context, "inchargedName");
                String companyName = ActionContextHelper.getFirstParameterValueAsString(context, "companyName");
                this.map = CommonFunction.doLoginMemsource(userNameMem, passwordMem, String.valueOf(Constants.MEMSOURCE_ID), email, inchargedName, companyName);
            } catch (Exception ex) {
                MemoryLogUtil.outputLogException(this.getClass(), ex);
                this.map = MemoryLogUtil.getBeanFaild(ex);
            }

            this.result = (Boolean) map.get("result");
            if (result) {
                this.message = this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.save.success");
            } else {
                this.message = (String) map.get("message");
            }

            TranslationApiLoginInfoService translationLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
            //Info memsource
            ApiLoginInfo memsourceLogin = translationLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
            if (memsourceLogin == null) {
                this.userName = StringUtils.EMPTY;
                this.password = StringUtils.EMPTY;
            } else {
                this.userName = memsourceLogin.getUserName();
                this.password = memsourceLogin.getPassword();
            }
        } catch (Exception e) {
            this.result = false;
            this.message = e.getMessage();
            MemoryLogUtil.outputLogException(this.getClass(), e);
        }
        return SUCCESS;
    }
    
    public String getInlinetag() {
        try {
            this.map = CommonFunction.renderInlineTag();
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String getNotInlinetag() {
        try {
            this.map = CommonFunction.renderNotInlineTag();
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String doSaveInlinetag() {
        try {
            ActionContext context = ActionContext.getContext();
            String value = ActionContextHelper.getFirstParameterValueAsString(context, "inline-tag-input");
            this.map = CommonFunction.doSaveInlineTag(value);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String doSaveNotInlinetag() {
        try {
            ActionContext context = ActionContext.getContext();
            String value = ActionContextHelper.getFirstParameterValueAsString(context, "not-inline-tag-input");
            this.map = CommonFunction.doSaveNotInlineTag(value);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String doSaveSelectedLanguage() {
        try {
            Map<String, Object> resultMap = new HashMap<>();
            I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
            ActionContext context = ActionContext.getContext();
            List<String> keyLanguages = ActionContextHelper.getParameterValuesAsStringList(context, "keyLanguage");
            List<String> nameLanguages = ActionContextHelper.getParameterValuesAsStringList(context, "nameLanguage");
            List<String> displayIndexs = ActionContextHelper.getParameterValuesAsStringList(context, "displayIndex");

            TranslationFavoriteLanguageService translationFavoriteLanguageService = TranslationMemoryToolFactory.getTranslationFavoriteLanguageService();
            translationFavoriteLanguageService.deleteAll();
            for (int i = 0; i < keyLanguages.size(); i++) {
                String langKey = keyLanguages.get(i);
                String langName = nameLanguages.get(i);
                String positon = displayIndexs.get(i);
                translationFavoriteLanguageService.add(positon, langKey, langName);
            }
            resultMap.put("result", true);
            resultMap.put("message", i18n.getText("kod.plugins.kodtranslationdirectory.translation.management.directory.server.setting.language.save.success"));
            this.map = resultMap;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String doSaveTemplateByLabel() {
        try {
            Map<String, Object> resultMap = new HashMap<>();
            TranslationLabelTemplateService translationLabelTemplateService = TranslationMemoryToolFactory.getTranslationLabelTemplateService();
            I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
            ActionContext context = ActionContext.getContext();
            String listLabelTemplate = ActionContextHelper.getFirstParameterValueAsString(context, "listLabelTemplate");
            if(listLabelTemplate == null || listLabelTemplate.isEmpty()){
                resultMap.put("result", false);
                resultMap.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.data.template"));
                this.map = resultMap;
                return SUCCESS;
            }
            JsonParser parser = new JsonParser();
            JsonElement tradeElement = parser.parse(listLabelTemplate);
            JsonArray trade = tradeElement.getAsJsonArray();
            if(trade.isJsonNull() || trade.size() == 0){
                resultMap.put("result", false);
                resultMap.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.data.template"));
                this.map = resultMap;
                return SUCCESS;
            }
            translationLabelTemplateService.deleteAll();
            for (JsonElement element : trade) {
                JsonObject obj = element.getAsJsonObject();
                translationLabelTemplateService.add(obj.get("label").getAsString(), obj.get("templateId").getAsString(), obj.get("templateName").getAsString(), obj.get("langKey").getAsString());
            }
            resultMap.put("result", true);
            resultMap.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.save.data.template.success"));
            this.map = resultMap;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map.put("result", false);
            this.map.put("message", ex.getMessage());
        }
        return SUCCESS;
    }
    
     public String doSaveGroupCoreSetting() {
        try {
            Map<String, Object> resultMap = new HashMap<>();
            TranslationGroupInfoService translationGroupInfoService = TranslationMemoryToolFactory.getTranslationGroupInfoService();
            ActionContext context = ActionContext.getContext();
            String groupName = ActionContextHelper.getFirstParameterValueAsString(context, "groupName");
            if(groupName == null || groupName.isEmpty()){
                resultMap.put("result", false);
                resultMap.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.data.group"));
                this.map = resultMap;
                return SUCCESS;
            }

            translationGroupInfoService.delete();
            translationGroupInfoService.add(groupName);
            resultMap.put("result", true);
            resultMap.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.save.data.group.success"));
            this.map = resultMap;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.map.put("result", false);
            this.map.put("message", ex.getMessage());
        }
        return SUCCESS;
    }
    
    public void renderOptions() {
        this.options += this.getOptionTranslationCore(Constants.KOD_TRANSLATION_MEMSOURCE, Constants.MEMSOURCE_STRING, Constants.MEMSOURCE_ID);
        if (options.isEmpty()) {
            this.result = false;
            this.message = this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.none.message");
        }
    }

    public String getOptionTranslationCore(String pluginKey, String value, int key) {
        Plugin plugin = pluginAccessor.getEnabledPlugin(pluginKey);
        if (plugin == null) {
            return "";
        }

        TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
        LanguageList transMemoryStatus = translationLanguageListService.get();
        StringBuilder option = new StringBuilder();
        option.append("<option value=\"");
        option.append(key);
        option.append("\"");
        if (transMemoryStatus != null && (transMemoryStatus.getTranslationToolId()).equals(key)) {
            option.append(" selected");
        }
        option.append(" >");
        option.append(this.getText(value));
        option.append("</option>");
        return option.toString();
    }

    @HtmlSafe
    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }
//

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public boolean isMsg() {
        return this.message.isEmpty() == false;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

}
