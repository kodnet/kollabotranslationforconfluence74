package jp.co.kodnet.confluence.plugins.kodtranslation.entites;

import java.util.Date;
import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface ApiLoginInfo extends Entity {

    public String getTranslationToolId();

    public void setTranslationToolId(String translationToolId);

    public String getTranslationToolName();

    public void setTranslationToolName(String translationToolName);

    public String getUserName();

    public void setUserName(String userName);

    public String getPassword();

    public void setPassword(String password);

    public String getLoggedToken();

    public void setLoggedToken(String loggedToken);

    public String getOrtherKey02();

    public void setOrtherKey02(String ortherKey02);

    /**
     * Memsource: clientId
     *
     * @return
     */
    public String getOrtherKey01();

    /**
     * Memsource: clientId
     *
     */
    public void setOrtherKey01(String ortherKey01);
    
     /**
     * Memsource: expires
     * 
     *@return
     */
    public Date getDateExpires();
    
     /**
     * Memsource: expires
     * 
     */
    public void setDateExpires(Date date);

}
