package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.DocumentUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.ImportExportUtil;

public class TranslationMemsourceSavePageCallBack<T> implements TransactionCallback<T> {

    protected String spaceKey;
    protected Long fromPageId;
    protected Long toPageId;
    protected String toLangKey;
    protected String fromLangKey;
    protected SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
    protected PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
    protected  LabelManager labelManager = TranslationMemoryToolFactory.getLabelManager();
    protected String xmlString;
    protected TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
    protected List<Long> listIdPageFrom = new ArrayList<>();
    protected Boolean isImportSourceLang = false;

    public TranslationMemsourceSavePageCallBack(String spaceKey, Long fromPageId, Long toPageId, String fromLangKey, String toLangkey, String xmlString, List<Long> listIdPageFrom, Boolean isImportSourceLang) {
        this.spaceKey = spaceKey;
        this.fromPageId = fromPageId;
        this.toPageId = toPageId;
        this.fromLangKey = fromLangKey;
        this.toLangKey = toLangkey;
        this.xmlString = xmlString;
        this.listIdPageFrom = listIdPageFrom;
        this.isImportSourceLang = isImportSourceLang;
    }

    @Override
    public T doInTransaction() {
        try {
            MemoryLogUtil.outputTime(true, "MemoryToolCopyPageCallBack", "doInTransaction");

            Page fromPage = pageManager.getPage(fromPageId);
            if (fromPage == null) {
                MemoryLogUtil.outputLog("WARNING: MemoryToolCopyPageCallBack --> fromPage is not existed:" + fromPageId + " <= ");
                return null;
            }
            Space space = spaceManager.getSpace(spaceKey);
            Page newPage = this.updateContentEntityObject(space, fromPage, xmlString);
            if(newPage == null){
                return null;
            }
            return (T) Long.valueOf(newPage.getId());
        } catch (Exception ex) {
            MemoryLogUtil.outputLog("CopyPage ERROR PAGE: (" + fromPageId + ") ");
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            throw new RuntimeException(ex);
        }
    }

    private Page updateContentEntityObject(Space space, Page fromPage, String xmlString) throws Exception {
        try {
            boolean onlyMacroDeskExist = DocumentUtil.onlyMacroDeckExist(xmlString, toLangKey);
            List<Page> lstPageSource = pageManager.getPages(space, true);
            Page toPage = null;
            if (this.toPageId != null) {
                toPage = pageManager.getPage(this.toPageId);
            } else {
                toPage = new Page();
            }
            MemoryLogUtil.outputTime(false, "MemoryToolExecuterImpl", "doSynchronizeAll", "Create anchor");
            TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
            List<TranslationJob> translationJobs = translationJobService.getBySpaceKey(spaceKey);
           //Get list page dest
            MemoryLogUtil.outputTime(true, "MemoryToolExecuterImpl", "doSynchronizeAll", "Mapping");
            String idPage = "";
            List<Page> listPageSource = new ArrayList<Page>();
            List<Page> listPageDest = new ArrayList<Page>();
            List<Page> listPageDestRoot = new ArrayList<Page>();
            for (TranslationJob translationJob : translationJobs) {
                Long fromPageId = translationJob.getFromPageId();
                Page fromPageSource = pageManager.getPage(fromPageId);
                if (listPageSource.contains(fromPageSource)) {
                    continue;
                }

                listPageSource.add(fromPageSource);
                Page toPageTarget = null;
                TranslationJob targetJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, fromPageId);
                if (targetJob != null) {
                    Long toPageId = targetJob.getToPageId();
                    if (toPageId != null) {
                        toPageTarget = pageManager.getPage(toPageId);
                    }
                }

                if (toPageTarget != null) {
                    listPageDest.add(toPageTarget);
                    listPageDestRoot.add(toPageTarget);
                } else {
                    listPageDest.add(null);
                    listPageDestRoot.add(null);
                }
            }
            MemoryLogUtil.outputTime(false, "MemoryToolExecuterImpl", "doSynchronizeAll", "Mapping");
            Map<String, List<String>> mapOld = DocumentUtil.getListTextOfHeadingAndAnchor(listPageSource, listPageDest, fromLangKey, toLangKey);
            MemoryLogUtil.outputLog("LIST OLD ANCHOR SIZE:" + mapOld.size());
            // Create  listNew => Save macro anchor name and heading text
            Map<String, List<String>> mapNew = DocumentUtil.getListTextOfHeadingAndAnchor(listPageDest, listPageDest, fromLangKey, toLangKey);
            MemoryLogUtil.outputLog("LIST NEW ANCHOR SIZE:" + mapNew.size());
            // Create dictionaryTitle => 「title, pageId」
            Map<String, String> dictionaryTitle = new HashMap<String, String>();
            for (Page page : listPageDest) {
                if (page != null && !dictionaryTitle.containsKey(page.getTitle())) {
                    TranslationJob translationJob = translationJobService.getOneByToPageId(page.getId());
                    if(translationJob == null){
                        continue;
                    }
                    Page pageSource = pageManager.getPage(translationJob.getFromPageId());
                    dictionaryTitle.put(page.getTitle(), page.getIdAsString());
                }
            }
            
            Map<String, String> dictionaryMacro = new HashMap<String, String>();
            for (Map.Entry<String, List<String>> entry : mapOld.entrySet()) {
                String pageId = entry.getKey();
                List<String> anchorOlds = entry.getValue();
                if (mapNew.containsKey(pageId) == false) {
                    continue;
                }

                List<String> anchorNews = mapNew.get(pageId);
                for (int i = 0; i < anchorOlds.size(); i++) {
                    if (i >= anchorNews.size()) {
                        break;
                    }

                    if (!dictionaryMacro.containsKey(anchorOlds.get(i))) {
                        dictionaryMacro.put(anchorOlds.get(i), anchorNews.get(i));
                        MemoryLogUtil.outputLog("MAPPING OLD ANCHOR:" + anchorOlds.get(i) + ", NEW ANCHOR:" + anchorNews.get(i));
                    }
                }
            }
            Map<String, List<String>> mapIncludePage = DocumentUtil.getDicIncludePage(listPageDest);
            //Replace link 
            xmlString = DocumentUtil.convertEmojiToHtmlHex(xmlString);
            xmlString = DocumentUtil.replaceAttributeIncludeMacro(xmlString);
            xmlString = DocumentUtil.replaceLink(spaceKey, xmlString, listPageSource, listPageDest);
            xmlString = DocumentUtil.replacePageOutgoingLinks(spaceKey, toPage, xmlString, this.fromLangKey, this.toLangKey);
            Map<String, String> mapAnchor = DocumentUtil.getMapLinkAnchor(xmlString);
            Map<String, String> mapHeader = DocumentUtil.getMapLinkHeader(xmlString);
            xmlString = DocumentUtil.replaceAttributeAnchor(spaceKey, fromPage, xmlString, this.toLangKey, mapAnchor, mapHeader);
            xmlString = DocumentUtil.replaceAnchor(toPage, xmlString, dictionaryTitle, dictionaryMacro, mapIncludePage);
            xmlString = DocumentUtil.xmlSynchronizeProccess(xmlString, isImportSourceLang, this.getSynchronizeProccess(), onlyMacroDeskExist, this.toLangKey);
            if (onlyMacroDeskExist == true && isImportSourceLang == false) {
                DocumentUtil.convertMacroDeckSync(xmlString, fromPageId, toLangKey);
                return fromPage;
            }
            if (isImportSourceLang == false) {
                xmlString = DocumentUtil.convertMacroDeckSync(xmlString, fromPageId, toLangKey);
            } else {
                xmlString = DocumentUtil.convertMacroDeckSyncSourceLang(fromPageId, xmlString, fromLangKey);
            }
            
            ImportExportUtil.importXmlString(toPage, xmlString, false, false);
            //Set title
            String title = toPage.getProperties().getStringProperty(Constants.TITLE_PROPERTY);
            if (isImportSourceLang == false) {
                title = title + "_" + toLangKey;
            }
            title = ImportExportUtil.changeTitle(lstPageSource, title, 1, toPage.getIdAsString());           
                
            if (this.toPageId == null) {
                toPage.setTitle(title);
                toPage.setSpace(space);
                toPage.setParentPage(fromPage);
                fromPage.addChild(toPage);
                pageManager.saveContentEntity(toPage, null);
                pageManager.saveContentEntity(fromPage, null);
                spaceManager.saveSpace(space);
                doCopyAttachment(fromPage, toPage);
            } else {
                pageManager.renamePage(toPage, title);
                pageManager.saveContentEntity(toPage, null);
            }
            DocumentUtil.replacePageAnchorIncomingLinks(spaceKey, fromPage, toPage, this.fromLangKey, this.toLangKey, mapAnchor, mapHeader);
            DocumentUtil.replacePageIncomingLinks(spaceKey, fromPage, toPage, this.fromLangKey, this.toLangKey);

            List<Label> labels = fromPage.getLabels();
            if (labels.size() > 0) {
                for (int j = 0; j < labels.size(); j++) {
                    labelManager.addLabel(toPage, labels.get(j));
                }
            }

            return toPage;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            return null;
        }
    }

    private String replaceLinkBodyPage(List<Page> lstPageSource, String xml) throws Exception {
        try {
            List<Page> listPageDest = new ArrayList<Page>();
            List<Page> listPageDestRoot = new ArrayList<Page>();
            for (Page pageItem : lstPageSource) {
                Long idPage = pageItem.getId();
                if(this.listIdPageFrom.contains(idPage)){
                    listPageDestRoot.add(pageItem);
                }
                TranslationJob trans = translationJobService.getByToLangKey(spaceKey, idPage, toLangKey);
                if (trans != null) {
                    Long toPageId = trans.getToPageId();
                    if(toPageId == null){
                        continue;
                    }
                    Page pageTrans = pageManager.getPage(toPageId);
                    if (pageTrans == null) {
                        listPageDest.add(null);
                        continue;
                    }
                    listPageDest.add(pageTrans);
                } else {
                    listPageDest.add(null);
                }
            }

            String xmlplaceLink = DocumentUtil.replaceLink(spaceKey, xml, listPageDestRoot, listPageDest);
            return xmlplaceLink;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            return xml;
        }
    }
    
    protected void doCopyAttachment(Page temp, Page newPage) {
        MemoryLogUtil.outputTime(true, "MemoryToolCopyPageCallBack", "createPage", "copy attachment");
        try {
            List<Attachment> attachmentList = temp.getAttachments();
            if (attachmentList != null && attachmentList.size() > 0) {
                boolean invalid = false;
                for (Attachment attachment : attachmentList) {
                    if (attachment.getFileSize() == 0) {
                        invalid = true;
                        break;
                    }
                }
                if (!invalid) {
                    // コマンド一覧の取得
                    // 添付ファイル一覧の取得
                    CommonFunction.copyAttachmentAddLanguage(newPage, temp);
                }
            }
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
        }
        MemoryLogUtil.outputTime(false, "MemoryToolCopyPageCallBack", "createPage", "copy attachment");
    }

    public List<Integer> getSynchronizeProccess() {
        List<Integer> result = new ArrayList<Integer>();
        result.add(DocumentUtil.SKIP_TRANSLATION);
        result.add(DocumentUtil.REMOVE_GENGO_MACRO_TRANSLATION);
        return result;
    }
}
