package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import java.io.OutputStream;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateAnalysisRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateAnalysisResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteAnalysisRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteAnalysisResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DownloadAnalysisRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DownloadAnalysisResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetAnalysisRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetAnalysisResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetListAnalysisRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetListAnalysisResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

/**
 *
 * @author baoph
 */
public class AnalysisConnector {

    //api2/v2/analyses
    private final static String CREATE_NEW_ANALYSIS_API = "api2/v2/analyses";
    //api2/v2/projects/{projectUid}/analyses
    private final static String GET_LIST_ANALYSIS_API = "api2/v2/projects/%s/analyses";
    //api2/v3/analyses/{analyseId}
    private final static String GET_ANALYSIS_API = "api2/v3/analyses/%s";
    //api2/v1/analyses/{analyseId}
    private final static String DELETE_ANALYSIS_API = "api2/v1/analyses/%s";
    //api2/v1/analyses/{analyseId}/download
    private final static String DOWNLOAD_ANALYSIS_API = "api2/v1/analyses/%s/download";
    private final String token;

    public AnalysisConnector(String token) {
        this.token = token;
    }

    public CreateAnalysisResponse create(String[] jobParts, String name) throws Exception {
        if (jobParts.length == 0) {
            return (CreateAnalysisResponse) MemoryRequestUtil.createResponseInstance(CreateAnalysisResponse.class, "EXCEPTION", "Not jobUid", CREATE_NEW_ANALYSIS_API, Constants.POST_METHOD);
        }

        JobInfo[] jobs = new JobInfo[jobParts.length];
        for (int i = 0; i < jobParts.length; i++) {
            jobs[i] = new JobInfo();
            jobs[i].setUid(jobParts[i]);
        }

        return create(jobs, name);
    }

    public CreateAnalysisResponse create(JobInfo[] jobs, String name) throws Exception {
        CreateAnalysisRequest request = new CreateAnalysisRequest();
        request.setToken(this.token);
        request.setIncludeFuzzyRepetitions(true);
        request.setCountSourceUnits(true);
        if (name != null || !name.isEmpty()) {
            request.setName(name);
        }

        request.setJobs(jobs);
        return (CreateAnalysisResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + CREATE_NEW_ANALYSIS_API, request, CreateAnalysisResponse.class);
    }

    public GetListAnalysisResponse getListAnalysis(String projectUid) throws Exception {
        GetListAnalysisRequest request = new GetListAnalysisRequest();
        request.setToken(this.token);
        String url = String.format(GET_LIST_ANALYSIS_API, projectUid);
        return (GetListAnalysisResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetListAnalysisResponse.class, Constants.GET_METHOD);
    }

    public GetAnalysisResponse getAnalysis(String analyseId) throws Exception {
        GetAnalysisRequest request = new GetAnalysisRequest();
        request.setToken(this.token);
        String url = String.format(GET_ANALYSIS_API, analyseId);
        return (GetAnalysisResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetAnalysisResponse.class, Constants.GET_METHOD);
    }

    public DeleteAnalysisResponse deleteAnalysis(String analyseId) throws Exception {
        DeleteAnalysisRequest request = new DeleteAnalysisRequest();
        request.setToken(this.token);
        String url = String.format(DELETE_ANALYSIS_API, analyseId);
        return (DeleteAnalysisResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, DeleteAnalysisResponse.class, Constants.DELETE_METHOD);
    }

    public DownloadAnalysisResponse downloadAnalysis(String analyseId, OutputStream outputStream) throws Exception {
        DownloadAnalysisRequest request = new DownloadAnalysisRequest();
        request.setToken(this.token);
        request.setFormat("CSV");
        String url = String.format(DOWNLOAD_ANALYSIS_API, analyseId);
        return (DownloadAnalysisResponse) MemoryRequestUtil.createRequestDownload(Constants.MEMSOURCE_API_BASE_URL + url, outputStream, request, DownloadAnalysisResponse.class, Constants.GET_METHOD);
    }
}
