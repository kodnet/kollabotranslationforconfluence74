package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import static jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil.LOG;
import static jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil.createResponseInstance;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CopyJobRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CopyJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateJobRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteJobRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteTargetRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteTargetResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetFileCompleteRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetFileCompleteResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobListRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobListResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.JobBilingualRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.JobBilingualResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.SetStatusJobRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.SetStatusJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UpdateTargetRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UpdateTargetResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UploadBiligualRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.UploadBiligualResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;

/**
 *
 * @author lucvd
 */
public class JobConnector {

    //api2/v1/projects/{projectUid}/jobs
    private final static String CREATE_API_URL = "api2/v1/projects/%s/jobs";
    //api2/v1/projects/{projectUid}/jobs/{jobUid}
    private final static String GET_API_URL = "api2/v1/projects/%s/jobs/%s";
    //api2/v2/projects/{projectUid}/jobs
    private final static String LIST_API_URL = "api2/v2/projects/%s/jobs";
    //api2/v1/projects/{projectUid}/jobs/batch
    private final static String DELETE_API_URL = "api2/v1/projects/%s/jobs/batch";
    //api2/v1/projects/{projectUid}/jobs/{jobUid}/targetFile
    private final static String GET_JOB_COMPLETE_FILE_API_URL = "api2/v1/projects/%s/jobs/%s/targetFile";

    private final static String XML_INLINE_TAGS = "a,b,big,i,small,tt,abbr,acronym,cite,code,dfn,em,kbd,strong,samp,time,var,bdo,img,map,object,q,script,span,sub,sup,label,ins,mark,del,image,attachment,link,page,structured-macro,parameter,plain-text-link-body,inline-comment-marker";
    //api2/v1/projects/{projectUid}/jobs/{jobUid}/setStatus
    private final static String SET_STATUS_API_URL = "api2/v1/projects/%s/jobs/%s/setStatus";

    private final static String GET_JOB_COMPLETE_SOURCE_FILE_API_URL = "api2/v1/projects/%s/jobs/%s/original";

    private final static String GET_JOB_COMPLETE_SOURCE_FILE_API_URL_BILINGUAL = "api2/v1/projects/%s/jobs/bilingualFile";

    private final static String COPY_SOURCE_FILE_TO_TARGET_FILE = "api2/v1/projects/%s/jobs/%s/copySourceToTarget";

    private final static String DELETE_JOB_TARGET_FILE = "api2/v1/projects/%s/jobs/translations";

    private final static String UPDATE_JOB_TARGET_FILE = "api2/v1/projects/%s/jobs/target";

    private final static String UPLOAD_JOB_BILINGUAL_FILE = "api2/v1/bilingualFiles";

    private final String token;

    public final List<CreateJobResponse> jobCreateRes = new ArrayList<CreateJobResponse>();

    public JobConnector(String token) {
        this.token = token;
    }

    public CreateJobResponse create(String projectUid, String fileName, InputStream inputStream, String[] targetLanguage) throws Exception {
        CreateJobRequest request = new CreateJobRequest();
        request.setTargetLangs(targetLanguage);
        TranslationApiLoginInfoService translationApiLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
        ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        Object valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_FILE_SETTING_CONFIG + apiLoginInfo.getUserName());
        String value = "";
        if (valueObject != null) {
            value = valueObject.toString();
        }

        request.setToken(this.token);
        request.setXmlFileSetting(value);
        String url = String.format(CREATE_API_URL, projectUid);
        return this.createRequest(Constants.MEMSOURCE_API_BASE_URL + url, request, fileName, inputStream);
    }

    public void createAsync(String projectUid, String fileName, InputStream inputStream, String[] targetLanguage) throws Exception {
        MemoryLogUtil.outputLog("$$$ JobConnector#createAsync  Start : FileName/" + fileName + "  Lang/" + Arrays.toString(targetLanguage));
        synchronized (jobCreateRes) {
            jobCreateRes.add(create(projectUid, fileName, inputStream, targetLanguage));
        }
        MemoryLogUtil.outputLog("$$$ JobConnector#createAsync  End");
    }

    public GetJobResponse get(String projectUid, String jobId) throws Exception {
        GetJobRequest request = new GetJobRequest();
        request.setToken(this.token);
        String url = String.format(GET_API_URL, projectUid, jobId);
        return (GetJobResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetJobResponse.class, Constants.GET_METHOD);
    }

    public GetJobListResponse getJobListResponse(String projectUid, Integer pageNumber, Integer workflowLevel) throws Exception {
        GetJobListRequest request = new GetJobListRequest();
        request.setToken(this.token);
        request.setWorkflowLevel(workflowLevel);
        request.setPageNumber(pageNumber);

        String url = String.format(LIST_API_URL, projectUid);
        return (GetJobListResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetJobListResponse.class, Constants.GET_METHOD);
    }

    public List<JobInfo> getJobListResponse(String projectUid, Integer workflowLevel) throws Exception {
        List<JobInfo> result = new ArrayList<JobInfo>();
        GetJobListResponse getJobListResponse = getJobListResponse(projectUid, 0, workflowLevel);
        if (!CommonFunction.isNullOrEmpty(getJobListResponse.getErrorCode())) {
            return result;
        }

        int totalPages = getJobListResponse.getTotalPages();
        result.addAll(Arrays.asList(getJobListResponse.getContent()));
        for (int i = 1; i < totalPages; i++) {
            getJobListResponse = getJobListResponse(projectUid, i, workflowLevel);
            if (!CommonFunction.isNullOrEmpty(getJobListResponse.getErrorCode())) {
                return result;
            }

            result.addAll(Arrays.asList(getJobListResponse.getContent()));
        }

        return result;
    }

    public List<JobInfo> getJobListById(String projectUid, List<String> jobParts, Integer workflowLevel) throws Exception {
        List<JobInfo> result = new ArrayList<JobInfo>();
        List<JobInfo> allJobInfo = getJobListResponse(projectUid, workflowLevel);
        for (JobInfo jobInfo : allJobInfo) {
            if (jobParts.contains(jobInfo.getUid()) == true) {
                result.add(jobInfo);
            }
        }

        return result;
    }

    public List<JobInfo> getJobListById(String projectUid, String[] jobParts, Integer workflowLevel) throws Exception {
        return getJobListById(projectUid, Arrays.asList(jobParts), workflowLevel);
    }

    public GetFileCompleteResponse getFileComplete(String projectUid, String jobId, OutputStream os) throws Exception {
        GetFileCompleteRequest request = new GetFileCompleteRequest();
        request.setToken(this.token);
        String url = String.format(GET_JOB_COMPLETE_FILE_API_URL, projectUid, jobId);
        return (GetFileCompleteResponse) MemoryRequestUtil.createRequestDownload(Constants.MEMSOURCE_API_BASE_URL + url, os, request, GetFileCompleteResponse.class, Constants.GET_METHOD);
    }

    public GetFileCompleteResponse getFileOriginalComplete(String projectUid, String jobId, OutputStream os) throws Exception {
        GetFileCompleteRequest request = new GetFileCompleteRequest();
        request.setToken(this.token);
        String url = String.format(GET_JOB_COMPLETE_SOURCE_FILE_API_URL, projectUid, jobId);
        return (GetFileCompleteResponse) MemoryRequestUtil.createRequestDownload(Constants.MEMSOURCE_API_BASE_URL + url, os, request, GetFileCompleteResponse.class, Constants.GET_METHOD);
    }

    public JobBilingualResponse getFileBiligualComplete(String projectUid, String jobId, OutputStream os) throws Exception {
        JobBilingualRequest request = new JobBilingualRequest();
        request.setToken(this.token);
        JobInfo[] job = new JobInfo[1];
        JobInfo jobInfo = new JobInfo();
        jobInfo.setUid(jobId);
        job[0] = jobInfo;
        request.setJobs(job);
        String url = String.format(GET_JOB_COMPLETE_SOURCE_FILE_API_URL_BILINGUAL, projectUid);
        return (JobBilingualResponse) MemoryRequestUtil.createRequestDownload(Constants.MEMSOURCE_API_BASE_URL + url, os, request, JobBilingualResponse.class, Constants.POST_METHOD);
    }

    public CopyJobResponse copySourceToTargetFile(String projectUid, String jobId) throws Exception {
        CopyJobRequest request = new CopyJobRequest();
        request.setToken(this.token);
        String url = String.format(COPY_SOURCE_FILE_TO_TARGET_FILE, projectUid, jobId);
        return (CopyJobResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, CopyJobResponse.class, Constants.POST_METHOD);
    }

    public UpdateTargetResponse updateJobTarget(String projectUid, String jobId, String fileName, InputStream inputStream) throws Exception {
        UpdateTargetRequest request = new UpdateTargetRequest();
        request.setToken(this.token);
        JobInfo[] job = new JobInfo[1];
        JobInfo jobInfo = new JobInfo();
        jobInfo.setUid(jobId);
        job[0] = jobInfo;
        request.setJobs(job);
        request.setPropagateConfirmedToTm(Boolean.TRUE);
        String url = String.format(UPDATE_JOB_TARGET_FILE, projectUid);
        return this.updateRequest(Constants.MEMSOURCE_API_BASE_URL + url, request, fileName, inputStream);
    }

    public UploadBiligualResponse uploadBiligualFile(InputStream inputStream) throws Exception {
        UploadBiligualRequest request = new UploadBiligualRequest();
        request.setToken(this.token);
        request.setSaveToTransMemory(Constants.SAVE_TO_TRANS_MEMORY);
        String url = String.format(UPLOAD_JOB_BILINGUAL_FILE);
        return this.uploadRequest(Constants.MEMSOURCE_API_BASE_URL + url, request, inputStream);
    }

    public DeleteTargetResponse deleteFileTargetComplete(String projectUid, String jobId) throws Exception {
        DeleteTargetRequest request = new DeleteTargetRequest();
        request.setToken(this.token);
        JobInfo[] job = new JobInfo[1];
        JobInfo jobInfo = new JobInfo();
        jobInfo.setUid(jobId);
        job[0] = jobInfo;
        request.setJobs(job);
        String url = String.format(DELETE_JOB_TARGET_FILE, projectUid);
        return (DeleteTargetResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, DeleteTargetResponse.class, Constants.DELETE_METHOD);
    }

    public DeleteJobResponse delete(String projectUid, JobInfo[] jobParts) throws Exception {
        DeleteJobRequest request = new DeleteJobRequest();
        request.setToken(this.token);
        request.setJobs(jobParts);
        String url = String.format(DELETE_API_URL, projectUid);
        return (DeleteJobResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, DeleteJobResponse.class, Constants.DELETE_METHOD);
    }

    public SetStatusJobResponse setStatus(String projectUid, String jobUid, String status) throws Exception {
        SetStatusJobRequest request = new SetStatusJobRequest();
        request.setToken(this.token);
        request.setRequestedStatus(status);
        String url = String.format(SET_STATUS_API_URL, projectUid, jobUid);
        return (SetStatusJobResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, SetStatusJobResponse.class);
    }

    public CreateJobResponse createRequest(String apiURL, CreateJobRequest requestModel, String fileName, InputStream inputStream) throws Exception {
        try {
            String logText = "MemSource POST=> URL:";
            String urlString = apiURL;
            logText += urlString;
            URL url = new URL(urlString);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            urlCon.setUseCaches(false);
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }

            urlCon.setRequestProperty("Content-Type", "application/octet-stream");
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
            urlCon.setRequestProperty("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);

            //Create header memesource
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(requestModel);
            urlCon.setRequestProperty("Memsource", jsonElement.toString());
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            if (requestModel != null) {
                OutputStream os = urlCon.getOutputStream();
                DataOutputStream output = new DataOutputStream(os);
                //PrintStream ps = new PrintStream(os);
                byte[] buffer = new byte[4096];
                int read = 0;
                while ((read = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.close();
                os.flush();
                // os.close();
            }

            System.out.println(logText);
            return (CreateJobResponse) MemoryRequestUtil.getResponseFile(urlCon, CreateJobResponse.class, CreateJobResponse.class);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return (CreateJobResponse) createResponseInstance(CreateJobResponse.class, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public UpdateTargetResponse updateRequest(String apiURL, UpdateTargetRequest requestModel, String fileName, InputStream inputStream) throws Exception {
        try {
            String logText = "MemSource POST=> URL:";
            String urlString = apiURL;
            logText += urlString;
            URL url = new URL(urlString);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.POST_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            urlCon.setUseCaches(false);
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }

            urlCon.setRequestProperty("Content-Type", "application/octet-stream");
            String json = MemoryRequestUtil.getParamerJsonStringFromObject(requestModel);
            //json = "{\"jobs\":[{\"uid\":\"2JIEg1hJsJS58aYoIaB01p\"}],\"propagateConfirmedToTm\": true}";
            json = json.replace("\r\n", "").replace("\r", "").replace("\n", "").replace(" ", "");
            urlCon.setRequestProperty("Memsource", json);
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
            urlCon.setRequestProperty("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);

            //Create header memesource
            Gson gson = new Gson();
            JsonElement jsonElement = gson.toJsonTree(requestModel);
            urlCon.setRequestProperty("Memsource", jsonElement.toString());
            System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

            if (requestModel != null) {
                OutputStream os = urlCon.getOutputStream();
                DataOutputStream output = new DataOutputStream(os);
                //PrintStream ps = new PrintStream(os);
                byte[] buffer = new byte[4096];
                int read = 0;
                while ((read = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                output.close();
                os.flush();
                // os.close();
            }

            System.out.println(logText);
            return (UpdateTargetResponse) MemoryRequestUtil.getResponseFile(urlCon, UpdateTargetResponse.class, CreateJobResponse.class);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return (UpdateTargetResponse) createResponseInstance(UpdateTargetResponse.class, "EXCEPTION", ex.getMessage(), apiURL, Constants.POST_METHOD);
        }
    }

    public UploadBiligualResponse uploadRequest(String apiURL, UploadBiligualRequest requestModel, InputStream inputStream) throws Exception {
        try {
            String logText = "MemSource PUT=> URL:";
            String urlString = apiURL;
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlString = apiURL + "?saveToTransMemory=" + URLEncoder.encode(requestModel.getSaveToTransMemory(), "UTF-8");
            }

            logText += urlString;
            URL url = new URL(urlString);
            HttpURLConnection urlCon = (HttpURLConnection) url.openConnection();
            urlCon.setRequestMethod(Constants.PUT_METHOD);
            urlCon.setAllowUserInteraction(false);
            urlCon.setDoOutput(true);
            urlCon.setDoInput(true);
            urlCon.setUseCaches(false);
            if (requestModel != null && requestModel.getToken() != null && requestModel.getToken().isEmpty() == false) {
                urlCon.setRequestProperty("Authorization", String.format("ApiToken %s", requestModel.getToken()));
            }

            urlCon.setRequestProperty("Content-Type", "application/octet-stream");
            if (requestModel != null) {
                OutputStream os = urlCon.getOutputStream();
                PrintStream ps = new PrintStream(os);
                DataOutputStream output = new DataOutputStream(os);
                //PrintStream ps = new PrintStream(os);
                byte[] buffer = new byte[4096];
                int read = 0;
                while ((read = inputStream.read(buffer)) != -1) {
                    output.write(buffer, 0, read);
                }

                ps.close();
                output.close();
                os.flush();
                // os.close();
            }

            System.out.println(logText);
            return (UploadBiligualResponse) MemoryRequestUtil.getResponseFile(urlCon, UploadBiligualResponse.class, CreateJobResponse.class);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(CommonFunction.class, ex);
            ex.printStackTrace();
            LOG.log(Level.SEVERE, null, ex);
            return (UploadBiligualResponse) createResponseInstance(UploadBiligualResponse.class, "EXCEPTION", ex.getMessage(), apiURL, Constants.PUT_METHOD);
        }
    }
}
