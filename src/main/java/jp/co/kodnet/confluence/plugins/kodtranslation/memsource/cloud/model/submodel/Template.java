package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author tampt
 */
public class Template {

    private String uid;
    private String templateName;
    private ClientTemplate client;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    } 

    public ClientTemplate getClient() {
        return client;
    }

    public void setClient(ClientTemplate client) {
        this.client = client;
    }
    
    public class ClientTemplate{
        private String uidClient;
        private String id;
        private String name;

        public String getUidClient() {
            return uidClient;
        }

        public void setUidClient(String uidClient) {
            this.uidClient = uidClient;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        } 
    }
}
