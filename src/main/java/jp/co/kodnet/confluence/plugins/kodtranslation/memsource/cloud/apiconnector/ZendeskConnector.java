package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.request.ZendeskRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ZendeskCreateRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ZendeskCreateResponse;

/**
 *
 * @author TAMPT
 */
public class ZendeskConnector {

    private final static String CREATE_REQUEST = "api/v2/requests.json";

    public ZendeskCreateResponse createRequest(String subject, String body, String tags) throws Exception {
        ZendeskCreateRequest request = new ZendeskCreateRequest();
        request.setSubject(subject);
        request.setBody(body);
        request.setTags(tags);
        return (ZendeskCreateResponse) ZendeskRequestUtil.createRequest(CREATE_REQUEST, request, ZendeskCreateResponse.class);
    }
}
