package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.LoginRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.LoginResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.WhoIsAmRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.WhoIsAmResponse;

/**
 *
 * @author lucvd
 */
public class AuthenticationConnector extends ConnectorBase {

    private final static String LOGIN_API_URL = "api2/v1/auth/login";
    private final static String WHO_IS_AM_API_URL = "api/v3/auth/whoAmI";

    public AuthenticationConnector() {

    }

    public LoginResponse login(String username, String password, String code) throws Exception {
        LoginRequest request = new LoginRequest(username, password, code);
        return (LoginResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + LOGIN_API_URL, request, LoginResponse.class);
    }

    public WhoIsAmResponse whoAmI(String token) throws Exception {
        WhoIsAmRequest request = new WhoIsAmRequest();
        request.setToken(token);

        return (WhoIsAmResponse) MemoryRequestUtil.createRequest(Constants.MEMSOURCE_API_BASE_URL + WHO_IS_AM_API_URL, request, WhoIsAmResponse.class);
    }
}
