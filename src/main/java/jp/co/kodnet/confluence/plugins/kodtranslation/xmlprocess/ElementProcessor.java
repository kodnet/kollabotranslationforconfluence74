package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author TamPT
 */
public interface ElementProcessor {

    boolean shouldProcess(XMLEvent paramXMLEvent);

    void marshal(XMLEvent paramXMLEvent, XMLEventReader paramXMLEventReader, XMLEventWriter paramXMLEventWriter) throws Exception;

    void unmarshal(XMLEvent paramXMLEvent, XMLEventReader paramXMLEventReader, XMLEventWriter paramXMLEventWriter) throws Exception;
}
