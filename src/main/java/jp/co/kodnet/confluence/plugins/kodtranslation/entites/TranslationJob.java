package jp.co.kodnet.confluence.plugins.kodtranslation.entites;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface TranslationJob extends Entity {

    public String getFromLanguageKey();

    public void setFromLanguageKey(String fromLanguageKey);

    public Long getFromPageId();

    public void setFromPageId(Long fromPageId);

    public String getToLanguageKey();

    public void setToLanguageKey(String toLanguageKey);

    public Long getToPageId();

    public void setToPageId(Long toPageId);

    public String getSpaceKey();

    public void setSpaceKey(String spaceKey);

    public String getTranslationBaseLanguageKey();

    public void setTranslationBaseLanguageKey(String translationBaseLanguageKey);

    public String getApiProjectId();

    public void setApiProjectId(String apiProjectId);

    public String getTranslationJobId();

    public void setTranslationJobId(String translationJobId);

    public int getTranslationStatus();

    public void setTranslationStatus(int translationStatus);


}
