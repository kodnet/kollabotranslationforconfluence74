package jp.co.kodnet.confluence.plugins.kodtranslation.entites;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface LabelTemplate extends Entity {

    public String getLabelTemplate();

    public void setLabelTemplate(String labelTemplate);

    public String getUid();

    public void setUid(String uid);

    public String getTemplateName();

    public void setTemplateName(String templateName);
    
    public String getLanguageKey();
    
    public void setLanguageKey(String langKey);

}
