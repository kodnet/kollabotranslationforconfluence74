package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import com.atlassian.json.jsonorg.JSONObject;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseJsonModeBase;

/**
 *
 * @author TAMPT
 */
public class ZendeskCreateRequest extends ResponseJsonModeBase {

    private final String REQUEST = "request";
    private final String REQUESTER = "requester";
    private final String NAME = "name";
    private final String ANONYMOUS = "Anonymous customer";
    private final String SUBJECT = "subject";
    private final String COMMENT = "comment";
    private final String BODY = "body";
    private final String TAGS = "tags";

    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    private String tags;

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        JSONObject jObject = toJson();
        return jObject.toString();
    }

    @Override
    public JSONObject toJson() {
        JSONObject jObject = new JSONObject();

        JSONObject jBody = new JSONObject();
        jBody.put(BODY, this.getBody());
        JSONObject jName = new JSONObject();
        jName.put(NAME, ANONYMOUS);
        JSONObject jValue = new JSONObject();
        jValue.put(COMMENT, jBody);
        jValue.put(SUBJECT, this.getSubject());

        jValue.put(REQUESTER, jName);
        jValue.put(TAGS, this.getTags());

        jObject.put(REQUEST, jValue);
        return jObject;
    }
}
