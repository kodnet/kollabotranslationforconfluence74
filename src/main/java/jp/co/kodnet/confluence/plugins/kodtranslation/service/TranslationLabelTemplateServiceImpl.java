package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import net.java.ao.Query;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LabelTemplate;

public class TranslationLabelTemplateServiceImpl implements TranslationLabelTemplateService {

    private final ActiveObjects ao;

    public TranslationLabelTemplateServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<LabelTemplate> getAll() {
        List<LabelTemplate> translationLabelTemplates= newArrayList(ao.find(LabelTemplate.class, Query.select().order("ID")));
        return translationLabelTemplates;
    }

    @Override
    public List<LabelTemplate> getByLabel(String label) {
        List<LabelTemplate> translationLabelTemplates = newArrayList(ao.find(LabelTemplate.class, Query.select().where("LABEL_TEMPLATE = ?", label)));
        return translationLabelTemplates;
    }

    @Override
    public LabelTemplate getByLabelAndLangKey(String label, String langKey) {
        List<LabelTemplate> translationLabelTemplates = newArrayList(ao.find(LabelTemplate.class, Query.select().where("LABEL_TEMPLATE = ? AND LANGUAGE_KEY = ?", label, langKey)));
        if (translationLabelTemplates.isEmpty()) {
            translationLabelTemplates = this.getByLabel(label);
            for (LabelTemplate translationLabelTemplate : translationLabelTemplates) {
                String langKeyTemplate = translationLabelTemplate.getLanguageKey();
                if((langKeyTemplate == null ||  langKeyTemplate.isEmpty()) && langKey.equalsIgnoreCase("ja")){
                    return translationLabelTemplate;
                }
            }
            return null;
        }
        return translationLabelTemplates.get(0);
    }

    @Override
    public LabelTemplate add(
            String labelTemplate,
            String uid,
            String templateName,
            String langKey
            ){
        LabelTemplate translationLabelTemplate = ao.create(LabelTemplate.class);
        translationLabelTemplate.setLabelTemplate(labelTemplate);
        translationLabelTemplate.setUid(uid);
        translationLabelTemplate.setTemplateName(templateName);
        translationLabelTemplate.setLanguageKey(langKey);
        translationLabelTemplate.save();
        return translationLabelTemplate;
    }

    @Override
    public LabelTemplate update(
            String labelTemplate,
            String uid,
            String templateName,
            String langKey
    ) {
        LabelTemplate translationLabelTemplate = this.getByLabelAndLangKey(labelTemplate, langKey);
        if (translationLabelTemplate != null) {
            translationLabelTemplate.setLabelTemplate(labelTemplate);
            translationLabelTemplate.setUid(uid);
            translationLabelTemplate.setTemplateName(templateName);
            translationLabelTemplate.setLanguageKey(langKey);
            translationLabelTemplate.save();
        }
        return translationLabelTemplate;
    }

    @Override
    public void delete(String label, String langKey) {
        LabelTemplate translationLabelTemplate = this.getByLabelAndLangKey(label, langKey);
        if (translationLabelTemplate != null) {
            ao.delete(translationLabelTemplate);
        }
    }

    @Override
    public void deleteAll() {
        List<LabelTemplate> ip = this.getAll();
        for (LabelTemplate item : ip) {
            ao.delete(item);
        }
    }
}
