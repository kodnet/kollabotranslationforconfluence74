package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

/**
 *
 * @author baoph
 */
public class CreateAnalysisRequest extends RequestModelBase {

    private boolean includeFuzzyRepetitions;
    private boolean countSourceUnits;
    private JobInfo[] jobs;
    private String name;

    public boolean isIncludeFuzzyRepetitions() {
        return includeFuzzyRepetitions;
    }

    public void setIncludeFuzzyRepetitions(boolean includeFuzzyRepetitions) {
        this.includeFuzzyRepetitions = includeFuzzyRepetitions;
    }

    public boolean isCountSourceUnits() {
        return countSourceUnits;
    }

    public void setCountSourceUnits(boolean countSourceUnits) {
        this.countSourceUnits = countSourceUnits;
    }

    public JobInfo[] getJobs() {
        return jobs;
    }

    public void setJobs(JobInfo[] jobs) {
        this.jobs = jobs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
