package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
//import jp.co.kodnet.confluence.plugins.kodtranslation.common.TranslationMemoryFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.EncodingUtil;
import static org.apache.commons.lang.StringEscapeUtils.escapeXml;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

/**
 *
 * @author TAMPT
 */
public class MacroDrawIoProcessor extends MacroProcessor {

    // <editor-fold defaultstate="collapsed" desc="variable">
    public final QName QNAME_MACRO_DATA_TRANSLATION = new QName("http://www.w3.org/1999/xhtml", "macro-data-translation");
    public final QName QNAME_MX_CELL = new QName("http://www.w3.org/1999/xhtml", "mxCell");
    public final QName QNAME_ID_ATTRIBUTE = new QName("http://www.w3.org/1999/xhtml", "id");
    public final String DIAGRAM_NAME_ATTRIBUTE = "diagramname";
    public final Page page;
    public final String xmlString;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public">
    public MacroDrawIoProcessor(XMLEventFactory xmlEventFactory, Page page, String xmlString) {
        super(xmlEventFactory, "drawio");
        this.page = page;
        this.xmlString = xmlString;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="marshal">
    @Override
    public void marshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter) throws Exception {
        xmlEventWriter.add(currentEvent);

        String fileName = StringUtils.EMPTY;
        boolean hasName = false;
        while (reader.hasNext()) {
            XMLEvent xmlEvent = reader.nextEvent();
            if (xmlEvent.isEndElement() && xmlEvent.asEndElement().getName().equals(QNAME_STRUCTURED_MACRO)) {
                if (!fileName.isEmpty()) {
                    createEventMarco(xmlEventWriter, fileName);
                }
                xmlEventWriter.add(xmlEvent);
                break;

            }

            if (!hasName && xmlEvent.isStartElement() && xmlEvent.asStartElement().getName().equals(QNAME_PARAMETER)) {
                Attribute att = xmlEvent.asStartElement().getAttributeByName(QNAME_NAME_ATTRIBUTE);
                if (att != null && att.getValue() != null && !att.getValue().isEmpty() && att.getValue().toLowerCase().equals(DIAGRAM_NAME_ATTRIBUTE)) {
                    hasName = true;
                }
            }

            if (hasName && xmlEvent.isCharacters()) {
                fileName = xmlEvent.asCharacters().getData();
                hasName = false;
            }

            xmlEventWriter.add(xmlEvent);
        }
    }

    private void createEventMarco(XMLEventWriter xmlEventWriter, String fileName) throws Exception {
        //Attachment attach = TranslationMemoryFunction.getLastVersionAttachment(page, fileName);
        Attachment attach =  null;
        if (attach == null) {
            return;
        }
        xmlEventWriter.add(this.xmlEventFactory.createStartElement(QNAME_MACRO_DATA_TRANSLATION, null, null));
        InputStream inputStream = attach.getContentsAsStream();
        String content = CommonFunction.convertInputStreamToString(inputStream);
        if (content.isEmpty()) {
            return;
        }

        Document doc = Jsoup.parse(content);
        Elements element = doc.select("diagram");
        if (element.size() > 0) {
            content = element.get(0).text();
            content = this.decodeDrawIO(content);
            doc = Jsoup.parse(content);
            element = doc.select("mxCell[value~=.+]");
            String idLayerRoot = "";
            Element elementLayerRoot = doc.select("mxCell[id~=.+]").get(0);
            if (elementLayerRoot != null) {
                idLayerRoot = elementLayerRoot.attr("id");
            }
            EndElement endElement = this.xmlEventFactory.createEndElement(QNAME_MX_CELL, null);
            for (int i = 0; i < element.size(); i++) {
                Element subElement = element.get(i);
                List<Attribute> attributes = new ArrayList(1);
                attributes.add(this.xmlEventFactory.createAttribute(QNAME_ID_ATTRIBUTE, subElement.attr("id")));
                String subContent = subElement.attr("value");
                boolean hasParentLayer = subElement.hasAttr("parent");
                List<Node> childNode = subElement.childNodes();
                if(hasParentLayer && childNode.isEmpty() && !("").equals(idLayerRoot)){
                    String idLayerChild = subElement.attr("parent");
                    if ((idLayerRoot).equals(idLayerChild)) {
                        continue;
                    }
                }

                StartElement startElement = this.xmlEventFactory.createStartElement(QNAME_MX_CELL, attributes.iterator(), null);
                xmlEventWriter.add(startElement);
                subContent = subContent.replace("\r\n", "\n").replace("\r", "\n").replace("\n", "<br/>");
                subContent = ecapseAttribute(subContent);
                Document subDoc = Jsoup.parse(subContent);
                createSubEventMacro(xmlEventWriter, subDoc.body());
                xmlEventWriter.add(endElement);
            }
        }

        xmlEventWriter.add(this.xmlEventFactory.createEndElement(QNAME_MACRO_DATA_TRANSLATION, new ArrayList().iterator()));
    }

    private void createSubEventMacro(XMLEventWriter xmlEventWriter, Element element) throws Exception {
        List<Node> allNodes = element.childNodes();
        for (Node node : allNodes) {
            if (node instanceof TextNode) {
                String text = ((TextNode) node).text();
                xmlEventWriter.add(this.xmlEventFactory.createCharacters(text));
            } else {
                Element ele = (Element) node;
                List<Attribute> attributes = new ArrayList();
                for (org.jsoup.nodes.Attribute att : ele.attributes().asList()) {
                    attributes.add(this.xmlEventFactory.createAttribute(att.getKey(), att.getValue()));
                }

                QName qName = new QName("http://www.w3.org/1999/xhtml", ele.tagName());
                StartElement startElement = this.xmlEventFactory.createStartElement(qName, attributes.iterator(), null);
                xmlEventWriter.add(startElement);
                createSubEventMacro(xmlEventWriter, ele);
                xmlEventWriter.add(this.xmlEventFactory.createEndElement(qName, null));
            }
        }
    }

    private String ecapseAttribute(String subContent) {
        Pattern pattern = Pattern.compile("<[^<^/]*>");
        Matcher matcher = pattern.matcher(subContent);
        while (matcher.find()) {
            String newTag = matcher.group();
            String[] attributes = newTag.split("=");
            for (String attribute : attributes) {
                String value = attribute;
                int first = value.indexOf("\"");
                int last = value.lastIndexOf("\"");
                if (first > -1 && last > -1 && last > first) {
                    value = value.substring(0, last);
                    value = value.substring(first + 1);
                    String newValue = value.replace("\"", "&quot;").replace("'", "&quot;");
                    newTag = newTag.replace(value, newValue);
                }
            }

            subContent = subContent.replace(matcher.group(), newTag);
        }

        return subContent;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="unmarshal">
    @Override
    public void unmarshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter) throws Exception {
        xmlEventWriter.add(currentEvent);

        String id = StringUtils.EMPTY;
        Attribute attr = currentEvent.asStartElement().getAttributeByName(this.QNAME_MACRO_ID);
        if (attr != null && attr.getValue() != null && !attr.getValue().isEmpty()) {
            id = attr.getValue();
        }

        boolean isAddEvent = true;
        while (reader.hasNext()) {
            XMLEvent xmlEvent = reader.nextEvent();
            if (xmlEvent.isEndElement() && xmlEvent.asEndElement().getName().getLocalPart().equalsIgnoreCase(QNAME_STRUCTURED_MACRO.getLocalPart())) {
                xmlEventWriter.add(xmlEvent);
                break;
            }

            if (isAddEvent && xmlEvent.isStartElement()
                    && (xmlEvent.asStartElement().getName().equals(QNAME_MACRO_DATA_TRANSLATION)
                    || xmlEvent.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_MACRO_DATA_TRANSLATION.getLocalPart()))) {
                isAddEvent = false;
                continue;
            }

            if (!isAddEvent && xmlEvent.isEndElement()
                    && (xmlEvent.asEndElement().getName().equals(QNAME_MACRO_DATA_TRANSLATION)
                    || xmlEvent.asEndElement().getName().getLocalPart().equalsIgnoreCase(QNAME_MACRO_DATA_TRANSLATION.getLocalPart()))) {
                isAddEvent = true;
                continue;
            }

            if (isAddEvent) {
                xmlEventWriter.add(xmlEvent);
            }
        }

        if (id.isEmpty()) {
            return;
        }

        //Regex: Get structured-macro has attribute id = id
        String regex = "<ac:structured-macro\\s*\\w*\\s*ac:macro-id\\s*=\\s*\"" + id + "\"?.*?>((?!<\\/ac:structured-macro>)[\\s\\S])+<\\/ac:structured-macro>";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(this.xmlString);
        String macroData = StringUtils.EMPTY;
        while (m.find()) {
            macroData = m.group();
            break;
        }

        Document doc = Jsoup.parse(macroData);
        Elements elements = doc.select("ac|structured-macro[ac:macro-id=\"" + id + "\"]");
        if (elements.isEmpty()) {
            return;
        }

        Element element = elements.first();
        elements = element.select("ac|parameter[ac:name=\"diagramName\"]");
        if (elements.isEmpty()) {
            return;
        }

        String fileName = elements.get(0).text();
        elements = element.select("macro-data-translation");
        if (elements.isEmpty()) {
            return;
        }

        element = elements.first();
        Map<String, String> dicData = createData(element, macroData);
        updateAttachment(dicData, fileName);
    }

    public void updateAttachment(Map<String, String> dicData, String fileName) throws Exception {
        //Attachment attach = TranslationMemoryFunction.getLastVersionAttachment(page, fileName);
        Attachment attach = null;
        if (attach == null) {
            return;
        }
        InputStream inputStream = attach.getContentsAsStream();
        String dataAttachment = CommonFunction.convertInputStreamToString(inputStream);
        if (dataAttachment.isEmpty()) {
            return;
        }

        inputStream.close();
        Document doc = Jsoup.parse(dataAttachment);
        Elements element = doc.select("diagram");
        if (element.size() > 0) {
            String contentOld = element.get(0).text();
            String content = this.decodeDrawIO(contentOld);
            doc = Jsoup.parse(content);
            element = doc.select("mxCell[value~=.+]");
            for (int i = 0; i < element.size(); i++) {
                Element subElement = element.get(i);
                String id = subElement.attr("id");
                if (dicData.containsKey(id)) {
                    //Regex: Get mxCell has attributes id = id
                    String replaceRegex = "<mxCell\\s*\\w*\\s*id( )*=\\s*\"" + id + "\"( )*?.*?>";
                    Pattern p = Pattern.compile(replaceRegex);
                    Matcher m = p.matcher(content);
                    while (m.find()) {
                        String replaceTemp = m.group();
                        //Replace regex value ="*" => new value
                        Matcher mTemp = Pattern.compile("value( )*=( )*\"([^\"])*\"").matcher(replaceTemp);
                        while (mTemp.find()) {
                            replaceTemp = replaceTemp.replace(mTemp.group(), "value=\"" + escapeXml(dicData.get(id)) + "\"");
                            break;
                        }
                        content = content.replace(m.group(), replaceTemp);
                        break;
                    }
                }
            }

            content = this.encodeDrawIO(content);
            dataAttachment = dataAttachment.replace(contentOld, content);
            //TranslationMemoryFunction.updateAttachment(this.page, attach, dataAttachment);
        }
    }

    public Map<String, String> createData(Element element, String macroData) throws Exception {
        Map<String, String> dicData = new HashMap<String, String>();
        Elements elements = element.select("mxCell");
        if (elements.isEmpty()) {
            return dicData;
        }

        for (Element ele : elements) {
            String id = ele.attr("id");
            String value = StringUtils.EMPTY;
            value = getValue(ele.childNodes());
            dicData.put(id, value);
        }

        return dicData;
    }

    public String getValue(List<Node> root) throws Exception {
        StringBuilder result = new StringBuilder();
        for (Node node : root) {
            if (node instanceof TextNode) {
                String text = ((TextNode) node).text();
                result.append(escapeXml(text));
            } else {
                Element element = (Element) node;
                result.append("<" + element.nodeName());
                boolean isLink = "a".equals(element.nodeName());
                for (org.jsoup.nodes.Attribute att : element.attributes()) {
                    String value = att.getValue();
                    if (isLink && "href".equals(att.getKey())) {
                        value = replaceLink(value);
                    }
                    result.append(" " + att.getKey() + "=\"" + value + "\"");
                }

                List<Node> listNode = element.childNodes();
                if (listNode.isEmpty()) {
                    result.append("/>");
                } else {
                    result.append(">");
                    result.append(getValue(listNode));
                    result.append("</" + element.nodeName() + ">");
                }
            }
        }

        return result.toString();
    }

    public String replaceLink(String value) {
        try {
            if (value.lastIndexOf("=") <= -1) {
                return value;
            }

            String pageId = value.substring(value.lastIndexOf("=") + 1);
            if (!StringUtils.isNumeric(pageId)) {
                return value;
            }

            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            Page pageTo = pageManager.getPage(Long.valueOf(pageId));
            if (pageTo == null || pageTo.isDeleted()) {
                return value;
            }

//            TranslationPageHistoryService translationPageHistoryService = TranslationMemoryToolFactory.getTranslationPageHistoryService();
//            TranslationHistory his = translationPageHistoryService.getTransPageHistoryByFromPageId(pageId, this.page.getSpaceKey());
//            if (his == null) {
//                return value;
//            }

//            value = value.substring(0, value.lastIndexOf("=") + 1) + his.getPageId();
            return value;
        } catch (Exception ex) {
            return value;
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="DrawIo">
    public static String decodeDrawIO(String input) throws Exception {
        //Base64 decode
        byte[] bytes = Base64.getDecoder().decode(input);
        //DeflaterRaw
        String output = DeflaterRaw(bytes);

        output = EncodingUtil.decodeURIComponent(output);
        //output = unescapeXml(output);
        return output;
    }

    public static String DeflaterRaw(byte[] bytes) throws Exception {
        Inflater inflater = new Inflater(true);
        inflater.setInput(bytes);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length);
        byte[] buff = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buff);
            baos.write(buff, 0, count);
        }
        baos.close();
        byte[] output = baos.toByteArray();
        return new String(output);
    }

    public static String encodeDrawIO(String input) throws Exception {
        //Encode URI
        String output = EncodingUtil.encodeURIComponent(input);
        //EncodeRaw
        byte[] bytes = FlaterRaw(output);
        //Base64 encode
        output = Base64.getEncoder().encodeToString(bytes);
        return output;
    }

    public static byte[] FlaterRaw(String input) throws Exception {
        ByteArrayOutputStream deflatedBytes = new ByteArrayOutputStream();
        Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
        DeflaterOutputStream deflaterStream = new DeflaterOutputStream(deflatedBytes, deflater);
        deflaterStream.write(input.getBytes());
        deflaterStream.finish();
        return deflatedBytes.toByteArray();
    }
    // </editor-fold>
}
