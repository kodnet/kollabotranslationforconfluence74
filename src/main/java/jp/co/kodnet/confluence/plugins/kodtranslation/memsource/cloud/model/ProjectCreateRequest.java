package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Client;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.WorkflowStep;

/**
 *
 * @author hailc
 */
public class ProjectCreateRequest extends RequestModelBase {

    private String name;
    private String sourceLang;
    private String[] targetLangs;
    private Client client;
    private WorkflowStep[] workflowSteps;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSourceLang() {
        return sourceLang;
    }

    public void setSourceLang(String sourceLang) {
        this.sourceLang = sourceLang;
    }

    public String[] getTargetLangs() {
        return targetLangs;
    }

    public void setTargetLangs(String[] targetLangs) {
        this.targetLangs = targetLangs;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getClientId() {
        if (this.client == null) {
            return "";
        }
        return this.client.getId();
    }

    public void setClientId(String clientId) {
        this.client = new Client();
        this.client.setId(clientId);
    }

    public WorkflowStep[] getWorkflowSteps() {
        return workflowSteps;
    }

    public void setWorkflowSteps(WorkflowStep[] workflowStep) {
        this.workflowSteps = workflowStep;
    }

}
