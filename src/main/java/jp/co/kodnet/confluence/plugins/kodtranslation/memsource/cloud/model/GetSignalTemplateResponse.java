package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;

/**
 *
 * @author tampt
 */
public class GetSignalTemplateResponse extends ResponseModelBase {

    private Client client;
    
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
   public class Client{
        private String uidClient;
        private String id;
        private String name;

        public String getUidClient() {
            return uidClient;
        }

        public void setUidClient(String uidClient) {
            this.uidClient = uidClient;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        } 
   }
}
