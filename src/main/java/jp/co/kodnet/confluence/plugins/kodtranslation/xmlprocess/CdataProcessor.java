package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class CdataProcessor extends SpanProcessor {

    private final XMLEventFactory xmlEventFactory;
    public static final QName SPAN_XMLTAG = new QName("http://www.w3.org/1999/xhtml", "span");
    public static final QName PRE_XMLTAG = new QName("http://www.w3.org/1999/xhtml", "pre");
    public static final QName SPAN_NBSPTAG = new QName("http://www.w3.org/1999/xhtml", "spannbsp");

    public CdataProcessor(XMLEventFactory xmlEventFactory) {
        super("sv-cdata-translate");
        this.xmlEventFactory = xmlEventFactory;
    }

    @Override
    public void marshal(XMLEvent paramXMLEvent, XMLEventReader paramXMLEventReader, XMLEventWriter paramXMLEventWriter) throws Exception {
    }

    @Override
    public void unmarshal(XMLEvent currentEvent, XMLEventReader reader, XMLEventWriter xmlEventWriter)
            throws XMLStreamException {
        XMLEvent event = reader.nextEvent();
        String content = "";
        while ((!event.isEndDocument() && !event.isEndElement()) || (event.isEndElement() && !event.asEndElement().getName().getLocalPart().equalsIgnoreCase(SPAN_XMLTAG.getLocalPart()))) {
            if (event.isCharacters() && (event.asCharacters().getData().equals("\r") || event.asCharacters().getData().equals("\r\n") || event.asCharacters().getData().equals("\n"))) {
                event = reader.nextEvent();
                continue;
            }

            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equalsIgnoreCase(PRE_XMLTAG.getLocalPart())) {
                if (content.equals("") == false) {
                    content = content + "\r\n";
                }
                while (!event.isEndElement() || (event.isEndElement() && !event.asEndElement().getName().equals(PRE_XMLTAG))) {
                    event = reader.nextEvent();
                    if (event.isCharacters() && (event.asCharacters().getData().equals("\r") || event.asCharacters().getData().equals("\r\n") || event.asCharacters().getData().equals("\n"))) {
                        continue;
                    }
                    if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equalsIgnoreCase(SPAN_NBSPTAG.getLocalPart())) {
                        content = content + " ";
                        event = reader.nextEvent();
                        continue;
                    }
                    if (event.isCharacters()) {
                        content = content + event.asCharacters().getData();
                    }
                }
            } else {
                content = event.asCharacters().getData();
            }

            event = reader.nextEvent();
        }

        xmlEventWriter.add(this.xmlEventFactory.createCData(content));
    }
}
