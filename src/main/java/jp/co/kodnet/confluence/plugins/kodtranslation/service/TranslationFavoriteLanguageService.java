package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.FavoriteLanguage;

@Transactional
public interface TranslationFavoriteLanguageService {
    
    List<FavoriteLanguage> getAll();
    
    FavoriteLanguage get();
    
    FavoriteLanguage getOne(String id);
    
    FavoriteLanguage getByLangKey(String langKey);
    
    List<FavoriteLanguage> getByTransToolId(String id);
      
    FavoriteLanguage add(
            String id,
            String languageKey,
            String languageDisplay
            );
    
    FavoriteLanguage update(
            String id,
            String languageKey,
            String languageDisplay
            );
    
    void delete(String id);
    
    void deleteAll(); 
}
