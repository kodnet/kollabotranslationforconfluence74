package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author lucvd
 */
public class GetFileCompleteRequest extends RequestModelBase {

    private String jobPart;

    public String getJobPart() {
        return jobPart;
    }

    public void setJobPart(String jobPart) {
        this.jobPart = jobPart;
    }

}
