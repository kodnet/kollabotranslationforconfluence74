package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectCreateRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectCreateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectDeleteRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectDeleteResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectGetInfoResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.ProjectGetRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.WorkflowStep;

/**
 *
 * @author hailc
 */
public class ProjectConnector {

    private final static String CREATE_API_URL = "api2/v1/projects";
    //
    private final static String CREATE_API_URL_WITH_TEMPLATE = "api2/v2/projects/applyTemplate/%s";
    //api2/v1/projects/{projectUid}
    private final static String GET_INFO_API_URL = "api2/v1/projects/%s";
    //api2/v1/projects/{projectUid}
    private final static String DELETE_API_URL = "api2/v1/projects/%s";

    private final String token;

    public ProjectConnector(String token) throws Exception {
        this.token = token;
    }

    public ProjectCreateResponse create(String name, String sourceLang, String[] targetLang, String clientId, String[] workflows, String templateId) throws Exception {
        ProjectCreateRequest request = new ProjectCreateRequest();
        request.setToken(this.token);
        request.setName(name);
        request.setSourceLang(sourceLang);
        request.setTargetLangs(targetLang);
        String clientTemplate = CommonFunction.getTemplateClient(this.token, templateId);
        if (clientTemplate == null || clientTemplate.equals(Constants.ERROR)) {
            request.setClientId(clientId);
        }
        
        if (workflows.length > 0) {
            WorkflowStep[] workflowStep = new WorkflowStep[workflows.length];
            for (int i = 0; i < workflows.length; i++) {
                workflowStep[i] = new WorkflowStep();
                workflowStep[i].setId(workflows[i]);
            }

            request.setWorkflowSteps(workflowStep);
        }

        boolean error = false;
        if(clientTemplate != null && clientTemplate.equals(Constants.ERROR)){
            error = true;
        }
        
        String url = Constants.MEMSOURCE_API_BASE_URL + CREATE_API_URL; 
        if (templateId != null && templateId.isEmpty() == false && error == false) {
            url = String.format(Constants.MEMSOURCE_API_BASE_URL + CREATE_API_URL_WITH_TEMPLATE, templateId);
        }

        return (ProjectCreateResponse) MemoryRequestUtil.createRequestJson(url, request, ProjectCreateResponse.class);
    }

//    public ProjectEditResponse edit(String[] targetLang, String projectUid) throws Exception {
//        ProjectEditRequest request = new ProjectEditRequest();
//        request.setToken(this.token);
//        request.setProject(projectUid);
//        request.setTargetLang(targetLang);
//
//        return (ProjectEditResponse) MemoryRequestUtil.createRequest(Constants.MEMSOURCE_API_BASE_URL + EDIT_API_URL, request, ProjectEditResponse.class);
//    }

    public ProjectGetInfoResponse getInfo(String projectUid) throws Exception {
        ProjectGetRequest request = new ProjectGetRequest();
        request.setToken(this.token);
        String url = String.format(GET_INFO_API_URL, projectUid);
        return (ProjectGetInfoResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, ProjectGetInfoResponse.class, Constants.GET_METHOD);
    }

    public ProjectDeleteResponse delete(String projectUid, boolean purge) throws Exception {
        ProjectDeleteRequest request = new ProjectDeleteRequest();
        request.setToken(this.token);
        request.setPurge(purge);
        String url = String.format(DELETE_API_URL, projectUid);
        return (ProjectDeleteResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, ProjectDeleteResponse.class, Constants.DELETE_METHOD);
    }
}
