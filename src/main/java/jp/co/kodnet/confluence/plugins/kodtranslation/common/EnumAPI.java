package jp.co.kodnet.confluence.plugins.kodtranslation.common;

/**
 *
 * @author huetq
 */
public enum EnumAPI {
    //AuthenticationConnector
    LOGIN_API_URL_AUTHENTICATIONCONNECTOR("api/v3/auth/login"),
    WHO_IS_AM_API_URL_AUTHENTICATIONCONNECTOR("api/v3/auth/whoAmI"),
    //ClientConnector
    GET_LIST_API_URL_CLIENTCONNECTOR("api/v2/client/list"),
    //FileConnector
    UPLOAD_REQUEST_BODY_API_URL_FILECONNECTOR("api/v2/file/uploadRequestBody"),
    DELETE_API_URL_FILECONNECTOR("api/v2/file/delete"),
    DOWNLOAD_API_URL_FILECONNECTOR("api/v2/file/download"),
    //JobConnector
    CREATE_API_URL_JOBCONNECTOR("api/v8/job/create"),
    GET_API_URL_JOBCONNECTOR("api/v8/job/get"),
    DELETE_API_URL_JOBCONNECTOR("api/v8/job/delete"),
    GET_JOB_COMPLETE_FILE_API_URL_JOBCONNECTOR("api/v8/job/getCompletedFile"),
    //LanguageConnector
    GET_API_URL_LANGUAGECONNECTOR("api/v2/language/listSupportedLangs"),
    //ProjectConnector
    CREATE_API_URL_PROJECTCONNECTOR("api/v3/project/create"),
    EDIT_API_URL_PROJECTCONNECTOR("api/v3/project/edit"),
    GET_INFO_API_URL_PROJECTCONNECTOR("api/v3/project/get"),
    DELETE_API_URL_PROJECTCONNECTOR("api/v3/project/delete"),
    //ReferenceConnector
    CREATE_API_URL_REFERENCECONNECTOR("api/v2/referenceFile/create"),
    //SegmentsCountConnector
    GET_API_URL_SEGMENTSCOUNTCONNECTOR("api/v8/job/getSegmentsCount"),
    //WorkflowStepConnector
    GET_API_URL_WORKFLOWSTEPCONNECTOR("api/v2/workflowStep/list"),
    // Analyses
    CREATE_NEW_ANALYSIS_CONNECTOR("api/v2/analyse/create"),
    GET_LIST_ANALYSIS_CONNECTOR("api/v2/analyse/listByProject"),
    GET_ANALYSIS_CONNECTOR("api/v2/analyse/get"),
    DELETE_ANALYSIS_CONNECTOR("api/v2/analyse/delete"),
    DOWNLOAD_ANALYSIS_CONNECTOR("api/v2/analyse/download");
    
    private String url;
    
    EnumAPI(String url) {
        this.url = url;
    }
    public String url() {
        return url;
    }
    
}
