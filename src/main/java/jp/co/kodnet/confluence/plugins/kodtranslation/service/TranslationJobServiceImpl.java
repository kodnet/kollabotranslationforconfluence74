package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import net.java.ao.Query;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;

public class TranslationJobServiceImpl implements TranslationJobService {

    private final ActiveObjects ao;

    public TranslationJobServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<TranslationJob> getAll() {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class));
        return translationJobs;
    }

    @Override
    public TranslationJob getOneByLang(String fromLanguageKey, String toLanguageKey, String spaceKey) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_LANGUAGE_KEY = ? AND TO_LANGUAGE_KEY = ? AND SPACE_KEY = ?", fromLanguageKey, toLanguageKey, spaceKey)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public TranslationJob getOneByLangAndFromPageId(String fromLanguageKey, String toLanguageKey, Long fromPageId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_LANGUAGE_KEY = ? AND TO_LANGUAGE_KEY = ? AND FROM_PAGE_ID = ?", fromLanguageKey, toLanguageKey, fromPageId)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public List<TranslationJob> getBySpaceKey(String spaceKey) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("SPACE_KEY = ?", spaceKey)));
        return translationJobs;
    }

    @Override
    public List<TranslationJob> getByPageIdAndStatus(Long fromPageId, int status) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ? AND TRANSLATION_STATUS != ?", fromPageId, status)));
        return translationJobs;
    }

    @Override
    public List<TranslationJob> getByPageIdIsTranlated(Long fromPageId, int status) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ? AND TO_PAGE_ID IS NOT NULL AND TRANSLATION_STATUS = ?", fromPageId, status)));
        return translationJobs;
    }

    @Override
    public TranslationJob getOneByPageId(Long fromPageId, Long toPageId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ? AND TO_PAGE_ID = ?", fromPageId, toPageId)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public List<TranslationJob> getByTranslationJobByFromPageId(Long fromPageId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ?", fromPageId)));
        return translationJobs;
    }
 
    @Override
    public TranslationJob getByToLangKey(String spaceKey, Long fromPageId, String toLangKey) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ? AND TO_LANGUAGE_KEY = ? AND SPACE_KEY = ?", fromPageId, toLangKey, spaceKey)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public TranslationJob getOneByFromPageId(Long fromPageId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ?", fromPageId)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public TranslationJob getOneByToPageId(Long toPageId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("TO_PAGE_ID = ?", toPageId)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public TranslationJob getByProjectId(Long fromPageId, String projectId, String jobId) {
        List<TranslationJob> translationJobs = newArrayList(ao.find(TranslationJob.class, Query.select().where("FROM_PAGE_ID = ? AND API_PROJECT_ID = ? AND TRANSLATION_JOB_ID = ?", fromPageId, projectId, jobId)));
        if (translationJobs.isEmpty()) {
            return null;
        }
        return translationJobs.get(0);
    }

    @Override
    public TranslationJob add(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
    ) {
        TranslationJob translationJob = ao.create(TranslationJob.class);
        translationJob.setFromLanguageKey(fromLanguageKey);
        translationJob.setFromPageId(fromPageId);
        translationJob.setToLanguageKey(toLanguageKey);
        translationJob.setToPageId(toPageId);
        translationJob.setSpaceKey(spaceKey);
        translationJob.setTranslationBaseLanguageKey(translationBaseLanguageKey);
        translationJob.setApiProjectId(apiProjectId);
        translationJob.setTranslationJobId(translationJobId);
        translationJob.setTranslationStatus(translationStatus);
        translationJob.save();
        return translationJob;
    }

    @Override
    public TranslationJob updateByLang(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
    ) {
        TranslationJob translationJob = this.getOneByLangAndFromPageId(fromLanguageKey, toLanguageKey, fromPageId);
        if (translationJob != null) {
            translationJob.setFromPageId(fromPageId);
            translationJob.setToPageId(toPageId);
            translationJob.setSpaceKey(spaceKey);
            translationJob.setTranslationBaseLanguageKey(translationBaseLanguageKey);
            translationJob.setApiProjectId(apiProjectId);
            translationJob.setTranslationJobId(translationJobId);
            translationJob.setTranslationStatus(translationStatus);
            translationJob.save();
        }
        return translationJob;
    }

    @Override
    public TranslationJob updateByPageId(
            String fromLanguageKey,
            Long fromPageId,
            String toLanguageKey,
            Long toPageId,
            String spaceKey,
            String translationBaseLanguageKey,
            String apiProjectId,
            String translationJobId,
            Integer translationStatus
    ) {
        TranslationJob translationJob = this.getOneByPageId(fromPageId, toPageId);
        if (translationJob != null) {
            translationJob.setFromLanguageKey(fromLanguageKey);
            translationJob.setToLanguageKey(toLanguageKey);
            translationJob.setSpaceKey(spaceKey);
            translationJob.setTranslationBaseLanguageKey(translationBaseLanguageKey);
            translationJob.setApiProjectId(apiProjectId);
            translationJob.setTranslationJobId(translationJobId);
            translationJob.setTranslationStatus(translationStatus);
            translationJob.save();
        }
        return translationJob;
    }

    @Override
    public void setToPageIdAndStatus(TranslationJob translationJob, Long toPageId, int status) {
        try {
            translationJob.setToPageId(toPageId);
            translationJob.setTranslationStatus(status);
            translationJob.save();
        } catch (Exception e) {
            MemoryLogUtil.outputLog("Exception set toPageId and Status : " + e.getMessage());
        }
    }

    @Override
    public void deleteByFromPageId(String fromLanguageKey, String toLanguageKey, String spaceKey, Long fromPageId) {
        TranslationJob translationJob = this.getOneByLangAndFromPageId(fromLanguageKey, toLanguageKey, fromPageId);
        if (translationJob != null) {
            ao.delete(translationJob);
        }
    }

    @Override
    public void deleteAll() {
        List<TranslationJob> ip = this.getAll();
        for (TranslationJob item : ip) {
            ao.delete(item);
        }
    }
}
