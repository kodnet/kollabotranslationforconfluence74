package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.SegmentsCount;

/**
 *
 * @author anhlq
 */
public class GetSegmentsCountResponse extends ResponseModelBase {

    private SegmentsCount[] segmentsCountsResults;

    public SegmentsCount[] getSegmentsCountsResults() {
        return segmentsCountsResults;
    }

    public void setSegmentsCountsResults(SegmentsCount[] segmentsCountsResults) {
        this.segmentsCountsResults = segmentsCountsResults;
    }

}
