package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

/**
 *
 * @author nhatnq
 */
public class GetJobListResponse extends ResponseModelBase {

    private Integer totalPages;
    private JobInfo[] content;

    public JobInfo[] getContent() {
        return content;
    }

    public void setContent(JobInfo[] content) {
        this.content = content;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
