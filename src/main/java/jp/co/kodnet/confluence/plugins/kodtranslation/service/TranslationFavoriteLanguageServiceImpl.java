package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import net.java.ao.Query;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.FavoriteLanguage;

public class TranslationFavoriteLanguageServiceImpl implements TranslationFavoriteLanguageService {

    private final ActiveObjects ao;

    public TranslationFavoriteLanguageServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<FavoriteLanguage> getAll() {
        List<FavoriteLanguage> translationFavoriteLanguages= newArrayList(ao.find(FavoriteLanguage.class));
        return translationFavoriteLanguages;
    }

    @Override
    public FavoriteLanguage getOne(String translationToolId) {
        List<FavoriteLanguage> translationFavoriteLanguages = newArrayList(ao.find(FavoriteLanguage.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        if (translationFavoriteLanguages.isEmpty()) {
            return null;
        }
        return translationFavoriteLanguages.get(0);
    }

    @Override
    public List<FavoriteLanguage> getByTransToolId(String translationToolId) {
        List<FavoriteLanguage> translationFavoriteLanguages = newArrayList(ao.find(FavoriteLanguage.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        return translationFavoriteLanguages;
    }

    @Override
    public FavoriteLanguage get() {
        List<FavoriteLanguage> translationFavoriteLanguages = newArrayList(ao.find(FavoriteLanguage.class));
        if (translationFavoriteLanguages.isEmpty()) {
            return null;
        }
        return translationFavoriteLanguages.get(0);
    }

    @Override
    public FavoriteLanguage getByLangKey(String langKey) {
        List<FavoriteLanguage> translationFavoriteLanguages = newArrayList(ao.find(FavoriteLanguage.class, Query.select().where("LANGUAGE_KEY = ?", langKey)));
        if (translationFavoriteLanguages.isEmpty()) {
            return null;
        }
        return translationFavoriteLanguages.get(0);
    }

    @Override
    public FavoriteLanguage add(
            String translationToolId,
            String languageKey,
            String languageDisplay
            ){
        FavoriteLanguage translationFavoriteLanguage = ao.create(FavoriteLanguage.class);
        translationFavoriteLanguage.setTranslationToolId(translationToolId);
        translationFavoriteLanguage.setLanguageKey(languageKey);
        translationFavoriteLanguage.setLanguageDisplay(languageDisplay);
        translationFavoriteLanguage.save();
        return translationFavoriteLanguage;
    }

    @Override
    public FavoriteLanguage update(
            String translationToolId,
            String languageKey,
            String languageDisplay
            ) {
        FavoriteLanguage translationFavoriteLanguage = this.getOne(translationToolId);
        if (translationFavoriteLanguage != null) {
            translationFavoriteLanguage.setLanguageKey(languageKey);
            translationFavoriteLanguage.setLanguageDisplay(languageDisplay);
            translationFavoriteLanguage.save();
        }
        return translationFavoriteLanguage;
    }

    @Override
    public void delete(String translationToolId) {
        List<FavoriteLanguage> translationFavoriteLanguages = newArrayList(ao.find(FavoriteLanguage.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        if (translationFavoriteLanguages.isEmpty()) {
            return;
        }
        for (FavoriteLanguage trans : translationFavoriteLanguages) {
            if (trans != null) {
                ao.delete(trans);
            }
        }
    }

    @Override
    public void deleteAll() {
        List<FavoriteLanguage> ip = this.getAll();
        for (FavoriteLanguage item : ip) {
            ao.delete(item);
        }
    }
}
