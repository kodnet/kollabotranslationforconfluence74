package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.TranslationLanguage;

/**
 *
 * @author anhlq
 */
public class GetLanguageResponse extends ResponseModelBase {

    private TranslationLanguage[] languages;

    public TranslationLanguage[] getLanguages() {
        return languages;
    }

    public void setLanguages(TranslationLanguage[] lstLanguage) {
        this.languages = lstLanguage;
    }

}
