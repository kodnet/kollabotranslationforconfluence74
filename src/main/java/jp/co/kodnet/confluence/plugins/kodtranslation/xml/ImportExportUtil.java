package jp.co.kodnet.confluence.plugins.kodtranslation.xml;

import com.atlassian.confluence.content.render.xhtml.StaxUtils;
import com.atlassian.confluence.content.render.xhtml.XhtmlConstants;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess.*;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author TamPT
 */
public class ImportExportUtil {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private static List<ElementProcessor> elementProcessors;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="export">
    /**
     * ページコンテンツをXmlに変換する
     *
     * @param page
     * @return
     */
    public static String exportXmlString(Page page) throws Exception {
        XMLEventFactory xmlEventFactory = TranslationMemoryToolFactory.getXmlEventFactory();
        elementProcessors = Arrays.asList(new ElementProcessor[]{
            new PageLinkBodyProcessor(xmlEventFactory),
            new MacroDrawIoProcessor(xmlEventFactory, page, StringUtils.EMPTY)
        });

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        marshal(page, os);
        String xmlPage = new String(os.toByteArray(), "UTF-8");
        xmlPage = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + xmlPage;
        return xmlPage;
    }

    /**
     * Xmlの作成
     *
     * @param page
     * @param outputStream
     * @throws Exception
     */
    private static void marshal(Page page, OutputStream outputStream) throws Exception {
        MemoryLogUtil.outputLog("=========Begin Marshal Page======= id : " + page.getIdAsString() );
        XMLUtils.CustomAttribute pageIdAttribute = new XMLUtils.CustomAttribute("pageId", Long.toString(page.getId()));
        XMLUtils.CustomAttribute versionIdAttribute = new XMLUtils.CustomAttribute("versionId", "" + page.getVersion());
        XMLUtils.writeStartElement("page", outputStream, new XMLUtils.CustomAttribute[]{pageIdAttribute, versionIdAttribute});
        XMLUtils.writeSimpleCDataElement("title", page.getTitle(), outputStream);
        XMLUtils.writeStartElement("content", XhtmlConstants.STORAGE_NAMESPACES, outputStream);
        XMLUtils.writeContent(transformStorageFormat(page.getTitle(), page.getBodyAsString()), outputStream);
        XMLUtils.writeEndElement("content", outputStream);
        XMLUtils.writeEndElement("page", outputStream);
        MemoryLogUtil.outputLog("=========End Marshal Page======= id : " + page.getIdAsString() );
    }

    /**
     * StorageFormatをXmlに変換する
     *
     * @param storageFormat
     * @return
     * @throws Exception
     */
    private static String transformStorageFormat(String title, String storageFormat) throws Exception {
        XmlOutputFactory xmlOutputFactory = TranslationMemoryToolFactory.getXmlOutputFactory();
        XmlEventReaderFactory xmlEventReaderFactory = TranslationMemoryToolFactory.getXmlEventReaderFactory();
        storageFormat = DocumentUtil.convertEmojiToHtmlHex(storageFormat);
        storageFormat = DocumentUtil.convertLinkPage(title, storageFormat);
        storageFormat = DocumentUtil.skipProcessChar(storageFormat, false);
        XMLEventReader xmlEventReader = null;
        XMLEventWriter xmlEventWriter = null;
        try {
            StringWriter stringWriter = new StringWriter();
            xmlEventWriter = xmlOutputFactory.createXMLEventWriter(stringWriter);
            xmlEventReader = xmlEventReaderFactory.createStorageXmlEventReader(new StringReader(storageFormat));
            XMLEvent event;
            while (xmlEventReader.hasNext()) {
                event = xmlEventReader.nextEvent();
                if (!event.isStartDocument()) {
                    if (event.isEndDocument()) {
                        break;
                    }
                    getProcessor(event).marshal(event, xmlEventReader, xmlEventWriter);
                }
            }
            xmlEventWriter.flush();
            String result = stringWriter.toString();
            result = DocumentUtil.skipProcessChar(result, true);
            return result;
        } catch (Exception e) {
            MemoryLogUtil.outputLog("PAGE ERROR TRANSFORM : " + title);
            throw new RuntimeException(e);
        } finally {
            StaxUtils.closeQuietly(xmlEventWriter);
            StaxUtils.closeQuietly(xmlEventReader);
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="import">
    /**
     * Xmlをページにインポートする
     *
     * @param page
     * @param xmlString
     * @param isUpdateTitle
     * @param isSaveContent
     * @return
     */
    public static boolean importXmlString(Page page, String xmlString, boolean isUpdateTitle, boolean isSaveContent) {
        try {
            XMLEventFactory xmlEventFactory = TranslationMemoryToolFactory.getXmlEventFactory();
            XmlEventReaderFactory xmlEventReaderFactory = TranslationMemoryToolFactory.getXmlEventReaderFactory();
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();

            xmlString = DocumentUtil.convertEmojiToHtmlHex(xmlString);
            elementProcessors = Arrays.asList(new ElementProcessor[]{
                new CdataProcessor(xmlEventFactory),
                new SvNbspProcessor(xmlEventFactory),
                new MacroDrawIoProcessor(xmlEventFactory, page, xmlString)
            });

            xmlString = DocumentUtil.skipProcessChar(xmlString, false);
            XMLEventReader xmlReader = xmlEventReaderFactory.createXmlEventReader(new StringReader(xmlString));
            String title = unmarshal(page, xmlReader, isUpdateTitle);
            page.getProperties().setStringProperty(Constants.TITLE_PROPERTY, title);
            if (isSaveContent == true) {
                page.getProperties().setStringProperty(Constants.PAGE_WAS_SAVED, Constants.PAGE_WAS_SAVED);
                pageManager.saveContentEntity(page, null);
            }

            if (isUpdateTitle) {
                pageManager.renamePage(page, title);
            }

            return true;
        } catch (Exception ex) {
            Logger.getLogger(ImportExportUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    /**
     * XmlをにStorageFormat変換する。 ページを保存する。
     *
     * @param page
     * @param xmlReader
     * @param isUpdateTitle
     * @return
     * @throws Exception
     */
    private static String unmarshal(Page page, XMLEventReader xmlReader, boolean isUpdateTitle) throws Exception {
        String titlePage = "";
        while (xmlReader.hasNext()) {
            XMLEvent xmlEvent = xmlReader.nextEvent();
            if (isPageStartElement(xmlEvent)) {
            } else if ((xmlEvent.isStartElement()) && (xmlEvent.asStartElement().getName().getLocalPart().equals("title"))) {
                titlePage = getTrimmedDataUntilEndTag(xmlReader, "title");
            } else if ((xmlEvent.isStartElement()) && (xmlEvent.asStartElement().getName().getLocalPart().equals("content"))) {
                if (!isUpdateTitle) {
                    page.setBodyAsString(getStorageFormatContent(xmlReader));
                }
            } else if ((xmlEvent.isEndElement()) && (xmlEvent.asEndElement().getName().getLocalPart().equals("page"))) {

            }
        }
        return titlePage;
    }

    /**
     * StartElementのチェック
     *
     * @param xmlEvent
     * @return
     * @throws Exception
     */
    private static boolean isPageStartElement(XMLEvent xmlEvent) throws Exception {
        return (xmlEvent.isStartElement()) && (xmlEvent.asStartElement().getName().getLocalPart().equals("page"));
    }

    /**
     * 文字列「\n」の削除
     *
     * @param xmlReader
     * @param endTag
     * @return
     * @throws Exception
     */
    private static String getTrimmedDataUntilEndTag(XMLEventReader xmlReader, String endTag) throws Exception {
        XMLEvent xmlEvent = xmlReader.nextEvent();
        String data = "";
        while ((!xmlEvent.isEndElement()) || (!xmlEvent.asEndElement().getName().getLocalPart().equals(endTag))) {
            switch (xmlEvent.getEventType()) {
                case XMLStreamConstants.CHARACTERS:
                    data = data + xmlEvent.asCharacters().getData();
                    data = data.replace("<![CDATA[", "");
                    data = data.replace("]]>", "");
                    data = data.replace("<--![CDATA[", "");
                    data = data.replace("]]-->", "");
                    break;
                case XMLStreamConstants.CDATA:
                    data = data + xmlEvent.asCharacters().getData();
                    break;
                default:
                    data = data + xmlEvent.asCharacters().getData();
                    break;
            }
            MemoryLogUtil.outputLog("GET TRIMMED DATA TITLE PAGE XML (" + data + ")");
            xmlEvent = xmlReader.nextEvent();
        }
        data = data.replaceAll("\n", " ").trim().replaceAll(" +", " ");
        return data;
    }

    /**
     * StoreFormatの取得
     *
     * @param storageReader
     * @return
     * @throws Exception
     */
    private static String getStorageFormatContent(XMLEventReader storageReader) throws Exception {
        String xmlConverted = convertXmlString(storageReader);
        return xmlConverted;
    }

    /**
     * XmlEventを文字列に返還する
     *
     * @param storageReader
     * @return
     * @throws Exception
     */
    public static String convertXmlString(XMLEventReader storageReader) throws Exception {
        XmlOutputFactory xmlOutputFactory = TranslationMemoryToolFactory.getXmlOutputFactory();
        StringBuilder stringBuilder = new StringBuilder();
        List<Object> listXMLEvent = new ArrayList<Object>();
        XMLEventWriter xmlEventWriter = null;
        while (storageReader.hasNext()) {
            XMLEvent event = (XMLEvent) storageReader.next();
            boolean isSkipped = false;

            if (event.isStartElement() == true && event.asStartElement().getAttributes() != null) {
                Iterator<Attribute> listAttr = event.asStartElement().getAttributes();
                while (listAttr.hasNext()) {
                    Attribute myAttribute = listAttr.next();
                    if (StringUtils.equals(myAttribute.getValue(), "sv-cdata-translate")) {
                        StringWriter stringWriter = new StringWriter();
                        xmlEventWriter = xmlOutputFactory.createXMLEventWriter(stringWriter);
                        getProcessor(event).unmarshal(event, storageReader, xmlEventWriter);
                        xmlEventWriter.flush();

                        listXMLEvent.add(stringWriter.getBuffer().toString());
                        isSkipped = true;
                    }
                }
            }

            if (event.isStartElement() == true && event.asStartElement().getName().equals(MacroProcessor.getStructuredMacro())) {
                StringWriter stringWriter = new StringWriter();
                xmlEventWriter = xmlOutputFactory.createXMLEventWriter(stringWriter);
                getProcessor(event).unmarshal(event, storageReader, xmlEventWriter);
                xmlEventWriter.flush();

                listXMLEvent.add(stringWriter.getBuffer().toString());
                isSkipped = true;
            }

            if (!isSkipped) {
                listXMLEvent.add(event);
            }
        }

        for (int i = 0; i < listXMLEvent.size(); i++) {
            Object currentObj = listXMLEvent.get(i);

            if (currentObj == null) {
                continue;
            }

            if (!(currentObj instanceof XMLEvent)) {
                stringBuilder.append(currentObj.toString());
                continue;
            }

            XMLEvent event = (XMLEvent) currentObj;

            if ((event.isEndElement()) && (event.asEndElement().getName().getLocalPart().equals("content"))) {
                break;
            }
            try {
                if (event.isStartElement()) {
                    if (!event.asStartElement().getName().getPrefix().isEmpty()) {
                        String prefix = event.asStartElement().getName().getPrefix();
                        stringBuilder.append("<");
                        stringBuilder.append(prefix);
                        stringBuilder.append(":");
                        stringBuilder.append(event.asStartElement().getName().getLocalPart());

                        if (event.asStartElement().getAttributes() != null) {
                            Iterator<Attribute> listAttr = event.asStartElement().getAttributes();
                            while (listAttr.hasNext()) {
                                Attribute myAttribute = listAttr.next();
                                stringBuilder.append(" ");
                                stringBuilder.append(prefix);
                                stringBuilder.append(":");
                                stringBuilder.append(myAttribute.getName().getLocalPart());
                                stringBuilder.append("=\"");
                                stringBuilder.append(escapeHtml(myAttribute.getValue()));
                                stringBuilder.append("\"");
                            }
                        }
                        stringBuilder.append(">");
                    } else {
                        stringBuilder.append("<");
                        stringBuilder.append(event.asStartElement().getName().getLocalPart());

                        if (event.asStartElement().getAttributes() != null) {
                            Iterator<Attribute> listAttr = event.asStartElement().getAttributes();
                            while (listAttr.hasNext()) {
                                Attribute myAttribute = listAttr.next();
                                stringBuilder.append(" ");
                                stringBuilder.append(myAttribute.getName().getLocalPart());
                                stringBuilder.append("=\"");
                                stringBuilder.append(escapeHtml(myAttribute.getValue()));
                                stringBuilder.append("\"");
                            }
                        }
                        stringBuilder.append(">");
                    }
                } else if (event.isCharacters()) {
                    boolean charEn = false;
                    String charac = event.asCharacters().getData();
                    if (charac.equals("\n")) {
                        charEn = true;
                    }
                    if (!charEn) {
                        if (event.asCharacters().isCData() == true) {
                            stringBuilder.append("<![CDATA[");
                            stringBuilder.append(escapeHtml(event.asCharacters().getData()));
                            stringBuilder.append("]]>");
                        } else {
                            stringBuilder.append(escapeHtml(event.asCharacters().getData()));
                        }
                    }
                } else {
                    if (event.isEndElement()) {
                        if (!event.asEndElement().getName().getPrefix().isEmpty()) {
                            String prefix = event.asEndElement().getName().getPrefix();
                            stringBuilder.append("</");
                            stringBuilder.append(prefix);
                            stringBuilder.append(":");
                            stringBuilder.append(event.asEndElement().getName().getLocalPart());
                            stringBuilder.append(">");
                        } else {
                            stringBuilder.append("</");
                            stringBuilder.append(event.asEndElement().getName().getLocalPart());
                            stringBuilder.append(">");
                        }
                    }
                }
            } catch (RuntimeException e) {
                throw new Exception("RuntimeException while processing.");
            }
        }

        String result = stringBuilder.toString();
        result = DocumentUtil.convertTagEmoji(result, true);
        result = DocumentUtil.skipProcessChar(result, true);
        return result;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="common">
    /**
     * processの取得
     *
     * @param xmlEvent
     * @return
     */
    private static ElementProcessor getProcessor(XMLEvent xmlEvent) {
        for (ElementProcessor processor : elementProcessors) {
            if (processor.shouldProcess(xmlEvent)) {
                return processor;
            }
        }
        return new NoopProcessor();
    }

    public static String changeTitle(List<Page> lstPage, String title, long l, String pageId) {
        String suffix = "_";
        if (!title.contains(suffix)) {
            for (Page page : lstPage) {
                try {
                    if (page != null && page.getTitle().equalsIgnoreCase(title) && !page.getIdAsString().equals(pageId)) {
                        title = title + suffix + l;
                        return changeTitle(lstPage, title, l++, pageId);
                    }
                } catch (Exception ex) {
                    MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + page.getIdAsString());
                    continue;
                }
            }
        } else {
            int index = title.lastIndexOf(suffix);
            String titleAfter = title.substring(0, index);
            String fix = title.substring(index + 1);
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(fix);
            if (matcher.matches()) {
                l = Long.parseLong(fix);
                for (Page page : lstPage) {
                    try {
                        if (page != null && page.getTitle().equalsIgnoreCase(title) && !page.getIdAsString().equals(pageId)) {
                            l = l + 1;
                            title = titleAfter + suffix + l;
                            return changeTitle(lstPage, title, l, pageId);
                        }
                    } catch (Exception ex) {
                        MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + page.getIdAsString());
                        continue;
                    }
                }
            } else {
                for (Page page : lstPage) {
                    try {
                        if (page != null && page.getTitle().equalsIgnoreCase(title) && !page.getIdAsString().equals(pageId)) {
                            title = title + suffix + l;
                            return changeTitle(lstPage, title, l++, pageId);
                        }
                    } catch (Exception ex) {
                        MemoryLogUtil.outputLog("EXCEPTION CHANGE TITLE PAGE ID : " + page.getIdAsString());
                        continue;
                    }
                }
            }
        }

        return title;
    }
    // </editor-fold>
}
