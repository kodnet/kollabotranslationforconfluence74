package jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base;

/**
 *
 * @author lucvd
 */
public class RequestModelBase {

    private String token;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
}
