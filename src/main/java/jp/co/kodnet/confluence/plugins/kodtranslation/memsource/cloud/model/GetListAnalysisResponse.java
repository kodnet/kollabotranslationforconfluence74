package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.AnalysisInfo;

/**
 *
 * @author baoph
 */
public class GetListAnalysisResponse extends ResponseModelBase {

    private AnalysisInfo[] content;

    public AnalysisInfo[] getContent() {
        return content;
    }

    public void setContent(AnalysisInfo[] content) {
        this.content = content;
    }
}
