package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.MemoryRequestUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetSignalTemplateRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetSignalTemplateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetTemplateRequest;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetTemplateResponse;

/**
 *
 * @author tampt
 */
public class TemplateConnector {

    private final static String GET_LIST_API_URL = "api2/v1/projectTemplates";
    private final static String GET_API_URL = "api2/v1/projectTemplates/%s";
    private final String token;


    public TemplateConnector(String token) {
        this.token = token;
    }

    public GetTemplateResponse get() throws Exception {
        GetTemplateRequest request = new GetTemplateRequest();
        request.setToken(this.token);
        return (GetTemplateResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + GET_LIST_API_URL, request, GetTemplateResponse.class, Constants.GET_METHOD);
    }
    
     public GetSignalTemplateResponse getOne(String templateUid) throws Exception {
        GetSignalTemplateRequest request = new GetSignalTemplateRequest();
        request.setToken(this.token);
        String url = String.format(GET_API_URL, templateUid);
        return (GetSignalTemplateResponse) MemoryRequestUtil.createRequestJson(Constants.MEMSOURCE_API_BASE_URL + url, request, GetSignalTemplateResponse.class, Constants.GET_METHOD);
    }
}
