package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author haunh
 */
public class PageRefInfo {

    private String spaceKey;
    private String pageTitle;
    
    public PageRefInfo(){}
    
    /**
     * @return the spaceKey
     */
    public String getSpaceKey() {
        return spaceKey;
    }

    /**
     * @param spaceKey the spaceKey to set
     */
    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    /**
     * @return the pageTitle
     */
    public String getPageTitle() {
        return pageTitle;
    }

    /**
     * @param pageTitle the pageTitle to set
     */
    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
    
}
