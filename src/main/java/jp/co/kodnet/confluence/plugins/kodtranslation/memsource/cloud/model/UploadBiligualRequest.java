package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

public class UploadBiligualRequest extends RequestModelBase {
    
    protected String saveToTransMemory;

    public String getSaveToTransMemory() {
        return saveToTransMemory;
    }

    public void setSaveToTransMemory(String saveToTransMemory) {
        this.saveToTransMemory = saveToTransMemory;
    }
       
}
