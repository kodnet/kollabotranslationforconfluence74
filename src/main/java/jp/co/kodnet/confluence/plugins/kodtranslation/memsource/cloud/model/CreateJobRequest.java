package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import java.util.HashMap;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.RequestModelBase;

/**
 *
 * @author lucvd
 */
public class CreateJobRequest extends RequestModelBase {

    private String[] targetLangs;
    private String xmlInlineTags;
    private boolean xmlInlineTagsAuto;
    private Map<String, Object> importSettings;

    /**
     * @return the targetLang
     */
    public String[] getTargetLangs() {
        return targetLangs;
    }

    /**
     * @param targetLangs the targetLang to set
     */
    public void setTargetLangs(String[] targetLangs) {
        this.targetLangs = targetLangs;
    }

    /**
     * @return the xmlInlineTags
     */
    public String getXmlInlineTags() {
        return xmlInlineTags;
    }

    /**
     * @param xmlInlineTags the xmlInlineTags to set
     */
    public void setXmlInlineTags(String xmlInlineTags) {
        this.xmlInlineTags = xmlInlineTags;
    }

    /**
     * @return the xmlInlineTagsAuto
     */
    public boolean isXmlInlineTagsAuto() {
        return xmlInlineTagsAuto;
    }

    /**
     * @param xmlInlineTagsAuto the xmlInlineTagsAuto to set
     */
    public void setXmlInlineTagsAuto(boolean xmlInlineTagsAuto) {
        this.xmlInlineTagsAuto = xmlInlineTagsAuto;
    }
    
    public void setXmlFileSetting(String xmlFileSetting) {
        if (xmlFileSetting == null || xmlFileSetting.isEmpty() == true) {
            return;
        }

        if (this.importSettings == null) {
            this.importSettings = new HashMap<String, Object>();
        }

        this.importSettings.put("uid", xmlFileSetting);
    }
}
