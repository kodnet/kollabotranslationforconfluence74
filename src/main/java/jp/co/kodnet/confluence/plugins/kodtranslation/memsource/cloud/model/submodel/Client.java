package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author anhlq
 */
public class Client {

    private String id;
    private String name;
    private String externalId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
}
