package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

/**
 *
 * @author kohei.sato
 */
public class CreateJobResponseForAsynchronization extends ResponseModelBase {

    private String unsupportedFile;
    private JobInfo jobPart;

    /**
     * @return the unsupportedFile
     */
    public String getUnsupportedFile() {
        return unsupportedFile;
    }

    /**
     * @param unsupportedFile the unsupportedFile to set
     */
    public void setUnsupportedFile(String unsupportedFile) {
        this.unsupportedFile = unsupportedFile;
    }

    /**
     * @return the jobPart
     */
    public JobInfo getJobPart() {
        return jobPart;
    }

    /**
     * @param jobPart the jobPart to set
     */
    public void setJobParts(JobInfo jobPart) {
        this.jobPart = jobPart;
    }
}
