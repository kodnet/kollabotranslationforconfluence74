package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.AsyncInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;

/**
 *
 * @author lucvd
 */
public class CreateJobResponse extends ResponseModelBase {

    private String[] unsupportedFiles;
    private JobInfo[] jobs;
    private AsyncInfo asyncRequest;

    /**
     * @return the unsupportedFiles
     */
    public String[] getUnsupportedFiles() {
        return unsupportedFiles;
    }

    /**
     * @param unsupportedFiles the unsupportedFiles to set
     */
    public void setUnsupportedFiles(String[] unsupportedFiles) {
        this.unsupportedFiles = unsupportedFiles;
    }

    /**
     * @return the jobParts
     */
    public JobInfo[] getJobs() {
        return jobs;
    }

    /**
     * @param jobs the jobParts to set
     */
    public void setJobs(JobInfo[] jobs) {
        this.jobs = jobs;
    }

    public AsyncInfo getAsyncRequest() {
        return asyncRequest;
    }

    public void setAsyncRequest(AsyncInfo asyncRequest) {
        this.asyncRequest = asyncRequest;
    }
}
