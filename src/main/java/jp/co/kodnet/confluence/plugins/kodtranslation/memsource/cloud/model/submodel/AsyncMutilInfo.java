package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author TamPT
 */
public class AsyncMutilInfo {

    protected AsyncInfo asyncRequest;
    protected AnalysisInfo analyse;

    public AsyncInfo getAsyncRequest() {
        return asyncRequest;
    }

    public void setAsyncRequest(AsyncInfo asyncRequest) {
        this.asyncRequest = asyncRequest;
    }

    public AnalysisInfo getAnalyse() {
        return analyse;
    }

    public void setAnalyse(AnalysisInfo analyse) {
        this.analyse = analyse;
    }
}
