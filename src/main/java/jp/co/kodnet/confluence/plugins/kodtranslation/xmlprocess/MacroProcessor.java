package jp.co.kodnet.confluence.plugins.kodtranslation.xmlprocess;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author TAMPT
 */
public abstract class MacroProcessor implements ElementProcessor {

    public static QName getStructuredMacro() {
        return new QName("http://atlassian.com/content", "structured-macro");
    }

    public final QName QNAME_STRUCTURED_MACRO = new QName("http://atlassian.com/content", "structured-macro");
    public final QName QNAME_MACRO_ID = new QName("http://atlassian.com/content", "macro-id");
    public final QName QNAME_PARAMETER = new QName("http://atlassian.com/content", "parameter");
    public final QName QNAME_NAME_ATTRIBUTE = new QName("http://atlassian.com/content", "name");

    public XMLEventFactory xmlEventFactory;
    public String macroName;

    public MacroProcessor(XMLEventFactory xmlEventFactory, String macroName) {
        this.xmlEventFactory = xmlEventFactory;
        this.macroName = macroName;
    }

    @Override
    public boolean shouldProcess(XMLEvent event) {
        boolean isShould = (event.isStartElement()) && event.asStartElement().getName().getLocalPart().equalsIgnoreCase(QNAME_STRUCTURED_MACRO.getLocalPart());
        if (!isShould) {
            return false;
        }

        Attribute att = event.asStartElement().getAttributeByName(QNAME_NAME_ATTRIBUTE);
        if (att == null) {
            return false;
        }
        if (att.getValue() == null || att.getValue().isEmpty()) {
            return false;
        }

        return att.getValue().toLowerCase().equals(this.macroName);
    }
}
