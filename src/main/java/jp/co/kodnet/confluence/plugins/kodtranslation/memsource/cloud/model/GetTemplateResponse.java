package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Template;

/**
 *
 * @author tampt
 */
public class GetTemplateResponse extends ResponseModelBase {

    private Template[] content;

    public Template[] getContent() {
        return content;
    }

    public void Template(Template[] content) {
        this.content = content;
    }
}
