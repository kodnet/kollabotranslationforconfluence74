package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.WorkflowStep;

/**
 *
 * @author anhlq
 */
public class GetWorkflowStepResponse extends ResponseModelBase {

    private WorkflowStep[] content;

    public WorkflowStep[] getContent() {
        return content;
    }

    public void setContent(WorkflowStep[] lstWorkflowStep) {
        this.content = lstWorkflowStep;
    }
}
