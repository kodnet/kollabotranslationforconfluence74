package jp.co.kodnet.confluence.plugins.kodtranslation.common;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.Labelling;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.fugue.Maybe;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.spring.container.ContainerManager;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.zip.Inflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LabelTemplate;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.AuthenticationConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.ClientConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.FileImportSettingConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.LanguageConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.TemplateConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CreateClientResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.FileImportSettingResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetClientResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetLanguageResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetSignalTemplateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetTemplateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.LoginResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Client;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Template;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.TranslationLanguage;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.DocumentUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.XMLUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author TamPT
 */
public class CommonFunction {

    // <editor-fold defaultstate="collapsed" desc="Common Function">
    /**
     * 文字列の「NULL」または「空」をチェックする
     *
     * @param val
     * @return
     */
    public static boolean isNullOrEmpty(String val) throws Exception {
        if (val == null) {
            return true;
        }

        return val.isEmpty();
    }

    /**
     * InputStreamを文字列に変換する
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static String convertInputStreamToString(InputStream inputStream) throws Exception {
        try {
            byte[] byteArray = convertInputStreamToBytes(inputStream);
            return new String(byteArray, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return "";
        }
    }

    public static byte[] convertInputStreamToBytes(InputStream inputStream) throws Exception {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            int nRead;
            byte[] data = new byte[1024];
            while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }

            buffer.flush();
            byte[] byteArray = buffer.toByteArray();
            return byteArray;
        } catch (Exception ex) {
            byte[] data = new byte[0];
            return data;
        }
    }

    public static Date getCurentDate() {
        return new Date();
    }

    public static Date parseDate(final String dateString, final String format) throws Exception {
        if (StringUtils.isEmpty(dateString) || StringUtils.isBlank(dateString)) {
            return null;
        }

        try {
            return new SimpleDateFormat(format).parse(dateString);
        } catch (ParseException e) {
            // dateString does not have this format
        }
        return null; // dateString does not match any accepted format
    }

    public static boolean hasPermission() throws Exception {
        UserAccessor userAccessor = TranslationMemoryToolFactory.getUserAccessor();
        String confluenceAdmin = UserAccessor.GROUP_CONFLUENCE_ADMINS;
        String translationDirectory = "translation-directory";
        ConfluenceUser userLogin = AuthenticatedUserThreadLocal.get();
        List<String> groupList = userAccessor.getGroupNamesForUserName(userLogin.getName());
        for (int i = 0; i < groupList.size(); i++) {
            if (groupList.get(i).equals(confluenceAdmin) || groupList.get(i).equals(translationDirectory)) {
                return true;
            }
        }

        return false;
    }

//    public static boolean isLicense() throws Exception {
//        TranslationMemoryToolUtil transUtil = TranslationMemoryToolFactory.getTranslationMemoryToolUtil();
//        return transUtil.isLicense();
//    }

    public static String getMsgLicenseKey() throws Exception {
        //Integer utilTyle = TranslationMemoryToolFactory.getTransMemoryStatus();
        I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
//        switch (utilTyle) {
//            case Constants.MEMSOURCE_ID:
//                return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.MEMSOURCE_NAME_LICENSE));
//            case Constants.TRADOS_ID:
//                return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.TRADOS_NAME_LICENSE));
//            case Constants.OMEGAT_ID:
//                return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.OMEGAT_NAME_LICENSE));
//            case Constants.GOOGLE_ID:
//                return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.GOOGLE_NAME_LICENSE));
//            case Constants.T400_ID:
//                return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.T400_NAME_LICENSE));
//        }

        return i18n.getText("kod.plugins.kodtranslationdirectory.translation.unlicensed", Arrays.asList(Constants.TRANSLATION_NAME));
    }

//    public static String getTranslationTool(Translation translation) throws Exception {
//        if (translation.getTransMemUserId() != null && translation.getTransMemUserId().equals(String.valueOf(Constants.TRADOS_ID))) {
//            return translation.getTransMemUserName();
//        }
//        if (translation.getTransMemUserId() != null && translation.getTransMemUserId().equals(String.valueOf(Constants.OMEGAT_ID))) {
//            return translation.getTransMemUserName();
//        }
//        if (translation.getTransMemUserId() != null && translation.getTransMemUserId().equals(String.valueOf(Constants.GOOGLE_ID))) {
//            return translation.getTransMemUserName();
//        }
//        if (translation.getTransMemUserId() != null && translation.getTransMemUserId().equals(String.valueOf(Constants.RICOH_ID))) {
//            return translation.getTransMemUserName();
//        }
//        if (translation.getTransMemUserId() != null && translation.getTransMemUserId().equals(String.valueOf(Constants.T400_ID))) {
//            return translation.getTransMemUserName();
//        }
//        return "Memsource";
//    }

    public static String substringB(String target, int beginIndex) throws Exception {
        try {
            return substringB(target, beginIndex, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String substringB(String target, int beginIndex, String charset) throws Exception {
        byte[] b;
        int endIndex;
        if (target == null) {
            return null;
        }
        b = target.getBytes(charset);
        endIndex = b.length;
        return substringB(target, beginIndex, endIndex, "UTF-8");
    }

    public static String substringB(String target, int beginIndex, int endIndex) throws Exception {
        try {
            return substringB(target, beginIndex, endIndex, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String substringB(String target, int beginIndex, int endIndex, String charset) throws Exception {
        int len = 0;
        StringBuffer buffer = new StringBuffer();
        if (target == null) {
            return null;
        }
        for (int i = 0; i < target.length(); i++) {
            String c = target.substring(i, i + 1);
            byte[] b = c.getBytes(charset);
            if (len >= beginIndex) {
                if (len + b.length > endIndex) {
                    return new String(buffer);
                } else {
                    buffer.append(c);
                }
            }
            len += b.length;
        }
        return new String(buffer);
    }

    public static void readConfig() throws Exception {
        InputStream fileStream = CommonFunction.class.getResourceAsStream("/config/translationconfig.js");
        if (fileStream != null) {
            Properties properties = new Properties();
            properties.load(fileStream);
            Constants.PLUGIN_KEY = properties.getProperty("PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.DIRECTORY_RESOURCE_KEY = Constants.PLUGIN_KEY + Constants.DIRECTORY_RESOURCE_KEY;
            Constants.HOME_RESOURCE_KEY = Constants.PLUGIN_KEY + Constants.HOME_RESOURCE_KEY;
            Constants.DIALOG_RESOURCE_KEY = Constants.PLUGIN_KEY + Constants.DIALOG_RESOURCE_KEY;

            Constants.KOD_TRANSLATION_MEMSOURCE = properties.getProperty("MEMSOURCE_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.MEMSOURCE_NAME_LICENSE = properties.getProperty("MEMSOURCE_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE = properties.getProperty("MEMSOURCE_PACKAGE").replace("\"", "").replace(";", "");
            Constants.MEMSOURCE_NAME = properties.getProperty("MEMSOURCE_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_MEMSOURCE_UTIL = Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE + Constants.KOD_TRANSLATION_MEMSOURCE_UTIL;
            Constants.KOD_TRANSLATION_HOME_MEMSOURCE_UTIL = Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE + Constants.KOD_TRANSLATION_HOME_MEMSOURCE_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_MEMSOURCE_UTIL = Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_MEMSOURCE_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_MEMSOURCE_UTIL = Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_MEMSOURCE_UTIL;
            Constants.KOD_TRANSLATION_VERSION_MEMSOURCE_UTIL = Constants.KOD_TRANSLATION_MEMSOURCE_PACKAGE + Constants.KOD_TRANSLATION_VERSION_MEMSOURCE_UTIL;

            Constants.KOD_TRANSLATION_TRADOS = properties.getProperty("TRADOS_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.TRADOS_NAME_LICENSE = properties.getProperty("TRADOS_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_TRADOS_PACKAGE = properties.getProperty("TRADOS_PACKAGE").replace("\"", "").replace(";", "");
            Constants.TRADOS_NAME = properties.getProperty("TRADOS_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_TRADOS_UTIL = Constants.KOD_TRANSLATION_TRADOS_PACKAGE + Constants.KOD_TRANSLATION_TRADOS_UTIL;
            Constants.KOD_TRANSLATION_HOME_TRADOS_UTIL = Constants.KOD_TRANSLATION_TRADOS_PACKAGE + Constants.KOD_TRANSLATION_HOME_TRADOS_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_TRADOS_UTIL = Constants.KOD_TRANSLATION_TRADOS_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_TRADOS_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_TRADOS_UTIL = Constants.KOD_TRANSLATION_TRADOS_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_TRADOS_UTIL;
            Constants.KOD_TRANSLATION_VERSION_TRADOS_UTIL = Constants.KOD_TRANSLATION_TRADOS_PACKAGE + Constants.KOD_TRANSLATION_VERSION_TRADOS_UTIL;

            Constants.KOD_TRANSLATION_OMEGAT = properties.getProperty("OMEGAT_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.OMEGAT_NAME_LICENSE = properties.getProperty("OMEGAT_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_OMEGAT_PACKAGE = properties.getProperty("OMEGAT_PACKAGE").replace("\"", "").replace(";", "");
            Constants.OMEGAT_NAME = properties.getProperty("OMEGAT_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_OMEGAT_UTIL = Constants.KOD_TRANSLATION_OMEGAT_PACKAGE + Constants.KOD_TRANSLATION_OMEGAT_UTIL;
            Constants.KOD_TRANSLATION_HOME_OMEGAT_UTIL = Constants.KOD_TRANSLATION_OMEGAT_PACKAGE + Constants.KOD_TRANSLATION_HOME_OMEGAT_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_OMEGAT_UTIL = Constants.KOD_TRANSLATION_OMEGAT_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_OMEGAT_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_OMEGAT_UTIL = Constants.KOD_TRANSLATION_OMEGAT_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_OMEGAT_UTIL;
            Constants.KOD_TRANSLATION_VERSION_OMEGAT_UTIL = Constants.KOD_TRANSLATION_OMEGAT_PACKAGE + Constants.KOD_TRANSLATION_VERSION_OMEGAT_UTIL;

            Constants.KOD_TRANSLATION_GOOGLE = properties.getProperty("GOOGLE_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.GOOGLE_NAME_LICENSE = properties.getProperty("GOOGLE_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_GOOGLE_PACKAGE = properties.getProperty("GOOGLE_PACKAGE").replace("\"", "").replace(";", "");
            Constants.GOOGLE_NAME = properties.getProperty("GOOGLE_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_GOOGLE_UTIL = Constants.KOD_TRANSLATION_GOOGLE_PACKAGE + Constants.KOD_TRANSLATION_GOOGLE_UTIL;
            Constants.KOD_TRANSLATION_HOME_GOOGLE_UTIL = Constants.KOD_TRANSLATION_GOOGLE_PACKAGE + Constants.KOD_TRANSLATION_HOME_GOOGLE_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_GOOGLE_UTIL = Constants.KOD_TRANSLATION_GOOGLE_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_GOOGLE_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_GOOGLE_UTIL = Constants.KOD_TRANSLATION_GOOGLE_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_GOOGLE_UTIL;
            Constants.KOD_TRANSLATION_VERSION_GOOGLE_UTIL = Constants.KOD_TRANSLATION_GOOGLE_PACKAGE + Constants.KOD_TRANSLATION_VERSION_GOOGLE_UTIL;

            Constants.KOD_TRANSLATION_RICOH = properties.getProperty("RICOH_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.RICOH_NAME_LICENSE = properties.getProperty("RICOH_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_RICOH_PACKAGE = properties.getProperty("RICOH_PACKAGE").replace("\"", "").replace(";", "");
            Constants.RICOH_NAME = properties.getProperty("RICOH_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_RICOH_UTIL = Constants.KOD_TRANSLATION_RICOH_PACKAGE + Constants.KOD_TRANSLATION_RICOH_UTIL;
            Constants.KOD_TRANSLATION_HOME_RICOH_UTIL = Constants.KOD_TRANSLATION_RICOH_PACKAGE + Constants.KOD_TRANSLATION_HOME_RICOH_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_RICOH_UTIL = Constants.KOD_TRANSLATION_RICOH_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_RICOH_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_RICOH_UTIL = Constants.KOD_TRANSLATION_RICOH_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_RICOH_UTIL;
            Constants.KOD_TRANSLATION_VERSION_RICOH_UTIL = Constants.KOD_TRANSLATION_RICOH_PACKAGE + Constants.KOD_TRANSLATION_VERSION_RICOH_UTIL;

            Constants.KOD_TRANSLATION_T400 = properties.getProperty("T400_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.T400_NAME_LICENSE = properties.getProperty("T400_NAME_LICENSE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_T400_PACKAGE = properties.getProperty("T400_PACKAGE").replace("\"", "").replace(";", "");
            Constants.T400_NAME = properties.getProperty("T400_STRING").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_T400_UTIL = Constants.KOD_TRANSLATION_T400_PACKAGE + Constants.KOD_TRANSLATION_T400_UTIL;
            Constants.KOD_TRANSLATION_HOME_T400_UTIL = Constants.KOD_TRANSLATION_T400_PACKAGE + Constants.KOD_TRANSLATION_HOME_T400_UTIL;
            Constants.KOD_TRANSLATION_DIRECTORY_T400_UTIL = Constants.KOD_TRANSLATION_T400_PACKAGE + Constants.KOD_TRANSLATION_DIRECTORY_T400_UTIL;
            Constants.KOD_TRANSLATION_DETAILS_T400_UTIL = Constants.KOD_TRANSLATION_T400_PACKAGE + Constants.KOD_TRANSLATION_DETAILS_T400_UTIL;
            Constants.KOD_TRANSLATION_VERSION_T400_UTIL = Constants.KOD_TRANSLATION_T400_PACKAGE + Constants.KOD_TRANSLATION_VERSION_T400_UTIL;

            Constants.KOD_TRANSLATION_VERSION = properties.getProperty("VERSION_PLUGIN_KEY").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_VERSION_PACKAGE = properties.getProperty("VERSION_PACKAGE").replace("\"", "").replace(";", "");
            Constants.KOD_TRANSLATION_VERSION_HTML_UTIL = Constants.KOD_TRANSLATION_VERSION_PACKAGE + Constants.KOD_TRANSLATION_VERSION_HTML_UTIL;
        }
    }
    
    public static String toString(Document doc) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        doc.setXmlStandalone(true);
        DOMSource dom = new DOMSource(doc);

        StringWriter sw = new StringWriter();
        StreamResult streamResult = new StreamResult(sw);
        transformer.setOutputProperty(OutputKeys.ENCODING, Settings.DEFAULT_DEFAULT_ENCODING);
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.transform(dom, streamResult);

        return sw.toString();
    }

    public static void addZipEntry(ZipOutputStream zos, String filename, String filetext) throws Exception {
        ZipEntry outZipEntry = new ZipEntry(filename);
        outZipEntry.setMethod(ZipOutputStream.DEFLATED);
        zos.putNextEntry(outZipEntry);
        zos.write(filetext.getBytes(Settings.DEFAULT_DEFAULT_ENCODING));
        zos.closeEntry();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="DataBase Config">
    /**
     *
     * @param supportedLanguageService
     */
    public static void insertLanguages(TranslationLanguageListService translationLanguageListService, InputStream fileStream, String toolId) throws Exception {
        CommonFunction.csv_read(translationLanguageListService, fileStream, toolId);
    }
//
//    /**
//     * DBに新規にテーブルを追加します。（テーブル名SelectedModuleType）
//     *
//     * @param selectedModuleTypeService
//     * @param fileStream
//     */
//    public static void insertSelectedModuleType(SelectedModuleTypeService selectedModuleTypeService, InputStream fileStream) throws Exception {
//        selectedModuleTypeService.deleteAll();
//        CommonFunction.csvInsertSelectedModuleType(selectedModuleTypeService, fileStream);
//    }

    /**
     * DBに新規にテーブルを追加します。（テーブル名SelectedModuleType）
     */
    public static boolean insertSelectedConnectorType() throws Exception {
        // インストール時にCSV(SelectedConnecterType.csv)の中身をDBに追加します
        InputStream fileStream = CommonFunction.class.getResourceAsStream("/csv/SelectedConnecterType.csv");
//        if (fileStream != null) {
//            TranslationMemoryMasterService translationMemoryMasterService = TranslationMemoryToolFactory.getTranslationMemoryMasterService();
//            return CommonFunction.csvSelectedConnectorType(translationMemoryMasterService, fileStream);
//        }

        return false;
    }

    public static boolean insertXpath() throws Exception {
        // インストール時にCSV(Xpath.csv)の中身をDBに追加します
        InputStream fileStream = CommonFunction.class.getResourceAsStream("/csv/Xpath.csv");
//        if (fileStream != null) {
////            SkipTransPathService skipTransPathService = TranslationMemoryToolFactory.getSkipTransPathService();
////            skipTransPathService.deleteAll();
//            return CommonFunction.csvXpath(skipTransPathService, fileStream);
//        }

        return false;
    }

    public static List<List<String>> csv_read(TranslationLanguageListService translationLanguageListService, InputStream fileStream, String toolId) throws Exception {

        int idx = 0;
        String line = "";
        List<List<String>> ret = new ArrayList<List<String>>();
        String cvsSplitBy = ",";
        BufferedReader br = null;

        try {
            InputStreamReader in = new InputStreamReader(fileStream, "UTF-8");
            br = new BufferedReader(in);

            while ((line = br.readLine()) != null) {
                if (idx == 0) {
                    String[] header = line.split(cvsSplitBy);
                    if (header.length == Constants.NUMBER_COLUMN_IN_CSV && header[0].equals("ID") && header[1].equals("Code") && header[2].equals("Language")) {
                        idx++;
                        continue;
                    } else {
                        break;
                    }
                } else {
                    String[] content = line.split(cvsSplitBy);
                    if (content.length >= Constants.NUMBER_COLUMN_IN_CSV) {
                        String content2 = line;
                        int index = line.indexOf(content[2]);
                        if (index > -1) {
                            content2 = content2.substring(index);
                        }

                        content2 = content2.replace("\"", "");
                        translationLanguageListService.add(toolId, content[1], content2);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }

        return ret;
    }

    /**
     * DBに新規にテーブルを追加します。（テーブル名SelectedModuleType）
     *
     * @param selectedModuleTypeService
     * @param fileStream
     * @return
     */
//    public static List<List<String>> csvInsertSelectedModuleType(SelectedModuleTypeService selectedModuleTypeService, InputStream fileStream) throws Exception {
//        int idx = 0;
//        String line = "";
//        List<List<String>> ret = new ArrayList<List<String>>();
//        String cvsSplitBy = ",";
//        BufferedReader br = null;
//
//        try {
//            InputStreamReader in = new InputStreamReader(fileStream, "UTF-8");
//            br = new BufferedReader(in);
//
//            while ((line = br.readLine()) != null) {
//                if (idx == 0) {
//                    String[] header = line.split(cvsSplitBy);
//                    if (header.length == Constants.CSV_SELECTED_MODULE_TYPE_ALL_COLUMN
//                            && header[0].equals(Constants.CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_1_NAME)
//                            && header[1].equals(Constants.CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_2_NAME)
//                            && header[2].equals(Constants.CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_3_NAME)
//                            && header[3].equals(Constants.CSV_SELECTED_MODULE_TYPE_HEADER_COLUMN_4_NAME)) {
//                        idx++;
//                        continue;
//                    } else {
//                        break;
//                    }
//                } else {
//                    String[] content = line.split(cvsSplitBy);
//                    if (content.length == Constants.CSV_SELECTED_MODULE_TYPE_ALL_COLUMN) {
//                        selectedModuleTypeService.add(Integer.parseInt(content[0]), content[1], Integer.parseInt(content[2]), Integer.parseInt(content[3]));
//                    }
//                }
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//
//        }
//
//        return ret;
//    }

    /**
     * DBに新規にテーブルを追加します。（テーブル名SelectedConnecterType）
     *
     * @param translationMemoryMasterService
     * @param translationMemoryStatusService
     * @param fileStream
     * @return
     */
//    public static boolean csvSelectedConnectorType(TranslationMemoryMasterService translationMemoryMasterService, InputStream fileStream) throws Exception {
//        int idx = 0;
//        String line = "";
//        List<List<String>> ret = new ArrayList<List<String>>();
//        String cvsSplitBy = ",";
//        BufferedReader br = null;
//
//        try {
//            List<TransMemoryMaster> lstMemoryMaster = translationMemoryMasterService.getAll();
//            List<Integer> memoryIds = new ArrayList<Integer>();
//            for (TransMemoryMaster transMemoryMaster : lstMemoryMaster) {
//                memoryIds.add(transMemoryMaster.getTmemID());
//            }
//
//            InputStreamReader in = new InputStreamReader(fileStream, "UTF-8");
//            br = new BufferedReader(in);
//            while ((line = br.readLine()) != null) {
//                String[] content = line.split(cvsSplitBy);
//                if (idx == 0) {
//                    if (content.length != Constants.CSV_SELECTED_CONNECTOR_TYPE_ALL_COLUMN
//                            || !content[0].equals(Constants.CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_1_NAME)
//                            || !content[1].equals(Constants.CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_2_NAME)
//                            || !content[2].equals(Constants.CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_3_NAME)
//                            || !content[3].equals(Constants.CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_4_NAME)
//                            || !content[4].equals(Constants.CSV_SELECTED_CONNECTOR_TYPE_HEADER_COLUMN_5_NAME)) {
//                        break;
//                    }
//                    idx++;
//                } else if (content.length == Constants.CSV_SELECTED_CONNECTOR_TYPE_ALL_COLUMN && memoryIds.contains(Integer.parseInt(content[3])) == false) {
//                    if (Integer.parseInt(content[4]) == 1) {
//                        translationMemoryMasterService.add(Integer.parseInt(content[3]), content[1], content[2], null);
//                        CommonFunction.setupMemorySettings(Integer.parseInt(content[3]));
//                    } else {
//                        translationMemoryMasterService.add(Integer.parseInt(content[3]), content[1], content[2], null);
//                    }
//                }
//            }
//
//            br.close();
//            in.close();
//            fileStream.close();
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    /**
     * DBに新規にテーブルを追加します。（テーブル名Xpath）
     *
     * @param skipTransPathService
     * @param fileStream
     * @return
     */
//    public static boolean csvXpath(SkipTransPathService skipTransPathService, InputStream fileStream) throws Exception {
//        String line = "";
//        BufferedReader br = null;
//        try {
//            InputStreamReader in = new InputStreamReader(fileStream, "UTF-8");
//            br = new BufferedReader(in);
//            while ((line = br.readLine()) != null) {
//                if (line != null) {
//                    skipTransPathService.addNew(line);
//                }
//            }
//
//            br.close();
//            in.close();
//            fileStream.close();
//            return true;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//    }

    public static void setupMemorySettings(Integer tmemId) throws Exception {
        Random random = new Random();
        random.longs(100000000000L, 999999999999L);
        //TranslationMemoryToolFactory.setHashKey(String.valueOf(random.nextLong()));
        //TranslationMemoryMasterService translationMemoryMasterService = TranslationMemoryToolFactory.getTranslationMemoryMasterService();
        //TranslationMemoryStatusService translationMemoryStatusService = TranslationMemoryToolFactory.getTranslationMemoryStatusService();
        //TransMemoryMaster master = translationMemoryMasterService.getTransMemoryMaster(tmemId);
//        if (master != null) {
//            String hashKeyMd5 = CommonFunction.getHashKeyLicenseMd5(master.getTmemname(), master.getTmemID().toString(), TranslationMemoryToolFactory.getHashKey());
//            translationMemoryMasterService.update(tmemId, hashKeyMd5);
//            translationMemoryStatusService.deleteAll();
//            translationMemoryStatusService.add(tmemId);
//        }
    }

    public static String getHashKeyLicenseMd5(String tmemName, String tmemId, String hashKey) throws Exception {
        try {
            String tmemMd5 = CommonFunction.convertStringToMD5(tmemName + tmemId);
            return CommonFunction.convertStringToMD5(tmemMd5 + hashKey);
        } catch (Exception e) {
            return "";
        }
    }

    public static String convertStringToMD5(String string) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(string.getBytes());
        byte byteData[] = md.digest();

        //convert the byte to hex format method
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    public static boolean checkIsImageDirectory(String spaceKey) {
        try {
            boolean isImageDirectory = false;
//            Object resultObj = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_CHECK_IMAGE_DIRECTORY_FROM_SPACE_KEY, spaceKey);
//            if (resultObj != null) {
//                isImageDirectory = (boolean) resultObj;
//            }
            return isImageDirectory;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isPageHasLabel(String label, Page page) {
        if (page.isDeleted() == true || page.isDraft() == true || page.isLatestVersion() == false) {
            return false;
        }

        List<Label> labels = page.getLabels();
        for (Label l : labels) {
            if (l.getName().equalsIgnoreCase(label) == true || l.getDisplayTitle().equalsIgnoreCase(label) == true) {
                return true;
            }
        }

        return false;
    }

    public static boolean isPageImageDirectory(Long pageId) {
        Page page = TranslationMemoryToolFactory.getPageManager().getPage(pageId);
        if (page == null) {
            return false;
        }

        return isPageImageDirectory(page);
    }
    
    
    public static String getTemplateId(Page page, String langkey) {
        String templateId = "";
        try {
            TranslationLabelTemplateService labelTemplateService = TranslationMemoryToolFactory.getTranslationLabelTemplateService();
            List<Label> labels = page.getLabels();
            for (Label label : labels) {
                String labelName = label.getDisplayTitle();
                LabelTemplate labelTemplate = labelTemplateService.getByLabelAndLangKey(labelName, langkey);
                if(labelTemplate != null){
                    return labelTemplate.getUid();
                }
            }
            return templateId;
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
            return templateId;
        }
    }

    public static boolean isPageImageDirectory(Page page) {
        return isPageHasLabel(Constants.IMAGE_DIRECTORY_LABEL, page);
    }

    public static boolean isPageUiControl(Long pageId) {
        Page page = TranslationMemoryToolFactory.getPageManager().getPage(pageId);
        if (page == null) {
            return false;
        }

        return isPageUiControl(page);
    }

    public static boolean isPageUiControl(Page page) {
        return isPageHasLabel(Constants.UI_CONTROL_TITLE_PAGE_LABEL, page);
    }

    public static boolean checkIsImageToHomePage(String spaceKey) {
        try {
            boolean isImageToHomePage = false;
//            Object resultObj = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_CHECK_IS_IMAGE_TO_HOME_PAGE_LANG_FROM_SPACE_KEY, spaceKey);
//            if (resultObj != null) {
//                isImageToHomePage = (boolean) resultObj;
//            }
            return isImageToHomePage;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static void convertLinkImageInAllPage(Page page, String titlePageTo, String spaceKey) throws Exception {
        try {
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            String xmlPage = page.getBodyAsString();
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeList = XMLUtils.getElementsByTagNameAndAttr(root, "ri:attachment", "ri:filename");
                if (!nodeList.isEmpty()) {
                    boolean isReplace = false;
                    for (Element element : nodeList) {
                        List<Element> listRiPage = XMLUtils.getElementsByTagNameAndAttr(element, "ri:page", "ri:content-title");
                        if (!listRiPage.isEmpty()) {
                            Element eRiPage = listRiPage.get(0);
                            if (eRiPage.hasAttribute("ri:space-key")) {
                                continue;
                            }
                            eRiPage.setAttribute("ri:content-title", titlePageTo);
                            eRiPage.setAttribute("ri:space-key", spaceKey);
                        } else {
                            Element eRiPage = document.createElement("ri:page");
                            Element eRiSpace= document.createElement("ri:space-key");
                            eRiPage.setAttribute("ri:content-title", titlePageTo);
                            eRiSpace.setAttribute("ri:space-key", spaceKey);
                            element.appendChild(eRiPage);
                            isReplace = true;
                        }
                    }
                    if (isReplace) {
                        xmlPage = DocumentUtil.convertXmlToString(document, false, true);
                        page.setBodyAsString(xmlPage);
                        pageManager.saveContentEntity(page, null);
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
    }
    
//    public static boolean checkIsImageInBaseLang(String spaceKey) {
//        try {
//            boolean isImageInBaseLang = false;
//            Object resultObj = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_CHECK_IS_IMAGE_IN_BASE_LANG_FROM_SPACE_KEY, spaceKey);
//            if (resultObj != null) {
//                isImageInBaseLang = (boolean) resultObj;
//            }
//            return isImageInBaseLang;
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public static boolean checkHaveVisualIndex(Page page) {
        try {
            String xmlPage = page.getBodyAsString();
            Document document = DocumentUtil.convertStringToDocument(xmlPage, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeVisualIndexList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "visualIndexTool");
                if (!nodeVisualIndexList.isEmpty()) {
                    return true;
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return false;
    }
    
//    public static boolean checkDocumentIsReleaseBySpaceKey(String spaceKey) throws Exception {
//        Boolean isRelease = false;
//        Object resultObj = DocumentKanriConnector.execute(DocumentKanriConnector.METHOD_GET_DOCUMENT_BY_SPACE, spaceKey);
//        if (resultObj != null) {
//            JsonParser parser = new JsonParser();
//            JsonObject jsonObject = (JsonObject) parser.parse(resultObj.toString());
//            isRelease = jsonObject.get("status").getAsBoolean();
//        }
//        return isRelease;
//    }
    
//    public static boolean checkDocumentIsReleaseByPageId(Long pageId) throws Exception {
//        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
//        Boolean isRelease = false; 
//        Page page = pageManager.getPage(pageId);
//        if ( page == null) {
//            return isRelease;
//        }
//        
//        Space space = page.getSpace();
//        if (space == null) {
//            return isRelease;
//        }
//        
//        isRelease = checkDocumentIsReleaseBySpaceKey(space.getKey());
//        return isRelease;
//    }

    public static List<String> getListImageNeedInPage(Page page, boolean isImageDirectory) throws Exception {
        List<String> lists = new ArrayList<String>();
        try {
            if (isImageDirectory != true) {
                return lists;
            }
            AttachmentManager attachmentManager = (AttachmentManager) ContainerManager.getInstance().getContainerContext().getComponent("attachmentManager");
            List<Attachment> listAttachment = page.getLatestVersionsOfAttachments();
            if (listAttachment.isEmpty()) {
                return lists;
            }
            Document document = DocumentUtil.convertStringToDocument(page.getBodyAsString(), true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeDrawioList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "drawio");
                for (Element element : nodeDrawioList) {
                    List<Element> diagramNameList = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "diagramName");
                    if (!diagramNameList.isEmpty()) {
                        String diagramName = diagramNameList.get(0).getTextContent();
                        Attachment diagramFile = attachmentManager.getAttachmentDao().getLatestAttachment(page, diagramName);
                        String diagramData = getDiagramData(diagramFile);
                        lists.addAll(getListAttrName(listAttachment, diagramData, diagramName));
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
        if (!lists.isEmpty()) {
            LinkedHashSet<String> hashSet = new LinkedHashSet<>(lists);
            lists = new ArrayList<>(hashSet);

        }
        return lists;
    }

    public static List<String> getListAttrDrawioInPage(Page page, boolean isImageDirectory) throws Exception {
        List<String> lists = new ArrayList<String>();
        try {
            if (isImageDirectory != true) {
                return lists;
            }
            List<Attachment> listAttachment = page.getLatestVersionsOfAttachments();
            if (listAttachment.isEmpty()) {
                return lists;
            }
            Document document = DocumentUtil.convertStringToDocument(page.getBodyAsString(), true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeDrawioList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "drawio");
                for (Element element : nodeDrawioList) {
                    List<Element> diagramNameList = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "diagramName");
                    if (!diagramNameList.isEmpty()) {
                        String diagramName = diagramNameList.get(0).getTextContent();
                        lists.add(diagramName);
                    }
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
        return lists;
    }

    public static List<Attachment> getAttachmentDrawio(Page currentPage) {
        AttachmentManager attachmentManager = (AttachmentManager) ContainerManager.getInstance().getContainerContext().getComponent("attachmentManager");
        List<Attachment> diagramFiles = new ArrayList<Attachment>();
        try {
            Document document = DocumentUtil.convertStringToDocument(currentPage.getBodyAsString(), true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeDrawioList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "drawio");
                for (Element element : nodeDrawioList) {
                    List<Element> diagramNameList = XMLUtils.getElementsByTagNameAndAttr(element, "ac:parameter", "ac:name", "diagramName");
                    if (!diagramNameList.isEmpty()) {
                        String diagramName = diagramNameList.get(0).getTextContent();
                        Attachment diagramFile = attachmentManager.getAttachmentDao().getLatestAttachment(currentPage, diagramName);
                        diagramFiles.add(diagramFile);
                    }
                }
            }
        } catch (Exception ex) {
            return diagramFiles;
        }
        return diagramFiles;
    }

    public static List<String> getListAttrName(List<Attachment> attachments, String diagramData, String imageDiagram) {
        List<String> listAttrName = new ArrayList<String>();
        for (Attachment attachment : attachments) {
            String nameAttr = attachment.getFileName();
            String pathAttr = attachment.getDownloadPathWithoutVersionOrApiRevision();
            if (listAttrName.contains(pathAttr)) {
                continue;
            }
            if (diagramData.contains(pathAttr)) {
                listAttrName.add(nameAttr);
            } else if ((imageDiagram + ".png").equals(nameAttr)) {
                listAttrName.add(nameAttr);
            }
        }
        return listAttrName;
    }

    public static String getDiagramData(Attachment attachmentDrawio) throws Exception {
        AttachmentManager attachmentManager = (AttachmentManager) ContainerManager.getInstance().getContainerContext().getComponent("attachmentManager");
        InputStream in = attachmentManager.getAttachmentData(attachmentDrawio);
        String drowIoData = convertInputStreamToString(in);
        Document docDrawIo = DocumentUtil.convertStringToDocument(drowIoData, true);
        NodeList nodes = docDrawIo.getElementsByTagName("diagram");
        if (nodes.getLength() <= 0) {
            return "";
        }

        Element diagram = (Element) nodes.item(0);
        String diagramData = diagram.getTextContent();
        diagramData = decodeDrawIO(diagramData);
        return diagramData;
    }

    public static String decodeDrawIO(String input) throws Exception {
        //Base64 decode
        byte[] bytes = Base64.decodeBase64(input);
        //DeflaterRaw
        String output = DeflaterRaw(bytes);
        //Decode URI
        output = decodeURIComponent(output);
        return output;
    }

    public static String decodeURIComponent(String s) {
        if (s == null) {
            return null;
        }

        String result = null;

        try {
            result = URLDecoder.decode(s, "UTF-8");
        } // This exception should never occur.
        catch (UnsupportedEncodingException e) {
            result = s;
        }

        return result;
    }

    public static String DeflaterRaw(byte[] bytes) throws Exception {
        Inflater inflater = new Inflater(true);
        inflater.setInput(bytes);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(bytes.length);
        byte[] buff = new byte[1024];
        while (!inflater.finished()) {
            int count = inflater.inflate(buff);
            baos.write(buff, 0, count);
        }
        baos.close();
        byte[] output = baos.toByteArray();
        return new String(output);
    }

//    public static boolean checkIsPageUIControl(PageDataEntity page) {
//        try {
//            String title = page.getTitle();
//            List<String> labels = page.getLabels();
//            if (labels.isEmpty()) {
//                return false;
//            }
//            int count = 0;
//            for (String label : labels) {
//                if (label.equals(Constants.UI_CONTROL_TITLE_PAGE_LABEL)) {
//                    count++;
//                }
//                if (label.equals(Constants.UI_CONTROL_TITLE_PAGE_LABEL_TBD)) {
//                    count++;
//                }
//            }
//            return !(count != 2 || !(title).equals(Constants.UI_CONTROL_TITLE_PAGE));
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public static boolean checkIsPageUIControl(Page page) {
        try {
            String title = page.getTitle();
            List<Label> labels = page.getLabels();
            if (labels.isEmpty()) {
                return false;
            }
            int count = 0;
            for (Label label : labels) {
                String key = label.getName();
                if (key.equalsIgnoreCase(Constants.UI_CONTROL_TITLE_PAGE_LABEL)) {
                    count++;
                }
                if (key.equalsIgnoreCase(Constants.UI_CONTROL_TITLE_PAGE_LABEL_TBD)) {
                    count++;
                }
            }
            return !(count != 2 || !(title).equals(Constants.UI_CONTROL_TITLE_PAGE));
        } catch (Exception e) {
            return false;
        }
    }

    public static JSONObject updateUrlImgData(JSONObject jsonObject, String img, Page page) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        try {
            SettingsManager settingsManager = TranslationMemoryToolFactory.getSettingsManager();
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            JSONArray pageImgJson = jsonObject.getJSONArray(img);
            if (pageImgJson == null) {
                return jsonObject;
            }
            JSONArray jsonArrPageImg = new JSONArray();
            for (int i = 0; i < pageImgJson.length(); i++) {
                Maybe<Attachment> listAtt = attachmentManager.getAttachmentForDownloadPath((String) pageImgJson.get(i));
                if (listAtt == null) {
                    continue;
                }
                for (Attachment attachment : listAtt) {
                    String fileName = attachment.getFileName();
                    if (fileName == null || ("").equals(fileName)) {
                        continue;
                    }
                    Attachment newAttachment = attachmentManager.getAttachment(page, fileName);
                    if (newAttachment == null) {
                        continue;
                    }
                    jsonArrPageImg.put(baseUrl + newAttachment.getDownloadPath());
                }
            }
            jsonObject.put(img, jsonArrPageImg);
            return jsonObject;
        } catch (Exception ex) {
            MemoryLogUtil.outputLog(ex.getMessage());
            return jsonObject;
        }
    }

    public static JSONObject updateLinkPageVisual(JSONObject jsonObject, String nameList, String spaceKeyFrom, String spaceKeyTo, boolean isSync) {
        SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
        PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
        try {
            if (isSync == false) {
                Space spaceFrom = spaceManager.getSpace(spaceKeyFrom);
                if (spaceFrom == null) {
                    return jsonObject;
                }
            }
            JSONObject pageJson = jsonObject.getJSONObject(nameList);
            if (pageJson == null) {
                return jsonObject;
            }
            JSONObject jsonArrPage = new JSONObject();
            Iterator<String> keys = pageJson.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (("").equals(key)) {
                    continue;
                }
                //数値の場合はページIDとして扱う
                JSONObject wwItem = pageJson.getJSONObject(key);

                String title = wwItem.getString("title");
                if (title == null || ("").equals(title)) {
                    continue;
                }
                String id = wwItem.getString("url");
                if (id == null || ("").equals(id) || isNumber(id) == false) {
                    continue;
                }

                if (isSync == false) {
                    Page pageLink = pageManager.getPage(spaceKeyFrom, title);
                    if (pageLink == null) {
                        continue;
                    }
                    String idPageLink = pageLink.getIdAsString();
                    if (!(id).equals(idPageLink)) {
                        continue;
                    }
                    Page pageLinkReplace = pageManager.getPage(spaceKeyTo, title);
                    if (pageLinkReplace == null) {
                        continue;
                    }
                    wwItem.put("url", pageLinkReplace.getIdAsString());
                } else {
                    Page pageLink = pageManager.getPage(Long.parseLong(id));
                    if (pageLink == null) {
                        continue;
                    }
                    String spaceKeyLink = pageLink.getSpaceKey();
                    if (!spaceKeyLink.equals(spaceKeyTo)) {
                        continue;
                    }
                    wwItem.put("title", pageLink.getTitle());
                    wwItem.put("url", pageLink.getIdAsString());
                }
                pageJson.put(key, wwItem);
            }
            jsonObject.put(nameList, pageJson);
            return jsonObject;
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
            return jsonObject;
        }
    }

    public static List<String> getListNameVisualIndex(String bodyString) {
        List<String> nameAttachmentVisual = new ArrayList<>();
        try {
            //Get all visual index in page => add name list visual index
            Document document = DocumentUtil.convertStringToDocument(bodyString, true);
            if (document != null) {
                Element root = document.getDocumentElement();
                List<Element> nodeVisualIndexList = XMLUtils.getElementsByTagNameAndAttr(root, "ac:structured-macro", "ac:name", "visualIndexTool");
                if (nodeVisualIndexList.isEmpty()) {
                    return nameAttachmentVisual;
                }
                for (Element element : nodeVisualIndexList) {
                    Element elementName = XMLUtils.getFirstElementByTagNameAndAttr(element, "ac:parameter", "ac:name", "name");
                    if (elementName == null) {
                        continue;
                    }
                    String name = elementName.getTextContent();
                    if (name == null || ("").equals(name)) {
                        continue;
                    }
                    nameAttachmentVisual.add(name);
                }
            }
            return nameAttachmentVisual;
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
            return nameAttachmentVisual;
        }
    }

    public static JSONObject getJSONObject(InputStream inputStream) {
        try {
            ByteArrayOutputStream into = new ByteArrayOutputStream();
            byte[] buf = new byte[4096];
            for (int n; 0 < (n = inputStream.read(buf));) {
                into.write(buf, 0, n);
            }
            into.close();
            String strData = new String(into.toByteArray(), "UTF-8");
            JSONObject obj = new JSONObject(strData);
            return obj;
        } catch (Exception e) {
            return null;
        }
    }

    public static void updateAttachment(String fileName, ContentEntityObject contentEntityObject, String dataText) {
        try {
            //Page currentPage = pageManager.getPage(Long.parseLong(pageid));
            byte[] dataByte = dataText.getBytes("utf-8");
            InputStream is = new ByteArrayInputStream(dataByte);
            AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
            Attachment attachment = attachmentManager.getAttachment(contentEntityObject, fileName);
            Attachment newAttachmentClone = (Attachment) attachment.clone();
            newAttachmentClone.convertToHistoricalVersion();
            attachment.setFileSize(dataByte.length);
            attachment.setContentType("application/json");
            attachmentManager.saveAttachment(attachment, newAttachmentClone, is);
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
        }
    }

      public static boolean checkIsPageVideo(Page page) {
        TransactionTemplate transactionTemplate = TranslationMemoryToolFactory.getTransactionTemplate();
        boolean result = transactionTemplate.execute(new TransactionCallback<Boolean>() {
            @Override
            public Boolean doInTransaction() {
                try {
                    List<Label> labels = page.getLabels();
                    for (Label label : labels) {
                        if (Constants.LABEL_PAGE_VIDEO.equals(label.getDisplayTitle())) {
                            return true;
                        }
                    }
                    return false;
                } catch (Exception e) {
                    return false;
                }
            }
        });
        return result;
    }
    
    private static boolean isNumber(String text) {
        try {
            Long.parseLong(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public static void copyAttachmentAddLanguage(Page page, Page oldPage) throws Exception {
        AttachmentManager attachmentManager = TranslationMemoryToolFactory.getAttachmentManager();
        List<Attachment> listAttachment = oldPage.getLatestVersionsOfAttachments();
        if (listAttachment.size() > 0) {
            for (int i = 0; i < listAttachment.size(); i++) {
                Attachment baseAttachment = listAttachment.get(i);
                String attrName = baseAttachment.getFileName();
                try {
                    List<Attachment> listBaseAttachment = attachmentManager.getAllVersions(baseAttachment);
                    Collections.sort(listBaseAttachment, new Comparator<Attachment>() {
                        @Override
                        public int compare(Attachment o1, Attachment o2) {
                            if (o1 == null) {
                                return 1;
                            }

                            if (o2 == null) {
                                return -1;
                            }

                            return Integer.compare(o1.getVersion(), o2.getVersion());
                        }
                    });

                    int currentIndex = -1;
                    for (int j = 0; j < listBaseAttachment.size(); j++) {
                        baseAttachment = listBaseAttachment.get(j);
                        if (baseAttachment == null) {
                            continue;
                        }

                        if (baseAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        Attachment newAttachment = new Attachment(baseAttachment.getFileName(), baseAttachment.getContentType(), baseAttachment.getFileSize(), null);
                        Attachment oldAttachment = null;
                        int index = baseAttachment.getVersion();
                        if (currentIndex == -1) {
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setVersion(index);
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            page.addAttachment(newAttachment);
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e1) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            currentIndex = newAttachment.getVersion();
                            continue;
                        }

                        index = currentIndex;
                        newAttachment = null;
                        newAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), currentIndex);
                        if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                            continue;
                        }

                        for (int k = index; k < baseAttachment.getVersion(); k++) {
                            if (newAttachment == null || newAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            oldAttachment = (Attachment) newAttachment.clone();
                            oldAttachment.convertToHistoricalVersion();
                            InputStream in = baseAttachment.getContentsAsStream();
                            newAttachment.setContentType(baseAttachment.getContentType());
                            newAttachment.setVersionComment(baseAttachment.getVersionComment());
                            for (Labelling label : baseAttachment.getLabellings()) {
                                newAttachment.addLabelling(label.copy());
                            }
                            newAttachment.setCreationDate(baseAttachment.getCreationDate());
                            newAttachment.setCreator(baseAttachment.getCreator());
                            newAttachment.setTitle(baseAttachment.getTitle());
                            try {
                                newAttachment.setFileSize(baseAttachment.getFileSize());
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            } catch (Exception e2) {
                                Integer fileSize = getSizeInputStream(in);
                                newAttachment.setFileSize(fileSize);
                                attachmentManager.saveAttachment(newAttachment, oldAttachment, baseAttachment.getContentsAsStream());
                            }
                            currentIndex = newAttachment.getVersion();
                        }

                        for (int k = index + 1; k < baseAttachment.getVersion(); k++) {
                            oldAttachment = null;
                            oldAttachment = attachmentManager.getAttachment(page, baseAttachment.getFileName(), k);
                            if (oldAttachment == null || oldAttachment.getFileSize() <= 0) {
                                continue;
                            }

                            attachmentManager.removeAttachmentVersionFromServer(oldAttachment);
                        }
                    }
                } catch (Exception ex) {
                    throw new RuntimeException("Copy attachment fail", ex);
                }
            }
        }
    }

    public static Integer getSizeInputStream(InputStream in) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        int b;
        while ((b = in.read()) != -1) {
            os.write(b);
        }

        return os.size();
    }
    
    public static Map<String, Object> renderInlineTag() throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        Object valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_INLINE_TAG_CONFIG);
        String value = "";
        if (valueObject != null) {
            value = valueObject.toString();
        }
        resultMap.put("result", true);
        resultMap.put("value", value);
        return resultMap;
    }

    public static Map<String, Object> doSaveInlineTag(String value) throws Exception {
        I18NBean i18nBean = TranslationMemoryToolFactory.getI18NBean();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        value = value.replace(" ", "");
        bandanaManager.setValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_INLINE_TAG_CONFIG, value);
        sendAPIFileImportSettingConfig();
        resultMap.put("result", true);
        resultMap.put("message", i18nBean.getText("kod.plugins.kodtranslationdirectory.translation.management.directory.setting.inlinetag.setting.success"));
        return resultMap;
    }

    public static Map<String, Object> renderNotInlineTag() throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        Object valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_NOT_INLINE_TAG), Constants.BANDANA_KEY_NOT_INLINE_TAG_CONFIG);
        String value = "";
        if (valueObject != null) {
            value = valueObject.toString();
        }
        resultMap.put("result", true);
        resultMap.put("value", value);
        return resultMap;
    }

    public static Map<String, Object> doSaveNotInlineTag(String value) throws Exception {
        I18NBean i18nBean = TranslationMemoryToolFactory.getI18NBean();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        value = value.replace(" ", "");
        bandanaManager.setValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_NOT_INLINE_TAG), Constants.BANDANA_KEY_NOT_INLINE_TAG_CONFIG, value);
        sendAPIFileImportSettingConfig();
        resultMap.put("result", true);
        resultMap.put("message", i18nBean.getText("kod.plugins.kodtranslationdirectory.translation.management.directory.setting.not.inlinetag.setting.success"));
        return resultMap;
    }
    
    public static void sendAPIFileImportSettingConfig() throws Exception {
        TranslationApiLoginInfoService translationApiLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
        ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        FileImportSettingConnector fileImportSettingConnector = new FileImportSettingConnector(apiLoginInfo.getLoggedToken());
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        Object valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_INLINE_TAG_CONFIG);
        String inlineTagValue = "";
        if (valueObject != null) {
            inlineTagValue = valueObject.toString();
        }

        valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_NOT_INLINE_TAG), Constants.BANDANA_KEY_NOT_INLINE_TAG_CONFIG);
        String notInlineTagValue = "";
        if (valueObject != null) {
            notInlineTagValue = valueObject.toString();
        }

        FileImportSettingResponse fileImportSettingResponse = fileImportSettingConnector.create(inlineTagValue, notInlineTagValue);
        bandanaManager.setValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_FILE_SETTING_CONFIG + apiLoginInfo.getUserName(), fileImportSettingResponse.getUid());
    }
   
    public static Map<String, Object> doLoginMemsource(String userName, String password, String clientId, String email, String inchargedName, String companyName) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
        TranslationApiLoginInfoService translationLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
        clientId = Constants.MEMSOURCE_CLIENT_NAME;
        email = Constants.MEMSOURCE_MAIL;
        inchargedName = Constants.MEMSOURCE_CONTACT_NAME;
        companyName = Constants.MEMSOURCE_COMPANY_NAME;

        // login server
        AuthenticationConnector authenticationConnector = new AuthenticationConnector();
        LoginResponse loginResponse = authenticationConnector.login(userName, password, null);
        if (!CommonFunction.isNullOrEmpty(loginResponse.getErrorCode())) {
            resultMap.put("result", false);
            resultMap.put("message", i18n.getText("kod.plugins.kodtranslationdirectory.translation.cloud.mem.save.error"));
            return resultMap;
        }

        Map<String, Object> resultCheck = isExistedClientNameMemsource(loginResponse.getToken(), clientId);
        String error = String.valueOf(resultCheck.get("error"));
        if (CommonFunction.isNullOrEmpty(error) == false) {
            resultMap.put("result", false);
            resultMap.put("message", error);
            return resultMap;
        }

        Client client = (Client) resultCheck.get("client");
        clientId = client.getId();
        Date expiresDate = CommonFunction.parseDate(loginResponse.getExpires(), Constants.MEMSOURCE_DATE_FORMAT);
        ApiLoginInfo apiLoginInfo = translationLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        if (apiLoginInfo != null) {
            translationLoginInfoService.update(String.valueOf(Constants.MEMSOURCE_ID), inchargedName, userName, password, loginResponse.getToken(), clientId, null, expiresDate);
        } else {
            translationLoginInfoService.add(String.valueOf(Constants.MEMSOURCE_ID), inchargedName, userName, password, loginResponse.getToken(), clientId, null, expiresDate);
        }

        TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
        translationLanguageListService.delete(String.valueOf(Constants.MEMSOURCE_ID));
        LanguageConnector languageConnector = new LanguageConnector(loginResponse.getToken());
        GetLanguageResponse languageResponse = languageConnector.get();
        TranslationLanguage[] translationLanguage = languageResponse.getLanguages();
        for (TranslationLanguage trans : translationLanguage) {
            String key = trans.getCode();
            String name = trans.getName();
            translationLanguageListService.add(String.valueOf(Constants.MEMSOURCE_ID), key, name);
        }
        //save inline tag
        FileImportSettingConnector fileImportSettingConnector = new FileImportSettingConnector(loginResponse.getToken());
        BandanaManager bandanaManager = TranslationMemoryToolFactory.getBandanaManager();
        Object valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_INLINE_TAG_CONFIG);
        String inlineTagValue = "";
        if (valueObject != null) {
            inlineTagValue = valueObject.toString();
        }

        valueObject = bandanaManager.getValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_NOT_INLINE_TAG), Constants.BANDANA_KEY_NOT_INLINE_TAG_CONFIG);
        String notInlineTagValue = "";
        if (valueObject != null) {
            notInlineTagValue = valueObject.toString();
        }

        FileImportSettingResponse fileImportSettingResponse = fileImportSettingConnector.create(inlineTagValue, notInlineTagValue);
        bandanaManager.setValue(new ConfluenceBandanaContext(Constants.BANDANA_CONTEXT_INLINE_TAG), Constants.BANDANA_KEY_FILE_SETTING_CONFIG + userName, fileImportSettingResponse.getUid());
        resultMap.put("result", true);
        resultMap.put("message", "");

        return resultMap;
    }

    public static Map<String, Object> isExistedClientNameMemsource(String token, String clientId) throws Exception {
        I18NBean i18Bean = TranslationMemoryToolFactory.getI18NBean();
        Map<String, Object> result = new HashMap<String, Object>();

        ClientConnector clientConnector = new ClientConnector(token);
        GetClientResponse lstClient = clientConnector.get();
        if (!CommonFunction.isNullOrEmpty(lstClient.getErrorCode())) {
            String rsMsg = getMessageFromResponse(lstClient);
            result.put("error", i18Bean.getText("kod.plugins.kodtranslationdirectory.translation.api.error", Arrays.asList(i18Bean.getText(Constants.MEMSOURCE_STRING), rsMsg, EnumAPI.GET_LIST_API_URL_CLIENTCONNECTOR.url())));
            result.put("data", "2");
            return result;
        }

        if (lstClient.getContent() == null) {
            result.put("error", i18Bean.getText("kod.plugins.kodtranslationdirectory.translation.cloud.mem.save.noclientid", Arrays.asList(i18Bean.getText(Constants.MEMSOURCE_STRING))));
            result.put("data", "2");
            return result;
        }

        boolean isExisted = false;
        for (Client client : lstClient.getContent()) {
            if (client.getName().equals(clientId)) {
                result.put("error", StringUtils.EMPTY);
                result.put("client", client);
                isExisted = true;
                return result;
            }
        }

        if (!isExisted) {
            String error = createClientName(clientConnector, clientId);
            result.put("error", error);
            result.put("data", "3");
        }

        return result;
    }

    public static String getTemplateClient(String token, String templateId) throws Exception {
        try {
            if(templateId == null || templateId.isEmpty()){
                return null;
            }
            TemplateConnector templateConnector = new TemplateConnector(token);
            GetSignalTemplateResponse templateResponse = templateConnector.getOne(templateId);
            if (!CommonFunction.isNullOrEmpty(templateResponse.getErrorCode())) {
                return Constants.ERROR;
            }
            
            if (templateResponse.getClient() == null) {
                return null;
            }
            String id = templateResponse.getClient().getId();
            String name = templateResponse.getClient().getName();
            if (id != null && !id.isEmpty()
                    && name != null && !name.isEmpty()) {
                return name;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    
     public static String getMessageFromResponse(ResponseModelBase response) throws Exception {
        String responseMsg = !CommonFunction.isNullOrEmpty(response.getErrorCode()) ? response.getErrorCode() : "";
        responseMsg += " " + (!CommonFunction.isNullOrEmpty(response.getErrorDescription()) ? response.getErrorDescription() : "");
        return response.getErrorCode() + " " + response.getErrorDescription();
    }
     
         /**
     *
     * @param clientId
     * @return
     */
    public static String createClientName(ClientConnector clientConnector, String clientId) throws Exception {
        CreateClientResponse client = clientConnector.create(clientId);
        if (!CommonFunction.isNullOrEmpty(client.getErrorCode())) {
            I18NBean i18Bean = TranslationMemoryToolFactory.getI18NBean();
            return i18Bean.getText("kod.plugins.kodtranslationdirectory.translation.cloud.mem.create.client.error", Arrays.asList(i18Bean.getText(Constants.MEMSOURCE_STRING)));
        }

        return StringUtils.EMPTY;
    }


    // </editor-fold>
}
