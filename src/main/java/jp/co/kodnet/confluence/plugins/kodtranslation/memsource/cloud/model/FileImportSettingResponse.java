package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model;

import jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base.ResponseModelBase;

/**
 *
 * @author TamPT
 */
public class FileImportSettingResponse extends ResponseModelBase {

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String uid;
}
