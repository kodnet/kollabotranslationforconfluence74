package jp.co.kodnet.confluence.plugins.kodtranslation.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.user.Group;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.FavoriteLanguage;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.GroupInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LabelTemplate;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.TemplateConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetTemplateResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Client;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.Template;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationFavoriteLanguageService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationGroupInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;

public class TranslationMemoryDetailsJson extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private Map<String, Object> resultBean = new HashMap<>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="bean method">
    @Override
    public Object getBean() {
        return resultBean;
    }
    // </editor-fold>

    public String getListBaseLanguageHtml() {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            this.resultBean = this.renderListBaseLanguageHtml(pageId);
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.resultBean = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public String renderTranslationInfoSetting() throws Exception {
        try {
            this.resultBean = this.getLstInfoTranslationCoreSetting();
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.resultBean = MemoryLogUtil.getBeanFaild(ex);
        }
        return SUCCESS;
    }

    public Map<String, Object> renderListBaseLanguageHtml(Long pageId) throws Exception {
        Map<String, Object> resultMap = new HashMap<>();
        StringBuilder strLanguages = new StringBuilder();
        StringBuilder strBaseLanguage = new StringBuilder();
        Map<String, String> lstLang = new HashMap<String, String>();
        TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
        TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();

        boolean hasBaseLanguage = false;
        TranslationJob translationJob = translationJobService.getOneByFromPageId(pageId);
        String baseLangKey = "";
        if (translationJob != null) {
            hasBaseLanguage = true;
            baseLangKey = translationJob.getFromLanguageKey();
        } else {
            translationJob = translationJobService.getOneByToPageId(pageId);
            if (translationJob != null) {
                hasBaseLanguage = true;
                baseLangKey = translationJob.getToLanguageKey();
            }
        }
        TranslationFavoriteLanguageService translationFavoriteLanguageService = TranslationMemoryToolFactory.getTranslationFavoriteLanguageService();
        List<FavoriteLanguage> lstFavoriteLanguage = translationFavoriteLanguageService.getAll();
        Map<String, String> lstLangFavorite = new HashMap<String, String>();
        for (FavoriteLanguage favoriteLanguage : lstFavoriteLanguage) {
            lstLang.put(favoriteLanguage.getLanguageKey(), favoriteLanguage.getLanguageDisplay());
        }

        int count = 0;
        for (Map.Entry<String, String> entry : lstLang.entrySet()) {

            strBaseLanguage.append("<option value=\"");
            strBaseLanguage.append(entry.getKey());
            strBaseLanguage.append("\"");
            if (baseLangKey != null && !("").equals(baseLangKey) && (entry.getKey()).equals(baseLangKey)) {
                strBaseLanguage.append(" selected");
            }
            strBaseLanguage.append(">");
            strBaseLanguage.append(entry.getValue());
            strBaseLanguage.append("</option>");

            strLanguages.append("<option value=\"");
            strLanguages.append(entry.getKey());
            strLanguages.append("\"");
            if (baseLangKey != null && !("").equals(baseLangKey) && !baseLangKey.equals(entry.getKey())) {
                strLanguages.append(">");
            } else {
                strLanguages.append(" style=\"display: none;\""); 
                strLanguages.append(">");    
            }
            strLanguages.append(entry.getValue());
            strLanguages.append("</option>");
            count++;
        }

        if (count > 0) {
            strBaseLanguage.append("<option disabled=\"disabled\" value=\"");
            strBaseLanguage.append("");
            strBaseLanguage.append("\"");
            strBaseLanguage.append(">");
            strBaseLanguage.append("－－－－－－－－－－－－－");
            strBaseLanguage.append("</option>");

            strLanguages.append("<option disabled=\"disabled\" value=\"");
            strLanguages.append("");
            strLanguages.append("\"");
            strLanguages.append(">");
            strLanguages.append("－－－－－－－－－－－－－");
            strLanguages.append("</option>");
        }

        List<LanguageList> supportedLanguages = translationLanguageListService.getByTransToolId(String.valueOf(Constants.MEMSOURCE_ID));
        for (LanguageList supportedLanguage : supportedLanguages) {
            String langCode = supportedLanguage.getLanguageKey();
            String langName = supportedLanguage.getLanguageDisplay();

            if (!lstLang.containsKey(langCode)) {
                strBaseLanguage.append("<option value=\"");
                strBaseLanguage.append(langCode);
                strBaseLanguage.append("\"");
                if (baseLangKey != null && !("").equals(baseLangKey) && (langCode).equals(baseLangKey)) {
                    strBaseLanguage.append(" selected");
                }
                strBaseLanguage.append(">");
                strBaseLanguage.append(langName);
                strBaseLanguage.append("</option>");

                if (!baseLangKey.equals(langCode)) {
                    strLanguages.append("<option value=\"");
                    strLanguages.append(langCode);
                    strLanguages.append("\"");
                    strLanguages.append(">");
                    strLanguages.append(langName);
                    strLanguages.append("</option>");
                }
                lstLang.put(langCode, langName);
            }
        }

        resultMap.put("result", true);
        resultMap.put("fromLang", strBaseLanguage.toString());
        resultMap.put("toLang", strLanguages.toString());
        resultMap.put("hasBaseLanguage", hasBaseLanguage);
        return resultMap;
    }

    public Map<String, Object> getLstInfoTranslationCoreSetting() throws Exception {
        Map<String, Object> result = new LinkedHashMap<>();
        JsonArray langsFavorite = getFavoriteLanguage();
        JsonArray langsRemain = getAllLanguageSupport();
        Gson gson = new Gson();
        MemoryLogUtil.outputLog("selectedLang " + gson.toJson(langsFavorite));
        MemoryLogUtil.outputLog("remainLang " + gson.toJson(langsRemain));
        result.put("selectedLang", gson.toJson(langsFavorite));
        result.put("remainLang", gson.toJson(langsRemain));
        result.put("groups", getInfoGroups());
        
        return result;
    }

    public JsonArray getFavoriteLanguage() throws Exception {
        JsonArray result = new JsonArray();
        TranslationFavoriteLanguageService translationFavoriteLanguageService = TranslationMemoryToolFactory.getTranslationFavoriteLanguageService();
        List<FavoriteLanguage> lstFavoriteLanguage = translationFavoriteLanguageService.getAll();
        for (FavoriteLanguage favoriteLanguage : lstFavoriteLanguage) {
            MemoryLogUtil.outputLog("FAVOURITE LANG : " + favoriteLanguage.getLanguageKey());
            JsonObject obj = new JsonObject();
            obj.addProperty("keyLang", favoriteLanguage.getLanguageKey());
            obj.addProperty("nameLang", favoriteLanguage.getLanguageDisplay());
            result.add(obj);
        }
        MemoryLogUtil.outputLog("FAVỎITE  LANG : " + result.toString());
        return result;
    }

    public JsonArray getAllLanguageSupport() throws Exception {
        JsonArray result = new JsonArray();
        TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
        List<LanguageList> supportedLanguages = translationLanguageListService.getAll();
        TranslationFavoriteLanguageService translationFavoriteLanguageService = TranslationMemoryToolFactory.getTranslationFavoriteLanguageService();
        for (LanguageList supportedLanguage : supportedLanguages) {
            MemoryLogUtil.outputLog("SUPPORT LANG : " + supportedLanguage.getLanguageKey());
            JsonObject obj = new JsonObject();
            FavoriteLanguage favoriteLanguage = translationFavoriteLanguageService.getByLangKey(supportedLanguage.getLanguageKey());
            if(favoriteLanguage != null){
                continue;
            }
            obj.addProperty("keyLang", supportedLanguage.getLanguageKey());
            obj.addProperty("nameLang", supportedLanguage.getLanguageDisplay());
            result.add(obj);
        }
        MemoryLogUtil.outputLog("SUPPORT LANG : " + result.toString());
        return result;
    }
    
    public String getInfoGroups() throws Exception {
        TranslationGroupInfoService translationGroupInfoService = TranslationMemoryToolFactory.getTranslationGroupInfoService();
        List<Group> listGroup = userAccessor.getGroupsAsList();
        GroupInfo groupInfo = translationGroupInfoService.get();
        String groupNameSelected = "";
        if(groupInfo != null){
            groupNameSelected = groupInfo.getGroupName();
        }
        StringBuilder strGroups = new StringBuilder();
        strGroups.append("<select class=\"select group-wiki-setting-selected\">");
        for (Group group : listGroup) {
            strGroups.append("<option data-name-group=\"");
            strGroups.append(group.getName());
            if(groupNameSelected.isEmpty() == false && group.getName().equals(groupNameSelected)){
                strGroups.append("\" selected=\"selected\"");
            }
            strGroups.append("\">");
            strGroups.append(group.getName());
            strGroups.append("</option>");
        }
        strGroups.append("</select>");
        return strGroups.toString();
    }

    public String renderTemplateMemsource() throws Exception {
        //Get token
        TranslationApiLoginInfoService translationApiLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
        ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        if (apiLoginInfo == null) {
            this.resultBean.put("result", false);
            this.resultBean.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
            return SUCCESS;
        }
        
        Date dateExpires = apiLoginInfo.getDateExpires();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        if (dateExpires != null && (sdf.parse(sdf.format(dateExpires)).before(sdf.parse(sdf.format(new Date()))) || sdf.parse(sdf.format(dateExpires)).equals(sdf.parse(sdf.format(new Date()))))) {
            CommonFunction.doLoginMemsource(apiLoginInfo.getUserName(), apiLoginInfo.getPassword(), String.valueOf(Constants.MEMSOURCE_ID), "", "", "");
            apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
        }

        String token = apiLoginInfo.getLoggedToken();
        if (token == null) {
            this.resultBean.put("result", false);
            this.resultBean.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
            return SUCCESS;
        }
        TemplateConnector templateConnector = new TemplateConnector(token);
        GetTemplateResponse templateResponse = templateConnector.get();
        if(templateResponse.getContent() == null){
            this.resultBean.put("result", false);
            this.resultBean.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.api.template.no.data"));
            return SUCCESS;
        }
        JsonObject map = new JsonObject();
        for (Template template : templateResponse.getContent()) {
            map.addProperty(template.getUid(), template.getTemplateName());
        }
        
        JsonObject mapLang = new JsonObject();
        TranslationFavoriteLanguageService translationFavoriteLanguageService = TranslationMemoryToolFactory.getTranslationFavoriteLanguageService();
        List<FavoriteLanguage> lstFavoriteLanguage = translationFavoriteLanguageService.getAll(); 
        int count = 0;
        for (FavoriteLanguage favoriteLanguage : lstFavoriteLanguage) {
           mapLang.addProperty(favoriteLanguage.getLanguageKey(), favoriteLanguage.getLanguageDisplay());
           count++;
        }
        
        if(count > 0){
           mapLang.addProperty(Constants.NONE_LANG, "－－－－－－－－－－－－－");
        }
        
        TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
        List<LanguageList> languageLists = translationLanguageListService.getAll();
        for (LanguageList languageList : languageLists) {
            if(mapLang.has(languageList.getLanguageKey())){
                continue;
            }
            mapLang.addProperty(languageList.getLanguageKey(), languageList.getLanguageDisplay());
        }
        
        TranslationLabelTemplateService translationLabelTemplateService = TranslationMemoryToolFactory.getTranslationLabelTemplateService();
        List<LabelTemplate> labelTemplates = translationLabelTemplateService.getAll(); 
        StringBuilder strBaseLanguage = new StringBuilder();
        for (int i = 0; i < labelTemplates.size(); i++) {
            List<String> lstLang = new ArrayList<>();
            count = 0;
            LabelTemplate labelTemplate = labelTemplates.get(i);
            String langKey = labelTemplate.getLanguageKey() == null ? "ja" : labelTemplate.getLanguageKey();
            strBaseLanguage.append("<tr>");
            strBaseLanguage.append("<td>");
            strBaseLanguage.append("<span class=\"aui-icon aui-icon-small aui-iconfont-cross remove-template-setting\" style=\"color: red;cursor: pointer;\">");
            strBaseLanguage.append("</td>");
            strBaseLanguage.append("<td>");
            strBaseLanguage.append("<input class=\"text label-setting\" type=\"text\" value=\"");
            strBaseLanguage.append(labelTemplate.getLabelTemplate());
            strBaseLanguage.append("\">");
            strBaseLanguage.append("</td>");
            strBaseLanguage.append("<td>");
            strBaseLanguage.append("<select class=\"select lang-key-setting-selected\">");
            
            for(FavoriteLanguage favoriteLanguage: lstFavoriteLanguage){
                strBaseLanguage.append("<option data-lang-key=\"");
                strBaseLanguage.append(favoriteLanguage.getLanguageKey());
                if (favoriteLanguage.getLanguageKey().equals(langKey)) {
                    strBaseLanguage.append("\" selected=\"selected\"");
                }
                strBaseLanguage.append("\">");
                strBaseLanguage.append(favoriteLanguage.getLanguageDisplay());
                strBaseLanguage.append("</option>");
                lstLang.add(favoriteLanguage.getLanguageKey());
                count++;
            }

            if (count > 0) {
                strBaseLanguage.append("<option disabled=\"disabled\" value=\"");
                strBaseLanguage.append("");
                strBaseLanguage.append("\"");
                strBaseLanguage.append(">");
                strBaseLanguage.append("－－－－－－－－－－－－－");
                strBaseLanguage.append("</option>");
            }
            
            for (LanguageList languageList : languageLists) {
                if(lstLang.contains(languageList.getLanguageKey())){
                    continue;
                }
                strBaseLanguage.append("<option data-lang-key=\"");
                strBaseLanguage.append(languageList.getLanguageKey());
                if (languageList.getLanguageKey().equals(langKey)) {
                    strBaseLanguage.append("\" selected=\"selected\"");
                }
                strBaseLanguage.append("\">");
                strBaseLanguage.append(languageList.getLanguageDisplay());
                strBaseLanguage.append("</option>");
            }
            strBaseLanguage.append("</select>");
            strBaseLanguage.append("</td>");
            strBaseLanguage.append("<td>");
            strBaseLanguage.append("<select class=\"select template-setting-selected\">");
            for (Template template : templateResponse.getContent()) {
                strBaseLanguage.append("<option data-api-uid=\"");
                strBaseLanguage.append(template.getUid());
                if (template.getUid().equals(labelTemplate.getUid())) {
                    strBaseLanguage.append("\" selected=\"selected\"");
                }
                strBaseLanguage.append("\">");
                strBaseLanguage.append(template.getTemplateName());
                strBaseLanguage.append("</option>");
            }
            strBaseLanguage.append("</select>");
            strBaseLanguage.append("</td>");
            strBaseLanguage.append("</tr>");
        }
        this.resultBean.put("result", true);
        this.resultBean.put("html", strBaseLanguage.toString());
        this.resultBean.put("template", map.toString());
        this.resultBean.put("languages", mapLang.toString());
        return SUCCESS;
    }

    // <editor-fold defaultstate="collapsed" desc="permission">
    @Override
    public boolean isPermitted() {
        return super.isPermitted();
    }
    // </editor-fold>
}
