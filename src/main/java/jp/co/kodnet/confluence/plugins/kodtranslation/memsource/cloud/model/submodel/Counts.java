package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel;

/**
 *
 * @author anhlq
 */
public class Counts {

    private int confirmedCharsCount;
    private int charsCount;

    public int getConfirmedCharsCount() {
        return confirmedCharsCount;
    }

    public void setConfirmedCharsCount(int confirmedCharsCount) {
        this.confirmedCharsCount = confirmedCharsCount;
    }

    public int getCharsCount() {
        return charsCount;
    }

    public void setCharsCount(int charsCount) {
        this.charsCount = charsCount;
    }

}
