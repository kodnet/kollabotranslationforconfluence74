package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import java.util.List;
import net.java.ao.Query;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;

public class TranslationLanguageListServiceImpl implements TranslationLanguageListService {

    private final ActiveObjects ao;

    public TranslationLanguageListServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }

    @Override
    public List<LanguageList> getAll() {
        List<LanguageList> translationLanguageLists= newArrayList(ao.find(LanguageList.class));
        return translationLanguageLists;
    }

    @Override
    public LanguageList getOne(String translationToolId) {
        List<LanguageList> translationLanguageLists = newArrayList(ao.find(LanguageList.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        if (translationLanguageLists.isEmpty()) {
            return null;
        }
        return translationLanguageLists.get(0);
    }

    @Override
    public List<LanguageList> getByTransToolId(String translationToolId) {
        List<LanguageList> translationLanguageLists = newArrayList(ao.find(LanguageList.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        return translationLanguageLists;
    }

    @Override
    public LanguageList get() {
        List<LanguageList> translationLanguageLists = newArrayList(ao.find(LanguageList.class));
        if (translationLanguageLists.isEmpty()) {
            return null;
        }
        return translationLanguageLists.get(0);
    }

    @Override
    public LanguageList getByLangKey(String langKey) {
        List<LanguageList> translationLanguageLists = newArrayList(ao.find(LanguageList.class, Query.select().where("LANGUAGE_KEY = ?", langKey)));
        if (translationLanguageLists.isEmpty()) {
            return null;
        }
        return translationLanguageLists.get(0);
    }

    @Override
    public LanguageList add(
            String translationToolId,
            String languageKey,
            String languageDisplay
            ){
        LanguageList translationLanguageList = ao.create(LanguageList.class);
        translationLanguageList.setTranslationToolId(translationToolId);
        translationLanguageList.setLanguageKey(languageKey);
        translationLanguageList.setLanguageDisplay(languageDisplay);
        translationLanguageList.save();
        return translationLanguageList;
    }

    @Override
    public LanguageList update(
            String translationToolId,
            String languageKey,
            String languageDisplay
            ) {
        LanguageList translationLanguageList = this.getOne(translationToolId);
        if (translationLanguageList != null) {
            translationLanguageList.setLanguageKey(languageKey);
            translationLanguageList.setLanguageDisplay(languageDisplay);
            translationLanguageList.save();
        }
        return translationLanguageList;
    }

    @Override
    public void delete(String translationToolId) {
        List<LanguageList> translationLanguageLists = newArrayList(ao.find(LanguageList.class, Query.select().where("TRANSLATION_TOOL_ID = ?", translationToolId)));
        if (translationLanguageLists.isEmpty()) {
            return;
        }
        for (LanguageList trans : translationLanguageLists) {
            if (trans != null) {
                ao.delete(trans);
            }
        }
    }

    @Override
    public void deleteAll() {
        List<LanguageList> ip = this.getAll();
        for (LanguageList item : ip) {
            ao.delete(item);
        }
    }
}
