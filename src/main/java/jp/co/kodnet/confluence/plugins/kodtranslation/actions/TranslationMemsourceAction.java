package jp.co.kodnet.confluence.plugins.kodtranslation.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.longrunning.LongRunningTaskId;
import com.atlassian.confluence.util.longrunning.LongRunningTaskManager;
import com.atlassian.user.Group;
import com.google.common.collect.Iterables;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.opensymphony.webwork.ServletActionContext;
import static com.opensymphony.xwork.Action.ERROR;
import static com.opensymphony.xwork.Action.SUCCESS;
import com.opensymphony.xwork.ActionContext;
import com.sun.tools.internal.xjc.Language;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback.TranslationMemsourceJobCallBack;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback.TranslationMemsourceTask;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.ActionContextHelper;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.GroupInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LabelTemplate;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback.TranslationMemsourceSyncCallBack;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback.TranslationMemsourceSyncTask;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationGroupInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLabelTemplateService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;

public class TranslationMemsourceAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="variable">
    private Map<String, Object> bean = new HashMap<>();
    private String message;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="bean method">
    @Override
    public Object getBean() {
        return bean;
    }
    // </editor-fold>

    public String checkExistsJobMemsource() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            String spaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "spaceKey");
            String fromLangKey = ActionContextHelper.getFirstParameterValueAsString(context, "fromLangKey");
            String toLangKey = ActionContextHelper.getFirstParameterValueAsString(context, "toLangKey");

            if (spaceKey == null || spaceKey.equals("")) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.space.select.not.found.error"));
                return ERROR;
            }

            if (pageId == 0) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
                return ERROR;
            }

            if (fromLangKey == null || fromLangKey.equals("")) {
                this.bean.put("result", false);
                return ERROR;
            }

            if (toLangKey == null || toLangKey.equals("")) {
                this.bean.put("result", false);
                return ERROR;
            }
            TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
            TranslationJob trans = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, pageId);
            if (trans == null) {
                this.bean.put("result", false);
                return SUCCESS;
            }
            this.bean.put("result", true);
            this.bean.put("projectId", trans.getApiProjectId());
            return SUCCESS;
        } catch (Exception ex) {
            Logger.getLogger(TranslationMemsourceAction.class.getName()).log(Level.SEVERE, null, ex);
            this.bean.put("result", false);
            message = ex.getMessage();
            this.bean.put("message", message);
        }
        return SUCCESS;
    }

    public String doCreateProjectMemsource() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            String spaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "spaceKey");
            String fromLangKey = ActionContextHelper.getFirstParameterValueAsString(context, "fromLangKey");
            String fromLangName = ActionContextHelper.getFirstParameterValueAsString(context, "fromLangName");
            String toLangKey = ActionContextHelper.getFirstParameterValueAsString(context, "toLangKey");
            String toLangName = ActionContextHelper.getFirstParameterValueAsString(context, "toLangName");
            String projectId = ActionContextHelper.getFirstParameterValueAsString(context, "projectId");

            if (spaceKey == null || spaceKey.equals("")) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.space.select.not.found.error"));
                return ERROR;
            }

            if (pageId == 0) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
                return ERROR;
            }

            if (fromLangKey == null || fromLangKey.equals("")) {
                this.bean.put("result", false);
                return ERROR;
            }

            if (toLangKey == null || toLangKey.equals("")) {
                this.bean.put("result", false);
                return ERROR;
            }

            Map<String, Object> mapInput = new HashMap<>();
            mapInput.put("pageId", pageId);
            mapInput.put("spaceKey", spaceKey);
            mapInput.put("fromLangKey", fromLangKey);
            mapInput.put("fromLangName", fromLangName);
            mapInput.put("toLangKey", toLangKey);
            mapInput.put("toLangName", toLangName);
            mapInput.put("projectId", projectId);

            TranslationMemsourceJobCallBack<String> createProjectMemsourceCallBack = new TranslationMemsourceJobCallBack<String>(mapInput);
            LongRunningTaskManager longRunningTaskManager = TranslationMemoryToolFactory.getLongRunningTaskManager();
            TranslationMemsourceTask memoryTask = new TranslationMemsourceTask(createProjectMemsourceCallBack);
            LongRunningTaskId taskId = longRunningTaskManager.startLongRunningTask(getAuthenticatedUser(), memoryTask);
            this.bean.put("result", true);
            this.bean.put("taskId", taskId.toString());
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.bean.put("result", false);
            message = ex.getMessage();
            this.bean.put("message", message);
        }
        return SUCCESS;
    }

    public String doSyncMemsourceContent() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            List<String> langKeys = ActionContextHelper.getParameterValuesAsStringList(context, "arrLangKeys");
            String spaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "spaceKey");
            String projectUid = ActionContextHelper.getFirstParameterValueAsString(context, "projectUid");
            String jobId = ActionContextHelper.getFirstParameterValueAsString(context, "jobId");
            Boolean recapture = ActionContextHelper.getFirstCheckBoxParameterValueAsBoolean(context, "recapture");

            if (spaceKey == null || spaceKey.equals("")) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.space.select.not.found.error"));
                return ERROR;
            }

            if (pageId == 0 && langKeys.isEmpty() == true) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
                return ERROR;
            }
            Map<String, Object> mapInput = new HashMap<>();
            mapInput.put("pageId", pageId);
            mapInput.put("langKeys", langKeys);
            mapInput.put("spaceKey", spaceKey);
            mapInput.put("recapture", recapture);
            mapInput.put("projectUid", projectUid);
            mapInput.put("jobId", jobId);

            TranslationMemsourceSyncCallBack<String> translationMemsourceSyncCallBack = new TranslationMemsourceSyncCallBack<String>(mapInput);
            LongRunningTaskManager longRunningTaskManager = TranslationMemoryToolFactory.getLongRunningTaskManager();
            TranslationMemsourceSyncTask translationMemsourceTask = new TranslationMemsourceSyncTask(translationMemsourceSyncCallBack);
            LongRunningTaskId taskId = longRunningTaskManager.startLongRunningTask(getAuthenticatedUser(), translationMemsourceTask);

            this.bean.put("result", true);
            this.bean.put("taskId", taskId.toString());
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            this.bean.put("result", false);
            message = e.getMessage();
            this.bean.put("message", message);
        }
        return SUCCESS;
    }

    public String doRenderListPageLangugeSync() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            String spaceKey = ActionContextHelper.getFirstParameterValueAsString(context, "spaceKey");
            if (spaceKey == null || spaceKey.equals("")) {
                this.bean.put("result", false);
                this.bean.put("message", getText(""));
                return ERROR;
            }

            if (pageId == 0) {
                this.bean.put("result", false);
                this.bean.put("message", getText(""));
                return ERROR;
            }

            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
            TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
            List<TranslationJob> listTranslationJobs = translationJobService.getByTranslationJobByFromPageId(pageId);
            if (listTranslationJobs.isEmpty()) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.message.is.page.no.project.lang"));
                return SUCCESS;
            }
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            JsonArray jsonArray = new JsonArray();
            for (int i = listTranslationJobs.size() - 1; i >= 0; i--) {
                JsonObject obj = new JsonObject();
                TranslationJob translationJob = listTranslationJobs.get(i);
                String key = translationJob.getToLanguageKey();
                if (key == null) {
                    continue;
                }
                LanguageList language = translationLanguageListService.getByLangKey(key);
                if (language == null) {
                    continue;
                }
                String keyLang = language.getLanguageKey();
                String nameLang = language.getLanguageDisplay();
                String projectUid = translationJob.getApiProjectId();
                if (projectUid == null) {
                    continue;
                }

                Long toPageId = translationJob.getToPageId();
                String linkToPageLang = "";
                String titlePage = "";
                boolean hasPageLang = false;
                if (toPageId != null) {
                    titlePage = pageManager.getPage(toPageId).getTitle();
                    linkToPageLang = baseUrl + "/pages/viewpage.action?pageId=" + String.valueOf(toPageId);
                    hasPageLang = true;
                }
                LanguageList fromLanguage = translationLanguageListService.getByLangKey(translationJob.getFromLanguageKey());
                String fromKeyLang = fromLanguage.getLanguageKey();
                String fromNameLang = fromLanguage.getLanguageDisplay();

                obj.addProperty("projectUid", projectUid);
                obj.addProperty("titlePage", titlePage);
                obj.addProperty("linkToPageLang", linkToPageLang);
                obj.addProperty("keyLang", keyLang);
                obj.addProperty("nameLang", nameLang);
                obj.addProperty("fromKeyLang", fromKeyLang);
                obj.addProperty("fromNameLang", fromNameLang);
                obj.addProperty("fromPageId", translationJob.getFromPageId());
                obj.addProperty("jobId", translationJob.getTranslationJobId());
                obj.addProperty("hasPageLang", hasPageLang);
                jsonArray.add(obj);
            }

            this.bean.put("result", true);
            this.bean.put("listPageLang", jsonArray.toString());
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            this.bean.put("result", false);
            message = e.getMessage();
            this.bean.put("message", message);
        }
        return SUCCESS;
    }

    public String validationGroupUser() throws Exception {
        try {
            boolean exceedMember = false;
            ConfluenceUser userLogin = AuthenticatedUserThreadLocal.get();
            List<String> groupOfUser = userAccessor.getGroupNames(userLogin);
            if (groupOfUser.isEmpty()) {
                this.bean.put("result", false);
                this.bean.put("exceedMember", exceedMember);
                return SUCCESS;
            }

            TranslationGroupInfoService translationGroupInfoService = TranslationMemoryToolFactory.getTranslationGroupInfoService();
            GroupInfo groupInfo = translationGroupInfoService.get();
            if (groupInfo == null) {
                this.bean.put("result", false);
                this.bean.put("exceedMember", exceedMember);
                return SUCCESS;
            }

            String groupNameSelected = groupInfo.getGroupName();
            if (!groupOfUser.contains(groupNameSelected)) {
                this.bean.put("result", false);
                this.bean.put("exceedMember", exceedMember);
                return SUCCESS;
            }

            Group group = userAccessor.getGroup(groupNameSelected);
            Iterable<ConfluenceUser> listMember = userAccessor.getMembers(group);
            int numberOfMembers = Iterables.size(listMember);
            if (numberOfMembers <= Constants.LIMIT_MEMBERS) {
                this.bean.put("exceedMember", exceedMember);
            } else {
                exceedMember = true;
                this.bean.put("exceedMember", exceedMember);
            }
            this.bean.put("result", true);
            return SUCCESS;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.bean.put("result", false);
            this.bean.put("exceedMember", false);
            return SUCCESS;
        }
    }

    public String validationTranslation() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            if (pageId == 0) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
                return ERROR;
            }
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            Page page = pageManager.getPage(pageId);
            if (page == null) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.language.page.not.found"));
                return ERROR;
            }
            TranslationLabelTemplateService labelTemplateService = TranslationMemoryToolFactory.getTranslationLabelTemplateService();
            List<Label> labels = page.getLabels();
            int count = 0;
            for (Label label : labels) {
                String labelName = label.getDisplayTitle();
                List<LabelTemplate> labelTemplates = labelTemplateService.getByLabel(labelName);
                if (!labelTemplates.isEmpty()) {
                    count++;
                }
            }
            if (count == 1) {
                this.bean.put("result", true);
                return SUCCESS;
            }
            if (count > 1) {
                this.bean.put("result", false);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.message.only.set.one.label.template"));
                return SUCCESS;
            }
            this.bean.put("result", false);
            this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.message.no.has.label.template.setting"));
            return SUCCESS;
        } catch (Exception ex) {
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            this.bean.put("result", false);
            return SUCCESS;
        }
    }

    public String checkPageLanguage() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);

            if (pageId == 0) {
                this.bean.put("result", false);
                return ERROR;
            }

            TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
            TranslationJob trans = translationJobService.getOneByToPageId(pageId);
            if (trans != null) {
                this.bean.put("result", true);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.message.is.page.lang"));
                this.bean.put("isPageLang", true);
                return SUCCESS;
            }

            trans = translationJobService.getOneByFromPageId(pageId);
            if (trans != null) {
                this.bean.put("result", true);
                this.bean.put("message", getText("kod.plugins.kodtranslationdirectory.translation.message.is.page.base.lang"));
                this.bean.put("isPageBaseLang", true);
                return SUCCESS;
            }

            this.bean.put("result", false);
            return SUCCESS;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            this.bean.put("result", false);
            message = e.getMessage();
            this.bean.put("message", message);
            return SUCCESS;
        }
    }

    public String getListPageSyncRecapture() throws Exception {
        try {
            ActionContext context = ActionContext.getContext();
            Long pageId = ActionContextHelper.getFirstParameterValueAsLong(context, "pageId", 0);
            TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
            TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
            PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
            List<TranslationJob> translationJobs = new ArrayList<>();
            TranslationJob transJob = translationJobService.getOneByToPageId(pageId);
            if (transJob != null) {
                translationJobs.add(transJob);
            }
            translationJobs.addAll(translationJobService.getByTranslationJobByFromPageId(pageId));
            if (translationJobs.isEmpty()) {
                this.bean.put("result", false);
                this.bean.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.message.not.page.sync"));
                return SUCCESS;
            }
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            StringBuilder stringBuilder = new StringBuilder();
            for (TranslationJob translationJob : translationJobs) {
                String langTarget = translationJob.getToLanguageKey();
                stringBuilder.append("<tr>");
                stringBuilder.append("<td>");
                stringBuilder.append("<input class=\"checkbox page-sync-recapture-selected\" type=\"checkbox\" data-lang-key-target=\"");
                stringBuilder.append(langTarget);
                stringBuilder.append("\" data-lang-key-source=\"");
                stringBuilder.append(translationJob.getFromLanguageKey());
                stringBuilder.append("\">");
                stringBuilder.append("</td>");
                stringBuilder.append("</td>");
                stringBuilder.append("<td>");
                LanguageList fromLanguage = translationLanguageListService.getByLangKey(translationJob.getFromLanguageKey());
                stringBuilder.append(fromLanguage.getLanguageDisplay());
                stringBuilder.append(" > ");
                LanguageList languageList = translationLanguageListService.getByLangKey(langTarget);
                stringBuilder.append(languageList.getLanguageDisplay());
                stringBuilder.append("</td>");
                stringBuilder.append("<td>");
                Long pageIdTarget = translationJob.getToPageId();
                if (pageIdTarget == null) {
                    stringBuilder.append("</td>");
                    continue;
                }

                Page pageTarget = pageManager.getPage(pageIdTarget);
                if (pageTarget == null) {
                    stringBuilder.append("</td>");
                    continue;
                }

                stringBuilder.append("<a href=\"");
                stringBuilder.append(baseUrl);
                stringBuilder.append("/pages/viewpage.action?pageId=");
                stringBuilder.append(pageTarget.getIdAsString());
                stringBuilder.append("\">");
                stringBuilder.append(pageTarget.getTitle());
                stringBuilder.append("</a>");
                stringBuilder.append("</td>");
                stringBuilder.append("</tr>");
            }

            if (stringBuilder.toString().isEmpty()) {
                this.bean.put("result", false);
                this.bean.put("message", this.getText("kod.plugins.kodtranslationdirectory.translation.message.not.page.sync"));
                return SUCCESS;
            }

            this.bean.put("result", true);
            this.bean.put("html", stringBuilder.toString());
            return SUCCESS;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            this.bean.put("result", false);
            message = e.getMessage();
            this.bean.put("message", message);
            return SUCCESS;
        }
    }

    public String doDownload() throws Exception {
        ActionContext context = ActionContext.getContext();
        Object object = context.getParameters().get("uri");
        if (object == null) {
            return null;
        }
        String uri = null;
        if (object instanceof String[] && ((String[]) object).length != 0) {
            uri = ((String[]) object)[0];
        } else if (object instanceof String) {
            uri = (String) object;
        }

        uri = URLDecoder.decode(uri, "UTF-8");
        uri = uri.replace("\\", "/");
        File file = new File(uri);
        byte[] bytes = Files.readAllBytes(file.toPath());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        HttpServletResponse response = ServletActionContext.getResponse();
        response.setContentType("text/txt");
        String fileName = URLEncoder.encode(file.getName(), "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-Disposition", "attachment; filename*=UTF-8''" + fileName);

        ServletOutputStream outPut = response.getOutputStream();
        out.write(bytes);
        out.writeTo(outPut);
        outPut.flush();
        outPut.close();
        return null;
    }
}
