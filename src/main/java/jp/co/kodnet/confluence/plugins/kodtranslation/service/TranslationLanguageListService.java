package jp.co.kodnet.confluence.plugins.kodtranslation.service;

import com.atlassian.activeobjects.tx.Transactional;
import java.util.List;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;

@Transactional
public interface TranslationLanguageListService {
    
    List<LanguageList> getAll();
    
    LanguageList get();
    
    LanguageList getOne(String translationToolId);
    
    LanguageList getByLangKey(String langKey);
    
    List<LanguageList> getByTransToolId(String translationToolId);
      
    LanguageList add(
            String translationToolId,
            String languageKey,
            String languageDisplay
            );
    
    LanguageList update(
            String translationToolId,
            String languageKey,
            String languageDisplay
            );
    
    void delete(String translationToolId);
    
    void deleteAll();  
}
