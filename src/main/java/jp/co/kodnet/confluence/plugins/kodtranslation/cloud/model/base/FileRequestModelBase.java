package jp.co.kodnet.confluence.plugins.kodtranslation.cloud.model.base;

/**
 *
 * @author anhlq
 */
public class FileRequestModelBase extends RequestModelBase {

    private String filename;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
