package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.core.util.ProgressMeter;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.ApiLoginInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.LanguageList;
import jp.co.kodnet.confluence.plugins.kodtranslation.entites.TranslationJob;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.JobConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.WorkflowStepConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.CopyJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.DeleteTargetResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetFileCompleteResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetWorkflowStepResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.JobBilingualResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationApiLoginInfoService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationLanguageListService;
import jp.co.kodnet.confluence.plugins.kodtranslation.xml.DocumentUtil;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @param <T>
 */
public class TranslationMemsourceSyncCallBack<T> implements TranslationMemsourceCallback<T> {

    protected ProgressMeter progress;
    protected Map<String, Map<String, String>> map = new HashMap<>();
    protected JsonObject resultList = new JsonObject();
    protected Map<String, Object> mapInput;
    protected TranslationApiLoginInfoService translationApiLoginInfoService = TranslationMemoryToolFactory.getTranslationApiLoginInfoService();
    protected SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
    protected PageManager pageManager = TranslationMemoryToolFactory.getPageManager();
    protected TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
    protected TranslationLanguageListService translationLanguageListService = TranslationMemoryToolFactory.getTranslationLanguageListService();
    protected SettingsManager settingsManager = TranslationMemoryToolFactory.getSettingsManager();
    protected I18NBean i18n = TranslationMemoryToolFactory.getI18NBean();
    protected String jobTitle = "";
    protected TransactionTemplate transactionTemplate = TranslationMemoryToolFactory.getTransactionTemplate();
    protected String paramText = "";
    protected String messageError = "";

    public TranslationMemsourceSyncCallBack(Map<String, Object> mapInput) {
        this.mapInput = mapInput;
    }

    @Override
    public void setProgress(ProgressMeter progress) {
        this.progress = progress;
    }

    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public T doInTransaction() {
        try {
            progress.setStatus(Constants.WAITTING_PREFIX);
            Long fromPageId = (Long) mapInput.get("pageId");
            List<String> langKeys = (List) mapInput.get("langKeys");
            String spaceKey = (String) mapInput.get("spaceKey");
            String projectUidSource = (String) mapInput.get("projectUid");
            String jobIdSource = (String) mapInput.get("jobId");

            //Get token
            ApiLoginInfo apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
            if (apiLoginInfo == null) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
                return null;
            }

            Date dateExpires = apiLoginInfo.getDateExpires();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            if (dateExpires != null && (sdf.parse(sdf.format(dateExpires)).before(sdf.parse(sdf.format(new Date()))) || sdf.parse(sdf.format(dateExpires)).equals(sdf.parse(sdf.format(new Date()))))) {
                CommonFunction.doLoginMemsource(apiLoginInfo.getUserName(), apiLoginInfo.getPassword(), String.valueOf(Constants.MEMSOURCE_ID), "", "", "");
            }
            apiLoginInfo = translationApiLoginInfoService.getOne(String.valueOf(Constants.MEMSOURCE_ID));
            String token = apiLoginInfo.getLoggedToken();
            if (token == null) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.no.login.message"));
                return null;
            }

            WorkflowStepConnector workflowStepConnector = new WorkflowStepConnector(token);
            GetWorkflowStepResponse lstWorkflowStep = workflowStepConnector.get();
            if (!CommonFunction.isNullOrEmpty(lstWorkflowStep.getErrorCode())) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + lstWorkflowStep.getErrorCode());
                return null;
            }
            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
            String url = "";
            if (projectUidSource != null && jobIdSource != null) {
                JobConnector job = new JobConnector(token);
                Long toPageId = fromPageId;
                Long newPageId = updateSourcePageLanguage(job, spaceKey, fromPageId, toPageId, projectUidSource, jobIdSource);
                if (newPageId != null) {
                    url = baseUrl + "/pages/viewpage.action?pageId=" + String.valueOf(newPageId);
                }
                this.progress.setPercentage(99);
                MemoryLogUtil.outputLog(paramText);
                this.progress.setStatus(this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.message.sync.page.success") + Constants.STRING_DISTINS_SYNC + url);
                return null;
            }

            // Check job in DB not translated
            List<TranslationJob> listJobNotTranslated = new ArrayList<>();
            for (String langKey : langKeys) {
                String[] arrLang = langKey.split(Constants.SPLIT_STRING_CEATE_LANG);
                String fromLangKey = arrLang[0];
                String toLangKey = arrLang[1];
                TranslationJob translationJob = translationJobService.getOneByLangAndFromPageId(fromLangKey, toLangKey, fromPageId);
                if (translationJob != null) {
                    listJobNotTranslated.add(translationJob);
                }
            }

            TranslationJob translationJob = translationJobService.getOneByToPageId(fromPageId);
            if (translationJob != null) {
                listJobNotTranslated.add(translationJob);
            }

            if (listJobNotTranslated.isEmpty()) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.INFO_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.message.not.page.sync"));
                return null;
            }

            // Get memsource from api => sync
            JobConnector jobConnector = new JobConnector(token);
            List<TranslationJob> listJobSynchronize = this.getListJobSync(jobConnector, listJobNotTranslated);
            if (listJobSynchronize.isEmpty()) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.INFO_PREFIX + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.message.not.page.sync"));
                return null;
            }

            createPageLangChild(jobConnector, listJobSynchronize, spaceKey);

            if (!this.messageError.isEmpty()) {
                this.progress.setPercentage(99);
                this.progress.setStatus(Constants.ERROR_PREFIX + this.messageError);
                return null;
            }

            this.progress.setPercentage(99);
            MemoryLogUtil.outputLog(paramText);
            this.progress.setStatus(this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.message.sync.page.success") + Constants.STRING_DISTINS_SYNC);
        } catch (Exception ex) {
            progress.setStatus(Constants.ERROR_PREFIX + ex.getMessage());
            MemoryLogUtil.outputLog(ex.getMessage());
        }
        return null;
    }

    //Get memsource from api => sync
    private List<TranslationJob> getListJobSync(JobConnector jobConnector, List<TranslationJob> translationJobs) throws Exception {
        List<TranslationJob> listJobSync = new ArrayList<>();
        MemoryLogUtil.outputLog("START GET LIST JOB FROM API ");
        for (TranslationJob translationJob : translationJobs) {
            try {
                String projectUid = translationJob.getApiProjectId();
                MemoryLogUtil.outputLog("PROJECT ID : " + projectUid);
                if (projectUid == null || ("").equals(projectUid)) {
                    continue;
                }

                String jobId = translationJob.getTranslationJobId();
                MemoryLogUtil.outputLog("JOB ID : " + jobId);
                if (jobId == null || ("").equals(jobId)) {
                    continue;
                }

                MemoryLogUtil.outputLog("GET JOB CONNECTOR : " + jobId);
                GetJobResponse jobResponse = jobConnector.get(projectUid, jobId);
                if (!CommonFunction.isNullOrEmpty(jobResponse.getErrorCode())) {
                    continue;
                }

                String status = jobResponse.getStatus();
                MemoryLogUtil.outputLog("GET STATUS : " + status);
                if (!(StringUtils.equalsIgnoreCase(status, Constants.MEMSOURCE_STATUS_DELIVERED))) {
                    continue;
                }

                listJobSync.add(translationJob);
            } catch (Exception e) {
                this.messageError = e.getMessage();
                MemoryLogUtil.outputLogException(this.getClass(), e);
            }
        }
        MemoryLogUtil.outputLog("END GET LIST JOB FROM API ");
        return listJobSync;
    }

    // Check page lang is exists
    private void createPageLangChild(JobConnector jobConnector, List<TranslationJob> translationJobs, String spaceKey) {
        try {
            MemoryLogUtil.outputLog("START CREATE PAGE CHILD : ");
            List<Long> listIdPageFrom = new ArrayList<>();
            List<TranslationJob> allTransJob = translationJobService.getAll();
            for (TranslationJob trans : allTransJob) {
                Long id = trans.getFromPageId();
                listIdPageFrom.add(id);
            }
            for (int i = 0; i < translationJobs.size(); i++) {
                try {
                    TranslationJob translationJob = translationJobs.get(i);
                    String projectUid = translationJob.getApiProjectId();
                    MemoryLogUtil.outputLog("PROJECT ID : " + projectUid);
                    if (projectUid == null || ("").equals(projectUid)) {
                        continue;
                    }

                    String jobId = translationJob.getTranslationJobId();
                    MemoryLogUtil.outputLog("JOB ID : " + jobId);
                    if (jobId == null || ("").equals(jobId)) {
                        continue;
                    }

                    Long toPageId = translationJob.getToPageId();
                    Long fromPageId = translationJob.getFromPageId();
                    Long newPageId = this.createPageTask(jobConnector, spaceKey, fromPageId, toPageId, projectUid, jobId, translationJob.getFromLanguageKey(), translationJob.getToLanguageKey(), listIdPageFrom);
                    if (newPageId == null) {
                        continue;
                    }

                    if (Objects.equals(newPageId, fromPageId)) {
                        continue;
                    }

                    toPageId = newPageId;
                    translationJobService.setToPageIdAndStatus(translationJob, toPageId, Constants.MEMSOURCE_STATUS_TRANSLATED);
                    LanguageList langName = translationLanguageListService.getByLangKey(translationJob.getToLanguageKey());
                    if (i < translationJobs.size() - 1) {
                        paramText = paramText + langName.getLanguageDisplay() + ",";
                    } else {
                        paramText = paramText + langName.getLanguageDisplay();
                    }
                } catch (Exception e) {
                    this.messageError = e.getMessage();
                    MemoryLogUtil.outputLogException(this.getClass(), e);
                }
            }
        } catch (Exception e) {
            this.messageError = e.getMessage();
            MemoryLogUtil.outputLogException(this.getClass(), e);
        }
    }

    private Long updateSourcePageLanguage(JobConnector jobConnector, String spaceKey, Long fromPageId, Long toPageId, String projectUid, String jobUid) throws Exception {
        TranslationJob translationJob = translationJobService.getByProjectId(fromPageId, projectUid, jobUid);
        LanguageList langName = translationLanguageListService.getByLangKey(translationJob.getFromLanguageKey());
        paramText = paramText + langName.getLanguageDisplay();
        return syncSourcePage(jobConnector, spaceKey, fromPageId, toPageId, translationJob.getFromLanguageKey(), projectUid, jobUid);
    }

    private Long createPageTask(JobConnector jobConnector, String spaceKey, Long fromPageId, Long toPageId, String projectUid, String jobUid, String fromLangKey, String toLangKey, List<Long> listIdPageFrom) throws Exception {
        ByteArrayOutputStream data = new ByteArrayOutputStream();
        GetFileCompleteResponse fileCompleteResponse = jobConnector.getFileComplete(projectUid, jobUid, data);
        if (!CommonFunction.isNullOrEmpty(fileCompleteResponse.getErrorCode())) {
            MemoryLogUtil.outputLog("Exception update Content Entity Object : " + fileCompleteResponse.getErrorDescription());
            this.messageError = fileCompleteResponse.getErrorDescription() + "<br>" + this.i18n.getText("kod.plugins.kodtranslationdirectory.translation.message.sync.error.qa");
            return null;
        }

        String xmlString = new String(data.toByteArray(), "UTF-8");
        MemoryLogUtil.outputTime(true, "MemoryToolSavePageCallback", "createPageTask", "MemoryToolSavePageCallBack");
        TranslationMemsourceSavePageCallBack<Long> task = new TranslationMemsourceSavePageCallBack<Long>(spaceKey, fromPageId, toPageId, fromLangKey, toLangKey, xmlString, listIdPageFrom, false);
        Long newPageId = transactionTemplate.execute(task);
        task = null;
        MemoryLogUtil.outputTime(false, "MemoryToolSavePageParentCallback", "createPageTask", "MemoryToolSavePageCallBack");
        return newPageId;
    }

    private Long syncSourcePage(JobConnector jobConnector, String spaceKey, Long fromPageId, Long toPageId, String langKey, String projectUid, String jobUid) throws Exception {
        try {
            ByteArrayOutputStream dataTarget = new ByteArrayOutputStream();
            JobBilingualResponse fileTarget = jobConnector.getFileBiligualComplete(projectUid, jobUid, dataTarget);
            DeleteTargetResponse deleteTargetResponse = jobConnector.deleteFileTargetComplete(projectUid, jobUid);
            CopyJobResponse copySourceToTarget = jobConnector.copySourceToTargetFile(projectUid, jobUid);
            ByteArrayOutputStream dataSource = new ByteArrayOutputStream();
            GetFileCompleteResponse fileSource = jobConnector.getFileComplete(projectUid, jobUid, dataSource);
            if (!CommonFunction.isNullOrEmpty(fileSource.getErrorCode())) {
                MemoryLogUtil.outputLog("Exception update Content Entity Object : " + fileSource.getErrorDescription());
                this.messageError = fileSource.getErrorDescription();
                return null;
            }
            String xmlString = new String(dataSource.toByteArray(), "UTF-8");

            deleteTargetResponse = jobConnector.deleteFileTargetComplete(projectUid, jobUid);
            ByteArrayInputStream in = new ByteArrayInputStream(dataTarget.toByteArray());
            //FileInputStream inputStream = new FileInputStream(dataTarget.toByteArray(), "UTF-8");
            jobConnector.uploadBiligualFile(in);

            MemoryLogUtil.outputTime(true, "MemoryToolSavePageCallback", "syncSourcePage", "MemoryToolSavePageCallBack");
            TranslationMemsourceSavePageCallBack<Long> task = new TranslationMemsourceSavePageCallBack<Long>(spaceKey, fromPageId, toPageId, langKey, langKey, xmlString, new ArrayList<>(), true);
            Long newPageId = transactionTemplate.execute(task);
            MemoryLogUtil.outputTime(false, "MemoryToolSavePageCallback", "syncSourcePage", "MemoryToolSavePageCallBack");
            return newPageId;
        } catch (Exception e) {
            MemoryLogUtil.outputLogException(this.getClass(), e);
            this.messageError = e.getMessage();
            return null;
        }
    }
}
