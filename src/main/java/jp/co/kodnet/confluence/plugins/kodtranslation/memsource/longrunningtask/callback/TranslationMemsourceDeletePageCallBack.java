package jp.co.kodnet.confluence.plugins.kodtranslation.memsource.longrunningtask.callback;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.CommonFunction;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.Constants;
import jp.co.kodnet.confluence.plugins.kodtranslation.common.MemoryLogUtil;
import jp.co.kodnet.confluence.plugins.kodtranslation.factory.TranslationMemoryToolFactory;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.apiconnector.JobConnector;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.GetJobListResponse;
import jp.co.kodnet.confluence.plugins.kodtranslation.memsource.cloud.model.submodel.JobInfo;
import jp.co.kodnet.confluence.plugins.kodtranslation.service.TranslationJobService;

public class TranslationMemsourceDeletePageCallBack<T> implements TransactionCallback<T> {

    protected String token;
    protected String projectUid;
    protected String[] workflow;
    protected String fromLangKey;
    protected String toLangKey;
    protected String spaceKey;
    protected Long fromPageId;
    protected Long toPageId;
    protected PageManager pageManager = TranslationMemoryToolFactory.getPageManager();   
    protected SpaceManager spaceManager = TranslationMemoryToolFactory.getSpaceManager();
    protected TranslationJobService translationJobService = TranslationMemoryToolFactory.getTranslationJobService();
        
    public TranslationMemsourceDeletePageCallBack(String token, String projectUid, String[] workflow,
            String fromLangKey, String toLangKey, String spaceKey, Long fromPageId, Long toPageId) {
        this.token = token;
        this.projectUid = projectUid;
        this.spaceKey = spaceKey;
        this.fromPageId = fromPageId;
        this.fromLangKey = fromLangKey;
        this.toLangKey = toLangKey;
        this.workflow = workflow;
        this.toPageId = toPageId;
    }

    @Override
    public T doInTransaction() {
        try {
            MemoryLogUtil.outputTime(true, "MemoryToolCopyPageCallBack", "doInTransaction");
            JobConnector jobConnector = new JobConnector(token);
            Boolean result  = this.deleteJobMemsource(jobConnector,  this.projectUid, this.workflow,
            this.fromLangKey, this.toLangKey, this.spaceKey, this.fromPageId, this.toPageId);
            return (T) result;
        } catch (Exception ex) {
            MemoryLogUtil.outputLog("Delete ERROR PAGE: (" + fromPageId + ") ");
            MemoryLogUtil.outputLogException(this.getClass(), ex);
            throw new RuntimeException(ex);
        }
    }

    private Boolean deleteJobMemsource(JobConnector jobConnector, String projectUid, String[] workflow, String fromLangKey, String toLangKey, String spaceKey, Long fromPageId, Long toPageId) {
        try {
            //delete page confluence
            if (toPageId != null) {
                Page toPageLang = pageManager.getPage(toPageId);
                if (toPageLang != null && !toPageLang.isDeleted()) {
                    Page parrent = toPageLang.getParent();
                    parrent.removeChild(toPageLang);
                    pageManager.removeContentEntity(toPageLang);
                    pageManager.saveContentEntity(parrent, null);
                }
            }
            //Delete job created from database
            translationJobService.deleteByFromPageId(fromLangKey, toLangKey, spaceKey, fromPageId);
            GetJobListResponse getJobListResponse = jobConnector.getJobListResponse(projectUid, 0, workflow.length);
            if (!CommonFunction.isNullOrEmpty(getJobListResponse.getErrorCode())) {
                return false;
            }
            for (JobInfo jobPart : getJobListResponse.getContent()) {
                jobConnector.setStatus(projectUid, jobPart.getUid(), Constants.MEMSOURCE_STATUS_CANCELLED);
            }
            return true;
        } catch (Exception e) {
            MemoryLogUtil.outputLog(e.getMessage());
            return false;
        }
    }
}
