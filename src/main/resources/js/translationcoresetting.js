/* global AJS */

$(document).ready(function () {

    //使用言語 by 翻訳メモリツール
    getInfoTranslationSetting();
    renderTemplateMemsource("#memsource-template-content", false);

    $(document).on('click', '#select-languages', function () {
        $('#api-language-list :selected').map(function (i, el) {
            var langFrom = $(el).clone();
            var langKey = langFrom.attr('key');
            var langText = langFrom.text();
            var index = langFrom.attr('displayIndex');
            var insertedFlg = false;
            $('#selected-language-list > option').each(function () {
                var curIndex = $(this).attr('displayIndex');
                if (parseInt(index) < parseInt(curIndex)) {
                    langFrom.insertBefore(this);
                    insertedFlg = true;
                    return false;
                }
            });
            if (!insertedFlg) {
                $('#selected-language-list').append($('<option>', {
                    displayIndex: index,
                    key: langKey,
                    text: langText
                }));
            }
            $(el).remove();
        });
    });

    $(document).on('click', '#cancel-select-language', function () {
        $('#selected-language-list :selected').map(function (i, el) {
            var langTo = $(el).clone();
            var langKey = langTo.attr('key');
            var langText = langTo.text();
            var index = langTo.attr('displayIndex');
            var insertedFlg = false;
            $('#api-language-list > option').each(function () {
                var curIndex = $(this).attr('displayIndex');
                if (parseInt(index) < parseInt(curIndex)) {
                    langTo.insertBefore(this);
                    insertedFlg = true;
                    return false;
                }
            });
            if (!insertedFlg) {
                $('#api-language-list').append($('<option>', {
                    displayIndex: index,
                    key: langKey,
                    text: langText
                }));
            }
            $(el).remove();
        });
    });

    $(document).on('click', '#save-selected-language', function () {
        var keys = [];
        var names = [];
        var indexs = [];
        $('#selected-language-list option').map(function (i, el) {
            keys[i] = $(el).attr('key');
            names[i] = $(el).text();
            indexs[i] = $(el).attr('displayIndex');
        });

        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            async: true,
            url: AJS.Meta.get('context-path') + '/translationdirectory/saveSelectedLanguage.action',
            data: {keyLanguage: keys, nameLanguage: names, displayIndex: indexs},
            error: function (error) {
                console.log('Error: ' + error);
            }
        }).done(function (data) {
            if (data.result) {
                showMessageCore(1, data.message);
            } else {
                showMessageCore(2, data.message);
            }
        });
    });
    
    $(document).on("click", "#translation-save-group-setting", function(){
        var groupSelected = $(".group-wiki-setting-selected option:selected");
        if(groupSelected.length > 0){
           var groupName = $(groupSelected).attr("data-name-group");
           saveGroupInfoCoreSetting(groupName);
        }
    });

    $(document).on('click', '#save-inline-tag', function () {
        var value = $('#inline-tag-input').val();
        $.ajax({
            type: 'post',
            url: AJS.Meta.get('context-path') + '/translationdirectory/dosaveinlinetag.action',
            dataType: 'json',
            data: {'inline-tag-input': value},
            success: function (data) {
                executeTranslationProcessClose();
                if (data.result) {
                    showMessageCore(1, data.message);
                } else {
                    showMessageCore(2, data.message);
                }
            },
            error: function () {
                executeTranslationProcessClose();
            }
        });
    });

    $(document).on('click', '#save-not-inline-tag', function () {
        var value = $('#not-inline-tag-input').val();
        $.ajax({
            type: 'post',
            url: AJS.Meta.get('context-path') + '/translationdirectory/dosavenotinlinetag.action',
            dataType: 'json',
            data: {'not-inline-tag-input': value},
            success: function (data) {
                executeTranslationProcessClose();
                if (data.result) {
                    showMessageCore(1, data.message);
                } else {
                    showMessageCore(2, data.message);
                }
            },
            error: function () {
                executeTranslationProcessClose();
            }
        });
    });

    // Memsourceで翻訳可能インライン要素
    function getInlineTag() {
        $.ajax({
            type: 'get',
            url: AJS.Meta.get('context-path') + '/translationdirectory/getinlinetag.action',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.result) {
                    $('#inline-tag-panel-content').empty();
                    var html = Confluence.Templates.TranslationDirectory.inlineTagSetting({value: data.value});
                    $('#inline-tag-panel-content').append(html);
                }
            },
            error: function () {;
            }
        });
    }
    // Memsourceで翻訳不要なインライン要素の指定
    function getNotInlineTag() {
        $.ajax({
            type: 'get',
            url: AJS.Meta.get('context-path') + '/translationdirectory/getnotinlinetag.action',
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.result) {
                    $('#not-inline-tag-panel-content').empty();
                    var html = Confluence.Templates.TranslationDirectory.notInlineTagSetting({value: data.value});
                    $('#not-inline-tag-panel-content').append(html);
                }
            },
            error: function () {
            }
        });
    }
    
    function saveGroupInfoCoreSetting(groupName){
         jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: AJS.Meta.get('context-path') + '/translationdirectory/savegroupinfoSetting.action',
            data: {groupName: groupName},
            error: function (error) {
                showMessageCore(2, error);
                executeTranslationProcessClose();
                console.log('Error: ' + error);
            }
        }).done(function (data) {
            if (data.result) {
                renderTemplateMemsource();
                showMessageCore(1, data.message);
            } else {
                showMessageCore(2, data.message);
            }
             executeTranslationProcessClose();
        });
    }
    
    function getInfoTranslationSetting() {
        executeProcessDisplay();
        var urlRemainLang = AJS.Meta.get('base-url') + '/translationdirectory/rendertranslationinfo.action';
        $.ajax({
            type: 'get',
            url: urlRemainLang,
            dataType: 'json',
            success: function (data) {
                $('#page-panel-favorite-lang-content').empty();
                var langFroms = JSON.parse(data.remainLang);
                var langSelects = JSON.parse(data.selectedLang);
                if (langSelects.length === 0) {
                    $('#page-panel-favorite-lang-content').append(renderFavoriteLanguageNullSelected(langFroms));
                } else {
                    $('#page-panel-favorite-lang-content').append(renderFavoriteLanguage(langFroms, langSelects));
                }
                var groupContent = $(document).find("#memsource-translation-management-groups");
                $(groupContent).empty();
                $(groupContent).append(data.groups);
                
                getInlineTag();
                getNotInlineTag();
                
                executeTranslationProcessClose();
            },
            error: function (e) {
                executeTranslationProcessClose();
                console.log(e);
            }
        });
    }

    function renderFavoriteLanguage(langFroms, langSelects) {
        return  Confluence.Templates.TranslationDirectory.languagePanel({langFroms: langFroms, langSelects: langSelects});
    }

    function renderFavoriteLanguageNullSelected(langFroms) {
        return  Confluence.Templates.TranslationDirectory.languagePanelNullFavoriteLanguage({langFroms: langFroms});
    }

    function executeProcessDisplay() {
        var strProcessing = AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.excute.processing");
        var blanket = '<div class="kod-blanket aui-blanket" tabindex="0" aria-hidden="false" style="z-index: 2980;"></div>';
        $('body').append(blanket);
        var process = '<div id="execute-progress-bar" class="aui-progress-indicator">';
        process += '<span><b id="execute-progress-title">' + strProcessing + '</b></span><span id="execute-progress-message"></span>';
        process += '<span class="aui-progress-indicator-value"></span></div>';
        $('body').append(process);
    }

    function executeTranslationProcessClose() {
        $('body').find('.kod-blanket').remove();
        $('body').find('#execute-progress-bar').remove();
    }
});