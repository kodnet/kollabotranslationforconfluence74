/* global AJS */

$(document).ready(function () {
    var NONE_LANG = "NONE_LANG";
    $(document).on("click", "#add-template-memsource-setting", function () {
        var template = $(this).attr("data-template-memsource");
        var languages = $(this).attr("data-language-memsource");
        var trTemplate = '<tr>'
                + '<td>'
                + '<span class="aui-icon aui-icon-small aui-iconfont-cross remove-template-setting" style="color: red;cursor: pointer;">'
                + '</td>'
                + '<td>'
                + '<input class="text label-setting" type="text" value="">'
                + '</td>'
                + '<td>'
                + '<select class=\"select lang-key-setting-selected\">';
        if (typeof languages === 'undefined' || languages === "") {
        } else {
            var json = JSON.parse(languages);
            for (var key in json) {
                trTemplate += '<option data-lang-key="'
                        + key;
                        if(key === NONE_LANG){
                           trTemplate += '" disabled="disabled';
                        }
                        trTemplate += '">'
                        + json[key]
                        + '</option>';
            }
        }
        trTemplate += '</select>'
                + '</td>'
                + '<td>'
                + '<select class=\"select template-setting-selected\">';
        if (typeof template === 'undefined' || template === "") {
        } else {
            var json = JSON.parse(template);
            for (var key in json) {
                trTemplate += '<option data-api-uid="'
                        + key
                        + '">'
                        + json[key]
                        + '</option>';
            }
        }
        trTemplate += '</select>'
                + '</td>'
                + '</tr>';
        var contentCoreSetting = $(document).find("#memsource-template-content");
        var contentDialogSetting = $(document).find("#dialog-translation-template-setting #content-template-setting")
        if (contentCoreSetting.length > 0) {
            $(contentCoreSetting).find("table tbody").append(trTemplate);
        }
        if (contentDialogSetting.length > 0) {
            $(contentDialogSetting).find("table tbody").append(trTemplate);
        }
    });

    $(document).on("click", "#memsource-template-content table tbody tr td span.remove-template-setting", function () {
        $(this).closest("tr").remove();
    });

    $(document).on("click", "#dialog-translation-template-setting #content-template-setting table tbody tr td span.remove-template-setting", function () {
        $(this).closest("tr").remove();
    });

    $(document).on("click", "#cancel-change-template-memsource-setting", function () {
        var contentCoreSetting = $(document).find("#memsource-template-content");
        var contentDialogSetting = $(document).find("#dialog-translation-template-setting #content-template-setting")
        if (contentCoreSetting.length > 0) {
            renderTemplateMemsource("#memsource-template-content", false);
        }
        if (contentDialogSetting.length > 0) {
            closeDialog();
        }
    });

    $(document).on("click", "#save-template-by-label-setting", function () {
        executeProcessDisplay();
        var dataPost = [];
        var dataValidate = {};
        var contentCoreSetting = $(document).find("#memsource-template-content");
        var contentDialogSetting = $(document).find("#dialog-translation-template-setting #content-template-setting");
        var tr;
        if (contentCoreSetting.length > 0) {
            tr = $(document).find("#memsource-template-content table tbody tr");
        }
        if (contentDialogSetting.length > 0) {
            tr = $(document).find("#dialog-translation-template-setting #content-template-setting table tbody tr");
        }
        if (typeof tr == 'undefined') {
            executeTranslationProcessClose();
            closeDialog();
            return;
        }
        var checkValidate = true;
        $(tr).each(function (e) {
            var labelSetting = $(this).find(".label-setting");
            if (labelSetting.length == 0) {
                return;
            }
            var templateSetting = $(this).find(".template-setting-selected option:selected");
            if (templateSetting.length == 0) {
                templateSetting = $(this).find(".template-setting-selected option:first");
                if (templateSetting.length == 0) {
                    return;
                }
            }
            var label = labelSetting.val();
            if (label == "") {
                return;
            }
            var langSetting = $(this).find(".lang-key-setting-selected option:selected");
            var langKey = langSetting.attr("data-lang-key");
            $(labelSetting).css('color', '');
            if (typeof dataValidate[label + "-" + langKey] !== 'undefined' &&  dataValidate[label + "-" + langKey] === langKey) {
                showMessageCore(2, AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.core.settings.save.data.template.error.same.label"));
                $(labelSetting).css('color', 'red');
               $(this).find(".lang-key-setting-selected").css('color', 'red');
                checkValidate = false;
                return false;
            }
            var templateId = templateSetting.attr("data-api-uid");
            var templateName = templateSetting.text();
            dataPost.push({label: label, langKey: langKey, templateId: templateId, templateName: templateName});
            dataValidate[label + "-" + langKey] = langKey;
        });

        if (checkValidate == false) {
            executeTranslationProcessClose();
            return;
        }

        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: AJS.Meta.get('context-path') + '/translationdirectory/saveTemplateLabelSetting.action',
            data: {listLabelTemplate: JSON.stringify(dataPost)},
            error: function (error) {
                closeDialog();
                showMessageCore(2, error);
                executeTranslationProcessClose();
                console.log('Error: ' + error);
            }
        }).done(function (data) {
            if (data.result) {
                if (contentCoreSetting.length > 0) {
                    renderTemplateMemsource("#memsource-template-content", false);
                }
                if (contentDialogSetting.length > 0) {
                    closeDialog();
                }
                showMessageCore(1, data.message);
            } else {
                closeDialog();
                showMessageCore(2, data.message);
            }
            executeTranslationProcessClose();
        });
    });
    
    $(document).on("click", "#close-dialog-validate-message", function(){
        closeDialog();
    });
});


function renderTemplateMemsource(content, isDialog) {
    var urlApiTemplate = AJS.Meta.get('base-url') + '/translationdirectory/rendertemplatememsource.action';
    $.ajax({
        type: 'get',
        url: urlApiTemplate,
        dataType: 'json',
        error: function (e) {
            console.log(e);
            executeTranslationProcessClose();
        }
    }).done(function (data) {
        var templateContent = $(document).find(content);
        $(templateContent).empty();
        if (data.result) {
            var table = Confluence.Templates.TranslationDirectory.templateMemsourceContent();
            if (templateContent.length > 0) {
                $(templateContent).html(table);
                $(templateContent).find("table tbody").append(data.html);
                $(templateContent).find("#add-template-memsource-setting").attr("data-template-memsource", data.template);
                $(templateContent).find("#add-template-memsource-setting").attr("data-language-memsource", data.languages);
            }
        } else {
            var message = data.message;
            $(templateContent).append(message);
        }
        if (isDialog == true) {
            var buttons = $(content).find("div.aui-buttons");
            if (buttons.length > 0) {
                var textCloseButton = AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.select.button.dialog.close");
                var closeButton =  $(content).find("#cancel-change-template-memsource-setting");
                $(closeButton).removeClass("aui-button-primary");
                $(closeButton).text(textCloseButton);
                $(content).find("div.aui-buttons").css({"padding": "0px", "float": "none"});
                $("#dialog-translation-template-setting").find("footer div.aui-dialog2-footer-actions").append($(content).find("div.aui-buttons"));
                $(content).find("div.aui-buttons").remove();
            } else {
                $("#dialog-translation-template-setting").find("footer div.aui-dialog2-footer-actions div.aui-buttons").show();
            }
        AJS.dialog2("#dialog-translation-template-setting").show();
        }
        executeTranslationProcessClose();
    });
}

function validateLabelTemplate(pageId){
    var result = false;
    jQuery.ajax({
        type: 'get',
        async: false,
        url: AJS.Meta.get("context-path") + '/translationdirectory/dotranslationvalidate.action',
        data:{pageId: pageId},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (e) {
            executeTranslationProcessClose();
            showMessageCore(2, e.message);
            console.log(e);
        }
    }).done(function (data) {
        if(data.result){ 
            result = true;
        }else{
            result = false;
            var title = AJS.I18n.getText('kod.plugins.kodtranslationdirectory.translation.header.dialog.set.label.template');
            var html = Confluence.Templates.TranslationDialog.dialogMessage({title:title, message:data.message});
            $("#main").append(html);
            AJS.dialog2("#dialog-validate-message").show();
        }
        executeTranslationProcessClose();
    });
    return result;
}

function executeProcessDisplay() {
    var strProcessing = AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.excute.processing");
    var blanket = '<div class="kod-blanket aui-blanket" tabindex="0" aria-hidden="false" style="z-index: 4000;"></div>';
    $('body').append(blanket);
    var process = '<div id="execute-progress-bar" class="aui-progress-indicator">';
    process += '<span><b id="execute-progress-title">' + strProcessing + '</b></span><span id="execute-progress-message"></span>';
    process += '<span class="aui-progress-indicator-value"></span></div>';
    $('body').append(process);
}

function executeTranslationProcessClose() {
    $('body').find('.kod-blanket').remove();
    $('body').find('#execute-progress-bar').remove();
}

function showMessageCore(option, content, title) {
    /*
     * 1 : SUCCESS
     * 2 : ERROR
     * 3 : INFO
     */
    switch (option) {
        case 1:
            $(document).find('#aui-message-success').empty();
            if (title === undefined) {
                AJS.flag({
                    type: "success",
                    body: content
                });
            } else {
                AJS.flag({
                    type: "success",
                    body: content
                });
            }
            break;
        case 2:
            $(document).find('#aui-message-error').empty();
            if (title === undefined) {
                AJS.flag({
                    type: "error",
                    body: content
                });
            } else {
                AJS.flag({
                    type: "error",
                    body: content
                });
            }
            break;
        case 3:
            $(document).find('#aui-message-info').empty();
            if (title === undefined) {
                AJS.flag({
                    type: "info",
                    body: content
                });
            } else {
                AJS.flag({
                    type: "info",
                    body: content
                });
            }
            break;
    }
}


function closeDialog() {
    var dialogCreate = $(document).find("#dialog-select-base-language-and-target-language");
    if (dialogCreate.length > 0) {
        $(dialogCreate).remove();
    }
    var dialogConfirm = $(document).find("#dialog-confirm-override-project-memsource");
    if (dialogConfirm.length > 0) {
        $(dialogConfirm).remove();
    }
    var dialogLinkProject = $(document).find("#dialog-translation-memsource-project-list");
    if (dialogLinkProject.length > 0) {
        $(dialogLinkProject).remove();
    }
    var dialogListPageSyncRecapture = $(document).find("#dialog-translation-sync-recapture-list");
    if (dialogListPageSyncRecapture.length > 0) {
        $(dialogListPageSyncRecapture).remove();
    }
    var dialogTemplateSetting = $(document).find("#dialog-translation-template-setting");
    if (dialogTemplateSetting.length > 0) {
        $(dialogTemplateSetting).remove();
    }    
    var dialogMessage = $(document).find("#dialog-validate-message");
    if (dialogMessage.length > 0) {
        $(dialogMessage).remove();
    }
    var _blanket = $(document).find(".aui-blanket");
    if (_blanket.length > 0) {
        $(_blanket).remove();
    }
}