/* global Confluence, AJS, contextPath */
//Translationのstatus
var pageId = AJS.Meta.get('page-id');
var spaceKey = $('meta[name="confluence-space-key"]').attr("content");
var MODE_LINK_PROJECT_JOB = 0;
var MODE_SYNC_SOURCE_LANGUGE = 1;

$(document).ready(function () {
    checkPageIsBaseLang(pageId);
    checkValidationGroupUser(false);
    
    $(document).on("click", "#translation-directory-menu-hoyaku", function () {
        executeProcessDisplay();
        if(!checkValidationGroupUser(true)){
            return;
        }
        var validate = validateLabelTemplate(pageId);
        if(validate == false){
            return;
        }
        $.ajax({
            type: 'GET',
            url: AJS.Meta.get('context-path') + '/translationdirectory/getlistbaselanguagehtml.action',
            data: {
                pageId: pageId
            },
            dataType: 'json',
            error: function (xhr, errorText) {
                executeTranslationProcessClose();
                console.log('Error ' + xhr.responseText);
            }
        }).done(function (data) {
            var dialog = Confluence.Templates.TranslationDialog.dialogTemplateTranslation();
            $("#main").append(dialog);
            var hasBaseLang = data.hasBaseLanguage;
            $("#dialog-select-base-language-and-target-language").find('#select-base-lang').html(data.fromLang);
            $("#dialog-select-base-language-and-target-language").find('#select-target-lang').html(data.toLang);

            executeTranslationProcessClose();
            AJS.dialog2("#dialog-select-base-language-and-target-language").show();

            if (!hasBaseLang) {
                disabledOptionLangSame();
            }
        });
    });

    $(document).on("click", "#send-data-trans", function () {
        var fromLang = $("#dialog-select-base-language-and-target-language").find("#select-base-lang").val();
        var toLang = $("#dialog-select-base-language-and-target-language").find("#select-target-lang").val();
        if (fromLang === toLang) {
            return;
        }
        var fromLangName = $("#dialog-select-base-language-and-target-language").find("#select-base-lang option:selected").text();
        var toLangName = $("#dialog-select-base-language-and-target-language").find("#select-target-lang option:selected").text();
        var result = checkExistsProjectMemsource(spaceKey, pageId, fromLang, toLang);
        if (typeof result !== 'undefined') {
            AJS.dialog2("#dialog-select-base-language-and-target-language").hide();
            var dialog = Confluence.Templates.TranslationDialog.dialogTemplateConfirmOverrideProject();
            $("#main").append(dialog);
            AJS.dialog2("#dialog-confirm-override-project-memsource").show();
            var buttonCofirm = $("#dialog-confirm-override-project-memsource").find("#ok-dialog-confirm-memsource");
            $(buttonCofirm).attr("projectId", result.projectId);
            $(buttonCofirm).attr("fromLangKey", fromLang);
            $(buttonCofirm).attr("fromLangName", fromLangName);
            $(buttonCofirm).attr("toLangKey", toLang);
            $(buttonCofirm).attr("toLangName", toLangName);


        } else {
            createProjectMemsource(spaceKey, pageId, fromLang, fromLangName, toLang, toLangName);
        }
    });

    $(document).on("click", "#ok-dialog-confirm-memsource", function (e) {
        var confirm = $("#dialog-confirm-override-project-memsource").find("input[name='radioConfirmOverride']:checked").val();

        if (typeof confirm !== undefined && confirm === "0") {
            var projectId = $(this).attr("projectId");
            var href = "https://cloud.memsource.com/web/project2/show/" + encodeURIComponent(projectId);
            window.open(href);
            closeDialog();
        }

        if (typeof confirm !== undefined && confirm === "1") {
            var fromLang = $(this).attr("fromLangKey");
            var fromLangName = $(this).attr("fromLangName");
            var toLang = $(this).attr("toLangKey");
            var toLangName = $(this).attr("toLangName");
            createProjectMemsource(spaceKey, pageId, fromLang, fromLangName, toLang, toLangName);
        }
    });

    $(document).on("click", "#close-dialog-select-lang", function () {
        closeDialog();
    });

    $(document).on("click", "#close-dialog-confirm-override", function () {
        AJS.dialog2("#dialog-select-base-language-and-target-language").show();
        var dialogConfirm = $(document).find("#dialog-confirm-override-project-memsource");
        if (dialogConfirm.length > 0) {
            $(dialogConfirm).remove();
        }
    });

    $(document).on("change", "#select-base-lang", function () {
        disabledOptionLangSame();
    });

    $(document).on("click", "#translation-directory-menu-sync", function () {
        executeProcessDisplay();
        if(!checkValidationGroupUser(true)){
            return;
        }
        checkPageSyncRecapture(pageId);
    });

//    $(document).on("click", "#translation-directory-menu-hoyaku-recapture", function () {
//        executeProcessDisplay();
//        checkPageSyncRecapture(pageId);
//    });

    $(document).on('click', '.close-message-flag-info-trans', function (e) {
        $(this).closest("div.aui-flag").remove();
    });

    $(document).on("click", "#translation-directory-link-project-memsource", function () {
        renderDialogListLanguageProject(MODE_LINK_PROJECT_JOB);
    });
    
    $(document).on("click", "#translation-directory-import-source-lang-memsource", function () {
        renderDialogListLanguageProject(MODE_SYNC_SOURCE_LANGUGE);
    });

    $(document).on("click", "#save-translation-sync-recapture-list", function () {
        var pageSyncChecked = $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected:checked");
        var arrLangKeys = [];
        $(pageSyncChecked).each(function (e) {
            arrLangKeys.push($(this).attr("data-lang-key-source") + "$$$" + $(this).attr("data-lang-key-target"));
        });

        doSyncPageMemsource(spaceKey, pageId, arrLangKeys);
    });

    $(document).on("change", ".page-sync-recapture-selected", function () {
        var pageSync = $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected");
        var pageSyncChecked = $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected:checked");
        if (pageSync.length !== pageSyncChecked.length) {
             $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected-all").prop("checked", false);
        }else{
            $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected-all").prop("checked", true);
        }
    });

    $(document).on("change", ".page-sync-recapture-selected-all", function () {
        var pageSync = $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected");
        var pageSyncChecked = $("#dialog-translation-sync-recapture-list").find(".page-sync-recapture-selected:checked");
        if (pageSync.length !== pageSyncChecked.length) {
            $(pageSync).prop("checked", true);
        }else{
           $(pageSync).prop("checked", false); 
        }
    });

    $(document).on("click", ".sync-source-language-memsource", function (){
        var projectUid = $(this).attr('data-project-uid');
        var pageIdSource = $(this).attr('data-from-page-id');
        var jobId = $(this).attr('data-job-id');
        doSyncPageMemsource(spaceKey, pageIdSource, "", projectUid, jobId);
    });

    $(document).on("click", "#translation-directory-memsource-template-setting", function () {
        executeProcessDisplay();
        if(!checkValidationGroupUser(true)){
            return;
        }
        var dialog = Confluence.Templates.TranslationDialog.templateMemsourceDialogSetting();
        $("#main").append(dialog);
        renderTemplateMemsource("#dialog-translation-template-setting #content-template-setting", true);
    });

    $(document).on("click", "#close-dialog-translation-project-list", function () {
        closeDialog();
    });
    
    $(document).on("click", "#close-dialog-translation-sync-recapture-list", function () {
        closeDialog();
    });
});

function renderDialogListLanguageProject(mode) {
    executeProcessDisplay();
    if (!checkValidationGroupUser(true)) {
        return;
    }
    var dataProject = getListProject(spaceKey, pageId);
    if (typeof dataProject !== 'undefined') {
        var dialog = Confluence.Templates.TranslationDialog.dialogTemplateMemsourceProjectList({dataProject: JSON.parse(dataProject), mode: mode});
        $("#main").append(dialog);
        AJS.dialog2("#dialog-translation-memsource-project-list").show();
        AJS.tablessortable.setTableSortable(AJS.$("#tbl-translation-memsource-project-list"));
    }
}

function checkValidationGroupUser(eventClick) {
    var  checkValidation = true;
    $.ajax({
        type: 'get',
        url: AJS.Meta.get("context-path") + '/translationdirectory/validationgroupuser.action',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            executeTranslationProcessClose();
            var isShowMenu = typeof data.result == 'undefined' ? false : data.result;
            if (isShowMenu == false) {
                var menuTrans = $(document).find('#action-menu-translation-manager-menu');
                if (menuTrans.length > 0) {
                    $(menuTrans).closest('div.aui-dropdown2-section').remove();
                }
                checkValidation = false;
            } else if(eventClick == true){
                var exceedMember = typeof data.exceedMember == 'undefined' ? false : data.exceedMember;
                if (exceedMember == true) {
                    var dialogMessage = $("#main").find("#dialog-validate-message");
                    if(dialogMessage.length > 0){
                        $(dialogMessage).remove();
                    }
                    var title = AJS.I18n.getText('kod.plugins.kodtranslationdirectory.translation.header.dialog.exceed.number.member');
                    var message = AJS.I18n.getText('kod.plugins.kodtranslationdirectory.translation.message.dialog.exceed.number.member');
                    var html = Confluence.Templates.TranslationDialog.dialogMessage({title: title, message: ""});
                    $("#main").append(html);
                    dialogMessage = $("#main").find("#dialog-validate-message");
                    $(dialogMessage).find(".aui-dialog2-content p").append(message);
                    AJS.dialog2("#dialog-validate-message").show();
                    checkValidation = false;
                }
            }

        },
        error: function (e) {
            console.log(e);
            executeTranslationProcessClose();
            var menuTrans = $(document).find('#action-menu-translation-manager-menu');
            if (menuTrans.length > 0) {
                $(menuTrans).closest('div.aui-dropdown2-section').remove();
            }
            checkValidation = false;
        }
    });
    return  checkValidation;
}

function checkPageSyncRecapture(pageIdParam) {
    closeDialog();
    $.ajax({
        type: 'get',
        url: AJS.Meta.get("context-path") + '/translationdirectory/getlistpagesyncrecapture.action',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: {
            pageId: pageIdParam
        },
        async: false,
        error: function (e) {
            executeTranslationProcessClose();
            console.log(e);
        }
    }).done(function (data) {
        executeTranslationProcessClose();
        if (data.result) {
            var dialog = Confluence.Templates.TranslationDialog.dialogRecapture();
            $(document).find("body").append(dialog);
            var tbody = $(document).find("#dialog-translation-sync-recapture-list tbody");
            if(tbody.length > 0){
                $(tbody).append(data.html);
                AJS.dialog2("#dialog-translation-sync-recapture-list").show();
            }
        }else{
            showMessage(2, data.message, "");
        }
    });
}

function getListProject(spaceKeyParam, pageIdParam) {
    var result;
    $.ajax({
        type: 'get',
        url: AJS.Meta.get("context-path") + '/translationdirectory/dorenderlistpagelangugesync.action',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: {
            pageId: pageIdParam,
            spaceKey: spaceKeyParam
        },
        async: false,
        error: function () {
            executeTranslationProcessClose();
            return;
        }
    }).done(function (data) {
        executeTranslationProcessClose();
        if (data.result === true) {
            result = data.listPageLang;
        } else {
            showMessage(2, data.message, "");
        }
    });
    return result;
}

function createProjectMemsource(spaceKey, pageId, fromLangKey, fromLangName, toLangKey, toLangName) {
    closeDialog();
    var title = AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.message.create.job.info");
    showMessage(3, "", title);
    AJS.$.ajax({
        type: "POST",
        url: AJS.Meta.get("context-path") + "/translationdirectory/docreateprojectmemsource.action",
        data: {
            pageId: pageId,
            spaceKey: spaceKey,
            fromLangKey: fromLangKey,
            fromLangName: fromLangName,
            toLangKey: toLangKey,
            toLangName: toLangName
        },
        async: true,
        error: function () {
            executeTranslationProcessClose();
            return;
        }
    }).done(function (data) {
        if (!data.result) {
            executeTranslationProcessClose();
            var message = '<b>Error: ' + data.message + '</b>';
            closeMessage();
            showMessage(0, message, "");
        } else {
            var funcSuccess = function (data) {
                executeTranslationProcessClose();
                var projectId = data.projectId;
                closeMessage();
                showMessage(5, projectId, data.html);
            };
            var funcWaitting = function (data) {
                var m = data.message;
                if (m !== null && m !== '' && typeof (m) !== "undefined") {
                    $("#execute-progress-message").html(' ' + m);
                }
            };

            callLongrunningTask(data.taskId, funcSuccess, funcWaitting);
        }
    });
}

function doSyncPageMemsource(spaceKey, pageId, arrLangKeys, projectUid, jobId) {
    closeDialog();
    var str = AJS.I18n.getText('kod.plugins.kodtranslationdirectory.translation.message.syncing.info');
    showMessage(3, "", str);
    AJS.$.ajax({
        type: "POST",
        url: AJS.Meta.get("context-path") + "/translationdirectory/doSyncMemsourceContent.action",
        data: {
            pageId: pageId,
            arrLangKeys: arrLangKeys,
            spaceKey: spaceKey,
            projectUid: projectUid,
            jobId: jobId
        },
        async: true,
        error: function () {
            executeTranslationProcessClose();
            return;
        }
    }).done(function (data) {
        if (!data.result) {
            executeTranslationProcessClose();
            var message = '<b>Error: ' + data.message + '</b>';
            closeMessage();
            showMessage(2, message, "");
        } else {
            var funcSuccess = function (data) {
                executeTranslationProcessClose();
                closeMessage();
                showMessage(4, data.urlPath, data.html);
            };
            var funcWaitting = function (data) {
                var m = data.message;
                if (m !== null && m !== '' && typeof (m) !== "undefined") {
                    $("#execute-progress-message").html(' ' + m);
                }
            };

            callLongrunningTask(data.taskId, funcSuccess, funcWaitting);
        }
    });
}

function callLongrunningTask(taskId, funcSuccess, funcWaitting, isCloseExcute) {
    $.ajax({
        type: 'post',
        url: AJS.Meta.get("context-path") + '/translationdirectory/dogettaskpercentage.action',
        dataType: 'json',
        data: {taskId: taskId},
        error: function () {
            executeTranslationProcessClose();
            return;
        }
    }).done(function (data) {
        console.log(data.percentage);
        if (data.percentage > 99) {
            closeMessage();
            if (data.error != '' && data.error != null && typeof (data.error) != undefined && data.error != 'null') {
                showMessage(6, data.urlPath, data.error);
            } else if (data.info != '' && data.info != null && typeof (data.info) != undefined && data.info != 'null') {
                showMessage(2, data.message, "");
            } else {
                if (funcSuccess != undefined) {
                    funcSuccess(data);
                }
            }

            if (typeof isCloseExcute == 'undefined') {
                executeTranslationProcessClose();
            }
        } else {
            if (funcWaitting != undefined) {
                funcWaitting(data);
            }
            setTimeout(function () {
                callLongrunningTask(taskId, funcSuccess, funcWaitting, isCloseExcute);
            }, 2000);
        }
    });
}

function checkExistsProjectMemsource(spaceKey, pageId, fromLangKey, toLangKey) {
    var result;
    $.ajax({
        type: 'get',
        url: AJS.Meta.get("context-path") + '/translationdirectory/checkexistsjobmemsource.action',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: {
            pageId: pageId,
            spaceKey: spaceKey,
            fromLangKey: fromLangKey,
            toLangKey: toLangKey
        },
        async: false,
        success: function (data) {
            if (data.result === true) {
                result = data;
            }
        },
        error: function (e) {
        }
    });
    return result;
}

function checkPageIsBaseLang(pageId) {
    var result;
    $.ajax({
        type: 'get',
        url: AJS.Meta.get("context-path") + '/translationdirectory/checkpagelanguage.action',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: {
            pageId: pageId,
        },
        async: false,
        success: function (data) {
            if (data.result === true) {
                var message = "";
                if (typeof data.isPageBaseLang !== 'undefined' && data.isPageBaseLang !== null && data.isPageBaseLang === true) {
                    message = data.message;
                } else {
                    message = data.message;
                }

                var html = Confluence.Templates.TranslationDialog.displayPageLang({message: message});
                $(document).find('div.page-metadata').children('ul').append(html);
            }
        },
        error: function (e) {
        }
    });
    return result;
}

function disabledOptionLangSame() {
    $("#dialog-select-base-language-and-target-language").find("#select-target-lang option").show();
    $("#dialog-select-base-language-and-target-language").find("#select-target-lang option").css("display", "block");
    var fromLang = $("#dialog-select-base-language-and-target-language").find("#select-base-lang option:selected");
    var fromKey = $(fromLang).val();
    var toLang = $("#dialog-select-base-language-and-target-language").find("#select-target-lang option[value='" + fromKey + "']");

    if (toLang.length > 0) {
        $(toLang).hide();
        $(toLang).prop("selected", false);
        var prevSelector = toLang.prev("option");
        var nextSelector = toLang.next("option");

        if ($(prevSelector).is(':disabled')) {
            prevSelector = prevSelector.next("option");
        }
        if ($(nextSelector).is(':disabled')) {
            nextSelector = nextSelector.next("option");
        }
        if (nextSelector.length > 0) {
            $(nextSelector).prop("selected", true);
        } else {
            $(prevSelector).prop("selected", true);
        }
    }
}

//1 = success
//0 = error
//2 = info
function showMessage(option, content, title) {
    switch (option) {
        case 0:
            AJS.flag({
                type: 'error',
                title: title,
                body: content
            });
            break;
        case 1:
            AJS.flag({
                type: 'success',
                title: title,
                body: content
            });
            break;
        case 2:
            AJS.flag({
                type: 'info',
                title: title,
                body: content
            });
            break;
        case 3:
            AJS.flag({
                type: 'info',
                title: title,
                body: '<ul class="aui-nav-actions-list">' +
                        '<li><a href="#" class="close-message-flag-info-trans">' +
                        AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.link.close.flag.message.info") +
                        '</a></li>' +
                        '</ul>'
            });
            break;
        case 4:
            var path = "#";
            if (content !== "") {
                path = content;
            }
            AJS.flag({
                type: 'success',
                title: title,
                body: '<ul class="aui-nav-actions-list">' +
                        '<li><a href="' +
                        path +
                        '" onClick="window.location.reload()">' +
                        AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.link.success.reload.page") +
                        '</a></li>' +
                        '<li><a href="#" class="close-message-flag-info-trans">' +
                        AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.link.close.flag.message.info") +
                        '</a></li>' +
                        '</ul>'
            });
            break;
        case 5:
            AJS.flag({
                type: 'success',
                title: title,
                body: '<ul class="aui-nav-actions-list">' +
                        '<li><a href="https://cloud.memsource.com/web/project2/show/' + encodeURIComponent(content) + '" target="_blank">' +
                        AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.link.success.open.project") +
                        '</a></li>' +
                        '<li><a href="#" class="close-message-flag-info-trans">' +
                        AJS.I18n.getText("kod.plugins.kodtranslationdirectory.translation.link.close.flag.message.info") +
                        '</a></li>' +
                        '</ul>'
            });
            break;
        case 6 :
            AJS.flag({
                type: 'error',
                body: '<p class="title">'
                        + title.replace('<br>', '</p><p>')
                        + '</p>'
                        + '<ul class="aui-nav-actions-list">'
                        + '<li><a href="' + content + '" class="" target="_blank">'
                        + AJS.I18n.getText('kod.plugins.kodtranslationdirectory.translation.message.sync.error.download.log')
                        + '</a></li>'
                        + '</ul>'
            });
            break;

    }
}

function closeMessage() {
    var flagMessge = $(document).find("div.aui-flag");
    if (flagMessge.length > 0) {
        $(flagMessge).remove();
    }
}

